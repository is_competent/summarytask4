<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set value="Restore" var="title"/>
	<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf" %>
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		
		<div class="space h20"></div>
		<div class="content autoalign">
		<form action="do" method="post">
			<input type="hidden" name="command" value="sendRestoreEmail">
			<label>Email:</label>
			<input type="email" name="toEmail" required="required">
			<p><input type="submit" value='<fmt:message key="common.label.restore"/>'>
		</form>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
		<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>