jQuery.fn.autoWidth = function(options) { 
  var settings = { 
        limitWidth   : false 
  };

  if(options) { 
        jQuery.extend(settings, options); 
    }

    var maxWidth = 0; 

  this.each(function(){ 
        if ($(this).width() > maxWidth){ 
          if(settings.limitWidth && maxWidth >= settings.limitWidth) { 
            maxWidth = settings.limitWidth; 
          } else { 
            maxWidth = $(this).width(); 
          } 
        } 
  });   

  this.width(maxWidth + 15); 
  
};
$(".autoalign label").autoWidth();
