$(function() {	
	scr();
	$(window).scroll(scr);
	
	function scr() {
		var scr = $(window).scrollTop();
		var tbHeight = $("#menu").offset().top + $("#menu").height();

		if (scr > tbHeight) {
			var ht = '<ul class="items"><li id="goup">Up</li></ul>' +
				$("#menu").html();
			$("#cloned").html(ht);
			$("#goup").click(function() {
				$("html, body").animate({
					scrollTop : 0
				}, 200);
			});	
			regis();
			$("#flow").show();
		} else {
			regis();
			$("#flow").hide();
		}
	}
});