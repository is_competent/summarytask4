/**
 * Required INPUT dictionary in your html. The dictionary must contain key as a
 * JQ selector and value - name of post parameter.
 */

String.prototype.replaceAll = function(search, replace) {
	return this.split(search).join(replace);
};

registry();

$("#btnNewAjax").click(function(e) {
	if (!validate()) {
		alert("Illegal data format");
		return;
	}
	var act = $("#formDiv input[name=command]").val();
	var postData = {
		"command" : act,
	};
	var name = "";
	for ( var selector in INPUT) {
		if (selector != "pattern") {
			var paramName = INPUT[selector];
			postData[paramName] = $(selector).val();
		}
	}

	for ( var key in CONST) {
		postData[key] = CONST[key];
	}
	name = getName();
	$.post("do", postData, ajaxNewDone(name));
});


function ajaxNewDone(name) {
	return function(data) {
		if (data.status == 200) {
			var html = INPUT.pattern.replaceAll("{id}", data.id).replaceAll(
					"{name}", name);

			$("#dynamicCont").prepend(html);
			$("#dynamicCont button").off("click");
			registry();
			$("#formDiv input[type=text]").val("");
		} else {
			alert("Can't create new item, server say: " + data.message);
		}
	};
}

function ajaxRemDone(id) {
	return function(data) {
		if (data.status == 200) {
			$("#" + id).parent().remove();
		} else {
			alert("Can't remove the item, server say: " + data.message);
		}
	};
}

function registry() {
	$("#dynamicCont button").click(function(e) {
		e.preventDefault();
		var conf = confirm("Do you want to delete the item?");
		if (conf) {
			var act = $("#formDiv input[name=remCommand]").val();
			var id = $(this).attr("id");
			var postData = {
				"id" : id,
				"command" : act,
			};
			for ( var key in CONST) {
				postData[key] = CONST[key];
			}
			$.post("do", postData, ajaxRemDone(id));
		}
	});
}