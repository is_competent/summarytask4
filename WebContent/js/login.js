var patternError = "Password format error: you must use only numbers, \n"
		+ "latin characters and @$# symbols, \nminimum password length is 5 characters.";

function crypt(field, target) {
	var pattern = /^[0-9a-z@$#]{5,}$/i;
	if (pattern.test($(field).val())) {
		var hash = CryptoJS.SHA256($(field).val());
		$(target).val(hash);
		return true;
	} else {
		return false;
	}
}
$("#login").click(function(e) {
	if (!crypt("#userpass", "#hPass")) {
		alert(patternError);
		e.preventDefault();
	}
});

$("#oldPass").val("");

$("#changePass").click(function(e) {
	var newPass = $("#newPass").val();
	var confPass = $("#confPass").val();
	
	if (newPass == "" || $("#oldPass").val() == "") {
		alert("Please, enter current password and new password");
		e.preventDefault();
		return;
	}

	if (newPass != confPass) {
		alert("You do not confirm the password.");
		e.preventDefault();
	} else {
		if (!crypt("#newPass", "#hNew") || !crypt("#oldPass", "#hOld")) {
			alert(patternError);
			e.preventDefault();
		} else {
			return confirm("Save?");
		}
	}
});

$("#restore").click(function(e) {
	e.preventDefault();
	checkCaptcha(function(data) {
		var newPass = $("#newPass").val();
		var confPass = $("#confPass").val();

		if (data.status != 200) {
			alert("Captcha is incorrect.");
		} else if (newPass != confPass) {
			alert("You do not confirm the password.");
			e.preventDefault();
		} else if (!crypt("#newPass", "#hNew")) {
			alert(patternError);
			e.preventDefault();
		} else {
			$("#form").submit();
		}
	});

});
