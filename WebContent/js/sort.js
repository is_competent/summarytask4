var table = $("table");

$('.sortable')
    .each(function(){
    	
        var th = $(this),
            thIndex = th.index(),
            inverse = false;

        th.click(function(){
        	$(".sortable div").attr("class", "");
        	$(".sortable").css("background-color", "#9ecaed");
        	$(this).css("background-color", "green"); //#247bc2
        	if (!inverse) {
        		$(this).find("div").attr("class", "desc");
        	} else {
        		$(this).find("div").attr("class", "asc");
        	}

            table.find('td').filter(function(){
                return $(this).index() === thIndex;

            }).sortElements(function(a, b){
            	var first = $.text([a]);
            	var second = $.text([b]);
            	
            	if ($.isNumeric(first)) {
            		first = parseInt(first);
            	}
            	
            	if ($.isNumeric(second)) {
            		second = parseInt(second);
            	}
            	// TODO:

                if( first == second )
                    return 0;

                return first > second ?
                    inverse ? -1 : 1
                    : inverse ? 1 : -1;

            }, function(){

                // parentNode is the element we want to move
                return this.parentNode; 

            });

            inverse = !inverse;

        });

    });
