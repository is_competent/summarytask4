<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set scope="page" value="login" var="localized"></c:set>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>
		<!-- Content starts here -->
		<div class="space h20"></div>
		
		<div class="content">
			<form action="<my:const name='TO_CONTROLLER'/>" method="POST"
				class="login">
				<input hidden="hidden" name="<my:const name='COMMAND'/>"
					value="login"> 
					<input hidden="hidden" name="<my:const name='USER_PASS'/>"
					id="hPass">
				<table>
					<tr>
						<td>Email</td>
						<td><input type="email" name="<my:const name='USER_EMAIL'/>"
							required="required"></td>
					</tr>
					<tr>
						<td><fmt:message key="common.label.password"/></td>
						<td><input id="userpass" type="password" required="required"></td>
					</tr>
				</table>
				<p class="help">
					<a href="registry.jsp"><fmt:message key="common.label.registry"/></a> <a
						href="restoreemail.jsp"><fmt:message key="common.label.forg_password"/></a><br>
				<p>
					<input type="submit" value="<fmt:message key="common.label.login"/>" id="login">
			</form>
		</div>
		<!-- End of content -->
		<!-- Footer starts here -->
		<div class="space h20"></div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>