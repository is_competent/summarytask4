SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL';

DROP SCHEMA IF EXISTS `SComision` ;
CREATE SCHEMA IF NOT EXISTS `SComision` DEFAULT CHARACTER SET utf8 ;
USE `SComision` ;

-- -----------------------------------------------------
-- Table `SComision`.`ROLE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`ROLE` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`ROLE` (
  `rId` INT(11) NOT NULL AUTO_INCREMENT ,
  `rName` VARCHAR(50) NOT NULL ,
  `rDescr` VARCHAR(255) NULL DEFAULT NULL ,
  PRIMARY KEY (`rId`) ,
  UNIQUE INDEX `rName_UNIQUE` (`rName` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 5
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`USER`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`USER` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`USER` (
  `uId` INT(11) NOT NULL AUTO_INCREMENT ,
  `uName` VARCHAR(50) NOT NULL ,
  `uLastName` VARCHAR(50) NOT NULL ,
  `uPatr` VARCHAR(50) NOT NULL ,
  `uEmail` VARCHAR(100) NOT NULL ,
  `uRole` INT(11) NOT NULL ,
  `uPass` VARCHAR(70) NOT NULL ,
  `uLang` VARCHAR(10) NOT NULL ,
  PRIMARY KEY (`uId`) ,
  UNIQUE INDEX `uEmail_UNIQUE` (`uEmail` ASC) ,
  INDEX `fk_USER_1` (`uRole` ASC) ,
  CONSTRAINT `fk_USER_1`
    FOREIGN KEY (`uRole` )
    REFERENCES `SComision`.`ROLE` (`rId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 29
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`AUTH`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`AUTH` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`AUTH` (
  `aId` VARCHAR(100) NOT NULL ,
  `aUId` INT(11) NOT NULL ,
  PRIMARY KEY (`aId`, `aUId`) ,
  INDEX `fk_AUTH_1` (`aUId` ASC) ,
  CONSTRAINT `fk_AUTH_1`
    FOREIGN KEY (`aUId` )
    REFERENCES `SComision`.`USER` (`uId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`BLACKLIST`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`BLACKLIST` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`BLACKLIST` (
  `bId` INT(11) NOT NULL ,
  PRIMARY KEY (`bId`) ,
  INDEX `fk_BLACKLIST_1` (`bId` ASC) ,
  CONSTRAINT `fk_BLACKLIST_1`
    FOREIGN KEY (`bId` )
    REFERENCES `SComision`.`USER` (`uId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`UNIVERSITY`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`UNIVERSITY` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`UNIVERSITY` (
  `unId` INT(11) NOT NULL AUTO_INCREMENT ,
  `unName` VARCHAR(100) NOT NULL ,
  `unAdmin` INT(11) NOT NULL ,
  PRIMARY KEY (`unId`) ,
  UNIQUE INDEX `unName_UNIQUE` (`unName` ASC) ,
  UNIQUE INDEX `unAdmin_UNIQUE` (`unAdmin` ASC) ,
  INDEX `fk_UNIVERSITY_1` (`unAdmin` ASC) ,
  CONSTRAINT `fk_UNIVERSITY_1`
    FOREIGN KEY (`unAdmin` )
    REFERENCES `SComision`.`USER` (`uId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 9
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`DEPT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`DEPT` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`DEPT` (
  `dId` INT(11) NOT NULL AUTO_INCREMENT ,
  `dName` VARCHAR(50) NOT NULL ,
  `dBudget` INT(11) NOT NULL ,
  `dCommon` INT(11) NOT NULL ,
  `dUniversity` INT(11) NOT NULL ,
  PRIMARY KEY (`dId`) ,
  INDEX `fk_DEPT_1` (`dUniversity` ASC) ,
  CONSTRAINT `fk_DEPT_1`
    FOREIGN KEY (`dUniversity` )
    REFERENCES `SComision`.`UNIVERSITY` (`unId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
AUTO_INCREMENT = 52
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`SUBJ`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`SUBJ` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`SUBJ` (
  `sId` INT(11) NOT NULL AUTO_INCREMENT ,
  `sName` VARCHAR(50) NOT NULL ,
  PRIMARY KEY (`sId`) ,
  UNIQUE INDEX `sName_UNIQUE` (`sName` ASC) )
ENGINE = InnoDB
AUTO_INCREMENT = 27
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`DEPT_SUBJ`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`DEPT_SUBJ` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`DEPT_SUBJ` (
  `dsDepId` INT(11) NOT NULL ,
  `dsSubjId` INT(11) NOT NULL ,
  PRIMARY KEY (`dsDepId`, `dsSubjId`) ,
  INDEX `fk_DEPT_SUBJ_1` (`dsSubjId` ASC) ,
  INDEX `fk_DEPT_SUBJ_2` (`dsDepId` ASC) ,
  CONSTRAINT `fk_DEPT_SUBJ_1`
    FOREIGN KEY (`dsSubjId` )
    REFERENCES `SComision`.`SUBJ` (`sId` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_DEPT_SUBJ_2`
    FOREIGN KEY (`dsDepId` )
    REFERENCES `SComision`.`DEPT` (`dId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`ENROLLEE`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`ENROLLEE` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`ENROLLEE` (
  `eId` INT(11) NOT NULL ,
  `eCity` VARCHAR(50) NOT NULL ,
  `eDistr` VARCHAR(50) NOT NULL ,
  `eEducName` VARCHAR(100) NOT NULL ,
  `eMarks` LONGBLOB NOT NULL ,
  `eAvMark` FLOAT NOT NULL ,
  PRIMARY KEY (`eId`) ,
  INDEX `fk_ENROLLEE_1` (`eId` ASC) ,
  CONSTRAINT `fk_ENROLLEE_1`
    FOREIGN KEY (`eId` )
    REFERENCES `SComision`.`USER` (`uId` )
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`ENROLLEE_DEP`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`ENROLLEE_DEP` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`ENROLLEE_DEP` (
  `edDeptId` INT(11) NOT NULL ,
  `edEnrId` INT(11) NOT NULL ,
  `edRegDate` DATE NOT NULL ,
  `edPrior` int(11) NOT NULL,
  PRIMARY KEY (`edDeptId`, `edEnrId`, `edRegDate`) ,
  INDEX `fk_ENROLLEE_DEP_1` (`edEnrId` ASC) ,
  INDEX `fk_ENROLLEE_DEP_2` (`edDeptId` ASC) ,
  CONSTRAINT `fk_ENROLLEE_DEP_1`
    FOREIGN KEY (`edEnrId` )
    REFERENCES `SComision`.`ENROLLEE` (`eId` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ENROLLEE_DEP_2`
    FOREIGN KEY (`edDeptId` )
    REFERENCES `SComision`.`DEPT` (`dId` )
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`ENROLLEE_MARK`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`ENROLLEE_MARK` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`ENROLLEE_MARK` (
  `emEnId` INT(11) NOT NULL ,
  `emSubjId` INT(11) NOT NULL ,
  `emCertMark` INT(11) NOT NULL ,
  `emTestMark` INT(11) NOT NULL DEFAULT '0' ,
  PRIMARY KEY (`emEnId`, `emSubjId`) ,
  INDEX `fk_ENROLLEE_MARK_1` (`emEnId` ASC) ,
  INDEX `fk_ENROLLEE_MARK_2` (`emSubjId` ASC) ,
  CONSTRAINT `fk_ENROLLEE_MARK_1`
    FOREIGN KEY (`emEnId` )
    REFERENCES `SComision`.`ENROLLEE` (`eId` )
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_ENROLLEE_MARK_2`
    FOREIGN KEY (`emSubjId` )
    REFERENCES `SComision`.`SUBJ` (`sId` )
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;


-- -----------------------------------------------------
-- Table `SComision`.`REPORT`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `SComision`.`REPORT` ;

CREATE  TABLE IF NOT EXISTS `SComision`.`REPORT` (
  `repDepName` INT(11) NOT NULL ,
  `repEnName` INT(11) NOT NULL ,
  `repIsBudget` BIT(1) NOT NULL DEFAULT b'0' ,
  `repDate` DATETIME NOT NULL ,
  `repAssertionDate` YEAR NOT NULL ,
  `repAvMark` FLOAT NOT NULL ,
  PRIMARY KEY (`repDepName`, `repEnName`, `repAssertionDate`) ,
  INDEX `fk_CERT_1` (`repDepName` ASC) ,
  INDEX `fk_CERT_2` (`repEnName` ASC) ,
  CONSTRAINT `fk_CERT_1`
    FOREIGN KEY (`repDepName` )
    REFERENCES `SComision`.`DEPT` (`dId` )
    ON UPDATE CASCADE,
  CONSTRAINT `fk_CERT_2`
    FOREIGN KEY (`repEnName` )
    REFERENCES `SComision`.`ENROLLEE` (`eId` )
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

-- -----------------------------------------------------
-- procedure applyToBudget
-- -----------------------------------------------------
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `applyToBudget`(userId INT, reportDate DATETIME) RETURNS bit(1)
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE depId INT DEFAULT 0;
    DECLARE inserted INT DEFAULT FALSE;
    DECLARE assertDate DATE;
    DECLARE avMark FLOAT;

    DECLARE userDepartments CURSOR FOR 
      (SELECT `edDeptId`, `edRegDate` FROM `ENROLLEE_DEP` 
          WHERE `edEnrId` = userId AND YEAR(`edRegDate`) = YEAR(reportDate) 
          ORDER BY `edPrior` DESC);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
      
    SELECT `eAvMark` INTO avMark FROM `ENROLLEE` WHERE `eId` = userId;
    
    OPEN userDepartments;
    dep_loop: LOOP
        FETCH userDepartments INTO depId, assertDate;
        -- break the loop
        IF done THEN
          LEAVE dep_loop;
        END IF;
        
        IF budgetVacantExist(depId, reportDate) THEN
            INSERT INTO `REPORT` 
                SELECT depId, userId, 1, reportDate, YEAR(assertDate), avMark;

            SET inserted = TRUE;
            SET done = TRUE;
        END IF;
    END LOOP;
    CLOSE userDepartments;

    return inserted;
END ;;
DELIMITER ;

-- -----------------------------------------------------
-- procedure applyToContract
-- -----------------------------------------------------
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `applyToContract`(userId INT, reportDate DATETIME) RETURNS bit(1)
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE depId INT DEFAULT 0;
    DECLARE inserted INT DEFAULT FALSE;
    DECLARE assertDate DATE;
    DECLARE avMark FLOAT;
    DECLARE userDepartments CURSOR FOR 
      (SELECT `edDeptId`, `edRegDate` FROM `ENROLLEE_DEP` 
          WHERE `edEnrId` = userId AND YEAR(`edRegDate`) = YEAR(reportDate) 
          ORDER BY `edPrior` DESC);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
      
    SELECT `eAvMark` INTO avMark FROM `ENROLLEE` WHERE `eId` = userId;
    
    OPEN userDepartments;
    dep_loop: LOOP
        FETCH userDepartments INTO depId, assertDate;
        -- break the loop
        IF done THEN
          LEAVE dep_loop;
        END IF;
        
        IF contractVacantExist(depId, reportDate) THEN
            INSERT INTO `REPORT` 
                SELECT depId, userId, 0, reportDate, YEAR(assertDate), avMark;

            SET inserted = TRUE;
            SET done = TRUE;
        END IF;
    END LOOP;
    CLOSE userDepartments;

    return inserted;
END ;;
DELIMITER ;

-- -----------------------------------------------------
-- procedure budgetVacantExist
-- -----------------------------------------------------
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `budgetVacantExist`(dep INT, nowDate DATE) RETURNS bit(1)
BEGIN
    DECLARE vacants INT DEFAULT 0;
    DECLARE engaged INT;

    SELECT `dBudget` INTO vacants FROM `DEPT` WHERE `dId` = dep;
    SELECT COUNT(*) INTO engaged FROM `REPORT` WHERE `repDepName` = dep AND 
    repAssertionDate = YEAR(nowDate);
    
    return vacants > engaged; 
END ;;
DELIMITER ;

-- -----------------------------------------------------
-- procedure contractVacantExist
-- -----------------------------------------------------
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `contractVacantExist`(dep INT, nowDate DATE) RETURNS bit(1)
BEGIN
    DECLARE vacants INT DEFAULT 0;
    DECLARE engaged INT;

    SELECT `dCommon` INTO vacants FROM `DEPT` WHERE `dId` = dep;
    SELECT COUNT(*) INTO engaged FROM `REPORT` WHERE `repDepName` = dep AND 
    repAssertionDate = YEAR(nowDate);
    
    return vacants > engaged; 
END ;;
DELIMITER ;

-- -----------------------------------------------------
-- procedure applyToContract
-- -----------------------------------------------------
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `applyToContract`(IN userId INT, IN reportDate DATETIME)
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE depId INT DEFAULT 0;
    DECLARE inserted INT DEFAULT FALSE;
    DECLARE assertDate DATE;
    DECLARE avMark FLOAT;
    DECLARE userDepartments CURSOR FOR 
      (SELECT `edDeptId`, `edRegDate` FROM `ENROLLEE_DEP` 
          WHERE `edEnrId` = userId AND YEAR(`edRegDate`) = YEAR(reportDate) 
          ORDER BY `edPrior` DESC);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
      
    SELECT `eAvMark` INTO avMark FROM `ENROLLEE` WHERE `eId` = userId;
    
    OPEN userDepartments;
    dep_loop: LOOP
        FETCH userDepartments INTO depId, assertDate;
        -- break the loop
        IF done THEN
          LEAVE dep_loop;
        END IF;
        
        IF contractVacantExist(depId, reportDate) THEN
            INSERT INTO `REPORT` 
                SELECT depId, userId, 0, reportDate, YEAR(assertDate), avMark;

            SET inserted = TRUE;
            SET done = TRUE;
        END IF;
    END LOOP;
    CLOSE userDepartments;
END ;;
DELIMITER ;

-- -----------------------------------------------------
-- procedure applyUser
-- -----------------------------------------------------
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `applyUser`(IN userId INT, IN reportDate DATETIME)
BEGIN
     
    IF NOT applyToBudget(userId, reportDate) THEN 
        call applyToContract(userId, reportDate);
    END IF;
END ;;
DELIMITER ;

-- -----------------------------------------------------
-- procedure buildReport
-- -----------------------------------------------------
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buildReport`()
BEGIN
    DECLARE reportDate DATETIME default NOW();
    DECLARE id INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE requiredApplicants INT;
    DECLARE engagedApplicants INT;

    DECLARE user CURSOR FOR 
    (SELECT `edEnrId` FROM `ENROLLEE_DEP`, `ENROLLEE`
        -- get applications with this year
        WHERE YEAR(`edRegDate`) = YEAR(reportDate)
        -- user is not blocked
        AND `edEnrId` NOT IN (SELECT * FROM `BLACKLIST`)
        -- join
        AND `eId` = `edEnrId`
        -- user is not already applied
        AND `eId` NOT IN (
                SELECT `repEnName` FROM `REPORT` 
                WHERE `repEnName` = `eId`
                AND YEAR(`repAssertionDate`) = YEAR(`edRegDate`)
        )
        -- get unique id
        GROUP BY `eId`
        -- score table
        ORDER BY `eAvMark` DESC
    );

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
   
    SELECT SUM(`dCommon`) INTO requiredApplicants FROM 
        (SELECT `edDeptId` FROM `ENROLLEE_DEP` GROUP BY `edDeptId`) AS T
    JOIN `DEPT` ON `edDeptId` = `dId`;    

    OPEN user;
    read_loop: LOOP
      FETCH user INTO id;
      
      SELECT COUNT(*) INTO engagedApplicants FROM `REPORT` 
      WHERE YEAR(`repAssertionDate`) = reportDate;
      -- break the loop
      IF done OR requiredApplicants <= engagedApplicants THEN
        LEAVE read_loop;
      END IF;
      
      IF NOT exist(id, reportDate) THEN
        call applyUser(id, reportDate);
      END IF;

      END LOOP;
      CLOSE user;

      -- returns timestamp of report
      SELECT reportDate as `repDate`;
END ;;
DELIMITER ;

-- -----------------------------------------------------
-- procedure buildReportWithoutPrior
-- -----------------------------------------------------

USE `SComision`;
DROP procedure IF EXISTS `SComision`.`buildReport`;

DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buildReportWithoutPrior`(IN university INT)
BEGIN
      DECLARE orderDate DATETIME default NOW();
      DECLARE offset INT DEFAULT 0;

      DECLARE i INT;
      DECLARE done INT DEFAULT FALSE;

      -- fetch all departments id'es registrated in this university
      DECLARE cursor_i CURSOR FOR (SELECT `dId` FROM `DEPT` 
                                        WHERE `dUniversity` = university);

      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

      OPEN cursor_i;

      read_loop: LOOP
        FETCH cursor_i INTO i;
        -- break the loop
        IF done THEN
          LEAVE read_loop;
        END IF;

        -- calculate the offset           
        SELECT `dCommon` INTO offset FROM `DEPT` WHERE `dId` = i LIMIT 1;
        -- SET offset = offset + 1;

        -- insert department report
        INSERT INTO `REPORT` 
            SELECT i, `eId`, isBudget(i, `eId`), orderDate, YEAR(`edRegDate`), `eAvMark` 
            FROM `ENROLLEE_DEP` 
            -- build score table
            JOIN (SELECT * FROM `ENROLLEE` 
                    -- Blocked enrollees not used
                    WHERE `eId` not in (SELECT * FROM `BLACKLIST`)
                    -- get all applicants applied to this department
                    AND `eId` IN (SELECT `edEnrId` FROM `ENROLLEE_DEP` 
                                    WHERE `edDeptId` = i)

                    ORDER BY `eAvMark` DESC LIMIT offset) 
            as T
            ON `eId` = `edEnrId`

            -- Enrollee apply in this year and does not exist already
            WHERE `edDeptId` = i AND YEAR(orderDate) = YEAR(`edRegDate`)
                  AND not exist(edEnrId, i, DATE(`edRegDate`));
        
      END LOOP;
      CLOSE cursor_i;

      -- returns timestamp of report
      SELECT orderDate as `repDate`;
END $$

DELIMITER ;

-- -----------------------------------------------------
-- procedure buildReportOld
-- -----------------------------------------------------

USE `SComision`;
DROP procedure IF EXISTS `SComision`.`buildReportOld`;

DELIMITER $$
USE `SComision`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `buildReportOld`(in university INT)
BEGIN
    DECLARE orderDate DATETIME default NOW();
    DECLARE offset INT DEFAULT 0;
    DECLARE depsMax INT;
    DECLARE depsMin INT;
    DECLARE i INT;

    SET depsMax := (SELECT MAX(`dId`) FROM `DEPT` WHERE `dUniversity` = university);
    SET depsMin := (SELECT MIN(`dId`) FROM `DEPT` WHERE `dUniversity` = university);
    SET i := depsMin;
    
    while i <= depsMax do
       
        -- check if there is such department
        if (SELECT COUNT(*) FROM `DEPT` WHERE `DEPT`.`dId` = i 
            AND `dUniversity` = university LIMIT 1) then

            -- calculate the offset           
            SELECT `dCommon` INTO offset FROM `DEPT` WHERE `dId` = i LIMIT 1;

            -- insert department report
            INSERT INTO `REPORT` 
            SELECT i, `eId`, isBudget(i, `eId`), orderDate, `edRegDate` FROM `ENROLLEE_DEP` 
            JOIN (SELECT * FROM `ENROLLEE` 
                    -- Blocked enrollees not used
                    WHERE `eId` not in (SELECT * FROM `BLACKLIST`)
                    ORDER BY `eAvMark` DESC LIMIT offset) as T
                ON `eId` = `edEnrId`
            WHERE `edDeptId` = i AND YEAR(orderDate) = YEAR(`edRegDate`)
                    AND not exist(edEnrId, i, DATE(`edRegDate`));
        end if;
        SET i := i + 1;
    end while;
    SELECT orderDate;

END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure calcAvgMark
-- -----------------------------------------------------

USE `SComision`;
DROP procedure IF EXISTS `SComision`.`calcAvgMark`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` PROCEDURE `calcAvgMark`(in user int)
BEGIN
    -- constants declarations
    DECLARE TEST_MAX_MARK INT DEFAULT 200;
    DECLARE TEST_HIGH_BOUND INT DEFAULT 88;
    
    DECLARE testMark FLOAT DEFAULT 0;
    DECLARE testCnt INT DEFAULT 0;
    
    -- Total mark
    DECLARE score FLOAT DEFAULT 0;

    SELECT AVG(`emCertMark`) INTO score 
                    FROM `ENROLLEE_MARK` WHERE `emEnId` = user;

    SELECT SUM(`emTestMark`), COUNT(*) INTO testMark, testCnt 
                    FROM `ENROLLEE_MARK` WHERE `emEnId` = user
                    AND `emTestMark` != 0;
    
    IF testCnt > 0 THEN
        -- trim test score to range [0; 88]
        SET testMark = (testMark / (TEST_MAX_MARK * testCnt)) * TEST_HIGH_BOUND;
    END IF;
    
    -- summate and round to 2 digits after point
    SET score = IFNULL(TRUNCATE(score + testMark, 2), 0);
    
    -- update value
    UPDATE `ENROLLEE` SET `eAvMark` = score WHERE `eId` = user;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function exist
-- -----------------------------------------------------

USE `SComision`;
DROP function IF EXISTS `SComision`.`exist`;

DELIMITER $$
USE `SComision`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `exist`(uId INT, depId INT, da DATE) RETURNS bit(1)
BEGIN
    DECLARE CNT INT DEFAULT 0;
    SET CNT = (SELECT COUNT(*) FROM `REPORT` WHERE `repEnName` = uId AND `repDepName` = depId
                AND `repAssertionDate` = YEAR(da));

    return cnt > 0;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure fail
-- -----------------------------------------------------

USE `SComision`;
DROP procedure IF EXISTS `SComision`.`fail`;

DELIMITER $$
USE `SComision`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `fail`(in msg VARCHAR(255), in erno INT)
BEGIN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = msg, MYSQL_ERRNO = erno;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- function isBudget
-- -----------------------------------------------------

USE `SComision`;
DROP function IF EXISTS `SComision`.`isBudget`;
DELIMITER $$
CREATE DEFINER=`root`@`localhost` FUNCTION `isBudget`(depId INT, enrId INT) RETURNS bit(1)
BEGIN
    DECLARE offset INT DEFAULT 0;
    
    -- calculate offset to budget
    SELECT `dBudget` INTO offset FROM `DEPT` WHERE `dId` = depId LIMIT 1;
    -- SET offset = offset + 1;
    
    -- check if there is such id in the bounded result
    return enrId in (SELECT `edEnrId` FROM `ENROLLEE_DEP`
                    JOIN (SELECT * FROM `ENROLLEE` 
                            -- blocked enrollees is not used
                            WHERE `eId` NOT IN (SELECT * FROM `BLACKLIST`) 
                            AND `eId` IN 
                                (SELECT `edEnrId` FROM `ENROLLEE_DEP` 
                                    WHERE `edDeptId` = depId)
                            ORDER BY `eAvMark` DESC 
                            LIMIT offset) as T ON `eId` = `edEnrId`
                    WHERE `edDeptId` = depId AND YEAR(NOW()) = YEAR(`edRegDate`));
   
END$$

DELIMITER ;

-- -----------------------------------------------------
-- procedure isCorrectMarks
-- -----------------------------------------------------

USE `SComision`;
DROP procedure IF EXISTS `SComision`.`isCorrectMarks`;

DELIMITER $$
USE `SComision`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `isCorrectMarks`(in user INT, in dep INT)
BEGIN
    if (SELECT COUNT(*) FROM `DEPT_SUBJ` 
        WHERE `dsSubjId` not in (SELECT `emSubjId` FROM `ENROLLEE_MARK` 
        WHERE `emEnId` = user)
        AND `dsDepId` = dep) > 0 then
            call fail('Subjects are not mutched', 1002);
    end if;
END$$

DELIMITER ;

-- -----------------------------------------------------
-- View `SComision`.`REPORT_VIEW`
-- -----------------------------------------------------
DROP VIEW IF EXISTS `SComision`.`REPORT_VIEW` ;
DROP TABLE IF EXISTS `SComision`.`REPORT_VIEW`;
USE `SComision`;
CREATE  OR REPLACE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW 
`SComision`.`REPORT_VIEW` AS 

SELECT 
	`SComision`.`USER`.`uId` AS `uId`,
	`SComision`.`USER`.`uEmail` AS `uEmail`,
	`SComision`.`UNIVERSITY`.`unName` AS `unName`,
	`SComision`.`DEPT`.`dName` AS `dName`,
	`SComision`.`USER`.`uLastName` AS `uLastName`,
	`SComision`.`USER`.`uName` AS `uName`,
	`SComision`.`USER`.`uPatr` AS `uPatr`,
	`SComision`.`REPORT`.`repDate` AS `repDate`,
	if(`SComision`.`REPORT`.`repIsBudget`,
		'BUDGET',
		'CONTRACT'
	) AS `repIsBudget`,
	`SComision`.`REPORT`.`repAvMark` AS `eAvMark`,
	`SComision`.`REPORT`.`repAssertionDate` AS `repAssertionDate` 
FROM 
	`SComision`.`REPORT` 
	join `SComision`.`DEPT` 
		on `SComision`.`DEPT`.`dId` = `SComision`.`REPORT`.`repDepName` 
	join `SComision`.`UNIVERSITY` 
		on `SComision`.`UNIVERSITY`.`unId` = `SComision`.`DEPT`.`dUniversity` 
	join `SComision`.`ENROLLEE` 
		on `SComision`.`ENROLLEE`.`eId` = `SComision`.`REPORT`.`repEnName`
	join `SComision`.`USER` 
		on `SComision`.`USER`.`uId` = `SComision`.`REPORT`.`repEnName`;
USE `SComision`;

DELIMITER $$

USE `SComision`$$
DROP TRIGGER IF EXISTS `SComision`.`checkUniq` $$
USE `SComision`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `SComision`.`checkUniq`
BEFORE INSERT ON `SComision`.`DEPT`
FOR EACH ROW
BEGIN
    if (SELECT COUNT(*) FROM DEPT WHERE `dUniversity` = NEW.`dUniversity`
        AND `dName` = NEW.`dName`) then
        call fail("There is such dep", 1005);
    end if;
    -- Check BL
    if (NEW.`dBudget` > NEW.`dCommon`) then
        call fail("Illegal data format", 1006);
    end if;
END$$


USE `SComision`$$
DROP TRIGGER IF EXISTS `SComision`.`checkUniqUpd` $$
USE `SComision`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `SComision`.`checkUniqUpd`
BEFORE UPDATE ON `SComision`.`DEPT`
FOR EACH ROW
BEGIN
    -- Check BL
	if (NEW.`dBudget` > NEW.`dCommon`) then
        call fail("Illegal data format", 1006);
    end if;
END$$


DELIMITER ;

DELIMITER $$

USE `SComision`$$
DROP TRIGGER IF EXISTS `SComision`.`checkMarks` $$
USE `SComision`$$
CREATE
DEFINER=`scadmin`@`%`
TRIGGER `SComision`.`checkMarks`
BEFORE INSERT ON `SComision`.`ENROLLEE_DEP`
FOR EACH ROW
BEGIN
    IF (SELECT COUNT(*) FROM ENROLLEE_DEP WHERE YEAR(NEW.`edRegDate`) = YEAR(`edRegDate`)
            AND `edDeptId` = NEW.`edDeptId` AND `edEnrId` = NEW.`edEnrId`) > 0 then 
        call fail("Duplicate entry for key", 1007);
    END if;
    IF (SELECT COUNT(*) FROM ENROLLEE_DEP WHERE YEAR(NEW.`edRegDate`) = YEAR(`edRegDate`)
            AND `edPrior` = NEW.`edPrior` AND `edEnrId` = NEW.`edEnrId`) > 0 then 
        call fail("Duplicate entry for prior", 1007);
    END if;
    
    call isCorrectMarks(NEW.edEnrId, NEW.edDeptId);
END$$


DELIMITER ;

DELIMITER $$

USE `SComision`$$
DROP TRIGGER IF EXISTS `SComision`.`updMarksAfterDelete` $$
USE `SComision`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `SComision`.`updMarksAfterDelete`
AFTER DELETE ON `SComision`.`ENROLLEE_MARK`
FOR EACH ROW
BEGIN
    call calcAvgMark(OLD.`emEnId`);
END$$


USE `SComision`$$
DROP TRIGGER IF EXISTS `SComision`.`updMarksAfterInsert` $$
USE `SComision`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `SComision`.`updMarksAfterInsert`
AFTER INSERT ON `SComision`.`ENROLLEE_MARK`
FOR EACH ROW
BEGIN
    call calcAvgMark(NEW.`emEnId`);
END$$


DELIMITER ;

DELIMITER $$

USE `SComision`$$
DROP TRIGGER IF EXISTS `SComision`.`dontRemoveRoot` $$
USE `SComision`$$
CREATE
DEFINER=`root`@`localhost`
TRIGGER `SComision`.`dontRemoveRoot`
BEFORE DELETE ON `SComision`.`USER`
FOR EACH ROW
BEGIN
    if OLD.`uId` = 1 then
        call fail("Can't remove root user", 1000);
    end if;
END$$


DELIMITER ;

CREATE USER `scadmin` IDENTIFIED BY 'scadmin';
GRANT ALL ON `SComision`.* to `scadmin`;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

INSERT INTO `SComision`.`USER` 
	(`uId`, `uName`, `uLastName`, `uPatr`, `uEmail`, 
	`uRole`, `uPass`, `uLang`)
VALUES
	(1, 'root', 'root', 'root', 'root@root.com', 1, SHA2('adminuser', 256), 'en');
	
INSERT INTO `ROLE` (`rId`,`rName`,`rDescr`) VALUES (1,'root','Super user');
INSERT INTO `ROLE` (`rId`,`rName`,`rDescr`) VALUES (2,'admin',NULL);
INSERT INTO `ROLE` (`rId`,`rName`,`rDescr`) VALUES (3,'enrollee',NULL);

INSERT INTO `SComision`.`SUBJ` (`sId`,`sName`)
VALUES  (0,'Українська мова'),
        (0,'Українська література'),
        (0,'Іноземна мова'),
        (0,'Зарубіжна література'),
        (0,'Історія України'),
        (0,'Всесвітня історія'),
        (0,'Громадянська освіта:'),
        (0,'Правознавство'),
        (0,'Економіка'),
        (0,'Людина і світ'),
        (0,'Художня культура'),
        (0,'Математика'),
        (0,'Фізика'),
        (0,'Астрономія'),
        (0,'Біологія'),
        (0,'Географія'),
        (0,'Хімія'),
        (0,'Екологія'),
        (0,'Технології'),
        (0,'Інформатика'),
        (0,'Фізична культура'),
        (0,'Захист Вітчизни');
