-- MySQL dump 10.13  Distrib 5.5.46, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: SComision
-- ------------------------------------------------------
-- Server version	5.5.46-0+deb7u1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AUTH`
--

DROP TABLE IF EXISTS `AUTH`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AUTH` (
  `aId` varchar(100) NOT NULL,
  `aUId` int(11) NOT NULL,
  PRIMARY KEY (`aId`,`aUId`),
  KEY `fk_AUTH_1` (`aUId`),
  CONSTRAINT `fk_AUTH_1` FOREIGN KEY (`aUId`) REFERENCES `USER` (`uId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BLACKLIST`
--

DROP TABLE IF EXISTS `BLACKLIST`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BLACKLIST` (
  `bId` int(11) NOT NULL,
  PRIMARY KEY (`bId`),
  KEY `fk_BLACKLIST_1` (`bId`),
  CONSTRAINT `fk_BLACKLIST_1` FOREIGN KEY (`bId`) REFERENCES `USER` (`uId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `DEPT`
--

DROP TABLE IF EXISTS `DEPT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DEPT` (
  `dId` int(11) NOT NULL AUTO_INCREMENT,
  `dName` varchar(50) NOT NULL,
  `dBudget` int(11) NOT NULL,
  `dCommon` int(11) NOT NULL,
  `dUniversity` int(11) NOT NULL,
  PRIMARY KEY (`dId`),
  KEY `fk_DEPT_1` (`dUniversity`),
  CONSTRAINT `fk_DEPT_1` FOREIGN KEY (`dUniversity`) REFERENCES `UNIVERSITY` (`unId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `SComision`.`checkUniq`
BEFORE INSERT ON `SComision`.`DEPT`
FOR EACH ROW
BEGIN
    if (SELECT COUNT(*) FROM DEPT WHERE `dUniversity` = NEW.`dUniversity`
        AND `dName` = NEW.`dName`) then
        call fail("There is such dep", 1005);
    end if;
    if (NEW.`dBudget` > NEW.`dCommon`) then
        call fail("Illegal data format", 1006);
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `SComision`.`checkUniqUpd`
BEFORE UPDATE ON `SComision`.`DEPT`
FOR EACH ROW
BEGIN
    if (NEW.`dBudget` > NEW.`dCommon`) then
        call fail("Illegal data format", 1006);
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `DEPT_SUBJ`
--

DROP TABLE IF EXISTS `DEPT_SUBJ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DEPT_SUBJ` (
  `dsDepId` int(11) NOT NULL,
  `dsSubjId` int(11) NOT NULL,
  PRIMARY KEY (`dsDepId`,`dsSubjId`),
  KEY `fk_DEPT_SUBJ_1` (`dsSubjId`),
  KEY `fk_DEPT_SUBJ_2` (`dsDepId`),
  CONSTRAINT `fk_DEPT_SUBJ_1` FOREIGN KEY (`dsSubjId`) REFERENCES `SUBJ` (`sId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_DEPT_SUBJ_2` FOREIGN KEY (`dsDepId`) REFERENCES `DEPT` (`dId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ENROLLEE`
--

DROP TABLE IF EXISTS `ENROLLEE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ENROLLEE` (
  `eId` int(11) NOT NULL,
  `eCity` varchar(50) NOT NULL,
  `eDistr` varchar(50) NOT NULL,
  `eEducName` varchar(100) NOT NULL,
  `eMarks` longblob NOT NULL,
  `eAvMark` float NOT NULL,
  PRIMARY KEY (`eId`),
  KEY `fk_ENROLLEE_1` (`eId`),
  CONSTRAINT `fk_ENROLLEE_1` FOREIGN KEY (`eId`) REFERENCES `USER` (`uId`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ENROLLEE_DEP`
--

DROP TABLE IF EXISTS `ENROLLEE_DEP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ENROLLEE_DEP` (
  `edDeptId` int(11) NOT NULL,
  `edEnrId` int(11) NOT NULL,
  `edRegDate` date NOT NULL,
  `edPrior` int(11) NOT NULL,
  PRIMARY KEY (`edEnrId`,`edRegDate`,`edDeptId`),
  KEY `fk_ENROLLEE_DEP_1` (`edEnrId`),
  KEY `fk_ENROLLEE_DEP_2` (`edDeptId`),
  CONSTRAINT `fk_ENROLLEE_DEP_1` FOREIGN KEY (`edEnrId`) REFERENCES `ENROLLEE` (`eId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ENROLLEE_DEP_2` FOREIGN KEY (`edDeptId`) REFERENCES `DEPT` (`dId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`scadmin`@`%`*/ /*!50003 TRIGGER `SComision`.`checkMarks`
BEFORE INSERT ON `SComision`.`ENROLLEE_DEP`
FOR EACH ROW
BEGIN
    IF (SELECT COUNT(*) FROM ENROLLEE_DEP WHERE YEAR(NEW.`edRegDate`) = YEAR(`edRegDate`)
            AND `edDeptId` = NEW.`edDeptId` AND `edEnrId` = NEW.`edEnrId`) > 0 then 
        call fail("Duplicate entry for key", 1007);
    END if;
    IF (SELECT COUNT(*) FROM ENROLLEE_DEP WHERE YEAR(NEW.`edRegDate`) = YEAR(`edRegDate`)
            AND `edPrior` = NEW.`edPrior` AND `edEnrId` = NEW.`edEnrId`) > 0 then 
        call fail("Duplicate entry for prior", 1007);
    END if;

    call isCorrectMarks(NEW.edEnrId, NEW.edDeptId);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ENROLLEE_MARK`
--

DROP TABLE IF EXISTS `ENROLLEE_MARK`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ENROLLEE_MARK` (
  `emEnId` int(11) NOT NULL,
  `emSubjId` int(11) NOT NULL,
  `emCertMark` int(11) NOT NULL,
  `emTestMark` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`emEnId`,`emSubjId`),
  KEY `fk_ENROLLEE_MARK_1` (`emEnId`),
  KEY `fk_ENROLLEE_MARK_2` (`emSubjId`),
  CONSTRAINT `fk_ENROLLEE_MARK_1` FOREIGN KEY (`emEnId`) REFERENCES `ENROLLEE` (`eId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_ENROLLEE_MARK_2` FOREIGN KEY (`emSubjId`) REFERENCES `SUBJ` (`sId`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `updMarksAfterInsert` AFTER INSERT ON  `ENROLLEE_MARK`
FOR EACH ROW
BEGIN
    call calcAvgMark(NEW.`emEnId`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `updMarksAfterDelete` AFTER DELETE ON  `ENROLLEE_MARK`
FOR EACH ROW
BEGIN
    call calcAvgMark(OLD.`emEnId`);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `REPORT`
--

DROP TABLE IF EXISTS `REPORT`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `REPORT` (
  `repDepName` int(11) NOT NULL,
  `repEnName` int(11) NOT NULL,
  `repIsBudget` bit(1) NOT NULL DEFAULT b'0',
  `repDate` datetime NOT NULL,
  `repAssertionDate` year(4) NOT NULL,
  `repAvMark` float NOT NULL,
  PRIMARY KEY (`repDepName`,`repEnName`,`repAssertionDate`),
  KEY `fk_CERT_1` (`repDepName`),
  KEY `fk_CERT_2` (`repEnName`),
  CONSTRAINT `fk_CERT_1` FOREIGN KEY (`repDepName`) REFERENCES `DEPT` (`dId`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_CERT_2` FOREIGN KEY (`repEnName`) REFERENCES `ENROLLEE` (`eId`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary table structure for view `REPORT_VIEW`
--

DROP TABLE IF EXISTS `REPORT_VIEW`;
/*!50001 DROP VIEW IF EXISTS `REPORT_VIEW`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE TABLE `REPORT_VIEW` (
  `uId` tinyint NOT NULL,
  `uEmail` tinyint NOT NULL,
  `unName` tinyint NOT NULL,
  `dName` tinyint NOT NULL,
  `uLastName` tinyint NOT NULL,
  `uName` tinyint NOT NULL,
  `uPatr` tinyint NOT NULL,
  `repDate` tinyint NOT NULL,
  `repIsBudget` tinyint NOT NULL,
  `eAvMark` tinyint NOT NULL,
  `repAssertionDate` tinyint NOT NULL
) ENGINE=MyISAM */;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `ROLE`
--

DROP TABLE IF EXISTS `ROLE`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ROLE` (
  `rId` int(11) NOT NULL AUTO_INCREMENT,
  `rName` varchar(50) NOT NULL,
  `rDescr` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`rId`),
  UNIQUE KEY `rName_UNIQUE` (`rName`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SUBJ`
--

DROP TABLE IF EXISTS `SUBJ`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SUBJ` (
  `sId` int(11) NOT NULL AUTO_INCREMENT,
  `sName` varchar(50) NOT NULL,
  PRIMARY KEY (`sId`),
  UNIQUE KEY `sName_UNIQUE` (`sName`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UNIVERSITY`
--

DROP TABLE IF EXISTS `UNIVERSITY`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UNIVERSITY` (
  `unId` int(11) NOT NULL AUTO_INCREMENT,
  `unName` varchar(100) NOT NULL,
  `unAdmin` int(11) NOT NULL,
  PRIMARY KEY (`unId`),
  UNIQUE KEY `unName_UNIQUE` (`unName`),
  UNIQUE KEY `unAdmin_UNIQUE` (`unAdmin`),
  KEY `fk_UNIVERSITY_1` (`unAdmin`),
  CONSTRAINT `fk_UNIVERSITY_1` FOREIGN KEY (`unAdmin`) REFERENCES `USER` (`uId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `USER`
--

DROP TABLE IF EXISTS `USER`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `USER` (
  `uId` int(11) NOT NULL AUTO_INCREMENT,
  `uName` varchar(50) NOT NULL,
  `uLastName` varchar(50) NOT NULL,
  `uPatr` varchar(50) NOT NULL,
  `uEmail` varchar(100) NOT NULL,
  `uRole` int(11) NOT NULL,
  `uPass` varchar(70) NOT NULL,
  `uLang` varchar(10) NOT NULL,
  PRIMARY KEY (`uId`),
  UNIQUE KEY `uEmail_UNIQUE` (`uEmail`),
  KEY `fk_USER_1` (`uRole`),
  CONSTRAINT `fk_USER_1` FOREIGN KEY (`uRole`) REFERENCES `ROLE` (`rId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=44 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER dontRemoveRoot before delete on `USER`
FOR EACH ROW
BEGIN
    if OLD.`uId` = 1 then
        call fail("Can't remove root user", 1000);
    end if;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Dumping routines for database 'SComision'
--
/*!50003 DROP FUNCTION IF EXISTS `applyToBudget` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `applyToBudget`(userId INT, reportDate DATETIME) RETURNS bit(1)
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE depId INT DEFAULT 0;
    DECLARE inserted INT DEFAULT FALSE;
    DECLARE assertDate DATE;
    DECLARE avMark FLOAT;

    DECLARE userDepartments CURSOR FOR 
      (SELECT `edDeptId`, `edRegDate` FROM `ENROLLEE_DEP` 
          WHERE `edEnrId` = userId AND YEAR(`edRegDate`) = YEAR(reportDate) 
          ORDER BY `edPrior` DESC);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
      
    SELECT `eAvMark` INTO avMark FROM `ENROLLEE` WHERE `eId` = userId;
    
    OPEN userDepartments;
    dep_loop: LOOP
        FETCH userDepartments INTO depId, assertDate;
        -- break the loop
        IF done THEN
          LEAVE dep_loop;
        END IF;
        
        IF budgetVacantExist(depId, reportDate) THEN
            INSERT INTO `REPORT` 
                SELECT depId, userId, 1, reportDate, YEAR(assertDate), avMark;

            SET inserted = TRUE;
            SET done = TRUE;
        END IF;
    END LOOP;
    CLOSE userDepartments;

    return inserted;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `applyToContract` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `applyToContract`(userId INT, reportDate DATETIME) RETURNS bit(1)
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE depId INT DEFAULT 0;
    DECLARE inserted INT DEFAULT FALSE;
    DECLARE assertDate DATE;
    DECLARE avMark FLOAT;
    DECLARE userDepartments CURSOR FOR 
      (SELECT `edDeptId`, `edRegDate` FROM `ENROLLEE_DEP` 
          WHERE `edEnrId` = userId AND YEAR(`edRegDate`) = YEAR(reportDate) 
          ORDER BY `edPrior` DESC);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
      
    SELECT `eAvMark` INTO avMark FROM `ENROLLEE` WHERE `eId` = userId;
    
    OPEN userDepartments;
    dep_loop: LOOP
        FETCH userDepartments INTO depId, assertDate;
        -- break the loop
        IF done THEN
          LEAVE dep_loop;
        END IF;
        
        IF contractVacantExist(depId, reportDate) THEN
            INSERT INTO `REPORT` 
                SELECT depId, userId, 0, reportDate, YEAR(assertDate), avMark;

            SET inserted = TRUE;
            SET done = TRUE;
        END IF;
    END LOOP;
    CLOSE userDepartments;

    return inserted;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `budgetVacantExist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `budgetVacantExist`(dep INT, nowDate DATE) RETURNS bit(1)
BEGIN
    DECLARE vacants INT DEFAULT 0;
    DECLARE engaged INT;

    SELECT `dBudget` INTO vacants FROM `DEPT` WHERE `dId` = dep;
    SELECT COUNT(*) INTO engaged FROM `REPORT` WHERE `repDepName` = dep AND 
    repAssertionDate = YEAR(nowDate);
    
    return vacants > engaged; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `contractVacantExist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `contractVacantExist`(dep INT, nowDate DATE) RETURNS bit(1)
BEGIN
    DECLARE vacants INT DEFAULT 0;
    DECLARE engaged INT;

    SELECT `dCommon` INTO vacants FROM `DEPT` WHERE `dId` = dep;
    SELECT COUNT(*) INTO engaged FROM `REPORT` WHERE `repDepName` = dep AND 
    repAssertionDate = YEAR(nowDate);
    
    return vacants > engaged; 
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `exist` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `exist`(uId INT, da DATE) RETURNS bit(1)
BEGIN
    DECLARE CNT INT DEFAULT 0;
     
    
        
    SET CNT = (SELECT COUNT(*) FROM `REPORT` WHERE `repEnName` = uId
                AND `repAssertionDate` = YEAR(da));

    return cnt > 0;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `isBudget` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `isBudget`(depId INT, enrId INT) RETURNS bit(1)
BEGIN
    DECLARE offset INT DEFAULT 0;
    
    -- calculate offset to budget
    SELECT `dBudget` INTO offset FROM `DEPT` WHERE `dId` = depId LIMIT 1;
    -- SET offset = offset + 1;
    
    -- check if there is such id in the bounded result
    return enrId in (SELECT `edEnrId` FROM `ENROLLEE_DEP`
                    JOIN (SELECT * FROM `ENROLLEE` 
                            -- blocked enrollees is not used
                            WHERE `eId` NOT IN (SELECT * FROM `BLACKLIST`) 
                            AND `eId` IN 
                                (SELECT `edEnrId` FROM `ENROLLEE_DEP` 
                                    WHERE `edDeptId` = depId)
                            ORDER BY `eAvMark` DESC 
                            LIMIT offset) as T ON `eId` = `edEnrId`
                    WHERE `edDeptId` = depId AND YEAR(NOW()) = YEAR(`edRegDate`));
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyToContract` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `applyToContract`(IN userId INT, IN reportDate DATETIME)
BEGIN
    DECLARE done INT DEFAULT FALSE;
    DECLARE depId INT DEFAULT 0;
    DECLARE inserted INT DEFAULT FALSE;
    DECLARE assertDate DATE;
    DECLARE avMark FLOAT;
    DECLARE userDepartments CURSOR FOR 
      (SELECT `edDeptId`, `edRegDate` FROM `ENROLLEE_DEP` 
          WHERE `edEnrId` = userId AND YEAR(`edRegDate`) = YEAR(reportDate) 
          ORDER BY `edPrior` DESC);
    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
      
    SELECT `eAvMark` INTO avMark FROM `ENROLLEE` WHERE `eId` = userId;
    
    OPEN userDepartments;
    dep_loop: LOOP
        FETCH userDepartments INTO depId, assertDate;
        -- break the loop
        IF done THEN
          LEAVE dep_loop;
        END IF;
        
        IF contractVacantExist(depId, reportDate) THEN
            INSERT INTO `REPORT` 
                SELECT depId, userId, 0, reportDate, YEAR(assertDate), avMark;

            SET inserted = TRUE;
            SET done = TRUE;
        END IF;
    END LOOP;
    CLOSE userDepartments;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `applyUser` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `applyUser`(IN userId INT, IN reportDate DATETIME)
BEGIN
     
    IF NOT applyToBudget(userId, reportDate) THEN 
        call applyToContract(userId, reportDate);
    END IF;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buildReport` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buildReport`()
BEGIN
    DECLARE reportDate DATETIME default NOW();
    DECLARE id INT;
    DECLARE done INT DEFAULT FALSE;
    DECLARE requiredApplicants INT;
    DECLARE engagedApplicants INT;

    DECLARE user CURSOR FOR 
    (SELECT `edEnrId` FROM `ENROLLEE_DEP`, `ENROLLEE`
        -- get applications with this year
        WHERE YEAR(`edRegDate`) = YEAR(reportDate)
        -- user is not blocked
        AND `edEnrId` NOT IN (SELECT * FROM `BLACKLIST`)
        -- join
        AND `eId` = `edEnrId`
        -- user is not already applied
        AND `eId` NOT IN (
                SELECT `repEnName` FROM `REPORT` 
                WHERE `repEnName` = `eId`
                AND YEAR(`repAssertionDate`) = YEAR(`edRegDate`)
        )
        -- get unique id
        GROUP BY `eId`
        -- score table
        ORDER BY `eAvMark` DESC
    );

    DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;
   
    SELECT SUM(`dCommon`) INTO requiredApplicants FROM 
        (SELECT `edDeptId` FROM `ENROLLEE_DEP` GROUP BY `edDeptId`) AS T
    JOIN `DEPT` ON `edDeptId` = `dId`;    

    OPEN user;
    read_loop: LOOP
      FETCH user INTO id;
      
      SELECT COUNT(*) INTO engagedApplicants FROM `REPORT` 
      WHERE YEAR(`repAssertionDate`) = reportDate;
      -- break the loop
      IF done OR requiredApplicants <= engagedApplicants THEN
        LEAVE read_loop;
      END IF;
      
      IF NOT exist(id, reportDate) THEN
        call applyUser(id, reportDate);
      END IF;

      END LOOP;
      CLOSE user;

      -- returns timestamp of report
      SELECT reportDate as `repDate`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buildReportOld` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buildReportOld`(in university INT)
BEGIN
    DECLARE orderDate DATETIME default NOW();
    DECLARE offset INT DEFAULT 0;
    DECLARE depsMax INT;
    DECLARE depsMin INT;
    DECLARE i INT;

    SET depsMax := (SELECT MAX(`dId`) FROM `DEPT` WHERE `dUniversity` = university);
    SET depsMin := (SELECT MIN(`dId`) FROM `DEPT` WHERE `dUniversity` = university);
    SET i := depsMin;
    
    while i <= depsMax do
       
        -- check if there is such department
        if (SELECT COUNT(*) FROM `DEPT` WHERE `DEPT`.`dId` = i 
            AND `dUniversity` = university LIMIT 1) then

            -- calculate the offset           
            SELECT `dCommon` INTO offset FROM `DEPT` WHERE `dId` = i LIMIT 1;

            -- insert department report
            INSERT INTO `REPORT` 
            SELECT i, `eId`, isBudget(i, `eId`), orderDate, `edRegDate` FROM `ENROLLEE_DEP` 
            JOIN (SELECT * FROM `ENROLLEE` 
                    -- Blocked enrollees not used
                    WHERE `eId` not in (SELECT * FROM `BLACKLIST`)
                    ORDER BY `eAvMark` DESC LIMIT offset) as T
                ON `eId` = `edEnrId`
            WHERE `edDeptId` = i AND YEAR(orderDate) = YEAR(`edRegDate`)
                    AND not exist(edEnrId, i, DATE(`edRegDate`));
        end if;
        SET i := i + 1;
    end while;
    SELECT orderDate;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `buildReportWithoutPrior` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `buildReportWithoutPrior`(IN university INT)
BEGIN
      DECLARE orderDate DATETIME default NOW();
      DECLARE offset INT DEFAULT 0;

      DECLARE i INT;
      DECLARE done INT DEFAULT FALSE;

      -- fetch all departments id'es registrated in this university
      DECLARE cursor_i CURSOR FOR (SELECT `dId` FROM `DEPT` 
                                        WHERE `dUniversity` = university);

      DECLARE CONTINUE HANDLER FOR NOT FOUND SET done = TRUE;

      OPEN cursor_i;

      read_loop: LOOP
        FETCH cursor_i INTO i;
        -- break the loop
        IF done THEN
          LEAVE read_loop;
        END IF;

        -- calculate the offset           
        SELECT `dCommon` INTO offset FROM `DEPT` WHERE `dId` = i LIMIT 1;
        -- SET offset = offset + 1;

        -- insert department report
        INSERT INTO `REPORT` 
            SELECT i, `eId`, isBudget(i, `eId`), orderDate, YEAR(`edRegDate`), `eAvMark` 
            FROM `ENROLLEE_DEP` 
            -- build score table
            JOIN (SELECT * FROM `ENROLLEE` 
                    -- Blocked enrollees not used
                    WHERE `eId` not in (SELECT * FROM `BLACKLIST`)
                    -- get all applicants applied to this department
                    AND `eId` IN (SELECT `edEnrId` FROM `ENROLLEE_DEP` 
                                    WHERE `edDeptId` = i)

                    ORDER BY `eAvMark` DESC LIMIT offset) 
            as T
            ON `eId` = `edEnrId`

            -- Enrollee apply in this year and does not exist already
            WHERE `edDeptId` = i AND YEAR(orderDate) = YEAR(`edRegDate`)
                  AND not exist(edEnrId, i, DATE(`edRegDate`));
        
      END LOOP;
      CLOSE cursor_i;

      -- returns timestamp of report
      SELECT orderDate as `repDate`;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `calcAvgMark` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `calcAvgMark`(in user int)
BEGIN
    -- constants declarations
    DECLARE TEST_MAX_MARK INT DEFAULT 200;
    DECLARE TEST_HIGH_BOUND INT DEFAULT 88;
    
    DECLARE testMark FLOAT DEFAULT 0;
    DECLARE testCnt INT DEFAULT 0;
    
    -- Total mark
    DECLARE score FLOAT DEFAULT 0;

    SELECT AVG(`emCertMark`) INTO score 
                    FROM `ENROLLEE_MARK` WHERE `emEnId` = user;

    SELECT SUM(`emTestMark`), COUNT(*) INTO testMark, testCnt 
                    FROM `ENROLLEE_MARK` WHERE `emEnId` = user
                    AND `emTestMark` != 0;
    
    IF testCnt > 0 THEN
        -- trim test score to range [0; 88]
        SET testMark = (testMark / (TEST_MAX_MARK * testCnt)) * TEST_HIGH_BOUND;
    END IF;
    
    -- summate and round to 2 digits after point
    SET score = IFNULL(TRUNCATE(score + testMark, 2), 0);
    
    -- update value
    UPDATE `ENROLLEE` SET `eAvMark` = score WHERE `eId` = user;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `fail` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'STRICT_TRANS_TABLES,STRICT_ALL_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,TRADITIONAL,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `fail`(in msg VARCHAR(255), in erno INT)
BEGIN
    SIGNAL SQLSTATE '45000'
      SET MESSAGE_TEXT = msg, MYSQL_ERRNO = erno;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `isCorrectMarks` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `isCorrectMarks`(in user INT, in dep INT)
BEGIN
    if (SELECT COUNT(*) FROM `DEPT_SUBJ` 
        WHERE `dsSubjId` not in 
            (SELECT `emSubjId` FROM `ENROLLEE_MARK` 
                WHERE `emEnId` = user AND `emTestMark` > 0)
         AND `dsDepId` = dep) > 0 then
            call fail('Subjects are not mutched', 1002);
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Final view structure for view `REPORT_VIEW`
--

/*!50001 DROP TABLE IF EXISTS `REPORT_VIEW`*/;
/*!50001 DROP VIEW IF EXISTS `REPORT_VIEW`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `REPORT_VIEW` AS select `USER`.`uId` AS `uId`,`USER`.`uEmail` AS `uEmail`,`UNIVERSITY`.`unName` AS `unName`,`DEPT`.`dName` AS `dName`,`USER`.`uLastName` AS `uLastName`,`USER`.`uName` AS `uName`,`USER`.`uPatr` AS `uPatr`,`REPORT`.`repDate` AS `repDate`,if(`REPORT`.`repIsBudget`,'BUDGET','CONTRACT') AS `repIsBudget`,`REPORT`.`repAvMark` AS `eAvMark`,`REPORT`.`repAssertionDate` AS `repAssertionDate` from ((((`REPORT` join `DEPT` on((`DEPT`.`dId` = `REPORT`.`repDepName`))) join `UNIVERSITY` on((`UNIVERSITY`.`unId` = `DEPT`.`dUniversity`))) join `ENROLLEE` on((`ENROLLEE`.`eId` = `REPORT`.`repEnName`))) join `USER` on((`USER`.`uId` = `REPORT`.`repEnName`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-03-06 15:49:43
