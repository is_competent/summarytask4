<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>

<!DOCTYPE html>
<html>
<c:set scope="page" value="menu.home" var="localized"></c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<c:set scope="page" value="Home" var="active"></c:set>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>
		<div class="space h20"></div>
		<div class="content autoalign">
			<fmt:message key="common.personalinfo" />
			<hr>
			<form method="POST" action="<my:const name="TO_CONTROLLER"/>" autocomplete="off">
				<input type="hidden" name="<my:const name="COMMAND"/>" value="changeInfo">
				<label><fmt:message key="common.lname" />:</label> <input
					value="${user.LName}" required="required"
					name="<my:const name="USER_LAST_NAME"/>"> <br> <label><fmt:message
						key="common.name" />:</label> <input value="${user.name}"
					required="required" name="<my:const name="USER_NAME"/>"> <br>
				<label><fmt:message key="common.patr" />:</label> <input
					value="${user.patr}" required="required"
					name="<my:const name="USER_PATR"/>"> <br> <label>Email:</label>
				<input type="email" value="${user.email}" required="required"
					name="<my:const name="USER_EMAIL"/>">
				<p>
					<input type="submit" value='<fmt:message key="common.upd"/>' onclick="return confirm('Save?');">
			</form>
			<div class="h20"></div>
				<%@ include file="/WEB-INF/jspf/changepass.jspf"%>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
	</div>
</body>
</html>