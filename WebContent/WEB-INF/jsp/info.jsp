<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>

<!DOCTYPE html>
<html>
<c:set scope="page" value="Info" var="title"></c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>
		
		<div class="space h20"></div>
		<div class="content">
			<h2 class="error"><fmt:message key="info.title"/></h2>
			<p class="info">${info}</p>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf" %>
</body>
</html>