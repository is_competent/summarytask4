<%@ include file="/WEB-INF/jspf/directiveJson.jspf"%>
{
	"status": ${empty jsonStatus? 400: jsonStatus },
	"message": "${empty emessage? Ok: emessage}",
	"id": ${empty jsonId? 0: jsonId}
}