<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set scope="page" value="admin.menu.enr" var="localized"></c:set>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>

		<!-- Content starts here -->
		<div class="space h20"></div>
		<div class="content">
		<fmt:message key="common.personalinfo"/>
		<hr>
			${user.LName} ${user.name} ${user.patr}<br>
			${user.email}
		<p>
		<hr>
		<p>
			<fmt:message key="admin.enrollee.school"/>: ${enr.distrName}, ${enr.city}, ${enr.educName}<br>
			<fmt:message key="admin.enrollee.score"/>: 
			<fmt:formatNumber maxFractionDigits="2">${enr.avMark}</fmt:formatNumber>
		<p>
		<form action="do" method="post">
			<input type="hidden" name="id" value="${user.id}">
		<c:choose>
			<%-- Unblock user if it is blocked --%>
			<c:when test="${isBlocked}">
				<input type="hidden" name="command" value="unblockEnrollee">
				<input type="submit" value="<fmt:message key='admin.unblockenrollee'/>"
				onclick="return confirm('Do you want to unblock the user');">
			</c:when>
			<%-- Block user if it is not blocked --%>
			<c:otherwise>
				<input type="hidden" name="command" value="blockEnrollee">
				<input type="submit" value="<fmt:message key='admin.blockenrollee'/>"
				onclick="return confirm('Do you want to block the user');">
			</c:otherwise>
		</c:choose>
		</form>
		<p>
			<img id="userMarks" src="data:image/jpg;base64, <my:convert src='${enr.marksScan}'/>">
		</div>
		<!-- End of content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>