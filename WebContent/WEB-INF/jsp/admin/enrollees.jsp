<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set scope="page" value="admin.menu.enr" var="localized"></c:set>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>

		<!-- Content starts here -->
		<div class="space h20"></div>
		<div class="content">
		<form action="do" autocomplete="off" id="searchForm">
			<input type="hidden" name="command" value="searchEnrollee">
			<input type="search" name="cond" pattern=".{3,}">
			<input type="submit" value="" title='<fmt:message key="admin.search"/>'>
		</form>
		<div class="h20"></div>
		<div class="h20"></div>
		<hr>
		<c:if test="${enrollees.isEmpty()}">
			<p id="notFound"><fmt:message key="admin.enrollee.notfound"/>
		</c:if>
		<ul id="searchResult">
			<c:forEach items="${enrollees}" var="item">
			<li><a href="do?command=describeEnrollee&amp;id=${item.key.id}">
			${item.key.LName} ${item.key.name} ${item.key.patr}<br>
			</a>${item.key.email}<br>Applications - ${item.value}</li>
			</c:forEach>
		</ul>
		</div>
		<!-- End of content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>