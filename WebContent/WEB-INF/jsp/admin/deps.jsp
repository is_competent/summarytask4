<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set scope="page" value="admin.menu.dep" var="localized"></c:set>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
<script>
	var INPUT = {
			"#inputId":"name",
			"#common":"com",
			"#budget":"budget",
			"pattern": '<li><button id="{id}">X</button><a href="do?command=describeDepartment&amp;id={id}">{name}</a></li>'
	};
	var CONST = {};
	function getName() {
		return $("#inputId").val();
	}
	
	function validate() {
		var result = false;
		result = $("#inputId").val().length > 1;
		result &= parseInt($("#budget").val()) >= 0;
		result &= parseInt($("#common").val()) >= parseInt($("#budget").val());
		
		return result;
	}
</script>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>

		<!-- Content starts here -->
		<div class="space h20"></div>
		<div class="content">
			<%@ include file="/WEB-INF/jspf/newdep.jspf"%>
			<div class="clear"></div>
		</div>
		<!-- End of content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>