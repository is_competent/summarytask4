<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>

<!DOCTYPE html>
<html>
<c:set scope="page" value="Registry" var="title"></c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>
		
		<div class="space h20"></div>
		<div class="content autoalign">
			<form id="form" method="POST" autocomplete="off"
				action="<my:const name="TO_CONTROLLER"/>">
				<input type="hidden" name="<my:const name="COMMAND"/>"value="restoreUser"> 
				
				<label><fmt:message key="common.lname" />:</label> 
				<input required="required" name="<my:const name="USER_LAST_NAME"/>"
				pattern="^.{3,100}$"> <br> 
				
				<label><fmt:message key="common.name" />:</label> 
				<input required="required" name="<my:const name="USER_NAME"/>"
				pattern="^.{3,100}$"> <br> 
				
				<label><fmt:message key="common.patr" />:</label> 
				<input required="required" name="<my:const name="USER_PATR"/>"
				pattern="^.{3,100}$"> <br> 
				
				<label>Email:</label>
				<input type="email" value="${restored.email}" required="required"
					name="<my:const name="USER_EMAIL"/>">
				<p>
				<p>
				<hr>

					<label><fmt:message key="admin.university" /></label>
				<input type="text" required="required" name="uname"
				pattern=".{2,100}">
					
				<p>
					<p>
				<hr>
				<input id="hNew" type="hidden" name="newPass">
				<label><fmt:message key="common.newpass" />:</label> 
				<input id="newPass" type="password" required="required"
				pattern="^[0-9A-Za-z@$#]{5,}$"> <br> 
				
				<label><fmt:message key="common.confirmpass" />:</label> 
				<input id="confPass" type="password" required="required"
				pattern="^[0-9A-Za-z@$#]{5,}$"><br>
			
				<p>
				<p>
				<hr>
				<img id="capIm" src="cap.png"><br>
					<a href="#" id="capRef">Refresh</a><br>
					<input required="required" id="capF"><p>
					<input id="restore" type="submit"
						value='<fmt:message key="common.upd"/>'>
			
			</form>
			<div class="h20"></div>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>