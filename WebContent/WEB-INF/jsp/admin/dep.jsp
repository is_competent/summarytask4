<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<c:set scope="page" value="admin.menu.dep" var="localized"></c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<script>
		var INPUT = {
			"#inputId" : "id",
			"pattern" : '<li><button id="{id}">X</button>{name}</li>'
		};
		var CONST = {
				"did": ${department.id}
		};
		function getName() {
			return $("#inputId option[value=" + $("#inputId").val()+"]").text();
		}

		function validate() {
			var result = false;
			result = $("#inputId").val().length > 1;
			result &= parseInt($("#budget").val()) >= 0;
			result &= parseInt($("#common").val()) >= parseInt($("#budget").val());
			
			return true;
		}
		
	</script>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>

		<!-- Content starts here -->
		<div class="space h20"></div>
		<div class="content autoalign">
			<form method="post" action="do" autocomplete="off">
				<input type="hidden" name="command" value="updateDepartment">
				<input type="hidden" name="id" value="${department.id}"> 
				<label><fmt:message
						key="admin.dep.name" />:</label><input type="text"
					value="${department.name }" name="name" pattern=".{2,100}"><br>
				<label><fmt:message key="admin.dep.budget" />:</label><input
					name="budget" value="${department.budgetPlaces }" type="text"
					pattern="^[0-9]{1,}$"><br> <label><fmt:message
						key="admin.dep.common" />:</label><input name="com" type="text"
					value="${department.commonPlaces }" pattern="^[0-9]{1,}$"><br>
				<p>
					<input type="submit" value="<fmt:message key="button.upd" />"
						id="subm">
			</form>
			<p>
				<fmt:message key="admin.dep.subjects" />
				<hr>
			<p>
				<%@ include file="/WEB-INF/jspf/depsubjs.jspf"%>
		
		</div>
		<!-- End of content -->
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<script>
	$("#subm").click(function(e) {
		var result = false;
		result = parseInt($("input[name=budget]").val()) >= 0;
		result &= parseInt($("input[name=com]").val()) >= 
		parseInt($("input[name=budget]").val());
		
		if(!result) {
			alert("Data format error");
			e.preventDefault();
		} else {
			result = confirm('Do yo want to change department?');
		}
		
		if (!result) {
			e.preventDefault();
		}
	});
	
	
</script>
				<%@ include file="/WEB-INF/jspf/scripts.jspf"%>

			</body>
</html>