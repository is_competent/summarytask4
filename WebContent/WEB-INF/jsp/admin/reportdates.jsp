<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set scope="page" value="admin.menu.report" var="localized"></c:set>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>

		<!-- Content starts here -->
		<div class="space h20"></div>
		<div class="content">
		<!--  
		<form action="do" method="POST">
			<input type="hidden" name="command" value="makeReport">
			<input type="submit" value='<fmt:message key="admin.button.makereport"/>'
			onclick="return confirm('Do you want to make report?');">
		</form>
		<p>
		-->
		<fmt:message key="admin.reports.bydate"/>
			<ul>
				<c:forEach items="${dates}" var="item">
					<li><a target="_blank" href="do?command=getReport&amp;date=${item}">
					<fmt:formatDate value="${item}" dateStyle="full"/></a></li>
				</c:forEach>
			</ul>
		</div>
		<!-- End of content -->
			<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>