<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set value="root.menu.invite" var="localized"/>
	<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf" %>
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		
		<div class="space h20"></div>
		<div class="content autoalign">
		
		<form action="<my:const name="CONTROLLER"/>" method="post">
			<label>Email:</label> 
			<input type="email" name="toEmail" required="required">
			<p><input type="submit" value='<fmt:message key="root.invite" />'>
		</form>
		
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
		<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>