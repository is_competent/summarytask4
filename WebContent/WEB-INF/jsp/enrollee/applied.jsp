<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set scope="page" value="enrollee.menu.applied" var="localized"></c:set>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>

		<!-- Content starts here -->
		<div class="space h20"></div>
		<div class="content">
		<fmt:message key="enrollee.menu.applied"/>
		<p>
		<hr>
		<p>
		<table id="departments" class="userApplications">
			<tr>
				<th class="sortable">
					<fmt:message key="enrollee.applied.university"/>
					<div></div>
				</th>
				<th class="sortable">
					<fmt:message key="enrollee.applied.department"/>
					<div></div>
				</th>
				<th class="sortable">
					<fmt:message key="enrollee.applied.status"/>
					<div></div>
				</th>
				<th class="sortable">
					<fmt:message key="enrollee.applied.applyDate"/>
					<div></div>
				</th>
			</tr>
			<tbody>
				<c:forEach items="${applications}" var="item">
					<c:set value='${item.applyStatus eq "CONSIDERED" ? " ":"class=success"}'
					var="classVar" scope="page"/>
					<tr ${classVar}>
						<td>${item.universityName}</td>
						<td>${item.departmentName}</td>
						<td>${item.applyStatus}</td>
						<td><fmt:formatDate value="${item.applyDate}"/></td>
					</tr>	
				</c:forEach>
			</tbody>
			</table>
		</div>
		<!-- End of content -->
		<!-- Footer starts here -->
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>