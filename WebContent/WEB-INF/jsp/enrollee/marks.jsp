<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
<c:set scope="page" value="enrollee.menu.marks" var="localized"></c:set>
<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<script>
		var INPUT = {
			"#inputId" : "sid",
			"#cmarkId" : "cmark",
			"#tmarkId" : "tmark",
			"pattern" : '<li><button id="{id}">X</button>{name}</li>'
		};
		var CONST = {};

		function getName() {
			var sbj = $("#inputId option[value=" + $("#inputId").val() + "]")
					.text();
			return sbj + ", <fmt:message key="enrollee.tmark"/> - " + $("#tmarkId").val() + ",  <fmt:message key="enrollee.cmark"/> - "
					+ $("#cmarkId").val();
		}

		function validate() {
			var result = false;
			var cMark = parseInt($("#cmarkId").val());
			var tMark = parseInt($("#tmarkId").val());
			result = tMark >= 0 && tMark <= 200;
			result &= cMark > 0 && cMark <= 12;
			return result;
		}
	</script>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>

		<!-- Content starts here -->
		<div class="space h20"></div>
		<div class="content autoalign">
			<div id="formDiv">
				<input type="hidden" name="command" value="createEnrolleeMark">
				<input type="hidden" name="remCommand" value="removeEnrolleeMark">

				<select id="inputId">
					<c:forEach items="${applicationScope.subjects}" var="item">
						<option value="${item.id}">${item.name}</option>
					</c:forEach>
				</select> <br> 
				<label><fmt:message key="enrollee.cmark"/>:</label>
				<input id="cmarkId" type="text" pattern="[0-9]{1,}"><br> 
				<label><fmt:message key="enrollee.tmark"/>:</label>
				<input id="tmarkId" type="text" value="0" pattern="[0-9]{1,}"><br>
				<div class="help">*<fmt:message key="enrollee.help.tmark"/></div>
				<p>
					<button id="btnNewAjax">
						<fmt:message key="common.create" />
					</button>
			</div>
			<div class="h20"></div>
			<fmt:message key="enrollee.menu.marks" />
			<hr />
			<div class="h20"></div>
			<ul id="dynamicCont">
				<c:forEach items="${marks}" var="item">
					<li><button id="${item.subjId}">X</button>${item.subjName},
						<fmt:message key="enrollee.tmark"/> - ${item.testMark}, <fmt:message key="enrollee.cmark"/> - ${item.certMark}</li>
				</c:forEach>
			</ul>
		</div>
		<!-- End of content -->
		<!-- Footer starts here -->
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>