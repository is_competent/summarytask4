<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set scope="page" value="enrollee.universities" var="localized"></c:set>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>

		<!-- Content starts here -->
		<div class="space h20"></div>
		<div class="content">
		<fmt:message key="enrollee.universities"/>
		<p>
		<hr>
			<ul>
				<c:forEach items="${universities}" var="item">
					<li><a href="do?command=showUniversityDepartments&amp;id=${item.id}">${item.name}</a></li>
				</c:forEach>
			</ul>
		</div>
		<!-- End of content -->
		<!-- Footer starts here -->
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</body>
</html>