<%@ include file="/WEB-INF/jspf/directive.jspf"%>
<%@ include file="/WEB-INF/jspf/taglib.jspf"%>
<!DOCTYPE html>
<html>
	<c:set scope="page" value="Restore" var="title"></c:set>
	<%@ include file="/WEB-INF/jspf/head.jspf"%>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf"%>
		<%@ include file="/WEB-INF/jspf/menu.jspf"%>
		
		<div class="space h20"></div>
		<div class="content autoalign">
		
			<form id="form" method="POST" autocomplete="off" action="<my:const name="TO_CONTROLLER"/>" >
				<input type="hidden" name="<my:const name="COMMAND"/>" value="restoreUser">
				
				<input id="hNew" type="hidden" name="newPass">
				
				<label><fmt:message key="common.newpass" />:</label> 
				<input id="newPass" type="password" required="required"
				pattern="^[0-9A-Za-z@$#]{5,}$"> <br> 
				
				<label><fmt:message	key="common.confirmpass" />:</label> 
				<input id="confPass" type="password" required="required"
				pattern="^[0-9A-Za-z@$#]{5,}$">
		
				<p>
				<p>
				<hr>
				<img id="capIm" src="cap.png"><br>
					<a href="#" id="capRef">Refresh</a><br>
					<input required="required" id="capF" pattern="[A-z0-9]+"><p>
					<input id="restore" type="submit" value='<fmt:message key="common.upd"/>'>
			</form>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf"%>
	</div>
</body>
	<%@ include file="/WEB-INF/jspf/scripts.jspf"%>
</html>