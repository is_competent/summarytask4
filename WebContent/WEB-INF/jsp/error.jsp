<%@ page isErrorPage="true" %>
<%@ include file="/WEB-INF/jspf/directive.jspf" %>
<%@ include file="/WEB-INF/jspf/taglib.jspf" %>

<!DOCTYPE html>
<html>
<c:set scope="page" value="Error" var="title"></c:set>
<%@ include file="/WEB-INF/jspf/head.jspf" %>
<body>
	<div class="wrapper">
		<%@ include file="/WEB-INF/jspf/board.jspf" %>
		<%@ include file="/WEB-INF/jspf/menu.jspf" %>
		
		<div class="space h20"></div>
		<div class="content">
			<h2 class="error">
					<fmt:message key="warn.title"/>
			</h2>
			
				<%-- this way we obtain an information about an exception (if it has been occurred) --%>
				<c:set var="code" value="${requestScope['javax.servlet.error.status_code']}"/>
				<c:set var="message" value="${requestScope['javax.servlet.error.message']}"/>
				
				<c:if test="${not empty code}">
					<div class="code">${code}</div>
				</c:if>			
				
				<c:if test="${not empty message}">
					<p class="emessage">${message}</p>
				</c:if>
				
				<%-- if we get this page using forward --%>
				<c:if test="${not empty emessage and empty code}">
					<p class="emessage">${emessage}</p>
				</c:if>
		</div>
		<%@ include file="/WEB-INF/jspf/footer.jspf" %>
	</div>
	<%@ include file="/WEB-INF/jspf/scripts.jspf" %>
</body>
</html>