<%@ tag body-content="empty" %>
<%@ attribute name="dep" required="true" type="java.util.Map"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<%-- Implements table of departments --%>

<table id="departments"> 
	<tr>
		<th class="sortable"><fmt:message key="enrollee.table.name"/><div></div></th>
		<th class="sortable"><fmt:message key="enrollee.table.budget"/><div></div></th>
		<th class="sortable"><fmt:message key="enrollee.table.total"/><div></div></th>
		<th><fmt:message key="enrollee.table.req_subj"/></th>
		<th><fmt:message key="enrollee.table.apply"/></th>
	</tr>
	<tbody>
	<c:forEach items="${dep}" var="item">
		<tr>
			<td>${item.key.name}</td>
			<td>${item.key.budgetPlaces}</td>
			<td>${item.key.commonPlaces}</td>
			<td>
				<ul>
					<c:forEach items="${item.value}" var="subj">
						<li>${subj.name}</li>
					</c:forEach>
				</ul>
			</td>
			<td>
				<form ${priorities.isEmpty()? "hidden": ""} action="do" method="post">
					<input type="hidden" name="command" value="apply">
					<input type="hidden" name="id" value="${item.key.id}">
					Priority: <select name="priority">
					<c:forEach items="${priorities}" var="item">
						<option value="${item}">${item}</option>
					</c:forEach>
					</select>
					<input type="submit" value="<fmt:message key="enrollee.table.apply"/>">
				</form>
			</td>
		</tr>
	</c:forEach>
	</tbody>
</table>