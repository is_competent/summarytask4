package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

/**
 * Builder for entity classes. Builds an entity (having type <strong>T</strong>)
 * using <b>ResultSet</b> or using <b>HttpServletRequest</b>
 * 
 * @author B.Suprun
 * 
 * @param <T>
 *            - Entity type
 */
public interface EntBuilder<T> {

	/**
	 * Builds an entity class using <b>ResultSet</b>-object retrieved from a
	 * database.
	 * 
	 * @param rs
	 *            - ResultSet object retrieved from a database
	 * @return - new entity
	 * @throws SQLException
	 *             if an exception has occurred
	 */
	T create(ResultSet rs) throws SQLException;

	/**
	 * Builds new entity class using parameters received from the request.
	 * 
	 * @param req
	 *            - Request from a client
	 * @return - new entity
	 */
	T create(HttpServletRequest req);

	/**
	 * Sets values generated after insertion to database.
	 * 
	 * @param ent
	 *            - An instance, where generated values will be setted
	 * @param generatedKey
	 *            - Generated values in database
	 * @throws SQLException
	 *             - If an exception with ResultSet has occurred
	 */
	void setAfterInsert(T ent, ResultSet generatedKey) throws SQLException;
}
