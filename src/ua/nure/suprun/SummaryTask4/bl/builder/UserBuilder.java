package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for User entity.
 * 
 * @author B.Suprun
 * 
 */
public class UserBuilder implements EntBuilder<User> {

	@Override
	public User create(ResultSet rs) throws SQLException {
		return new DefaultEntBuilder<>(User.class).create(rs);
	}

	@Override
	public User create(HttpServletRequest req) {
		User u = new User();
		RequestParser rp = RequestParser.getParser(req);

		u.setEmail(rp.getString(Params.USER_EMAIL));
		u.setId(rp.getInt(Params.USER_ID, Constant.ID_NOT_SET));
		u.setLang(rp.getString(Params.USER_LANG, "en"));
		u.setLName(rp.getString(Params.USER_LAST_NAME));
		u.setName(rp.getString(Params.USER_NAME));
		u.setPass(rp.getString(Params.USER_PASS, null));
		u.setPatr(rp.getString(Params.USER_PATR));
		u.setRole(rp.getInt(Params.USER_ROLE, Constant.ID_NOT_SET));

		return u;
	}

	@Override
	public void setAfterInsert(User ent, ResultSet generatedKey)
			throws SQLException {
		ent.setId(generatedKey.getInt(1));
	}

}
