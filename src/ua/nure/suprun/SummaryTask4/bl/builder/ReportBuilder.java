package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.ReportView;

/**
 * Builder for ReportView entity.
 * 
 * @author B.Suprun
 * 
 */
public class ReportBuilder implements EntBuilder<ReportView> {

	@Override
	public ReportView create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(ReportView.class).create(rs);
	}

	@Override
	public ReportView create(HttpServletRequest req) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setAfterInsert(ReportView ent, ResultSet generatedKey)
			throws SQLException {
		// Nothing to do
	}

}
