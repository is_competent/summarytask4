package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.Authorization;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for Authorization entity.
 * 
 * @author B.Suprun
 * 
 */
public class AuthBuilder implements EntBuilder<Authorization> {

	@Override
	public Authorization create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(Authorization.class).create(rs);
	}

	@Override
	public Authorization create(HttpServletRequest req) {
		Authorization aut = new Authorization();

		RequestParser parser = RequestParser.getParser(req);

		aut.setId(parser.getString(Params.AUT_ID));
		aut.setUserId(parser.getInt(Params.AUT_USER_ID));

		return aut;
	}

	@Override
	public void setAfterInsert(Authorization ent, ResultSet generatedKey)
			throws SQLException {
		// Nothing to do
	}

}
