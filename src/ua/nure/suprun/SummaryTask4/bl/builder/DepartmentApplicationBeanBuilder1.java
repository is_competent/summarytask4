package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.bean.DepartmentApplicationBean;
import ua.nure.suprun.SummaryTask4.constants.Columns;

public class DepartmentApplicationBeanBuilder1 implements EntBuilder<DepartmentApplicationBean>{

  @Override
  public DepartmentApplicationBean create(ResultSet rs) throws SQLException {
    DepartmentApplicationBean dab = new DepartmentApplicationBean();
    
    dab.setId(rs.getInt(Columns.DEPT_ID));
    dab.setDepartmentName(rs.getString(Columns.DEPT_NAME));
    dab.setUniversityName(rs.getString(Columns.UNIV_NAME));
    dab.setApplyStatus(rs.getString(Columns.REPORT_VIEW_BUDGET));
    dab.setApplyDate(rs.getDate(Columns.ENROLLEE_DEP_DATE));
    
    return dab;
  }

  @Override
  public DepartmentApplicationBean create(HttpServletRequest req) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setAfterInsert(DepartmentApplicationBean ent, ResultSet generatedKey) {
    throw new UnsupportedOperationException();
  }

}
