package ua.nure.suprun.SummaryTask4.bl.builder;

import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.bl.entity.User;

public class Demo {

  private static final Department CS = new Department();

  static {
    CS.setBudgetPlaces(10);
    CS.setCommonPlaces(10);
    CS.setId(10);
    CS.setName("CS");
    CS.setUniversity(1);
  }

  public static void main(String[] args) {
    User uAdmin = new User();
    uAdmin.setId(10);

    University un = UniversityCreator.getBuilder().setAdmin(uAdmin).setId(10).addDepartment(CS)
        .addDepartment(CS).setName("KHNURE").build();
    
    System.out.println(un);
  }

}
