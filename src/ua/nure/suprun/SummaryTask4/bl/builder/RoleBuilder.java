package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.Role;

/**
 * Builder for Role entity.
 * 
 * @author B.Suprun
 * 
 */
public class RoleBuilder implements EntBuilder<Role> {

	@Override
	public Role create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(Role.class).create(rs);
	}

	@Override
	public Role create(HttpServletRequest req) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void setAfterInsert(Role ent, ResultSet generatedKey)
			throws SQLException {
		ent.setId(generatedKey.getInt(1));
	}

}
