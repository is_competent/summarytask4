package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for University entity.
 * 
 * @author B.Suprun
 * 
 */
public class UniversityBuilder implements EntBuilder<University> {

	@Override
	public University create(ResultSet rs) throws SQLException {
		return new DefaultEntBuilder<>(University.class).create(rs);
	}

	@Override
	public University create(HttpServletRequest req) {
		University un = new University();
		RequestParser rp = RequestParser.getParser(req);

		un.setAdmin(rp.getInt(Params.UNIVERSITY_ADMIN, Constant.ID_NOT_SET));
		un.setId(rp.getInt(Params.UNIVERSITY_ID, Constant.ID_NOT_SET));
		un.setName(rp.getString(Params.UNIVERSITY_NAME));

		return un;
	}

	@Override
	public void setAfterInsert(University ent, ResultSet generatedKey)
			throws SQLException {
		ent.setId(generatedKey.getInt(1));
	}

}
