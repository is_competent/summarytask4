package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Used for retrieving all available unique report's date.
 * 
 * @author B.Suprun
 * 
 */
public class DateBuilder implements EntBuilder<Date> {

	@Override
	public Date create(ResultSet rs) throws SQLException {
	  Timestamp t = new DefaultPrimitiveEntBuilder<>(java.sql.Timestamp.class, 
	      Columns.REPORT_VIEW_DATE).create(rs);
	  
	  return new Date(t.getTime());
	}

	@Override
	public Date create(HttpServletRequest req) {
		RequestParser rp = RequestParser.getParser(req);

		return rp.getDate(Params.RV_DATE);
	}

	@Override
	public void setAfterInsert(Date ent, ResultSet generatedKey)
			throws SQLException {
		throw new UnsupportedOperationException();
	}

}
