package ua.nure.suprun.SummaryTask4.bl.builder;

import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.bl.entity.User;

/**
 * Creator for university entity.
 * 
 * @author B.Suprun
 * 
 */
public final class UniversityCreator {

  private University innerUniversity;

  private UniversityCreator() {
    innerUniversity = new University();
  }

  public static synchronized UniversityCreator getBuilder() {
    return new UniversityCreator();
  }

  public UniversityCreator setId(int id) {
    innerUniversity.setId(id);

    return this;
  }

  public UniversityCreator setName(String name) {
    innerUniversity.setName(name);

    return this;
  }

  public UniversityCreator setAdmin(User admin) {
    innerUniversity.setAdmin(admin.getId());

    return this;
  }

  public UniversityCreator addDepartment(Department dep) {
    innerUniversity.getDepartments().add(dep);

    return this;
  }

  public University build() {
    return innerUniversity;
  }
}
