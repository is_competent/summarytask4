package ua.nure.suprun.SummaryTask4.bl.builder;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for Enrollee entity.
 * 
 * @author B.Suprun
 * 
 */
public class EnrolleeBuilder implements EntBuilder<Enrollee> {

	private static final Logger LOG = Logger.getLogger(EnrolleeBuilder.class);

	@Override
	public Enrollee create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(Enrollee.class).create(rs);
	}

	/**
	 */
	@Override
	public Enrollee create(HttpServletRequest req) {
		Enrollee e = new Enrollee();
		RequestParser rp = RequestParser.getParser(req);

		try {
			e.setAvMark(rp.getDouble(Params.ENROLLEE_AV_MARK, 0));
			e.setCity(rp.getString(Params.ENROLLEE_CITY));
			e.setDistrName(rp.getString(Params.ENROLLEE_DISTRICT));
			e.setEducName(rp.getString(Params.ENROLLEE_EDUC_NAME));
			e.setId(rp.getInt(Params.ENROLLEE_ID, Constant.ID_NOT_SET));
			e.setMarksScan(rp.getBuffer());
		} catch (IOException ex) {
			LOG.error(ex);
			throw new IllegalArgumentException("Buffer is not available.", ex);
		}
		return e;
	}

	@Override
	public void setAfterInsert(Enrollee ent, ResultSet generatedKey)
			throws SQLException {
		// Nothing to do
	}

}
