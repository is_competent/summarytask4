package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.db.util.NamedStatementUtil;

public class DefaultEntBuilder<T> implements EntBuilder<T> {
  
  private static final Logger LOG = Logger.getLogger(DefaultEntBuilder.class);
  private Class<T> clazz;
  
  
  public DefaultEntBuilder(Class<T> cl) {
    clazz = cl;
  }

  @Override
  public T create(ResultSet rs) throws SQLException {
    try {
      T entity = clazz.newInstance();
      NamedStatementUtil.initialize(rs, entity); 
      LOG.trace("Entity created: " + entity);
      return entity;
    } catch(IllegalAccessException | InstantiationException ex) {
      LOG.fatal(ex);
      throw new SQLException("Object creation exception", ex);
    }
  }

  @Override
  public T create(HttpServletRequest req) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setAfterInsert(T ent, ResultSet generatedKey) {
    // Nothing to do
  }

}
