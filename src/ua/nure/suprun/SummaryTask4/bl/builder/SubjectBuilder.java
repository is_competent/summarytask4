package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for Subject entity.
 * 
 * @author B.Suprun
 * 
 */
public class SubjectBuilder implements EntBuilder<Subject> {

	@Override
	public Subject create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(Subject.class).create(rs);
	}

	@Override
	public Subject create(HttpServletRequest req) {
		Subject s = new Subject();
		RequestParser rp = RequestParser.getParser(req);

		s.setId(rp.getInt(Params.SUBJ_ID));
		s.setName(rp.getString(Params.SUBJ_NAME));

		return s;
	}

	@Override
	public void setAfterInsert(Subject ent, ResultSet generatedKey)
			throws SQLException {
		ent.setId(generatedKey.getInt(1));
	}

}
