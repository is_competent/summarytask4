package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeDept;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for EnrolleeDepartment entity.
 * 
 * @author B.Suprun
 * 
 */
public class EnrolleeDeptBuilder implements EntBuilder<EnrolleeDept> {

	@Override
	public EnrolleeDept create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(EnrolleeDept.class).create(rs);
	}

	@Override
	public EnrolleeDept create(HttpServletRequest req) {
		EnrolleeDept ed = new EnrolleeDept();
		RequestParser rp = RequestParser.getParser(req);

		ed.setDepId(rp.getInt(Params.ED_DEP_ID));
		ed.setEnId(rp.getInt(Params.ED_ENROLLEE_ID, Constant.ID_NOT_SET));
		ed.setPriority(rp.getInt(Params.ED_PRIORITY));

		return ed;
	}

	@Override
	public void setAfterInsert(EnrolleeDept ent, ResultSet generatedKey){
		// Nothing to do
	}

}
