package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for Department entity.
 * 
 * @author B.Suprun
 * 
 */
public class DepartmentBuilder implements EntBuilder<Department> {

	@Override
	public Department create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(Department.class).create(rs);
	}

	@Override
	public Department create(HttpServletRequest req) {
		Department d = new Department();
		RequestParser rp = RequestParser.getParser(req);

		d.setBudgetPlaces(rp.getInt(Params.DEP_BUDGET));
		d.setCommonPlaces(rp.getInt(Params.DEP_COMMON));
		d.setId(rp.getInt(Params.DEP_ID, Constant.ID_NOT_SET));
		d.setName(rp.getString(Params.DEP_NAME));
		d.setUniversity(rp.getInt(Params.DEP_UNIVERSITY, Constant.ID_NOT_SET));

		return d;
	}

	@Override
	public void setAfterInsert(Department ent, ResultSet generatedKey)
			throws SQLException {
		ent.setId(generatedKey.getInt(1));
	}

}
