package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeMark;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for EnrolleeMark entity.
 * 
 * @author B.Suprun
 * 
 */
public class EnrolleeMarkBuilder implements EntBuilder<EnrolleeMark> {

	@Override
	public EnrolleeMark create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(EnrolleeMark.class).create(rs);
	}

	@Override
	public EnrolleeMark create(HttpServletRequest req) {
		EnrolleeMark em = new EnrolleeMark();
		RequestParser rp = RequestParser.getParser(req);

		em.setCertMark(rp.getInt(Params.EM_CERT_MARK));
		em.setTestMark(rp.getInt(Params.EM_TEST_MARK));
		em.setEnId(rp.getInt(Params.EM_ENROLLEE_ID, Constant.ID_NOT_SET));
		em.setSubjId(rp.getInt(Params.EM_SUBJ));

		return em;
	}

	@Override
	public void setAfterInsert(EnrolleeMark ent, ResultSet generatedKey)
			throws SQLException {
		// Nothing to do
	}

}
