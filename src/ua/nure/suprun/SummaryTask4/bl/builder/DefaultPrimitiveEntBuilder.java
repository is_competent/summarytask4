package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.db.util.NamedStatementUtil;

public class DefaultPrimitiveEntBuilder<T> implements EntBuilder<T> {

  private Class<T> clazz;
  private int index;
  private String colName;
  
  public DefaultPrimitiveEntBuilder(Class<T> cl) {
    clazz = cl;
    index = 1;
  }

  public DefaultPrimitiveEntBuilder(Class<T> cl, int colIndex) {
    clazz = cl;
    index = colIndex;
  }

  public DefaultPrimitiveEntBuilder(Class<T> cl, String cName) {
    clazz = cl;
    colName = cName;
  }

  @Override
  public T create(ResultSet rs) throws SQLException {
    Object entity = null;

    if (index != 0) {
      entity = NamedStatementUtil.initializePrimitive(rs, clazz, index);
    } else if (colName != null) {
      entity = NamedStatementUtil.initializePrimitive(rs, clazz, colName);
    } else {
      throw new IllegalArgumentException();
    }

    return clazz.cast(entity);
  }

  @Override
  public T create(HttpServletRequest req) {
    throw new UnsupportedOperationException();
  }

  @Override
  public void setAfterInsert(T ent, ResultSet generatedKey) {
    // Nothing to do
  }

}
