package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.DepartmentSubject;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for DepartmentSubject entity.
 * 
 * @author B.Suprun
 * 
 */
public class DepartmentSubjectBuilder implements EntBuilder<DepartmentSubject> {

	@Override
	public DepartmentSubject create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(DepartmentSubject.class).create(rs);
	}

	@Override
	public DepartmentSubject create(HttpServletRequest req) {
		DepartmentSubject ds = new DepartmentSubject();
		RequestParser rp = RequestParser.getParser(req);

		ds.setDepId(rp.getInt(Params.DS_DEP_ID));
		ds.setSubjId(rp.getInt(Params.DS_SUBJ_ID));

		return ds;
	}

	@Override
	public void setAfterInsert(DepartmentSubject ent, ResultSet generatedKey)
			throws SQLException {
		// Nothing to do
	}

}
