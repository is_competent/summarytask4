package ua.nure.suprun.SummaryTask4.bl.builder;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.Blacklist;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Builder for Blacklist entity.
 * 
 * @author B.Suprun
 * 
 */
public class BlacklistBuilder implements EntBuilder<Blacklist> {

	@Override
	public Blacklist create(ResultSet rs) throws SQLException {
	  return new DefaultEntBuilder<>(Blacklist.class).create(rs);
	}

	@Override
	public void setAfterInsert(Blacklist ent, ResultSet generatedKey)
			throws SQLException {

		// nothing to do
	}

	@Override
	public Blacklist create(HttpServletRequest req) {

		Blacklist bl = new Blacklist();

		RequestParser rp = RequestParser.getParser(req);

		bl.setId(rp.getInt(Params.BL_ID));

		return bl;
	}

}
