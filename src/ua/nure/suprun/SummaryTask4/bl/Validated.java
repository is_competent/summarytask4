package ua.nure.suprun.SummaryTask4.bl;

import javax.xml.bind.ValidationException;

/**
 * Entity-class should implement this interface if there are some difficult data
 * validation cases.
 * 
 * @author B.Suprun
 * 
 */
public interface Validated {

	/**
	 * Validates entity data.
	 * 
	 * @throws ValidationException
	 *             If some of the values do not match.
	 */
	void validate() throws ValidationException;
}
