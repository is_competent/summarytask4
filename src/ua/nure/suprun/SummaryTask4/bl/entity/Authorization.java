package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.constants.Columns;

/**
 * Entity class for AUTH table.
 * 
 * @author B.Suprun
 * 
 */
@DBEntity("AUTH")
public class Authorization implements Serializable {

  private static final long serialVersionUID = 6859940612154121297L;

  @MaxLength(100)
  @DBEntField(value = Columns.AUTH_ID)
  @DBEntPk
  private String id;

  @MoreOrEqual(1)
  @DBEntField(value = Columns.AUTH_USER_ID)
  private int userId;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public int getUserId() {
    return userId;
  }

  public void setUserId(int userId) {
    this.userId = userId;
  }

  @Override
  public String toString() {
    return "Authorization [id=" + id + ", user=" + userId + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((id == null) ? 0 : id.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    Authorization other = (Authorization) obj;
    if (id == null) {
      if (other.id != null) {
        return false;
      }
    } else if (!id.equals(other.id)) {
      return false;
    }
    return true;
  }

}
