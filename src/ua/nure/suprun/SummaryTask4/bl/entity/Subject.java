package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MinLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Patterns;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Entity class for SUBJ table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({ @Request(name = ReqName.SUBJ_GET_SUBJ, 
value = "Select * FROM <:tn> JOIN <:DepartmentSubject as ds> ON {ds.subjId} = {id}"
    + " WHERE {ds.depId} = :id") })
@DBEntity("SUBJ")
public class Subject implements Serializable {

  private static final long serialVersionUID = 4755524187670156434L;

  @MoreOrEqual(0)
  @DBEntField(value = Columns.SUBJ_ID, updatable = false)
  @DBEntPk
  private int id;

  @MinLength(3)
  @MaxLength(50)
  @Pattern(Patterns.NAME)
  @DBEntField(Columns.SUBJ_NAME)
  private String name;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return "Subject [id=" + id + ", name=" + name + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    Subject other = (Subject) obj;
    if (id != other.id) {
      return false;
    }
    return true;
  }

}
