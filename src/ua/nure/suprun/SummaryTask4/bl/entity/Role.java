package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MinLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Patterns;

/**
 * Entity class for ROLE table.
 * 
 * @author B.Suprun
 * 
 */
@DBEntity("ROLE")
public class Role implements Serializable {

	private static final long serialVersionUID = -7049546877195783908L;

	@MoreOrEqual(0)
	@DBEntField(value = Columns.ROLE_ID, updatable = false)
	@DBEntPk
	private int id;

	@MinLength(3)
	@MaxLength(50)
	@Pattern(Patterns.NAME)
	@DBEntField(Columns.ROLE_NAME)
	private String name;

	@MaxLength(255)
	@DBEntField(Columns.ROLE_DESCR)
	private String descr;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", name=" + name + ", descr=" + descr + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		
		if (obj == null) {
			return false;
		}
		
		if (getClass() != obj.getClass()) {
			return false;
		}
		
		Role other = (Role) obj;
		if (id != other.id) {
			return false;
		}
		return true;
	}

}
