package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.constants.Columns;

/**
 * Entity class for BLACKLIST table.
 * 
 * @author B.Suprun
 * 
 */
@DBEntity("BLACKLIST")
public class Blacklist implements Serializable {

  private static final long serialVersionUID = -7763965451617701032L;
  @MoreOrEqual(0)
  @DBEntField(value = Columns.BLACKLIST_ID)
  @DBEntPk
  private int id;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }

    Blacklist other = (Blacklist) obj;
    if (id != other.id) {
      return false;
    }
    return true;
  }
}
