package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;

import javax.xml.bind.ValidationException;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;
import ua.nure.suprun.SummaryTask4.bl.Validated;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Patterns;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Class entity for DEPT table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({
    @Request(name = ReqName.DEP_BY_NAME_ASC, 
    value = "SELECT * FROM <:tn> WHERE {university} = :university ORDER BY {name}"),

    @Request(name = ReqName.DEP_BY_NAME_DESC, 
    value = "SELECT * FROM <:tn> WHERE {university} = :university ORDER BY {name} DESC"),

    @Request(name = ReqName.DEP_BY_BUDGET_ASC, 
    value = "SELECT * FROM <:tn> WHERE {university} = :university ORDER BY {budgetPlaces}"),

    @Request(name = ReqName.DEP_BY_BUDGET_DESC, 
    value = "SELECT * FROM <:tn> WHERE {university} = :university ORDER BY {budgetPlaces} DESC"),
    
    @Request(name = ReqName.DEP_BY_COMM_ASC, 
    value = "SELECT * FROM <:tn> WHERE {university} = :university ORDER BY {commonPlaces}"),
    
    @Request(name = ReqName.DEP_BY_COMM_DESC, 
    value = "SELECT * FROM <:tn> WHERE {university} = :university ORDER BY {commonPlaces} DESC") })
@DBEntity("DEPT")
public class Department implements Serializable, Validated {

  private static final long serialVersionUID = -5545152844652028370L;

  @MoreOrEqual(0)
  @DBEntField(value = Columns.DEPT_ID, updatable = false)
  @DBEntPk
  private int id;

  @MaxLength(50)
  @Pattern(Patterns.UNIVERSITY_NAME)
  @DBEntField(Columns.DEPT_NAME)
  @DBEntPk
  private String name;

  @MoreOrEqual(0)
  @DBEntField(value = Columns.DEPT_COMMON)
  private int commonPlaces;

  @MoreOrEqual(0)
  @DBEntField(value = Columns.DEPT_BUDGET)
  private int budgetPlaces;

  @MoreOrEqual(0)
  @DBEntField(value = Columns.DEPT_UNIVERSITY, updatable = false)
  private int university;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getCommonPlaces() {
    return commonPlaces;
  }

  public void setCommonPlaces(int commonPlaces) {
    this.commonPlaces = commonPlaces;
  }

  public int getBudgetPlaces() {
    return budgetPlaces;
  }

  public void setBudgetPlaces(int budgetPlaces) {
    this.budgetPlaces = budgetPlaces;
  }

  public int getUniversity() {
    return university;
  }

  public void setUniversity(int university) {
    this.university = university;
  }

  @Override
  public String toString() {
    return "Department [id=" + id + ", name=" + name + ", commonPlaces=" + commonPlaces
        + ", budgetPlaces=" + budgetPlaces + ", university=" + university + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    Department other = (Department) obj;
    if (id != other.id) {
      return false;
    }
    
    return true;
  }

  @Override
  public void validate() throws ValidationException {
    if (budgetPlaces > commonPlaces) {
      throw new ValidationException("budgetPlaces > commonPlaces");
    }

  }

}
