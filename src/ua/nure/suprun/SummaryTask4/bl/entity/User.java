package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MinLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Patterns;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Entity class for USER table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({
    @Request(name = ReqName.USER_LANG, value = "UPDATE <:tn> SET {lang} = :lang WHERE {id} = :id"),

    @Request(name = ReqName.USER_LOGIN, value = "SELECT * FROM <:tn> WHERE {email} = :email AND {pass} = :pass "
        + "AND {id} NOT IN (SELECT * FROM <:Blacklist>)"),

    @Request(name = ReqName.USER_BY_LOGIN, value = "SELECT * FROM <:tn> WHERE {email} = :email"),

    @Request(name = ReqName.USER_BY_ID, value = "SELECT * FROM <:tn> WHERE {id} = :id"),

    @Request(name = ReqName.USER_APPLIED_TO_UNIV, value = "SELECT * FROM <:tn> WHERE {id}"
        + " IN (SELECT {ed.enId} FROM <:Department as d>"
        + " JOIN <:EnrolleeDept as ed> ON {d.id} = {ed.depId} WHERE {d.university} ="
        + " :university) ORDER BY {lName}"),

    @Request(name = ReqName.USER_SEARCH, value = "SELECT * "
        + "FROM <:tn> WHERE {role} ="
        + " (Select {r.id} FROM <:Role as r> WHERE {r.name} = 'enrollee') "
        + " AND CONCAT({lName},' ', {name}, ' ', {patr}, ' ', {email}) LIKE :like LIMIT 100"),

    @Request(name = ReqName.USER_GET_ALL_ENROLLEES, value = "SELECT * "
        + "FROM <:tn> WHERE {role} = (Select {r.id} FROM <:Role as r> WHERE"
        + " {r.name} = 'enrollee') LIMIT 100"),

    @Request(name = ReqName.USER_GET_BLOCKED_ENROLLEE, value = "SELECT * FROM <:tn> WHERE {role} = (Select {r.id} FROM <:Role as r> WHERE {r.name}"
        + " = 'enrollee') AND {id} IN (SELECT * FROM <:Blacklist>)"), 
    @Request(name = ReqName.USER_APP_CNT, value = "SELECT COUNT(*) FROM <:EnrolleeDept as ed> WHERE" 
       + " {ed.enId} = :id AND ({ed.depId}, {ed.enId}) NOT IN"
      + " (SELECT repDepName, repEnName FROM REPORT)")
})
@DBEntity("USER")
public class User implements Serializable {

  private static final long serialVersionUID = -8883409299643938269L;

  @MoreOrEqual(0)
  @DBEntField(value = Columns.USER_ID, updatable = false)
  @DBEntPk
  private int id;

  @MinLength(3)
  @Pattern(Patterns.NAME)
  @DBEntField(Columns.USER_NAME)
  private String name;

  @MinLength(3)
  @Pattern(Patterns.NAME)
  @DBEntField(Columns.USER_LAST_NAME)
  private String lName;

  @MinLength(3)
  @Pattern(Patterns.NAME)
  @DBEntField(Columns.USER_PATR)
  private String patr;

  @Pattern(Patterns.EMAIL)
  @DBEntField(Columns.USER_EMAIL)
  private String email;

  @MoreOrEqual(1)
  @DBEntField(Columns.USER_ROLE)
  private int role;

  @Pattern(Patterns.PASS_HEX)
  @DBEntField(Columns.USER_PASS)
  private String pass;

  @MaxLength(3)
  @MinLength(2)
  @Pattern(Patterns.LANG)
  @DBEntField(Columns.USER_LANG)
  private String lang;

  public User() {
  }

  public User(User u) {
    this.id = u.id;
    this.name = u.name;
    this.lName = u.lName;
    this.patr = u.patr;
    this.email = u.email;
    this.role = u.role;
    this.pass = u.pass;
    this.lang = u.lang;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getLName() {
    return lName;
  }

  public void setLName(String lName) {
    this.lName = lName;
  }

  public String getPatr() {
    return patr;
  }

  public void setPatr(String patr) {
    this.patr = patr;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public int getRole() {
    return role;
  }

  public void setRole(int role) {
    this.role = role;
  }

  public String getPass() {
    return pass;
  }

  public void setPass(String pass) {
    this.pass = pass;
  }

  public String getLang() {
    return lang;
  }

  public void setLang(String lang) {
    this.lang = lang;
  }

  @Override
  public String toString() {
    return "User [id=" + id + ", name=" + name + ", lName=" + lName + ", patr=" + patr + ", email="
        + email + ", role=" + role + ", lang=" + lang + "]";
  }

  /**
   * Only by id.
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    User other = (User) obj;
    if (id != other.id) {
      return false;
    }

    return true;
  }

}
