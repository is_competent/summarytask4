package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.LessOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Entity class for ENROLLEE_MARK sub table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({ 
      @Request(name = ReqName.EM_REMOVE, 
      value = "DELETE FROM <:tn> WHERE {enId} = :enId AND {subjId} = :subjId") 
  })
@DBEntity("ENROLLEE_MARK")
public class EnrolleeMark implements Serializable {

  private static final long serialVersionUID = -7566354090818245396L;

  @MoreOrEqual(1)
  @DBEntField(Columns.ENROLLEE_MARK_ENROLLEE)
  @DBEntPk
  private int enId;

  @MoreOrEqual(1)
  @DBEntField(Columns.ENROLLEE_MARK_SUBJ)
  private int subjId;

  private String subjName;

  @MoreOrEqual(1)
  @LessOrEqual(12)
  @DBEntField(Columns.ENROLLEE_MARK_CERT)
  private int certMark;

  @MoreOrEqual(0)
  @LessOrEqual(200)
  @DBEntField(Columns.ENROLLEE_MARK_TEST)
  private int testMark;

  public int getEnId() {
    return enId;
  }

  public void setEnId(int enId) {
    this.enId = enId;
  }

  public int getSubjId() {
    return subjId;
  }

  public void setSubjId(int subjId) {
    this.subjId = subjId;
  }

  public int getCertMark() {
    return certMark;
  }

  public void setCertMark(int certMark) {
    this.certMark = certMark;
  }

  public int getTestMark() {
    return testMark;
  }

  public void setTestMark(int testMark) {
    this.testMark = testMark;
  }

  @Override
  public String toString() {
    return "EnrolleeMark [enId=" + enId + ", subjId=" + subjId + ", certMark=" + certMark
        + ", testMark=" + testMark + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + enId;
    result = prime * result + subjId;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    EnrolleeMark other = (EnrolleeMark) obj;
    if (enId != other.enId) {
      return false;
    }

    if (subjId != other.subjId) {
      return false;
    }
    return true;
  }

  public String getSubjName() {
    return subjName;
  }

  public void setSubjName(String string) {
    this.subjName = string;
  }

}
