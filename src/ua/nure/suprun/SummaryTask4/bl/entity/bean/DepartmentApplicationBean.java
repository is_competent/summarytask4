package ua.nure.suprun.SummaryTask4.bl.entity.bean;

import java.io.Serializable;
import java.sql.Date;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.constants.Columns;

public class DepartmentApplicationBean implements Serializable {

  private static final long serialVersionUID = -9158910717950958451L;
  
  @DBEntField(Columns.DEPT_ID)
  private int id;
  
  @DBEntField(Columns.UNIV_NAME)
  private String universityName;
  
  @DBEntField(Columns.DEPT_NAME)
  private String departmentName;

  @DBEntField(Columns.REPORT_VIEW_BUDGET)
  private String applyStatus;

  @DBEntField(Columns.ENROLLEE_DEP_DATE)
  private Date applyDate;

  public DepartmentApplicationBean() {
    applyStatus = "considered";
  }

  public DepartmentApplicationBean(int id, String universityName, String departmentName,
      String applyStatus) {
    this.id = id;
    this.universityName = universityName;
    this.departmentName = departmentName;
    this.applyStatus = applyStatus;
  }

  public DepartmentApplicationBean(int id, String universityName, String departmentName) {
    this.id = id;
    this.universityName = universityName;
    this.departmentName = departmentName;
    applyStatus = "considered";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }
    DepartmentApplicationBean other = (DepartmentApplicationBean) obj;
    if (id != other.id) {
      return false;
    }
    return true;
  }

  @Override
  public String toString() {
    return "DepartmentApplicationBean [id=" + id + ", universityName=" + universityName
        + ", departmentName=" + departmentName + ", applyStatus=" + applyStatus + "]";
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getUniversityName() {
    return universityName;
  }

  public void setUniversityName(String universityName) {
    this.universityName = universityName;
  }

  public String getDepartmentName() {
    return departmentName;
  }

  public void setDepartmentName(String departmentName) {
    this.departmentName = departmentName;
  }

  public String getApplyStatus() {
    return applyStatus;
  }

  public void setApplyStatus(String applyStatus) {
    this.applyStatus = applyStatus;
  }

  public Date getApplyDate() {
    return new Date(applyDate.getTime());
  }

  public void setApplyDate(Date applyDate) {
    this.applyDate = new Date(applyDate.getTime());
  }

}
