package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;
import java.util.Arrays;

import javax.xml.bind.ValidationException;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;
import ua.nure.suprun.SummaryTask4.bl.Validated;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Patterns;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Entity class for ENROLLEE table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({ @Request(name = ReqName.ENROLLEE_APPLIED, 
value = "SELECT {d.id}, {d.name}, {un.name}, {ed.applyDate},"
    + " IF(`repIsBudget` IS NOT NULL,"
    + " IF(`repIsBudget`, 'BUDGET', 'CONTRACT'),"
    + " 'CONSIDERED') AS `repIsBudget`"
    + " FROM <:EnrolleeDept as ed>"
    + " JOIN <:Department as d> ON {d.id} =  {ed.depId}"
    + " JOIN <:University as un> ON {un.id} = {d.university}"
    + " LEFT JOIN `REPORT` ON `repDepName` = {ed.depId} AND"
    + " `repAssertionDate` = YEAR({ed.applyDate}) AND `repEnName` = {ed.enId}"
    + " WHERE {ed.enId} = :id GROUP BY {d.id}, {un.id}, {ed.applyDate}") })
@DBEntity("ENROLLEE")
public class Enrollee implements Serializable, Validated {

  private static final long serialVersionUID = -1218443642217723996L;

  @MoreOrEqual(1)
  @DBEntField(value = Columns.ENROLLEE_ID, updatable = false)
  @DBEntPk
  private int id;

  @MoreOrEqual(0)
  @DBEntField(Columns.ENROLLEE_AV_MARK)
  private double avMark;

  @DBEntField(value = Columns.ENROLLEE_MARKS_SCAN, updatable = false)
  private transient byte[] marksScan;

  @Pattern(Patterns.SCHOOL_NAME)
  @DBEntField(Columns.ENROLLEE_EDUCATION_NAME)
  private String educName;

  @MaxLength(50)
  @Pattern(Patterns.UNIVERSITY_NAME)
  @DBEntField(Columns.ENROLLEE_DISTRICT)
  private String distrName;

  @MaxLength(50)
  @Pattern(Patterns.UNIVERSITY_NAME)
  @DBEntField(Columns.ENROLLEE_CITY)
  private String city;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public double getAvMark() {
    return avMark;
  }

  public void setAvMark(double avMark) {
    this.avMark = avMark;
  }

  public byte[] getMarksScan() {
    if (marksScan != null) {
      return marksScan.clone();
    } else {
      return null;
    }
  }

  public void setMarksScan(byte[] marksScan) {
    this.marksScan = Arrays.copyOf(marksScan, marksScan.length);
  }

  public String getEducName() {
    return educName;
  }

  public void setEducName(String educName) {
    this.educName = educName;
  }

  public String getDistrName() {
    return distrName;
  }

  public void setDistrName(String distrName) {
    this.distrName = distrName;
  }

  public String getCity() {
    return city;
  }

  public void setCity(String city) {
    this.city = city;
  }

  @Override
  public String toString() {
    return "Enrollee [id=" + id + ", avMark=" + avMark + ", educName=" + educName + ", distrName="
        + distrName + ", city=" + city + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    Enrollee other = (Enrollee) obj;
    if (id != other.id) {
      return false;
    }

    return true;
  }

  @Override
  public void validate() throws ValidationException {
    final int fifeMB = 1024 * 1024 * 5;
    if (marksScan.length > fifeMB) {
      throw new ValidationException("File is too long, it must be less then 5 mb");
    }
  }

}
