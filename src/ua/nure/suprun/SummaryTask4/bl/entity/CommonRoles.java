package ua.nure.suprun.SummaryTask4.bl.entity;

/**
 * Common system roles.
 * 
 * @author B.Suprun
 * 
 */
public enum CommonRoles {
  ROOT("root", 1), ADMIN("admin", 2), ENROLLEE("enrollee", 3), NO_CONTROL("no-control", 4);

  private Role role;
  private static final String DEFAULT_ROOT_EMAIL = "root@root.com";
  private static final String DEFAULT_ADMIN_NAME = "Invited";

  private CommonRoles(String roleName, int roleId) {
    Role r = new Role();
    r.setId(roleId);
    r.setName(roleName);
    role = r;
  }

  public Role getRole() {
    return role;
  }

  /**
   * Specifies the ROOT user is created by default (i.e., With default email).
   * 
   * @param user
   *          - User is being checked
   * @return {@code true} if it is default created user, or {@code false} if it is not
   */
  public static boolean isDefaultRoot(User user) {
    return user.getEmail().equals(DEFAULT_ROOT_EMAIL);
  }

  public static Role getUserRole(User user) {
    if (user == null) {
      return NO_CONTROL.getRole();
    }
    for (CommonRoles role : values()) {
      Role r = role.getRole();
      if (r.getId() == user.getRole()) {
        return r;
      }
    }
    return NO_CONTROL.getRole();
  }

  /**
   * Determines if this user is invited administrator (i.e. With default name, lName and
   * patronymic).
   * 
   * @param user
   * @return {@code true} if this user is invited admin, or {@code false} otherwise.
   */
  public static boolean isInvitedAdmin(User user) {
    if (user == null) {
      return false;
    } else {
      return DEFAULT_ADMIN_NAME.equals(user.getName())
          && DEFAULT_ADMIN_NAME.equals(user.getLName())
          && DEFAULT_ADMIN_NAME.equals(user.getPatr()) && ADMIN.getRole().getId() == user.getRole();
    }
  }
}
