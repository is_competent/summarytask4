package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Class-entity for DEPT_SUBJ sub table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({ @Request(name = ReqName.DS_REMOVE, 
value = "DELETE FROM <:tn> WHERE {depId} = :depId AND {subjId} = :subjId") })
@DBEntity("DEPT_SUBJ")
public class DepartmentSubject implements Serializable {

  private static final long serialVersionUID = -8440841531449581923L;

  @MoreOrEqual(1)
  @DBEntField(value = Columns.DEPT_SUBJ_DEP)
  @DBEntPk
  private int depId;

  @MoreOrEqual(1)
  @DBEntField(Columns.DEPT_SUBJ_SUBJECT)
  @DBEntPk
  private int subjId;

  public int getDepId() {
    return depId;
  }

  public void setDepId(int depId) {
    this.depId = depId;
  }

  public int getSubjId() {
    return subjId;
  }

  public void setSubjId(int subjId) {
    this.subjId = subjId;
  }

  @Override
  public String toString() {
    return "DepartmentSubject [depId=" + depId + ", subjId=" + subjId + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + depId;
    result = prime * result + subjId;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    DepartmentSubject other = (DepartmentSubject) obj;
    if (depId != other.depId) {
      return false;
    }
    
    if (subjId != other.subjId) {
      return false;
    }
    return true;
  }

}
