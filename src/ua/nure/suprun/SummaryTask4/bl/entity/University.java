package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MinLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Patterns;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Entity class for UNIVERSITY table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({ @Request(name = ReqName.UNIVERSITY_GET_BY_ADMIN, value = "SELECT * FROM <:tn> WHERE {admin} = :admin") })
@DBEntity("UNIVERSITY")
public class University implements Serializable {

  private static final long serialVersionUID = 7047507630636703271L;

  @MoreOrEqual(0)
  @DBEntField(value = Columns.UNIV_ID, updatable = false)
  @DBEntPk
  private int id;

  private List<Department> departments = new ArrayList<>();

  @MinLength(3)
  @MaxLength(100)
  @Pattern(Patterns.UNIVERSITY_NAME)
  @DBEntField(Columns.UNIV_NAME)
  private String name;

  @MoreOrEqual(1)
  @DBEntField(Columns.UNIV_ADMIN)
  private int admin;

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getAdmin() {
    return admin;
  }

  public void setAdmin(int admin) {
    this.admin = admin;
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + id;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    University other = (University) obj;
    if (id != other.id) {
      return false;
    }

    return true;
  }

  @Override
  public String toString() {
    return "University [id=" + id + ", name=" + name + ", userAdmin=" + admin + ", departments = "
        + departments + "]";
  }

  public List<Department> getDepartments() {
    return departments;
  }

  public void setDepartments(List<Department> departments) {
    this.departments = departments;
  }

}
