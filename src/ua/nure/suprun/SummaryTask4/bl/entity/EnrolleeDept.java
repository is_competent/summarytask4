package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;
import java.sql.Date;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.LessOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Entity class for ENROLLEE_DEP sub table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({
  @Request(name = ReqName.ENROLLEE_DEP_PRIORITIES, 
      value = "SELECT {priority} FROM <:tn> WHERE {enId} = :id AND YEAR({applyDate}) = YEAR(NOW())")
})
@DBEntity("ENROLLEE_DEP")
public class EnrolleeDept implements Serializable {

  private static final long serialVersionUID = 4314995846452461081L;

  @MoreOrEqual(1)
  @DBEntField(Columns.ENROLLEE_DEP_ENROLLEE)
  @DBEntPk
  private int enId;

  @MoreOrEqual(1)
  @DBEntField(Columns.ENROLLEE_DEP_DEP)
  private int depId;

  @DBEntField(Columns.ENROLLEE_DEP_DATE)
  private Date applyDate = new Date(System.currentTimeMillis());
  
  @DBEntField(Columns.ENROLLEE_DEP_PRIOR)
  @LessOrEqual(Constant.MAX_APPLICATIONS_COUNT)
  @MoreOrEqual(1)
  private int priority;

  public int getEnId() {
    return enId;
  }

  public void setEnId(int enId) {
    this.enId = enId;
  }

  public int getDepId() {
    return depId;
  }

  public void setDepId(int depId) {
    this.depId = depId;
  }

  @Override
  public String toString() {
    return "EnrolleeDept [enId=" + enId + ", depId=" + depId + ", prior = " + priority +"]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + depId;
    result = prime * result + enId;
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }

    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    EnrolleeDept other = (EnrolleeDept) obj;
    if (depId != other.depId) {
      return false;
    }

    if (enId != other.enId) {
      return false;
    }

    return true;
  }

  public Date getApplyDate() {
    return new Date(applyDate.getTime());
  }

  public void setApplyDate(Date apllyDate) {
    this.applyDate = new Date(applyDate.getTime());
  }

  public int getPriority() {
    return priority;
  }

  public void setPriority(int priority) {
    this.priority = priority;
  }
}
