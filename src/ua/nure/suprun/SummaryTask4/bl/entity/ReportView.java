package ua.nure.suprun.SummaryTask4.bl.entity;

import java.io.Serializable;
import java.sql.Date;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Patterns;
import ua.nure.suprun.SummaryTask4.constants.ReqName;

/**
 * Entity class for REPORT_VIEW virtual table.
 * 
 * @author B.Suprun
 * 
 */
@SQLRequests({
    @Request(name = ReqName.RV_CREATE, value = "{CALL buildReport()}"),

    @Request(name = ReqName.RV_SHOW, 
    value = "SELECT * FROM <:tn> WHERE DATE({date}) = :date AND {universityName} = "
        + ":universityName"),

    @Request(name = ReqName.RV_GET_BY_TIMESTAMP, 
    value = "SELECT * FROM <:tn> WHERE {date} = :date"),

    @Request(name = ReqName.RV_DATES, 
    value = "SELECT {date} FROM <:tn> WHERE {universityName} = :universityName GROUP BY "
        + "DATE({date})"),

    @Request(name = ReqName.RV_ENROLLEES_APPLICATIONS, 
    value = "SELECT * FROM <:tn> WHERE {uId} = :uId GROUP BY DATE({date})") })
@DBEntity("REPORT_VIEW")
public class ReportView implements Serializable {

  private static final long serialVersionUID = 9157301747993433663L;

  @MaxLength(8)
  @DBEntField(Columns.REPORT_VIEW_BUDGET)
  private String studyMode;

  @DBEntField(Columns.REPORT_VIEW_DATE)
  private Date date;

  @Pattern(Patterns.NAME)
  @DBEntField(Columns.REPORT_VIEW_PATR)
  private String enrPatronimic;

  @Pattern(Patterns.NAME)
  @DBEntField(Columns.REPORT_VIEW_NAME)
  private String enrName;

  @Pattern(Patterns.NAME)
  @DBEntField(Columns.REPORT_VIEW_LAST_NAME)
  private String enrLastName;

  @Pattern(Patterns.UNIVERSITY_NAME)
  @DBEntField(Columns.REPORT_VIEW_DEP_NAME)
  private String depName;

  @Pattern(Patterns.UNIVERSITY_NAME)
  @DBEntField(Columns.REPORT_VIEW_UNIVERSITY)
  private String universityName;

  @DBEntField(Columns.REPORT_VIEW_USER_ID)
  private int uId;

  @DBEntField(Columns.REPORT_VIEW_USER_EMAIL)
  private String uEmail;

  @DBEntField(Columns.REPORT_VIEW_USER_MARK)
  private double avgMark;

  @DBEntField(Columns.REPORT_VIEW_ASERTION_DATE)
  private Date assertionDate;

  public String getStudyMode() {
    return studyMode;
  }

  public void setStudyMode(String studyMode) {
    this.studyMode = studyMode;
  }

  public Date getDate() {
    if (date != null) {
      return new Date(date.getTime());
    } else {
      return null;
    }
  }
  
  public void setDate(Date date) {
    this.date = new Date(date.getTime());
  }

  public String getEnrPatronimic() {
    return enrPatronimic;
  }

  public void setEnrPatronimic(String enrPatronimic) {
    this.enrPatronimic = enrPatronimic;
  }

  public String getEnrName() {
    return enrName;
  }

  public void setEnrName(String enrName) {
    this.enrName = enrName;
  }

  public String getEnrLastName() {
    return enrLastName;
  }

  public void setEnrLastName(String enrLastName) {
    this.enrLastName = enrLastName;
  }

  public String getDepName() {
    return depName;
  }

  public void setDepName(String depName) {
    this.depName = depName;
  }

  public String getUniversityName() {
    return universityName;
  }

  public void setUniversityName(String universityName) {
    this.universityName = universityName;
  }

  @Override
  public String toString() {
    return "ReportView [studyMode=" + studyMode + ", date=" + date + ", enrPatronimic="
        + enrPatronimic + ", enrName=" + enrName + ", enrLastName=" + enrLastName + ", depName="
        + depName + ", universityName=" + universityName + "]";
  }

  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((date == null) ? 0 : date.hashCode());
    result = prime * result + ((depName == null) ? 0 : depName.hashCode());
    result = prime * result + ((enrLastName == null) ? 0 : enrLastName.hashCode());
    result = prime * result + ((enrName == null) ? 0 : enrName.hashCode());
    result = prime * result + ((enrPatronimic == null) ? 0 : enrPatronimic.hashCode());
    result = prime * result + ((studyMode == null) ? 0 : studyMode.hashCode());
    result = prime * result + ((universityName == null) ? 0 : universityName.hashCode());
    return result;
  }

  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }

    if (getClass() != obj.getClass()) {
      return false;
    }

    ReportView other = (ReportView) obj;
    if (date == null) {
      if (other.date != null) {
        return false;
      }
    } else if (!date.equals(other.date)) {
      return false;
    }

    if (depName == null) {
      if (other.depName != null) {
        return false;
      }
    } else if (!depName.equals(other.depName)) {
      return false;
    }

    if (enrLastName == null) {
      if (other.enrLastName != null) {
        return false;
      }
    } else if (!enrLastName.equals(other.enrLastName)) {
      return false;
    }

    if (enrName == null) {
      if (other.enrName != null) {
        return false;
      }
    } else if (!enrName.equals(other.enrName)) {
      return false;
    }

    if (enrPatronimic == null) {
      if (other.enrPatronimic != null) {
        return false;
      }
    } else if (!enrPatronimic.equals(other.enrPatronimic)) {
      return false;
    }

    if (studyMode == null) {
      if (other.studyMode != null) {
        return false;
      }
    } else if (!studyMode.equals(other.studyMode)) {
      return false;
    }

    if (universityName == null) {
      if (other.universityName != null) {
        return false;
      }
    } else if (!universityName.equals(other.universityName)) {
      return false;
    }

    return true;
  }

  public int getUId() {
    return uId;
  }

  public void setUId(int uId) {
    this.uId = uId;
  }

  public String getUEmail() {
    return uEmail;
  }

  public void setUEmail(String uEmail) {
    this.uEmail = uEmail;
  }

  public double getAvgMark() {
    return avgMark;
  }

  public void setAvgMark(double avgMark) {
    this.avgMark = avgMark;
  }

  public Date getAssertionDate() {
    if (assertionDate != null) {
      return new Date(assertionDate.getTime());
    } else {
      return null;
    }
  }

  public void setAssertionDate(Date assertionDate) {
    this.assertionDate = new Date(assertionDate.getTime());
  }

}
