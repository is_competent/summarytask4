package ua.nure.suprun.SummaryTask4.db.dao;

import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.MySQLDAOFactory;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Provides creating DAOFactory for different DBMS providers.<br>
 * Supported DBMS is:<br>
 * 1. MySQL.
 * 
 * @author B.Suprun
 * 
 */
public final class AbstractDAOFactory {
	/**
	 * Supported DBMS listed here.
	 */
	public static final int MYSQL = 1;

	private AbstractDAOFactory() { }

	/**
	 * Main factory method for creating DAOFactory instances.
	 * 
	 * @param type
	 *            - Type of DBMS SEE: {@link AbstractDAOFactory Supported DBMS}
	 * @return DAOFactory implementor.
	 * @throws DBException
	 *             - If an error with creating DAOFActory instance has occurred.
	 * @throws IllegalArgumentException
	 *             if there is no DBMS with such <b>type</b>
	 */
	public static DAOFactory getDAO(int type) throws DBException {

		switch (type) {
		case MYSQL:
			return MySQLDAOFactory.newInstance();
		default:
			throw new IllegalArgumentException("Undefined type");
		}
	}
}
