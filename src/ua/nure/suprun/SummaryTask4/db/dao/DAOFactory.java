package ua.nure.suprun.SummaryTask4.db.dao;

import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.ReportDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.RoleDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.SubjectDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UniversityDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;

/**
 * Provides creating instances for manipulating with different domains.
 * 
 * @author B.Suprun
 * 
 */
public interface DAOFactory {

	DepartmentDAO getDepartmentDAO();

	EnrolleeDAO getEnrolleeDAO();

	ReportDAO getReportDAO();

	RoleDAO getRoleDAO();

	SubjectDAO getSubjectDAO();

	UniversityDAO getUniversityDAO();

	UserDAO getUserDAO();
}
