package ua.nure.suprun.SummaryTask4.db.dao.domains;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import ua.nure.suprun.SummaryTask4.bl.entity.ReportView;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * DAO interface for basics report manipulation.
 * 
 * @author B.Suprun
 * 
 */
public interface ReportDAO {

  /**
   * Makes new report for all universities.
   *
   * @return java.sql.Timestamp of report creation
   * @throws DMLException
   *           if an has been occurred
   */
  Timestamp createReport() throws DMLException;

  /**
   * Extracts report by date for specified university.
   * 
   * @param date
   *          - Interested date.
   * @param univ
   *          - Specified {@link University}
   * @return
   * @throws DMLException
   */
  List<ReportView> showReport(Date date, University univ) throws DMLException;

  /**
   * Extracts all available report dates for specified university.
   * 
   * @param univ
   * @return
   * @throws DMLException
   */
  List<Date> getDates(University univ) throws DMLException;

  /**
   * Returns list of inserted in report enrollees by timestamp.
   * 
   * @param time
   *          - Unix timestamp
   * @return List of reportView records using timestamp.
   * @throws DMLException
   *           if there is some error.
   */
  List<ReportView> getInsertedEnrolees(Timestamp time) throws DMLException;

  /**
   * Returns list of departments where specified user is enrolleed.
   * 
   * @param uId
   *          - User id.
   * @return
   * @throws DMLException
   *           - If an error has occurred.
   */
  List<ReportView> getEnrolleeApplicationsStatus(int uId) throws DMLException;

}
