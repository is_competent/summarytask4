package ua.nure.suprun.SummaryTask4.db.dao.domains;

import java.util.List;

import ua.nure.suprun.SummaryTask4.bl.entity.Role;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * DAO interface for ROLE table and its sub tables.
 * 
 * @author B.Suprun
 * 
 */
public interface RoleDAO {

	/**
	 * Extracts all Roles.
	 * 
	 * @return
	 * @throws DMLException
	 */
	List<Role> getAll() throws DMLException;
}
