package ua.nure.suprun.SummaryTask4.db.dao.domains;

import java.util.List;

import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.DepartmentSubject;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * DAO interface for Department table and its sub tables.
 * 
 * @author B.Suprun
 * 
 */
public interface DepartmentDAO {

	/**
	 * Returns all available departments sorted by name.
	 * 
	 * @param un
	 *            - {@link University} instance.
	 * @param asc
	 *            - If is true, record will be sorted in ascend order, otherwise
	 *            in descend.
	 * @return
	 * @throws DMLException
	 */
	List<Department> sortByName(University un, boolean asc) throws DMLException;

	/**
	 * Returns all available departments sorted by count of budget places.
	 * 
	 * @param un
	 *            - {@link University} instance.
	 * @param asc
	 *            - If is true, record will be sorted in ascend order, otherwise
	 *            in descend.
	 * @return
	 * @throws DMLException
	 */
	List<Department> sortByBudget(University un, boolean asc)
			throws DMLException;

	/**
	 * Returns all available departments sorted by count of common available
	 * places.
	 * 
	 * @param un
	 *            - {@link University} instance.
	 * @param asc
	 *            - If is true, record will be sorted in ascend order, otherwise
	 *            in descend.
	 * @return
	 * @throws DMLException
	 */
	List<Department> sortByCommon(University un, boolean asc)
			throws DMLException;

	/**
	 * Creates new department.
	 * 
	 * @param dep
	 * @return
	 * @throws DMLException
	 */
	boolean createNew(Department dep) throws DMLException;

	/**
	 * Appends new subject to department.
	 * 
	 * @param subj
	 * @param depId
	 * @return
	 * @throws DMLException
	 */
	boolean addSubject(DepartmentSubject subj) throws DMLException;

	/**
	 * Removes a subject from department.
	 * 
	 * @param subj
	 * @param depId
	 * @return
	 * @throws DMLException
	 */
	boolean removeSubject(DepartmentSubject subj) throws DMLException;

	/**
	 * Updates specified department.
	 * 
	 * @param dep
	 * @return
	 * @throws DMLException
	 */
	boolean update(Department dep) throws DMLException;

	/**
	 * Removes specified department.
	 * 
	 * @param dep
	 * @return
	 * @throws DMLException
	 */
	boolean remove(Department dep) throws DMLException;

	/**
	 * Returns department by id.
	 * 
	 * @param id
	 *            - Required Id.
	 * @return
	 * @throws DBException 
	 */
	Department get(int id) throws DBException;
}
