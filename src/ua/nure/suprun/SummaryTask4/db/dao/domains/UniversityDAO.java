package ua.nure.suprun.SummaryTask4.db.dao.domains;

import java.util.List;

import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * DAO interface for University table.
 * 
 * @author B.Suprun
 * 
 */
public interface UniversityDAO {

  /**
   * Extracts all available Universities.
   * 
   * @return
   * @throws DMLException
   */
  List<University> getAll() throws DMLException;

  /**
   * Returns an {@link University} instance for which specified user is admin.
   * 
   * @param admin
   *          - ID of user.
   * @return {@link University} instance
   * @throws DMLException
   *           If an error has occurred.
   */
  University getByAdmin(int admin) throws DMLException;

  /**
   * Returns university by id.
   * 
   * @param id
   *          - university id
   * @return {@link University}
   * @throws DMLException
   *           if there is some problem.
   */
  University get(int id) throws DMLException;
}
