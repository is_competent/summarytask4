package ua.nure.suprun.SummaryTask4.db.dao.domains;

import java.util.List;

import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeDept;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeMark;
import ua.nure.suprun.SummaryTask4.bl.entity.bean.DepartmentApplicationBean;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * DAO interface for ENROLLEE table and its sub tables.
 * 
 * @author B.Suprun
 * 
 */
public interface EnrolleeDAO {

	/**
	 * Get an enrollee by id
	 * 
	 * @param userId
	 * @return
	 * @throws DMLException
	 */
	Enrollee get(int userId) throws DMLException;

	/**
	 * Appends new mark to enrollee.
	 * 
	 * @param mark
	 * @return
	 * @throws DMLException
	 */
	boolean addMark(EnrolleeMark mark) throws DMLException;

	/**
	 * Removes a mark from enrollee.
	 * 
	 * @param mark
	 * @return
	 * @throws DMLException
	 */
	boolean removeMark(EnrolleeMark mark) throws DMLException;

	/**
	 * Extracts all enrollee's mark.
	 * 
	 * @param enr
	 * @return
	 * @throws DMLException
	 */
	List<EnrolleeMark> getMarks(Enrollee enr) throws DMLException;

	/**
	 * Apply to department.
	 * 
	 * @param ed
	 * @return
	 * @throws DMLException
	 */
	boolean applyDep(EnrolleeDept ed) throws DMLException;

	/**
	 * Returns user's applies.
	 * 
	 * @param user
	 *            - User for which applies are searched.
	 * @return
	 * @throws DMLException
	 *             if there is some problem.
	 */
	List<DepartmentApplicationBean> getApplications(int user) throws DMLException;

  List<Integer> getPriorities(int id) throws DMLException;
}
