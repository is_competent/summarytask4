package ua.nure.suprun.SummaryTask4.db.dao.domains;

import java.util.List;

import ua.nure.suprun.SummaryTask4.bl.entity.Authorization;
import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * DAO interface for User table and its sub tables.
 * 
 * @author B.Suprun
 * 
 */
public interface UserDAO {

  /**
   * Checks for existing User with such login/pass and user is not blocked.
   * 
   * @param login
   * @param pass
   * @return {@link User} instance if there is such user, or {@code null} if there is no such user
   * @throws DMLException
   */
  User login(String login, String pass) throws DMLException;

  /**
   * Extracts User by login.
   * 
   * @param login
   * @return {@link User} instance if there is such user, or {@code null} if there is no such user.
   * @throws DMLException
   */
  User get(String login) throws DMLException;

  /**
   * Extracts user by id.
   * 
   * @param id
   * @return {@link User} instance if there is such user, or {@code null} if there is no such user.
   * @throws DMLException
   */
  User get(int id) throws DMLException;

  /**
   * Extracts user with related authorization info.
   * 
   * @param aut
   * @return {@link User} instance if there is such user, or {@code null} if there is no such user.
   * @throws DMLException
   */
  User get(Authorization aut) throws DMLException;

  /**
   * Appends the user to the BLACKLIST.
   * 
   * @param toBeBlocked
   * @return {@code true} if user successfully was blocked, otherwise {@code false}.
   * @throws DMLException
   */
  boolean block(int toBeBlocked) throws DMLException;

  /**
   * Removes the user from the BLACKLIST.
   * 
   * @param toBeUnblocked
   * @return {@code true} if user successfully was unblocked, otherwise {@code false}.
   * @throws DMLException
   */
  boolean unblock(int toBeUnblocked) throws DMLException;

  /**
   * Determines if user are blocked or not.
   * 
   * @param user
   *          - Checking user
   * @return {@code true} if user are blocked, or {@code false} if are not.
   * @throws DMLException
   *           If an error has occured.
   */
  boolean isBlocked(int user) throws DMLException;

  /**
   * Restore user password. Writes authorization code to AUTH table.
   * 
   * @param user
   * @return
   * @throws DMLException
   */
  boolean restore(Authorization aut) throws DMLException;

  /**
   * Creates new enrollee-user.
   * 
   * @param user
   * @return
   * @throws DMLException
   */
  boolean registryEnrolle(User user, Enrollee enr) throws DMLException;

  /**
   * Creates new user and writes authorization information.
   * 
   * @param user
   *          - Default invited user.
   * @param auth
   *          - Authorization code.
   * @return {@code true} if all was written correct, or {@code false} otherwise.
   * @throws DMLException
   */
  boolean inviteAdmin(User user, Authorization auth) throws DMLException;

  /**
   * Updates user's data and creates new university.
   * 
   * @param user
   *          - {@link User} instance.
   * @param un
   *          - New university for which the user will be administrator.
   * @return {@code true} if all was written correct, or {@code false} otherwise.
   * @throws DMLException
   */
  boolean registryAdmin(User user, University un) throws DMLException;

  /**
   * Changes user's language.
   * 
   * @param user
   * @return {@code true} if language was changed successfully, or {@code false} otherwise.
   * @throws DMLException
   */
  boolean changeLang(User user) throws DMLException;

  /**
   * Updates specified user.
   * 
   * @param user
   *          - User to be updated.
   * @return {@code true} if user was updated successfully, or {@code false} otherwise
   * @throws DMLException
   *           If some problems are found.
   */
  boolean update(User user) throws DMLException;

  /**
   * Search users by name, last name, patronimyc and email.
   * 
   * @param cond
   *          Search condition.
   * @return List of found users.
   * @throws DMLException
   *           if there is error.
   */
  List<User> search(String cond) throws DMLException;

  /**
   * Retrieves all enrollees
   * 
   * @return List of found users.
   * @throws DMLException
   *           if there is error.
   */
  List<User> getAllEnrolees() throws DMLException;

  /**
   * Return blocked enrollees.
   * 
   * @return List of user.
   * @throws DMLException if an error has occurred.
   */
  List<User> getBlockedEnrollees() throws DMLException;

  int getAppCnt(int userId) throws DMLException;
}
