package ua.nure.suprun.SummaryTask4.db.dao.domains;

import java.util.List;

import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * DAO interface for Subject table.
 * 
 * @author B.Suprun
 * 
 */
public interface SubjectDAO {

	/**
	 * Extracts all available Subjects.
	 * 
	 * @return
	 * @throws DMLException
	 */
	List<Subject> getAll() throws DMLException;

	/**
	 * Extracts all subjects required for specified department.
	 * 
	 * @param d
	 *            - Specified department.
	 * @return List of subjects.
	 * @throws DMLException
	 *             If there is some error.
	 */
	List<Subject> getDepartmentSubjects(int d) throws DMLException;

	/**
	 * Removes specified Subject.
	 * 
	 * @param s
	 * @return
	 * @throws DMLException
	 */
	boolean removeSubject(Subject s) throws DMLException;

	/**
	 * Makes new subject.
	 * 
	 * @param s
	 * @return
	 * @throws DMLException
	 */
	boolean insert(Subject s) throws DMLException;
}
