package ua.nure.suprun.SummaryTask4.db.builder;

import java.sql.SQLException;

import javax.xml.bind.ValidationException;

import ua.nure.suprun.SummaryTask4.bl.Validated;
import ua.nure.suprun.SummaryTask4.db.NamedParameterStatement;

/**
 * Provides methods for generating and parameters filling commonly used SQL
 * REQUEST such as:
 * <ul>
 * <li>UPDATE by primary key</li>
 * <li>DELETE by primary key</li>
 * <li>SELECT by primary key</li>
 * <li>SELECT ALL</li>
 * <li>INSERT</li>
 * <li>provides requests storing</li>
 * </ul>
 * 
 * @author B.Suprun
 * 
 * @param <T>
 */
public interface RequestBuilder {

	/**
	 * Common SQL REQUEST templates.
	 */
	String SQL_INSERT_TEMPLATE = "INSERT INTO `%s` (%s) VALUES (%s)";

	String SQL_DELETE_TEMPLATE = "DELETE FROM `%s` WHERE %s";

	String SQL_SELECT_BY_PK_TEMPLATE = "SELECT * FROM %s WHERE %s";

	String SQL_SELECT_TEMPLATE = "SELECT * FROM %s";

	String SQL_UPDATE_TEMPLATE = "UPDATE `%s` SET %s WHERE %s";

	/**
	 * Returns stored request associated with <b>reqName</b>. The stored request
	 * may have different placeholders such as: <br>
	 * 1. Table name <b><:tn></b> - the method must replace this placeholder on
	 * the actual table's name. <br>
	 * 2. Column name <b><?classFieldName></b> - the method must replace this
	 * placeholder on the actual table column's name associated with
	 * <b>classFieldName</b> field.
	 * 
	 * @param ent
	 *            - Class for which requests are stored
	 * @param reqName
	 *            - Name of required request
	 * @return SQL request associated with this name, or {@code null} if there
	 *         is no request with that name
	 */
	String getStoredRequest(Class<?> ent, String reqName);

	/**
	 * Returns all available stored requests for that Class. A stored request
	 * may have different placeholders such as: <br>
	 * 1. Table name <b><:tn></b> - the method must replace this placeholder on
	 * the actual table's name. <br>
	 * 2. Column name <b><?classFieldName></b> - the method must replace this
	 * placeholder on the actual table column's name associated with
	 * <b>classFieldName</b> field.
	 * 
	 * @param ent
	 *            - Interested class.
	 * @return Array of all available requests or an empty array if there are no
	 *         stored requests
	 */
	String[] getStoredRequests(Class<?> ent);

	/**
	 * Creates SQL INSERT request using <b>ent</b> instance.
	 * 
	 * @param ent
	 *            - An instance for which request will be generated.
	 * @return SQL INSERT request with named parameters.
	 */
	<T> String createInsertRequest(T ent);

	/**
	 * Creates SQL SELECT request using <b>ent</b> class.
	 * 
	 * @param ent
	 *            - Class instance for entity-class.
	 * @return SQL SELECT request for extracting all items from db.
	 */
	String createSelectAllRequest(Class<?> ent);

	/**
	 * Fills parameters required for SQL INSERT request using <b>ent</b>
	 * instance
	 * 
	 * @param ps
	 *            - NamedParameterStatement instance.
	 * @param ent
	 *            - Class-entity instance.
	 * @throws SQLException
	 *             if an error has occurred with filling
	 */
	<T> void fillInsertRequest(NamedParameterStatement ps, T ent)
			throws SQLException;

	/**
	 * Creates a SQL SELECT request by primary key.
	 * 
	 * @param ent
	 *            - An instance of entity-class.
	 * @return created request with named parameters.
	 */
	<T> String createSelectByPKRequest(T ent);

	/**
	 * Creates a SQL DELETE request by primary key.
	 * 
	 * @param ent
	 *            - An instance of entity-class.
	 * @return created request with named parameters.
	 */
	<T> String createDeleteByPKRequest(T ent);

	/**
	 * Fills a SQL REQUEST where parameter is primary key.
	 * 
	 * @param ps
	 *            - {@link NamedParameterStatement} instance.
	 * @param ent
	 *            - An entity-class instance.
	 * @throws SQLException
	 *             - if something goes wrong.
	 */
	<T> void fillWithPKRequest(NamedParameterStatement ps, T ent)
			throws SQLException;

	/**
	 * Creates a SQL UPDATE request by primary key for all fields in the
	 * <b>ent</b> instance which is not marked as updatable = false.
	 * 
	 * @param ent
	 *            - An entity-class.
	 * @return SQL UPDATE request with named parameters.
	 */
	<T> String createUpdateRequest(T ent);

	/**
	 * Fills a SQL UPDATE request by primary key.
	 * 
	 * @param ps
	 *            - {@link NamedParameterStatement} instance.
	 * @param ent
	 *            - An entity-class instance.
	 * @throws SQLException
	 *             - if something goes wrong.
	 */
	<T> void fillUpdateRequest(NamedParameterStatement ps, T ent)
			throws SQLException;

	/**
	 * Validates all fields annotated with <b>annotation.validation</b>
	 * annotations and invoke {@link Validated#validate()} if <b>ent</b>
	 * implements this interface.
	 * 
	 * @param ent
	 *            - An entity-class.
	 * @throws ValidationException
	 *             if there is a problem with data format.
	 */
	<T> void validate(T ent) throws ValidationException;
}
