package ua.nure.suprun.SummaryTask4.db.builder;

import java.sql.SQLException;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.ValidationException;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.util.AnnotationOperations;
import ua.nure.suprun.SummaryTask4.anotation.util.InsertFilter;
import ua.nure.suprun.SummaryTask4.anotation.util.UpdateFilter;
import ua.nure.suprun.SummaryTask4.anotation.validation.validatorsimp.Validator;
import ua.nure.suprun.SummaryTask4.bl.Validated;
import ua.nure.suprun.SummaryTask4.db.NamedParameterStatement;
import ua.nure.suprun.SummaryTask4.db.util.NamedStatementUtil;

/**
 * The class provides default implementation of RequestBuilder interface.
 * 
 * @author B.Suprun
 * 
 * @param <T>
 *          - An entity class annotated by Database annotations.
 */
public class DefaultRequestBuilder implements RequestBuilder {
  
  private static final Logger LOG = Logger.getLogger(DefaultRequestBuilder.class);

  /**
   * Table name placeholder in SQL request.
   */
  private static final String ENT_PLACEHOLDER = "(?i)<:([\\w0-9]+)(?:\\sas\\s)?([\\w0-9]+)?>";

  private static final String TEMPLATE = "(?i)<:%s>";

  /**
   * Pattern for replacing class's field name on the actual table's column name.
   */
  private static final String COLUMN_NAME_PLACEHOLDER = "(?i)\\{([\\w0-9]+(?:\\.))?([\\w0-9]+)\\}";

  private static final String COLUMN_TEMPLATE = "(?i)\\{%s\\}";

  // util methods, help for generating requests

  /**
   * Method generates table columns sequence which will be placed to TABLE_NAME() in SQL INSERT
   * request
   * 
   * @param fields
   *          - Name of columns
   * @return Columns sequence formated as `first_column`, `second_column`...
   */
  private String generateInsertFieldsSeq(Collection<String> fields) {
    StringBuilder result = new StringBuilder();

    for (String field : fields) {
      result.append('`').append(field).append('`').append(',').append(' ');
    }
    return result.substring(0, result.length() - 2);
  }

  /**
   * Method generates named parameters sequence which will be placed to VALUES() in SQL INSERT
   * request
   * 
   * @param fields
   *          - Named parameters
   * @return Named parameters sequence formated as :firstParam, :secondParam...
   */
  private String generateInsertParameters(Set<String> fields) {
    StringBuilder result = new StringBuilder();

    for (String field : fields) {
      result.append(':').append(field).append(',').append(' ');
    }
    return result.substring(0, result.length() - 2);
  }

  /**
   * Method generates SQL SELECT request by primary key. If there are fields annotated with
   * ForeignKey, it appends JOIN requests into result String
   * 
   * <p>
   * <b>NOTE:</b> For proper work, annotated field must have type that is annotated by
   * {@link DBEntity}
   * 
   * @param ent
   *          - An instance annotated with db.annotation
   * @return - SQL SELECT request by primary key.
   */
  private <T> String getJoinedRequest(T ent) {
    final String entName = AnnotationOperations.getEntityName(ent.getClass());
    final String pkEntName = AnnotationOperations.getPKFieldEntityName(ent.getClass());
    final String pkName = AnnotationOperations.getPKFieldName(ent.getClass());
    
    return String.format(SQL_SELECT_BY_PK_TEMPLATE, "`" + entName + "`", "`" + entName
        + "`.`" + pkEntName + "` = :" + pkName);
  }

  /**
   * Generates update sequence.
   * 
   * @param fields
   *          - Map of class field name - table column name
   * @return Sequence such as `column1` = :field1, `column2` = :field2...
   */
  private String generateUpdateFieldsSeq(Map<String, String> fields) {
    StringBuilder result = new StringBuilder();

    for (Map.Entry<String, String> entries : fields.entrySet()) {
      String entFieldName = entries.getValue();
      String fieldName = entries.getKey();

      result.append('`').append(entFieldName).append('`').append(" = :").append(fieldName)
          .append(", ");
    }
    return result.substring(0, result.length() - 2);
  }

  // interface implementation

  /**
   * Generates SQL INSERT request using the entity annotated with <b>db.annotation</b>s.
   * <p>
   * <b>NOTE:</b> Fields with {@link DBEntField} where insertable flag is false does not used
   */
  @Override
  public <T> String createInsertRequest(T ent) {
    final String entity = AnnotationOperations.getEntityName(ent.getClass());
    Map<String, String> fields = AnnotationOperations.getFieldNames(ent.getClass(),
        DBEntField.class, new InsertFilter());

    final String fieldSeq = generateInsertFieldsSeq(fields.values());
    final String insertParameters = generateInsertParameters(fields.keySet());

    return String.format(SQL_INSERT_TEMPLATE, entity, fieldSeq, insertParameters);
  }

  /**
   * Generates SQL SELECT request by primary key using the entity annotated with
   * <b>db.annotation</b>s.
   */
  @Override
  public <T> String createSelectByPKRequest(T ent) {
    return getJoinedRequest(ent);
  }

  /**
   * Generates SQL DELETE by primary key request using the entity annotated with
   * <b>db.annotation</b>s.
   */
  @Override
  public <T> String createDeleteByPKRequest(T ent) {
    final String entity = AnnotationOperations.getEntityName(ent.getClass());
    final String pkEntName = AnnotationOperations.getPKFieldEntityName(ent.getClass());
    final String pkName = AnnotationOperations.getPKFieldName(ent.getClass());

    return String.format(SQL_DELETE_TEMPLATE, entity, "`" + pkEntName + "`=:" + pkName);
  }

  /**
   * Generates SQL UPDATE by primary key request using the entity annotated with
   * <b>db.annotation</b>s.
   * <p>
   * <b>NOTE:</b> Fields with {@link DBEntField} where updatable flag is false does not used
   */
  @Override
  public <T> String createUpdateRequest(T ent) {
    final String entity = AnnotationOperations.getEntityName(ent.getClass());
    Map<String, String> fields = AnnotationOperations.getFieldNames(ent.getClass(),
        DBEntField.class, new UpdateFilter());
    final String fieldSeq = generateUpdateFieldsSeq(fields);

    final String pkEntName = AnnotationOperations.getPKFieldEntityName(ent.getClass());
    final String pkName = AnnotationOperations.getPKFieldName(ent.getClass());

    return String.format(SQL_UPDATE_TEMPLATE, entity, fieldSeq, "`" + pkEntName + "` = :" + pkName);
  }

  /**
   * Generates SQL SELECT request using the entity annotated with <b>db.annotation</b>s.
   */
  @Override
  public String createSelectAllRequest(Class<?> ent) {
    final String entName = AnnotationOperations.getEntityName(ent);
    StringBuilder join = new StringBuilder();

    return String.format(SQL_SELECT_TEMPLATE, "`" + entName + "`" + join);
  }

  /**
   * Tries to fill required for INSERT request parameters.
   * <p>
   * <b>SEE:</b> {@link NamedStatementUtil#fill(NamedParameterStatement, Object)}
   */
  @Override
  public <T> void fillInsertRequest(NamedParameterStatement ps, T ent) throws SQLException {
    NamedStatementUtil.fill(ps, ent);
  }

  /**
   * Tries to fill required parameters.
   * <p>
   * <b>SEE:</b> {@link NamedStatementUtil#fill(NamedParameterStatement, Object)}
   */
  @Override
  public <T> void fillWithPKRequest(NamedParameterStatement ps, T ent) throws SQLException {
    NamedStatementUtil.fill(ps, ent);
  }

  /**
   * Tries to fill required for UPDATE request parameters.
   * <p>
   * <b>SEE:</b> {@link NamedStatementUtil#fill(NamedParameterStatement, Object)}
   */
  @Override
  public <T> void fillUpdateRequest(NamedParameterStatement ps, T ent) throws SQLException {
    NamedStatementUtil.fill(ps, ent);
  }

  /**
   * Validates instance fields annotated with db.annotation.validation and if the class implements
   * {@link Validated} does also.
   * <p>
   * <b>SEE:</b> {@link Validator}, {@link Validated}
   */
  @Override
  public <T> void validate(T ent) throws ValidationException {
    Validator dv = new Validator();

    dv.validate(ent);

    if (ent instanceof Validated) {
      ((Validated) ent).validate();
    }
  }

  /**
   * Extracts stored request by name from the class.
   */
  @Override
  public String getStoredRequest(Class<?> ent, String reqName) {
    SQLRequests req = AnnotationOperations.getStoredRequests(ent);

    for (Request r : req.value()) {
      if (r.name().equals(reqName)) {
        return replacePlaceholders(ent, r.value());
      }
    }
    return null;
  }

  /**
   * Extracts all available stored requests.
   */
  @Override
  public String[] getStoredRequests(Class<?> ent) {
    SQLRequests req = AnnotationOperations.getStoredRequests(ent);
    String[] requests = new String[req.value().length];

    int i = 0;

    for (Request r : req.value()) {
      requests[i++] = replacePlaceholders(ent, r.value());
    }
    return requests;
  }

  /**
   * Replaces all available placeholders.
   * 
   * @param thClass
   *          - Class which store the request.
   * @param req
   *          - Stored request
   * @return - Request
   */
  private String replacePlaceholders(Class<?> thClass, String req) {
    String reqOut = req;
    try {
      Map<String, String> dictionary = createDictionary(reqOut);
      LOG.trace(dictionary);
      
      reqOut = replaceEntities(reqOut, thClass);
      reqOut = replacesFields(reqOut, dictionary, thClass);
    } catch (Exception ex) {
      throw new IllegalArgumentException(ex);
    }
    return reqOut;
  }

  /**
   * Replaces field placeholder.
   * 
   * @param sql
   *          - Request
   * @param dict
   *          - Dictionary with class names synonyms
   * @param cl
   *          - - Class which store the request.
   * @return
   * @throws ClassNotFoundException
   *           if there is such class.
   */
  private String replacesFields(String sql, Map<String, String> dict, Class<?> cl)
      throws ClassNotFoundException {
    String sqlOut = sql;

    Pattern p = Pattern.compile(COLUMN_NAME_PLACEHOLDER);
    Matcher matcher = p.matcher(sqlOut);
    String packageName = AnnotationOperations.getEntityPackage();

    while (matcher.find()) {
      String columnName;
      String synonym = matcher.group(1);

      if (synonym != null) {
        // Remove dot at the end
        synonym = synonym.substring(0, synonym.length() - 1);
      }
      
      String fieldName = matcher.group(2);
      String className = dict.get(synonym);
      columnName = getColumnName(packageName, className, cl, fieldName);

      if (columnName == null) {
        throw new IllegalArgumentException("There is no such field " + fieldName + " for request "
            + sqlOut);
      }
      String fieldNameInRequest = String.format(COLUMN_TEMPLATE, fieldName);

      if (synonym != null) {
        fieldNameInRequest = String.format(COLUMN_TEMPLATE, synonym + "." + fieldName);
      }

      sqlOut = sqlOut.replaceAll(fieldNameInRequest, "`" + columnName + "`");
    }

    return sqlOut;

  }

  /**
   * Returns name of column for specified class.
   * 
   * @param packName
   *          - Package with entities.
   * @param clName
   *          - Name of class in which field is exist.
   * @param thisCl
   *          - Class which store the request.
   * @param field
   *          - name of field in specified class.
   * @return
   * @throws ClassNotFoundException
   *           if there is no such class
   */
  private String getColumnName(String packName, String clName, Class<?> thisCl, String field)
      throws ClassNotFoundException {
    Map<String, String> fields = null;
    LOG.trace(packName + " " + clName + " " + field);
    if (clName == null) {
      fields = AnnotationOperations.getFieldNames(thisCl, DBEntField.class);
    } else {
      Class<?> cl = Class.forName(packName + "." + clName);
      fields = AnnotationOperations.getFieldNames(cl, DBEntField.class);
    }

    return fields.get(field);
  }

  /**
   * Replaces name of entity with related name of table.
   * 
   * @param sql
   *          - Request.
   * @param cl
   *          - Class in which request is stored.
   * @return
   * @throws ClassNotFoundException
   *           if there is no such class.
   */
  private String replaceEntities(String sql, Class<?> cl) throws ClassNotFoundException {
    String sqlOut = sql;
    
    Pattern p = Pattern.compile(ENT_PLACEHOLDER);
    Matcher matcher = p.matcher(sqlOut);
    String packageName = AnnotationOperations.getEntityPackage();

    while (matcher.find()) {
      String tableName;
      String className = matcher.group(1);
      String synonym = matcher.group(2);
      tableName = getTableName(packageName, className, cl);

      String classNameInRequest = String.format(TEMPLATE, className);

      // Synonim is present
      if (synonym != null) {
        classNameInRequest = String.format(TEMPLATE, className + " as " + synonym);
      }

      sqlOut = sqlOut.replaceAll(classNameInRequest, "`" + tableName + "`");
    }

    return sqlOut;

  }

  /**
   * Creates Map with pairs of class synonym and full class name.
   * 
   * @param sql
   *          - Request.
   * @return
   * @throws ClassNotFoundException
   *           if there is no such class.
   */
  private Map<String, String> createDictionary(String sql) throws ClassNotFoundException {
    Map<String, String> dictionary = new HashMap<>();
    Pattern p = Pattern.compile(ENT_PLACEHOLDER);
    Matcher matcher = p.matcher(sql);

    while (matcher.find()) {
      String className = matcher.group(1);
      String synonym = matcher.group(2);

      if (synonym != null) {
        dictionary.put(synonym, className);
      }
    }
    return dictionary;
  }

  /**
   * Returns proper table name
   * 
   * @param pack
   *          - Name of package.
   * @param className
   *          - Name of class in placeholder.
   * @param cl
   *          - Class in which request is stored.
   * @return
   * @throws ClassNotFoundException
   *           if there is no such class.
   */
  private String getTableName(String pack, String className, Class<?> cl)
      throws ClassNotFoundException {
    if ("tn".equals(className)) {
      return AnnotationOperations.getEntityName(cl);
    } else {
      return AnnotationOperations.getEntityName(Class.forName(pack + "." + className));
    }
  }
}
