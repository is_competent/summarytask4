package ua.nure.suprun.SummaryTask4.db.util;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Blob;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.util.AnnotationOperations;
import ua.nure.suprun.SummaryTask4.db.NamedParameterStatement;

/**
 * Provides auto filling named parameters into {@link NamedParameterStatement} for common used
 * types, such as <br>
 * <ol>
 * <li>Integer
 * <li>Double
 * <li>Boolean
 * <li>Byte
 * <li>Short
 * <li>Long
 * <li>Float
 * <li>BigDecimal
 * <li>String
 * <li>byte[]
 * <li>java.sql.Date
 * <li>java.sql.Time
 * <li>java.sql.Timestamp
 * <li>Blob
 * <li>java.net.URL
 * <li>For other types setObject() will be called 
 * </ol>
 * 
 * @author B.Suprun
 * 
 */
public class NamedStatementUtil {
  private static final Logger LOG = Logger.getLogger(NamedParameterStatement.class);

  /**
   * Provides auto filling named parameters into {@link NamedParameterStatement}. List of named
   * parameters are being retrieved from {@link NamedParameterStatement#getParameters()}
   * 
   * @param ps
   *          - {@link NamedParameterStatement} instance.
   * @param ent
   *          - An entity-class annotated with db.annotation
   * @throws SQLException
   *           If an error has occurred or an unsupported type was found (
   *           {@link NamedStatementUtil supported types}).
   */
  public static <T> void fill(NamedParameterStatement ps, T ent) throws SQLException {
    try {
      Map<String, Object> values = null;
      values = AnnotationOperations.getFieldValues(ent, DBEntField.class);
      Set<String> parameters = ps.getParameters();

      for (String name : parameters) {
        Object o = values.get(name);
        LOG.trace("Inserting parameter: [" + name + ", value " + o + "]");
        
        setValueToStatement(ps, name, o);
      }
    } catch (IllegalAccessException ex) {
      throw new SQLException("Illegal acces",ex);
    }

  }

  public static void setValueToStatement(NamedParameterStatement ps, String name, Object o)
      throws SQLException {
    if (o == null) {
      LOG.debug("Parameter not found: " + name);
      return;
    }

    if (o instanceof Integer) {
      ps.setInt(name, (Integer) o);
    } else if (o instanceof Double) {
      ps.setDouble(name, (Double) o);
    } else if (o instanceof Boolean) {
      ps.setBoolean(name, (Boolean) o);
    } else if (o instanceof Byte) {
      ps.setByte(name, (Byte) o);
    } else if (o instanceof Short) {
      ps.setShort(name, (Short) o);
    } else if (o instanceof Long) {
      ps.setLong(name, (Long) o);
    } else if (o instanceof Float) {
      ps.setFloat(name, (Float) o);
    } else if (o instanceof BigDecimal) {
      ps.setBigDecimal(name, (BigDecimal) o);
    } else if (o instanceof String) {
      ps.setString(name, (String) o);
    } else if (o instanceof byte[]) {
      ps.setBytes(name, (byte[]) o);
    } else if (o instanceof Date) {
      ps.setDate(name, (Date) o);
    } else if (o instanceof Time) {
      ps.setTime(name, (Time) o);
    } else if (o instanceof Timestamp) {
      ps.setTimestamp(name, (Timestamp) o);
    } else if (o instanceof Blob) {
      ps.setBlob(name, (Blob) o);
    } else if (o instanceof URL) {
      ps.setURL(name, (URL) o);
    } else {
      ps.setObject(name, o);
    }
  }

  public static <T> void initialize(ResultSet rs, T ent) throws SQLException {
    try {
      List<Field> fields = AnnotationOperations
          .getAnnotatedFields(ent.getClass(), DBEntField.class);

      List<String> columnNames = getColumnNames(rs);
      int index;

      for (Field field : fields) {
        DBEntField annotation = field.getAnnotation(DBEntField.class);

        LOG.debug("Annotation: " + annotation);

        index = columnNames.indexOf(annotation.value());

        if (index == -1) {
          LOG.trace("No such column: " + annotation.value());
          continue;
        }
        
        index = index + 1;
        
        Object o = getValueFromResultSet(rs, field.getType(), index);
        LOG.trace("Object is: " + o);

        if (o == null) {
          LOG.debug("Null object");
          continue;
        }

        AnnotationOperations.invokeSet(field, ent, o);
      }
    } catch (IllegalAccessException ex) {
      LOG.error(ex);
      throw new SQLException("Illegal access", ex);
    }
  }

  private static Object getValueFromResultSet(ResultSet rs, Class<?> o, int index)
      throws SQLException {

    Object result = null;

    if (o == Integer.class) {
      result = rs.getInt(index);
    } else if (o == Double.class) {
      result = rs.getDouble(index);
    } else if (o == Boolean.class) {
      result = rs.getBoolean(index);
    } else if (o == Byte.class) {
      result = rs.getByte(index);
    } else if (o == Short.class) {
      result = rs.getShort(index);
    } else if (o == Long.class) {
      result = rs.getLong(index);
    } else if (o == Float.class) {
      result = rs.getFloat(index);
    } else if (o == BigDecimal.class) {
      result = rs.getBigDecimal(index);
    } else if (o == String.class) {
      result = rs.getString(index);
    } else if (o == byte[].class) {
      result = rs.getBytes(index);
    } else if (o == Date.class) {
      result = rs.getDate(index);
    } else if (o == Time.class) {
      result = rs.getTime(index);
    } else if (o == Timestamp.class) {
      result = rs.getTimestamp(index);
    } else if (o == Blob.class) {
      result = rs.getBlob(index);
    } else if (o == URL.class) {
      result = rs.getURL(index);
    } else {
      result = rs.getObject(index);
    }

    return result;
  }

  public static Object initializePrimitive(ResultSet rs, Class<?> cl, int index)
      throws SQLException {
    Object o = getValueFromResultSet(rs, cl, index);

    return o;
  }

  public static Object initializePrimitive(ResultSet rs, Class<?> cl, String columnName)
      throws SQLException {
    List<String> cols = getColumnNames(rs);
    int index = cols.indexOf(columnName);

    if (index == -1) {
      throw new SQLException("No such column");
    }
    
    index = index + 1;
    
    return initializePrimitive(rs, cl, index);
  }

  private static List<String> getColumnNames(ResultSet rs) throws SQLException {
    List<String> columns = new ArrayList<String>();
    ResultSetMetaData metadata = rs.getMetaData();

    for (int i = 1; i <= metadata.getColumnCount(); i++) {
      columns.add(metadata.getColumnName(i));
    }

    return columns;
  }
}
