package ua.nure.suprun.SummaryTask4.db.util;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.Blob;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.List;

/**
 * Provides auto filling parameters into {@link PreparedStatement} for common
 * used types, such as <br>
 * <ol>
 * <li>Integer
 * <li>Double
 * <li>Boolean
 * <li>Byte
 * <li>Short
 * <li>Long
 * <li>Float
 * <li>BigDecimal
 * <li>String
 * <li>byte[]
 * <li>java.sql.Date
 * <li>java.sql.Time
 * <li>java.sql.Timestamp
 * <li>Blob
 * <li>java.net.URL
 * <li>Object
 * </ol>
 * 
 * @author B.Suprun
 * 
 */
public class PreparedStatementUtil {

	/**
	 * Provides auto filling parameters into {@link PreparedStatement}.
	 * 
	 * @param ps
	 *            - {@link PreparedStatement} instance.
	 * @param values
	 *            - List of values which is being inserted.
	 * @throws SQLException
	 *             If an error has occurred or an unsupported type was found (
	 *             {@link NamedStatementUtil supported types}).
	 */
	public static <T> void fill(PreparedStatement ps, List<Object> values)
			throws SQLException {
		int k = 0;

		for (Object o : values) {
			k++;
			if (o instanceof Integer) {
				ps.setInt(k, (Integer) o);
			} else if (o instanceof Double) {
				ps.setDouble(k, (Double) o);
			} else if (o instanceof Boolean) {
				ps.setBoolean(k, (Boolean) o);
			} else if (o instanceof Byte) {
				ps.setByte(k, (Byte) o);
			} else if (o instanceof Short) {
				ps.setShort(k, (Short) o);
			} else if (o instanceof Long) {
				ps.setLong(k, (Long) o);
			} else if (o instanceof Float) {
				ps.setFloat(k, (Float) o);
			} else if (o instanceof BigDecimal) {
				ps.setBigDecimal(k, (BigDecimal) o);
			} else if (o instanceof String) {
				ps.setString(k, (String) o);
			} else if (o instanceof byte[]) {
				ps.setBytes(k, (byte[]) o);
			} else if (o instanceof Date) {
				ps.setDate(k, (Date) o);
			} else if (o instanceof Time) {
				ps.setTime(k, (Time) o);
			} else if (o instanceof Timestamp) {
				ps.setTimestamp(k, (Timestamp) o);
			} else if (o instanceof Blob) {
				ps.setBlob(k, (Blob) o);
			} else if (o instanceof URL) {
				ps.setURL(k, (URL) o);
			} else if (o.getClass() == Object.class) {
				ps.setObject(k, o);
			} else {
				throw new SQLException(
						"Undefined type, please override RequestBuilder#fill method");
			}
		}
	}

	/**
	 * Provides auto filling parameters into {@link PreparedStatement} starting
	 * from specified <b>number</b> value.
	 * 
	 * @param ps
	 *            - {@link PreparedStatement} instance.
	 * @param values
	 *            - List of values which is being inserted.
	 * @param number
	 *            - Index of next inserted parameter.
	 * @throws SQLException
	 *             If an error has occurred or an unsupported type was found (
	 *             {@link NamedStatementUtil supported types}).
	 */
	public static <T> void fill(PreparedStatement ps, List<Object> values,
			int number) throws SQLException {
		int k = number;

		for (Object o : values) {
			if (o instanceof Integer) {
				ps.setInt(k, (Integer) o);
			} else if (o instanceof Double) {
				ps.setDouble(k, (Double) o);
			} else if (o instanceof Boolean) {
				ps.setBoolean(k, (Boolean) o);
			} else if (o instanceof Byte) {
				ps.setByte(k, (Byte) o);
			} else if (o instanceof Short) {
				ps.setShort(k, (Short) o);
			} else if (o instanceof Long) {
				ps.setLong(k, (Long) o);
			} else if (o instanceof Float) {
				ps.setFloat(k, (Float) o);
			} else if (o instanceof BigDecimal) {
				ps.setBigDecimal(k, (BigDecimal) o);
			} else if (o instanceof String) {
				ps.setString(k, (String) o);
			} else if (o instanceof byte[]) {
				ps.setBytes(k, (byte[]) o);
			} else if (o instanceof Date) {
				ps.setDate(k, (Date) o);
			} else if (o instanceof Time) {
				ps.setTime(k, (Time) o);
			} else if (o instanceof Timestamp) {
				ps.setTimestamp(k, (Timestamp) o);
			} else if (o instanceof Blob) {
				ps.setBlob(k, (Blob) o);
			} else if (o instanceof URL) {
				ps.setURL(k, (URL) o);
			} else if (o.getClass() == Object.class) {
				ps.setObject(k, o);
			} else {
				throw new SQLException(
						"Undefined type, please override RequestBuilder#fill method");
			}
			k++;
		}
	}
}
