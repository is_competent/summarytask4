package ua.nure.suprun.SummaryTask4.db;

import java.io.InputStream;
import java.io.Reader;
import java.math.BigDecimal;
import java.net.URL;
import java.sql.Array;
import java.sql.Blob;
import java.sql.Clob;
import java.sql.Connection;
import java.sql.Date;
import java.sql.NClob;
import java.sql.ParameterMetaData;
import java.sql.PreparedStatement;
import java.sql.Ref;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.RowId;
import java.sql.SQLException;
import java.sql.SQLWarning;
import java.sql.SQLXML;
import java.sql.Time;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;

/**
 * PreparedStatement with named parameters instead of question marks. SQL
 * Request may be such: <b>SELECT * FROM A_TABLE WHERE iD = :id</b> Where
 * <b>:id</b> is a named parameter.
 * 
 * @author B.Suprun
 * 
 */
public class NamedParameterStatement implements AutoCloseable {
  
  private static final String ERROR = "undefined parameter";

	private PreparedStatement ps;
	
	private static final Logger LOG = Logger.getLogger(NamedParameterStatement.class);

	/**
	 * Map of the parameter names and its indexes.
	 */
	private Map<String, List<Integer>> parameters;

	/**
	 * Pattern for named parameters.
	 */
	private static final String PATTERN = "(?i)(?<=[\\s\\(\\)\\=]):([a-z0-9]+)";

	/**
	 * Create new NamedParameterStatement instance.
	 * 
	 * @param c
	 *            - Connection used for creating PreparedStatement.
	 * @param sql
	 *            - SQL request with named parameters
	 * @throws SQLException
	 *             If there are some problems with PreparedStatement object
	 *             creation
	 */
	public NamedParameterStatement(Connection c, String sql)
			throws SQLException {
	  String sqlOut = sql;
	  
		parameters = new LinkedHashMap<String, List<Integer>>();

		// reads all parameters and writing parameter indexes
		readParameters(sqlOut);
		LOG.info("Parsed parameters: " + parameters);

		// Replace all named parameters by question marks
		sqlOut = replace(sqlOut);
		LOG.info("SQL: " + sqlOut);
		
		// Also create PreparedStatement
		ps = c.prepareStatement(sqlOut, PreparedStatement.RETURN_GENERATED_KEYS);
	}

	/**
	 * Returns all found named parameters.
	 * 
	 * @return - Set of found named parameters
	 */
	public Set<String> getParameters() {
		return parameters.keySet();
	}

	/**
	 * Retrieves all named parameters in the request.
	 * 
	 * @param sql
	 *            - SQL Request with named parameters
	 */
	private void readParameters(String sql) {
		Pattern p = Pattern.compile(PATTERN);
		Matcher m = p.matcher(sql);

		// index of named parameter
		int i = 1;

		while (m.find()) {
			String pName = m.group(1);

			List<Integer> l = parameters.get(pName);

			// if there is no such named parameter
			// just create new ArrayList and put new parameter
			// into parameters Map
			if (l == null) {
				l = new ArrayList<Integer>();
				parameters.put(pName, l);
			}

			l.add(i++);
		}

	}

	/**
	 * Just replace all named parameters by question marks
	 * 
	 * @param sql
	 *            - SQL request with named parameters
	 * @return SQL request with question marks instead of named parameters (i.e.
	 *         PreparedStatement compatible)
	 */
	private String replace(String sql) {
		return sql.replaceAll(PATTERN, "?");
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setBoolean(String paramName, boolean x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBoolean(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setByte(String paramName, byte x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setByte(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setShort(String paramName, short x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setShort(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setInt(String paramName, int x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setInt(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setLong(String paramName, long x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setLong(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setFloat(String paramName, float x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setFloat(parameterIndex, x);
		}
	}

	public void setDouble(String paramName, double x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setDouble(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setBigDecimal(String paramName, BigDecimal x)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBigDecimal(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setString(String paramName, String x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setString(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setBytes(String paramName, byte[] x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBytes(parameterIndex, x);
		}
	}

	public void setDate(String paramName, Date x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setDate(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setTime(String paramName, Time x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setTime(parameterIndex, x);
		}
	}

	public void setTimestamp(String paramName, Timestamp x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setTimestamp(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setObject(String paramName, Object x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setObject(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param x
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setBlob(String paramName, Blob x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBlob(parameterIndex, x);
		}
	}

	public void setURL(String paramName, URL x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setURL(parameterIndex, x);
		}
	}

	public <T> T unwrap(Class<T> iface) throws SQLException {
		return ps.unwrap(iface);
	}

	public ResultSet executeQuery(String sql) throws SQLException {
		return ps.executeQuery(sql);
	}

	public ResultSet executeQuery() throws SQLException {
		return ps.executeQuery();
	}

	public boolean isWrapperFor(Class<?> iface) throws SQLException {
		return ps.isWrapperFor(iface);
	}

	public int executeUpdate(String sql) throws SQLException {
		return ps.executeUpdate(sql);
	}

	public int executeUpdate() throws SQLException {
		return ps.executeUpdate();
	}

	/**
	 * Sets parameter <b>sqlType</b> in place of named parameter.
	 * 
	 * @param paramName
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param sqlType
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setNull(String paramName, int sqlType) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setNull(parameterIndex, sqlType);
		}
	}

	public void close() throws SQLException {
		ps.close();
	}

	public int getMaxFieldSize() throws SQLException {
		return ps.getMaxFieldSize();
	}

	public void setMaxFieldSize(int max) throws SQLException {
		ps.setMaxFieldSize(max);
	}

	public int getMaxRows() throws SQLException {
		return ps.getMaxRows();
	}

	public void setMaxRows(int max) throws SQLException {
		ps.setMaxRows(max);
	}

	public void setEscapeProcessing(boolean enable) throws SQLException {
		ps.setEscapeProcessing(enable);
	}

	public int getQueryTimeout() throws SQLException {
		return ps.getQueryTimeout();
	}

	public void setQueryTimeout(int seconds) throws SQLException {
		ps.setQueryTimeout(seconds);
	}

	public void cancel() throws SQLException {
		ps.cancel();
	}

	public SQLWarning getWarnings() throws SQLException {
		return ps.getWarnings();
	}

	public void clearWarnings() throws SQLException {
		ps.clearWarnings();
	}

	public void setCursorName(String name) throws SQLException {
		ps.setCursorName(name);
	}

	public void setAsciiStream(String paramName, InputStream x, int length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setAsciiStream(parameterIndex, x, length);
		}
	}

	public void setBinaryStream(String paramName, InputStream x, int length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBinaryStream(parameterIndex, x, length);
		}
	}

	public ResultSet getResultSet() throws SQLException {
		return ps.getResultSet();
	}

	public int getUpdateCount() throws SQLException {
		return ps.getUpdateCount();
	}

	public void clearParameters() throws SQLException {
		ps.clearParameters();
	}

	public boolean getMoreResults() throws SQLException {
		return ps.getMoreResults();
	}

	public void setObject(String paramName, Object x, int targetSqlType)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setObject(parameterIndex, x, targetSqlType);
		}
	}

	public void setFetchDirection(int direction) throws SQLException {
		ps.setFetchDirection(direction);
	}

	public int getFetchDirection() throws SQLException {
		return ps.getFetchDirection();
	}

	public void setFetchSize(int rows) throws SQLException {
		ps.setFetchSize(rows);
	}

	public int getFetchSize() throws SQLException {
		return ps.getFetchSize();
	}

	public boolean execute() throws SQLException {
		return ps.execute();
	}

	public int getResultSetConcurrency() throws SQLException {
		return ps.getResultSetConcurrency();
	}

	public int getResultSetType() throws SQLException {
		return ps.getResultSetType();
	}

	public void addBatch(String sql) throws SQLException {
		ps.addBatch(sql);
	}

	public void addBatch() throws SQLException {
		ps.addBatch();
	}

	public void setCharacterStream(String paramName, Reader reader, int length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setCharacterStream(parameterIndex, reader, length);
		}
	}

	public void clearBatch() throws SQLException {
		ps.clearBatch();
	}

	public int[] executeBatch() throws SQLException {
		return ps.executeBatch();
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param x
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param sqlType
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setRef(String paramName, Ref x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setRef(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param x
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param sqlType
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setClob(String paramName, Clob x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setClob(parameterIndex, x);
		}
	}

	/**
	 * Sets parameter <b>x</b> in place of named parameter.
	 * 
	 * @param x
	 *            - Named parameter, just name of parameter without ":" mark
	 * @param sqlType
	 *            - Named parameter value
	 * @throws SQLException
	 *             If there is no parameter with such name, or
	 *             ParameterStatement related method throws the exception
	 */
	public void setArray(String paramName, Array x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setArray(parameterIndex, x);
		}
	}

	public Connection getConnection() throws SQLException {
		return ps.getConnection();
	}

	public ResultSetMetaData getMetaData() throws SQLException {
		return ps.getMetaData();
	}

	public void setDate(String paramName, Date x, Calendar cal)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setDate(parameterIndex, x, cal);
		}
	}

	public boolean getMoreResults(int current) throws SQLException {
		return ps.getMoreResults(current);
	}

	public void setTime(String paramName, Time x, Calendar cal)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setTime(parameterIndex, x, cal);
		}
	}

	public ResultSet getGeneratedKeys() throws SQLException {
		return ps.getGeneratedKeys();
	}

	public void setTimestamp(String paramName, Timestamp x, Calendar cal)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setTimestamp(parameterIndex, x, cal);
		}
	}

	public int executeUpdate(String sql, int autoGeneratedKeys)
			throws SQLException {
		return ps.executeUpdate(sql, autoGeneratedKeys);
	}

	public void setNull(String paramName, int sqlType, String typeName)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setNull(parameterIndex, sqlType, typeName);
		}
	}

	public int executeUpdate(String sql, int[] columnIndexes)
			throws SQLException {
		return ps.executeUpdate(sql, columnIndexes);
	}

	public ParameterMetaData getParameterMetaData() throws SQLException {
		return ps.getParameterMetaData();
	}

	public void setRowId(String paramName, RowId x) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setRowId(parameterIndex, x);
		}
	}

	public int executeUpdate(String sql, String[] columnNames)
			throws SQLException {
		return ps.executeUpdate(sql, columnNames);
	}

	public void setNString(String paramName, String value) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setNString(parameterIndex, value);
		}
	}

	public void setNCharacterStream(String paramName, Reader value, long length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setNCharacterStream(parameterIndex, value, length);
		}
	}

	public boolean execute(String sql, int autoGeneratedKeys)
			throws SQLException {
		return ps.execute(sql, autoGeneratedKeys);
	}

	public void setNClob(String paramName, NClob value) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setNClob(parameterIndex, value);
		}
	}

	public void setClob(String paramName, Reader reader, long length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setClob(parameterIndex, reader, length);
		}
	}

	public boolean execute(String sql, int[] columnIndexes) throws SQLException {
		return ps.execute(sql, columnIndexes);
	}

	public void setBlob(String paramName, InputStream inputStream, long length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBlob(parameterIndex, inputStream, length);
		}
	}

	public void setNClob(String paramName, Reader reader, long length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setNClob(parameterIndex, reader, length);
		}
	}

	public boolean execute(String sql, String[] columnNames)
			throws SQLException {
		return ps.execute(sql, columnNames);
	}

	public void setSQLXML(String paramName, SQLXML xmlObject)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setSQLXML(parameterIndex, xmlObject);
		}
	}

	public void setObject(String paramName, Object x, int targetSqlType,
			int scaleOrLength) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setObject(parameterIndex, x, targetSqlType, scaleOrLength);
		}
	}

	public int getResultSetHoldability() throws SQLException {
		return ps.getResultSetHoldability();
	}

	public boolean isClosed() throws SQLException {
		return ps.isClosed();
	}

	public void setPoolable(boolean poolable) throws SQLException {
		ps.setPoolable(poolable);
	}

	public void setAsciiStream(String paramName, InputStream x, long length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setAsciiStream(parameterIndex, x, length);
		}
	}

	public boolean isPoolable() throws SQLException {
		return ps.isPoolable();
	}

	public void closeOnCompletion() throws SQLException {
		ps.closeOnCompletion();
	}

	public void setBinaryStream(String paramName, InputStream x, long length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBinaryStream(parameterIndex, x, length);
		}
	}

	public boolean isCloseOnCompletion() throws SQLException {
		return ps.isCloseOnCompletion();
	}

	public void setCharacterStream(String paramName, Reader reader, long length)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setCharacterStream(parameterIndex, reader, length);
		}
	}

	public void setAsciiStream(String paramName, InputStream x)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setAsciiStream(parameterIndex, x);
		}
	}

	public void setBinaryStream(String paramName, InputStream x)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBinaryStream(parameterIndex, x);
		}
	}

	public void setCharacterStream(String paramName, Reader reader)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setCharacterStream(parameterIndex, reader);
		}
	}

	public void setNCharacterStream(String paramName, Reader value)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setNCharacterStream(parameterIndex, value);
		}
	}

	public void setClob(String paramName, Reader reader) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setClob(parameterIndex, reader);
		}
	}

	public void setBlob(String paramName, InputStream inputStream)
			throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setBlob(parameterIndex, inputStream);
		}
	}

	public void setNClob(String paramName, Reader reader) throws SQLException {
		List<Integer> l = parameters.get(paramName);

		if (l == null) {
			throw new SQLException(ERROR);
		}

		for (Integer parameterIndex : l) {
			ps.setNClob(parameterIndex, reader);
		}
	}

}
