package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.EntBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Role;
import ua.nure.suprun.SummaryTask4.db.dao.domains.RoleDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * RoleDAO implementation for MySQL.
 * 
 * @author B.Suprun
 * 
 */
public class RoleDAOImp extends BasicDAOImp<Role> implements RoleDAO {

	private static final Logger LOG = Logger.getLogger(RoleDAOImp.class);

	public RoleDAOImp(EntBuilder<Role> builder, DataSource ds) {
		super(builder, ds);
	}

	@Override
	public List<Role> getAll() throws DMLException {
		LOG.info("Start retriewing all roles.");

		return getAll(Role.class);
	}
}
