package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.EntBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.ReqName;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UniversityDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * UniversityDAO implementation for MySQL.
 * 
 * @author B.Suprun
 * 
 */
public class UniversityDAOImp extends BasicDAOImp<University> implements
		UniversityDAO {

	private static final Logger LOG = Logger.getLogger(UniversityDAOImp.class);

	public UniversityDAOImp(EntBuilder<University> builder, DataSource ds) {
		super(builder, ds);
	}

	@Override
	public List<University> getAll() throws DMLException {
		LOG.info("Start retrieving all universities");

		return super.getAll(University.class);
	}

	@Override
	public University getByAdmin(int admin) throws DMLException {
		
		LOG.info("Retriewing university with admin: " + admin);
	
		List<University> result = invokeSelect(ReqName.UNIVERSITY_GET_BY_ADMIN, 
		    University.class, builder, admin);

		LOG.trace("Universities is: " + result);
		
		return getSingleItem(result);
		
	}

	@Override
	public University get(int id) throws DMLException {
		University un = new University();
		un.setId(id);
		return getByPK(un);
	}

}
