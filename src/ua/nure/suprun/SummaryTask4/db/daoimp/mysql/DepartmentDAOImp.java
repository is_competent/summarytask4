package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentSubjectBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.EntBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.DepartmentSubject;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.ReqName;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * DepartmentDAO implementation for MySQL.
 * 
 * @author B.Suprun
 * 
 */
public class DepartmentDAOImp extends BasicDAOImp<Department> implements
		DepartmentDAO {

	private static final Logger LOG = Logger.getLogger(DepartmentDAOImp.class);

	public DepartmentDAOImp(EntBuilder<Department> builder, DataSource ds) {
		super(builder, ds);
	}

	@Override
	public List<Department> sortByName(University un, boolean asc)
			throws DMLException {
	  
		LOG.info("Sorting by name");
		
		String sql;
		Department dep = new Department();
		dep.setUniversity(un.getId());

		if (asc) {
			sql = rBuilder.getStoredRequest(Department.class,
					ReqName.DEP_BY_NAME_ASC);
		} else {
			sql = rBuilder.getStoredRequest(Department.class,
					ReqName.DEP_BY_NAME_DESC);
		}

		return invokeSelect(dep, sql);
	}

	@Override
	public List<Department> sortByBudget(University un, boolean asc)
			throws DMLException {
		LOG.info("Sorting by budget");
		String sql;
		Department dep = new Department();
		dep.setUniversity(un.getId());

		if (asc) {
			sql = rBuilder.getStoredRequest(Department.class,
					ReqName.DEP_BY_BUDGET_ASC);
		} else {
			sql = rBuilder.getStoredRequest(Department.class,
					ReqName.DEP_BY_BUDGET_DESC);
		}

		return invokeSelect(dep, sql);
	}

	@Override
	public List<Department> sortByCommon(University un, boolean asc)
			throws DMLException {
		LOG.info("Sorting by common places");
		String sql;
		Department dep = new Department();
		dep.setUniversity(un.getId());

		if (asc) {
			sql = rBuilder.getStoredRequest(Department.class,
					ReqName.DEP_BY_COMM_ASC);
		} else {
			sql = rBuilder.getStoredRequest(Department.class,
					ReqName.DEP_BY_COMM_DESC);
		}

		return invokeSelect(dep, sql);
	}

	@Override
	public boolean addSubject(DepartmentSubject subj) throws DMLException {
		LOG.info("Adding a subject to department");
		LOG.debug("DepartmentSubject is: " + subj);
		String sql = rBuilder.createInsertRequest(subj);

		LOG.debug("Generated request is: " + sql);
		return invokeSimpleInsert(subj, sql, new DepartmentSubjectBuilder());
	}

	@Override
	public boolean removeSubject(DepartmentSubject subj) throws DMLException {
		LOG.info("Removing a subject from department");
		LOG.debug("DepartmentSubject is: " + subj);
		String sql = rBuilder.getStoredRequest(DepartmentSubject.class, ReqName.DS_REMOVE);

		LOG.debug("Generated request is: " + sql);
		return invokeDelete(subj, sql);
	}

	@Override
	public boolean remove(Department dep) throws DMLException {
		return delete(dep);
	}

	@Override
	public boolean createNew(Department dep) throws DMLException {
		return insert(dep);
	}

	@Override
	public Department get(int id) throws DBException{
		Department d = new Department();
		d.setId(id);
		LOG.info("Start search department --> " + d);
		
		return getByPK(d);
	}
}
