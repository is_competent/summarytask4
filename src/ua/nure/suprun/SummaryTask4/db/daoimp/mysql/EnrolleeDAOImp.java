package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DefaultEntBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.DefaultPrimitiveEntBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeDeptBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeMarkBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.EntBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeDept;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeMark;
import ua.nure.suprun.SummaryTask4.bl.entity.bean.DepartmentApplicationBean;
import ua.nure.suprun.SummaryTask4.constants.ReqName;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * EnrolleeDAO implementation for MySQL.
 * 
 * @author B.Suprun
 * 
 */
public class EnrolleeDAOImp extends BasicDAOImp<Enrollee> implements EnrolleeDAO {

  private static final Logger LOG = Logger.getLogger(EnrolleeDAOImp.class);

  public EnrolleeDAOImp(EntBuilder<Enrollee> builder, DataSource ds) {
    super(builder, ds);
  }

  @Override
  public Enrollee get(int userId) throws DMLException {
    LOG.info("Retriewing enrollee by id");
    LOG.debug("Enrollee id is: " + userId);
    Enrollee en = new Enrollee();
    en.setId(userId);

    return getByPK(en);
  }

  @Override
  public boolean addMark(EnrolleeMark mark) throws DMLException {
    LOG.info("Adding enrole's mark");
    LOG.debug("EnrolleeMark is: " + mark);
    String sql = rBuilder.createInsertRequest(mark);
    LOG.debug("Generated request is: " + sql);

    return invokeSimpleInsert(mark, sql, new EnrolleeMarkBuilder());
  }

  @Override
  public boolean removeMark(EnrolleeMark mark) throws DMLException {
    LOG.info("Removing enrole's mark");
    LOG.debug("EnrolleeMark is: " + mark);
    String sql = rBuilder.getStoredRequest(EnrolleeMark.class, ReqName.EM_REMOVE);
    LOG.debug("Generated request is: " + sql);

    return invokeDelete(mark, sql);
  }

  @Override
  public List<EnrolleeMark> getMarks(Enrollee enr) throws DMLException {
    LOG.info("Retrieving enrole's marks");
    LOG.debug("Enrollee is: " + enr);
    EnrolleeMark em = new EnrolleeMark();
    em.setEnId(enr.getId());

    String sql = rBuilder.createSelectByPKRequest(em);
    LOG.debug("Generated request is: " + sql);

    return invokeSelect(em, sql, new EnrolleeMarkBuilder());
  }

  @Override
  public boolean applyDep(EnrolleeDept ed) throws DMLException {
    LOG.info("Applying to dep");
    LOG.debug("EnrolleeDep is: " + ed);

    String sql = rBuilder.createInsertRequest(ed);
    LOG.debug("Generated request is: " + sql);

    try {
      return invokeSimpleInsert(ed, sql, new EnrolleeDeptBuilder());
    } catch (DBException ex) {
      // for more informative error message
      Throwable t = ex.getCause();
      
      if (t.getMessage().contains("Subjects are not mutched")) {
        throw new DMLException("Subjects are not mutched", ex);
      } else if (t.getMessage().contains("Duplicate entry for prior")) {
        throw new DMLException("There is application with such priority value", ex);
      } else if (t.getMessage().contains("Duplicate entry for key")) {
        throw new DMLException("You are allready applied to this department", ex);
      }

      throw ex;
    }
  }

  @Override
  public List<DepartmentApplicationBean> getApplications(int user) throws DMLException {
    LOG.debug("Start extract applied");
   
    EntBuilder<DepartmentApplicationBean> entBuilder = 
        new DefaultEntBuilder<>(DepartmentApplicationBean.class);
        
    return invokeSelect(ReqName.ENROLLEE_APPLIED, Enrollee.class, entBuilder, user);
  }

  @Override
  public List<Integer> getPriorities(int id) throws DMLException {
    LOG.debug("Start extract priorities");
    
    EntBuilder<Integer> entBuilder = new DefaultPrimitiveEntBuilder<>(Integer.class);
    
    return invokeSelect(ReqName.ENROLLEE_DEP_PRIORITIES, EnrolleeDept.class, entBuilder, id);
  }
}
