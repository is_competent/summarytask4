package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.EntBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.constants.ReqName;
import ua.nure.suprun.SummaryTask4.db.dao.domains.SubjectDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * SubjectDAO implementation for MySQL.
 * 
 * @author B.Suprun
 * 
 */
public class SubjectDAOImp extends BasicDAOImp<Subject> implements SubjectDAO {

	private static final Logger LOG = Logger.getLogger(SubjectDAOImp.class);

	public SubjectDAOImp(EntBuilder<Subject> builder, DataSource ds) {
		super(builder, ds);
	}

	@Override
	public List<Subject> getAll() throws DMLException {
		LOG.info("Starting retriewing subjects");

		return getAll(Subject.class);
	}

	@Override
	public boolean removeSubject(Subject s) throws DMLException {
		LOG.info("Starting removing subject");
		LOG.debug("Subject is: " + s);

		return delete(s);
	}

	@Override
	public List<Subject> getDepartmentSubjects(int dep) throws DMLException {
		LOG.debug("Start extracting subjects");
		
		return invokeSelect(ReqName.SUBJ_GET_SUBJ, Subject.class, builder, dep);
	}
}
