package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import javax.sql.DataSource;
import javax.xml.bind.ValidationException;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.bl.builder.EntBuilder;
import ua.nure.suprun.SummaryTask4.db.NamedParameterStatement;
import ua.nure.suprun.SummaryTask4.db.builder.DefaultRequestBuilder;
import ua.nure.suprun.SummaryTask4.db.builder.RequestBuilder;
import ua.nure.suprun.SummaryTask4.db.util.NamedStatementUtil;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * The class provides implementation for basic operations with database.
 * 
 * @author B.Suprun
 * 
 * @param <T>
 *          - An entity-class
 */
public class BasicDAOImp<T> {

  private static final Logger LOG = Logger.getLogger(BasicDAOImp.class);

  /**
   * Class-builder for the entity-class
   */
  protected EntBuilder<T> builder;

  /**
   * Class-builder for the entity-class
   */
  protected RequestBuilder rBuilder;

  /**
   * DataSource for database.
   */
  protected DataSource ds;

  /**
   * Creates new instance with placed parameters and DefaultRequestBuilder.
   * 
   * @param builder
   *          - {@link EntBuilder Entity builder}.
   * @param ds
   *          - {@link DataSource}
   */
  public BasicDAOImp(EntBuilder<T> builder, DataSource ds) {
    this.builder = builder;
    this.ds = ds;
    rBuilder = new DefaultRequestBuilder();
  }

  /**
   * Creates instance with specified parameters.
   * 
   * @param builder
   *          - Entity builder.
   * @param rBuilder
   *          - Request builder.
   * @param ds
   *          - Data Source
   */
  public BasicDAOImp(EntBuilder<T> builder, RequestBuilder rBuilder, DataSource ds) {
    this.builder = builder;
    this.rBuilder = rBuilder;
    this.ds = ds;
  }

  /**
   * Returns all records from the table.
   * 
   * @param clazz
   *          - Class-entity class instance.
   * @return List of records.
   * @throws DMLException
   */
  public List<T> getAll(Class<T> clazz) throws DMLException {
    String sql = rBuilder.createSelectAllRequest(clazz);

    return invokeSelect(null, sql);
  }

  /**
   * Retrieves record from database with primary key as written in the <b>ent</b>.
   * 
   * @param ent
   *          - An entity-class instance.
   * @return Extracted record from the db.
   * @throws DMLException
   *           if an error has occurred or there is no such record.
   *           <p>
   *           <b>SEE:</b> {@link RequestBuilder#createSelectByPKRequest(Object)},
   *           {@link BasicDAOImp#invokeSelect(Object, String)}
   */
  public T getByPK(T ent) throws DMLException {
    String sql = rBuilder.createSelectByPKRequest(ent);
    List<T> res = invokeSelect(ent, sql);

    return getSingleItem(res);

  }

  /**
   * Updates record in the db with such primary key as written in the <b>ent</b>. <b>NOTE:</b>
   * Fields with {@link DBEntField#updatable()} = false will be not updated.
   * 
   * @param ent
   *          - An entity-class instance.
   * @return {@code true} if record was updated or {@code false} if did not.
   * @throws DMLException
   *           -if an error with updating has occurred.
   *           <p>
   *           <b>SEE:</b> {@link RequestBuilder#createUpdateRequest(Object)},
   *           {@link BasicDAOImp#invokeSimpleUpd(Object, String)}
   */
  public boolean update(T ent) throws DMLException {
    String sql = rBuilder.createUpdateRequest(ent);

    return invokeSimpleUpd(ent, sql);
  }

  /**
   * Deletes record in the db with such primary key as written in the <b>ent</b>.
   * 
   * @param ent
   *          - An entity-class instance.
   * @return {@code true} if record was deleted or {@code false} if did not.
   * @throws DMLException
   *           -if an error with deleting has occurred.
   *           <p>
   *           <b>SEE:</b> {@link RequestBuilder#createDeleteByPKRequest(Object)},
   *           {@link BasicDAOImp#invokeDelete(Object, String)}
   */
  public boolean delete(T ent) throws DMLException {
    String sql = rBuilder.createDeleteByPKRequest(ent);

    return invokeDelete(ent, sql);
  }

  /**
   * Inserts record in the db. <b>NOTE:</b> Fields with {@link DBEntField#insertable()} = false will
   * be not inserted.
   * 
   * @param ent
   *          - An entity-class instance.
   * @return {@code true} if record was inserted or {@code false} if did not.
   * @throws DMLException
   *           -if an error with inserting has occurred.
   *           <p>
   *           <b>SEE:</b> {@link RequestBuilder#createInsertRequest(Object)},
   *           {@link BasicDAOImp#invokeSimpleInsert(Object, String)}
   */
  public boolean insert(T ent) throws DMLException {
    String sql = rBuilder.createInsertRequest(ent);

    return invokeSimpleInsert(ent, sql, builder);
  }

  /**
   * Invokes simple update using this <b>sql</b> request.
   * 
   * @param ent
   *          - An entity-class.
   * @param sql
   *          - A named SQL Update request.
   * @return {@code true} if record was updated or {@code false} if did not.
   * @throws DMLException
   *           -if an error with updating has occurred.
   */
  protected <E> boolean invokeSimpleUpd(E ent, String sql) throws DMLException {
    Connection c = null;

    try {
      c = ds.getConnection();
      c.setAutoCommit(true);

      return invokeSimpleUpd(ent, new NamedParameterStatement(c, sql));
    } catch (SQLException ex) {
      LOG.fatal(ex);
      throw new DMLException("Error with updating item", ex);
    } finally {
      close(c);
    }
  }

  /**
   * Invokes simple insert using this <b>sql</b> request.
   * 
   * @param ent
   *          - An entity-class.
   * @param sql
   *          - A named SQL INSERT request.
   * @return {@code true} if record was inserted or {@code false} if did not.
   * @throws DMLException
   *           -if an error with inserting has occurred.
   */
  protected <E> boolean invokeSimpleInsert(E ent, String sql, EntBuilder<E> builder)
      throws DMLException {
    Connection c = null;

    try {
      c = ds.getConnection();
      c.setAutoCommit(true);

      return invokeSimpleInsert(ent, new NamedParameterStatement(c, sql), builder);
    } catch (SQLException ex) {
      LOG.error(ex);
      throw new DMLException("Error with inserting item", ex);
    } finally {
      close(c);
    }
  }

  /**
   * Invokes delete using this <b>sql</b> request.
   * 
   * @param ent
   *          - An entity-class.
   * @param sql
   *          - A named SQL DELETE request.
   * @return {@code true} if record was deleted or {@code false} if did not.
   * @throws DMLException
   *           -if an error with deleting has occurred.
   */
  protected <E> boolean invokeDelete(E ent, String sql) throws DMLException {
    Connection c = null;

    try {
      c = ds.getConnection();
      c.setAutoCommit(true);

      return invokeDelete(ent, new NamedParameterStatement(c, sql));
    } catch (SQLException ex) {
      LOG.error(ex);
      throw new DMLException("Error with deleting item", ex);
    } finally {
      close(c);
    }

  }

  /**
   * Invokes select using this <b>sql</b> request and EntBuilder passed to the constructor.
   * 
   * @param ent
   *          - An entity-class.
   * @param sql
   *          - SQL Select request.
   * @return List of retrieved records.
   * @throws DMLException
   *           if an error has occurred.
   */
  protected List<T> invokeSelect(T ent, String sql) throws DMLException {
    return invokeSelect(ent, sql, builder);
  }

  protected <E> List<E> invokeSelect(String sql, EntBuilder<E> builder, Object... namedParams)
      throws DMLException {
    Connection c = null;
    NamedParameterStatement st = null;
    try {
      c = ds.getConnection();
      st = new NamedParameterStatement(c, sql);

      int iteration = 0;
      if (namedParams != null && namedParams.length > 0) {
        Set<String> params = st.getParameters();
        for (String name : params) {
          NamedStatementUtil.setValueToStatement(st, name, namedParams[iteration++]);
        }
      }

      return invokeSelect(null, st, builder);
    } catch (SQLException ex) {
      LOG.fatal(ex);

      throw new DMLException("An error has occurred while retrieving the records", ex);
    } finally {
      close(c);
    }
  }

  protected <E> List<E> invokeSelect(String sqlName, Class<?> cl, EntBuilder<E> builder,
      Object... namedParams) throws DMLException {
    String sql = rBuilder.getStoredRequest(cl, sqlName);
    LOG.trace("Retriewed sql is: " + sql);

    return invokeSelect(sql, builder, namedParams);
  }

  /**
   * Invokes SELECT request using specified {@link EntBuilder}.
   * 
   * @param ent
   *          - An entity-class.
   * @param sql
   *          - SQL SELECT request.
   * @param builder
   *          - EntBuilder instance.
   * @return List of retrieved records.
   * @throws DMLException
   *           if an error has occurred.
   */
  protected <E> List<E> invokeSelect(E ent, String sql, EntBuilder<E> builder) throws DMLException {
    Connection c = null;

    try {
      c = ds.getConnection();
      NamedParameterStatement ps = new NamedParameterStatement(c, sql);

      return invokeSelect(ent, ps, builder);
    } catch (SQLException ex) {
      LOG.error(ex);
      throw new DMLException("Error with retrieving items", ex);
    } finally {
      close(c);
    }
  }

  /**
   * Invokes SELECT request using specified {@link NamedParameterStatement}.<br>
   * <b>NOTE:</b> NamedParameterStatement will be closed after invoking.
   * 
   * @param ent
   *          - An entity-class.
   * @param ps
   *          - {@link NamedParameterStatement} instance.
   * @param builder
   *          - {@link EntBuilder} instance for building instances using retrieved records.
   * @return List of retrieved records.
   * @throws SQLException
   *           if an error has occurred.
   */
  protected <E> List<E> invokeSelect(E ent, NamedParameterStatement ps, EntBuilder<E> builder)
      throws SQLException {

    ResultSet rs = null;

    try {
      if (ent != null) {
        NamedStatementUtil.fill(ps, ent);
      }

      rs = ps.executeQuery();
      List<E> res = new ArrayList<>();

      while (rs.next()) {
        res.add(builder.create(rs));
      }
      return res;
    } finally {
      close(rs);
      close(ps);
    }
  }

  /**
   * Invokes SQL DELETE request on the {@link NamedParameterStatement} instance. <br>
   * <b>NOTE:</b> NamedParameterStatement will be closed after invoking.
   * 
   * @param ent
   *          - An entity-class.
   * @param ps
   *          - {@link NamedParameterStatement}
   * @return{@code true} if record was deleted or {@code false} if did not.
   * @throws SQLException
   *           -if an error with deleting has occurred.
   */
  protected <E> boolean invokeDelete(E ent, NamedParameterStatement ps) throws SQLException {
    try {
      if (ent != null) {
        rBuilder.fillInsertRequest(ps, ent);
      }
      int n = ps.executeUpdate();

      return n > 0;
    } finally {
      close(ps);
    }

  }

  /**
   * Invokes SQL UPDATE request on the {@link NamedParameterStatement} instance. <br>
   * <b>NOTE:</b> NamedParameterStatement will be closed after invoking.
   * 
   * @param ent
   *          - An entity-class.
   * @param ps
   *          - {@link NamedParameterStatement}
   * @return{@code true} if record was updated or {@code false} if did not.
   * @throws SQLException
   *           -if an error has occurred or data format is incorrect.
   */
  protected <E> boolean invokeSimpleUpd(E ent, NamedParameterStatement ps) throws SQLException {
    try {

      if (ent != null) {
        rBuilder.validate(ent);
        rBuilder.fillInsertRequest(ps, ent);
      }
      int n = ps.executeUpdate();

      return n > 0;
    } catch (ValidationException ex) {
      LOG.warn(ex);
      throw new SQLException("Data format error", ex);
    } finally {
      close(ps);
    }
  }

  /**
   * Invokes SQL INSERT request on the {@link NamedParameterStatement} instance and then invoke
   * {@link EntBuilder#setAfterInsert(Object, ResultSet)} with <b>ent</b> as parameter <br>
   * <b>NOTE:</b> NamedParameterStatement will be closed after invoking.
   * 
   * @param ent
   *          - An entity-class.
   * @param ps
   *          - {@link NamedParameterStatement}
   * @return{@code true} if record was inserted or {@code false} if did not.
   * @throws SQLException
   *           -if an error has occurred or data format is incorrect.
   */
  protected <E> boolean invokeSimpleInsert(E ent, NamedParameterStatement ps, EntBuilder<E> builder)
      throws SQLException {
    try {
      if (ent != null) {
        rBuilder.validate(ent);
        rBuilder.fillInsertRequest(ps, ent);
      }

      int n = ps.executeUpdate();

      if (n > 0) {
        setAfterInsert(ent, ps, builder);
      }
      return n > 0;
    } catch (ValidationException ex) {
      LOG.warn(ex);
      throw new SQLException("Data format error", ex);
    } finally {
      close(ps);
    }
  }

  /**
   * Executes stored procedure, fills parameters before inserting if <b>ent</b> is not null
   * 
   * @param ent
   *          - Class-entity for filling parameters.
   * @param sql
   *          - SQL code.
   * @return {@code true} if code executed successfully or {@code false} otherwise
   * @throws DMLException
   *           - if an error has occurred or some problems with data format were found
   */
  protected <E> boolean execute(E ent, String sql) throws DMLException {
    Connection c = null;
    NamedParameterStatement ps = null;
    try {
      c = ds.getConnection();
      ps = new NamedParameterStatement(c, sql);

      if (ent != null) {
        rBuilder.validate(ent);
        rBuilder.fillInsertRequest(ps, ent);
      }

      return ps.execute();
    } catch (SQLException ex) {
      LOG.error(ex);
      throw new DMLException("Can't build the report", ex);
    } catch (ValidationException ex) {
      LOG.warn(ex);
      throw new DMLException("Data format error", ex);
    } finally {
      close(ps);
      close(c);
    }
  }

  // util methods

  /**
   * Util-method
   * 
   * @param ent
   * @param ps
   * @throws SQLException
   */
  protected <E> void setAfterInsert(E ent, NamedParameterStatement ps, EntBuilder<E> builder)
      throws SQLException {
    ResultSet gRs = ps.getGeneratedKeys();

    if (gRs.next()) {
      builder.setAfterInsert(ent, gRs);
    } else {
      LOG.warn("Nothing was generating");
    }
  }

  /**
   * Util-method for quietly closing the Connection.
   * 
   * @param c
   */
  protected void close(Connection c) {
    try {
      if (c != null) {
        c.close();
        LOG.debug("Connection has been closed");
      }
    } catch (SQLException ex) {
      LOG.error(ex);
    }
  }

  /**
   * Util-method for quietly closing the ResultSet.
   * 
   * @param rs
   */
  protected void close(ResultSet rs) {
    try {
      if (rs != null) {
        rs.close();
        LOG.debug("ResultSet has been closed");
      }
    } catch (SQLException ex) {
      LOG.error(ex);
    }
  }

  /**
   * Util-method for quietly closing the NamedParameterStatement.
   * 
   * @param nps
   */
  protected void close(NamedParameterStatement nps) {
    try {
      if (nps != null) {
        nps.close();
        LOG.debug("Statement has been closed");
      }
    } catch (SQLException ex) {
      LOG.error(ex);
    }
  }

  protected void close(PreparedStatement nps) {
    try {
      if (nps != null) {
        nps.close();
        LOG.debug("Statement has been closed");
      }
    } catch (SQLException ex) {
      LOG.error(ex);
    }
  }

  /**
   * Util-method for quietly rollbacking a transaction.
   * 
   * @param c
   */
  protected void rollback(Connection c) {
    try {
      if (c != null) {
        c.rollback();
        LOG.debug("Transaction has been rollbacked");
      }
    } catch (SQLException ex) {
      LOG.error(ex);
    }
  }

  protected <E> E getSingleItem(List<E> list) {
    if (!list.isEmpty()) {
      return list.get(0);
    } else {
      return null;
    }
  }

  protected <E> E getSingleItemWithException(List<E> list) throws DMLException {
    if (!list.isEmpty()) {
      return list.get(0);
    } else {
      throw new DMLException("Empty list");
    }
  }
}
