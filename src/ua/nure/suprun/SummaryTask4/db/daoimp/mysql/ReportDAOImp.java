package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DateBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.DefaultPrimitiveEntBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.EntBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.ReportView;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.ReqName;
import ua.nure.suprun.SummaryTask4.db.dao.domains.ReportDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * ReportDAO implementation for MySQL.
 * 
 * @author B.Suprun
 * 
 */
public class ReportDAOImp extends BasicDAOImp<ReportView> implements ReportDAO {

  private static final Logger LOG = Logger.getLogger(ReportDAOImp.class);

  public ReportDAOImp(EntBuilder<ReportView> builder, DataSource ds) {
    super(builder, ds);
  }

  @Override
  public Timestamp createReport() throws DMLException {
    LOG.info("Creating a report");
    
    EntBuilder<Timestamp> b = new DefaultPrimitiveEntBuilder<>(Timestamp.class);
    List<Timestamp> result = invokeSelect(ReqName.RV_CREATE, ReportView.class, b);

    return getSingleItem(result);
  }

  @Override
  public List<ReportView> showReport(Date date, University univ) throws DMLException {
    LOG.info("Show a report");
    LOG.debug("University is: " + univ + ", date is: " + date);
    String sql = rBuilder.getStoredRequest(ReportView.class, ReqName.RV_SHOW);

    ReportView rv = new ReportView();
    rv.setDate(date);
    rv.setUniversityName(univ.getName());

    LOG.debug("ReportView is: " + rv);

    return invokeSelect(rv, sql);
  }

  @Override
  public List<Date> getDates(University univ) throws DMLException {
    LOG.info("Show dates");
    LOG.debug("University is: " + univ);
   
    EntBuilder<Date> b = new DateBuilder();
    
    return invokeSelect(ReqName.RV_DATES, ReportView.class, b, univ.getName());
  }

  @Override
  public List<ReportView> getInsertedEnrolees(Timestamp time) throws DMLException {
    LOG.info("Show enrolles");
    LOG.debug("Timestamp is: " + time);
    
    return invokeSelect(ReqName.RV_GET_BY_TIMESTAMP, ReportView.class, builder, time);
  }

  @Override
  public List<ReportView> getEnrolleeApplicationsStatus(int uId) throws DMLException {
    LOG.debug("Start invoke");

    return invokeSelect(ReqName.RV_ENROLLEES_APPLICATIONS,ReportView.class, builder, uId);
  }

}
