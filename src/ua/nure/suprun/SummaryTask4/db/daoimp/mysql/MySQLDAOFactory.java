package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.ReportBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.RoleBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.SubjectBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.UniversityBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.UserBuilder;
import ua.nure.suprun.SummaryTask4.db.dao.DAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.ReportDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.RoleDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.SubjectDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UniversityDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * DAOFactory for MySQL DBMS.
 * 
 * @author B.Suprun
 * 
 */
public final class MySQLDAOFactory implements DAOFactory {
  
  private static final Logger LOG = Logger.getLogger(MySQLDAOFactory.class);
  
	/**
	 * Name of Tomcat connection pool.
	 */
	private static final String RESOURCE_NAME = "java:/comp/env/jdbc/mydb";

	private DataSource ds;

	/**
	 * Singleton
	 */
	private static MySQLDAOFactory self;

	/**
	 * Looking for JNI connection pool.
	 * 
	 * @throws DBException
	 *             - If there is no such JNI is found
	 */
	private MySQLDAOFactory() throws DBException {
	  
		try {
		  
			Context c = new InitialContext();
			ds = (DataSource) c.lookup(RESOURCE_NAME);
			
			LOG.debug("DataSource was found");
		} catch (NamingException ex) {
			LOG.fatal("No named resource was found", ex);
			throw new DBException("Can't connect to database", ex);
		}
	}

	/**
	 * Singleton
	 * 
	 * @return MySQLDAOFactory instance.
	 * @throws DBException
	 */
	public static synchronized DAOFactory newInstance() throws DBException {
		if (self == null) {
			self = new MySQLDAOFactory();
			LOG.debug("MySQLFactory was created");
		}

		return self;
	}

	@Override
	public DepartmentDAO getDepartmentDAO() {
		return new DepartmentDAOImp(new DepartmentBuilder(), ds);
	}

	@Override
	public EnrolleeDAO getEnrolleeDAO() {
		return new EnrolleeDAOImp(new EnrolleeBuilder(), ds);
	}

	@Override
	public ReportDAO getReportDAO() {
		return new ReportDAOImp(new ReportBuilder(), ds);
	}

	@Override
	public RoleDAO getRoleDAO() {
		return new RoleDAOImp(new RoleBuilder(), ds);
	}

	@Override
	public SubjectDAO getSubjectDAO() {
		return new SubjectDAOImp(new SubjectBuilder(), ds);
	}

	@Override
	public UniversityDAO getUniversityDAO() {
		return new UniversityDAOImp(new UniversityBuilder(), ds);
	}

	@Override
	public UserDAO getUserDAO() {
		return new UserDAOImp(new UserBuilder(), ds);
	}

}
