package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.AuthBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.BlacklistBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.DefaultPrimitiveEntBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.EntBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.UniversityBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Authorization;
import ua.nure.suprun.SummaryTask4.bl.entity.Blacklist;
import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.ReqName;
import ua.nure.suprun.SummaryTask4.db.NamedParameterStatement;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * User DAO implementation for mysql.
 * 
 * @author B.Suprun
 * 
 */
public class UserDAOImp extends BasicDAOImp<User> implements UserDAO {

  private static final Logger LOG = Logger.getLogger(UserDAOImp.class);

  public UserDAOImp(EntBuilder<User> builder, DataSource ds) {
    super(builder, ds);
  }

  @Override
  public User login(String login, String pass) throws DMLException {
    LOG.info("Start loging in");
    LOG.debug("Login: " + login);

    User tempUser = new User();
    String sql = rBuilder.getStoredRequest(User.class, ReqName.USER_LOGIN);

    LOG.debug("SQL retriewed: " + sql);

    tempUser.setEmail(login);
    tempUser.setPass(pass);

    List<User> users = invokeSelect(tempUser, sql);

    LOG.debug("User found: " + users);

    return getSingleItem(users);
  }

  @Override
  public User get(String login) throws DMLException {
    LOG.info("Retriewing user by login");
    LOG.debug("Login: " + login);

    List<User> users = invokeSelect(ReqName.USER_BY_LOGIN, User.class, builder, login);

    return getSingleItem(users);
  }

  @Override
  public User get(int id) throws DMLException {
    LOG.info("Retriewing user by id");
    LOG.debug("id: " + id);

    List<User> users = invokeSelect(ReqName.USER_BY_ID, User.class, builder, id);

    return getSingleItem(users);
  }

  @Override
  public User get(Authorization aut) throws DMLException {
    LOG.info("Retriewing user by authorization code");
    LOG.debug("Auth: " + aut);

    User user = null;
    try {
      String sql = rBuilder.createDeleteByPKRequest(aut);
      LOG.debug("SQL built: " + sql);

      if (invokeDelete(aut, sql)) {
        LOG.info("User by code found.");
        user = get(aut.getUserId());
      } else {
        LOG.info("There is no such code");
      }
    } catch (DMLException ex) {
      LOG.warn(ex, ex);
      throw new DMLException("There is no such authorization key", ex);
    }
    return user;
  }

  @Override
  public boolean block(int toBeBlocked) throws DMLException {
    LOG.info("Start blocking user");

    Blacklist bl = new Blacklist();
    bl.setId(toBeBlocked);

    String sql = rBuilder.createInsertRequest(bl);
    LOG.debug("SQL built: " + sql);

    return invokeSimpleInsert(bl, sql, new BlacklistBuilder());
  }

  @Override
  public boolean unblock(int toBeUnblocked) throws DMLException {
    LOG.info("Start unblocking user");

    Blacklist bl = new Blacklist();
    bl.setId(toBeUnblocked);

    String sql = rBuilder.createDeleteByPKRequest(bl);
    LOG.debug("SQL built: " + sql);

    return invokeDelete(bl, sql);
  }

  @Override
  public boolean restore(Authorization aut) throws DMLException {
    LOG.info("Start restoring user");

    String sql = rBuilder.createInsertRequest(aut);
    LOG.debug("SQL built: " + sql);

    return invokeSimpleInsert(aut, sql, new AuthBuilder());
  }

  @Override
  public boolean changeLang(User user) throws DMLException {
    LOG.info("Changing user's lang to " + user.getLang());

    String sql = rBuilder.getStoredRequest(User.class, ReqName.USER_LANG);
    LOG.debug("SQL retriewed: " + sql);

    return invokeSimpleUpd(user, sql);
  }

  @Override
  public boolean registryEnrolle(User user, Enrollee enr) throws DMLException {
    LOG.info("Start registry enrollee");

    Connection c = null;
    boolean result;
    try {
      c = ds.getConnection();
      c.setAutoCommit(false);

      // Generating insert requests
      String enrSql = rBuilder.createInsertRequest(enr);
      LOG.debug("Enrollee SQL insert is: " + enrSql);
      String userSql = rBuilder.createInsertRequest(user);
      LOG.debug("User SQL insert is: " + userSql);

      // Creating Statements for both entities
      NamedParameterStatement enrPs = new NamedParameterStatement(c, enrSql);
      NamedParameterStatement userPs = new NamedParameterStatement(c, userSql);

      // Insert the user
      result = invokeSimpleInsert(user, userPs, builder);
      LOG.debug("User inserted = " + result);
      enr.setId(user.getId());

      if (result) {
        LOG.info("The user has been inserted");

        // Insert the enrollee
        result &= invokeSimpleInsert(enr, enrPs, new EnrolleeBuilder());
        LOG.debug("Enrollee inserted = " + result);
      }

      if (!result) {
        LOG.info("Something went wrong");
        throw new SQLException("Something went wrong");
      }

      c.commit();
      LOG.info("Transaction commited");
      return result;
    } catch (SQLException ex) {
      LOG.error(ex);
      rollback(c);

      throw new DMLException("Can't registry Enrollee", ex);
    } finally {
      close(c);
    }
  }

  @Override
  public boolean inviteAdmin(User user, Authorization auth) throws DMLException {
    LOG.info("Start inviting user");

    Connection c = null;
    boolean result;
    Blacklist bl = new Blacklist();
    try {
      c = ds.getConnection();
      c.setAutoCommit(false);

      // Generating insert requests
      String autSql = rBuilder.createInsertRequest(auth);
      LOG.debug("Auth SQL insert is: " + autSql);
      String userSql = rBuilder.createInsertRequest(user);
      LOG.debug("User SQL insert is: " + userSql);
      String blSql = rBuilder.createInsertRequest(bl);
      LOG.debug("User SQL insert is: " + blSql);

      // Creating Statements for both entities
      NamedParameterStatement autPs = new NamedParameterStatement(c, autSql);
      NamedParameterStatement userPs = new NamedParameterStatement(c, userSql);
      NamedParameterStatement blPs = new NamedParameterStatement(c, blSql);

      // Insert the user
      result = invokeSimpleInsert(user, userPs, builder);
      LOG.debug("User inserted = " + result);
      auth.setUserId(user.getId());
      bl.setId(user.getId());

      if (result) {
        LOG.info("The user has been inserted");

        // Insert the authorization info
        result &= invokeSimpleInsert(auth, autPs, new AuthBuilder());
        LOG.debug("Authorization inserted = " + result);

        LOG.debug("Insert user to BLACKLIST for security");
        result &= invokeSimpleInsert(bl, blPs, new BlacklistBuilder());
      }

      if (!result) {
        LOG.info("Something went wrong");
        throw new SQLException("Something went wrong");
      }

      c.commit();
      LOG.info("Transaction commited");
      return result;
    } catch (SQLException ex) {
      LOG.error(ex, ex);
      rollback(c);

      throw new DMLException("Can't invite User", ex);
    } finally {
      close(c);
    }
  }

  @Override
  public List<User> search(String cond) throws DMLException {
    String searchCond = "%" + cond + "%";
    LOG.debug("Start searching for condition --> " + cond);

    return invokeSelect(ReqName.USER_SEARCH, User.class, builder, searchCond);
  }

  @Override
  public List<User> getAllEnrolees() throws DMLException {
    LOG.debug("Start searching all users");

    return invokeSelect(ReqName.USER_GET_ALL_ENROLLEES, User.class, builder);
  }

  @Override
  public boolean registryAdmin(User user, University un) throws DMLException {
    LOG.info("Start registry user");

    Connection c = null;
    boolean result;
    Blacklist bl = new Blacklist();
    bl.setId(user.getId());
    try {
      c = ds.getConnection();
      c.setAutoCommit(false);

      // Generating insert requests
      String unSql = rBuilder.createInsertRequest(un);
      LOG.debug("Univ SQL insert is: " + unSql);
      String userSql = rBuilder.createUpdateRequest(user);
      LOG.debug("User SQL insert is: " + userSql);
      String blSql = rBuilder.createDeleteByPKRequest(bl);
      LOG.debug("User SQL insert is: " + blSql);

      // Creating Statements for both entities
      NamedParameterStatement unPs = new NamedParameterStatement(c, unSql);
      NamedParameterStatement userPs = new NamedParameterStatement(c, userSql);
      NamedParameterStatement blPs = new NamedParameterStatement(c, blSql);

      // Update the user
      result = invokeSimpleUpd(user, userPs);
      LOG.debug("User updated = " + result);
      un.setAdmin(user.getId());

      if (result) {
        LOG.info("The user has been upd");

        // Insert new university
        result &= invokeSimpleInsert(un, unPs, new UniversityBuilder());
        LOG.debug("University has been inserted = " + result);

        LOG.debug("Unblock the user");
        result &= invokeDelete(bl, blPs);
      }

      if (!result) {
        LOG.info("Something went wrong");
        throw new SQLException("Something went wrong");
      }

      c.commit();
      LOG.info("Transaction commited");

      return result;
    } catch (SQLException ex) {
      LOG.error(ex, ex);
      rollback(c);
      throw new DMLException("Can't registry new Administrator", ex);
    } finally {
      close(c);
    }
  }

  @Override
  public boolean isBlocked(int user) throws DMLException {
    LOG.debug("Start checking");
    Blacklist bl = new Blacklist();
    bl.setId(user);

    String sql = rBuilder.createSelectByPKRequest(bl);
    LOG.trace("SQL is: " + sql);

    return !invokeSelect(bl, sql, new BlacklistBuilder()).isEmpty();
  }

  @Override
  public List<User> getBlockedEnrollees() throws DMLException {
    LOG.debug("Start");
    String sql = rBuilder.getStoredRequest(User.class, ReqName.USER_GET_BLOCKED_ENROLLEE);
    LOG.debug("SQL is: " + sql);

    return invokeSelect(null, sql);
  }

  @Override
  public int getAppCnt(int userId) throws DMLException {
    EntBuilder<Integer> eBuilder = new DefaultPrimitiveEntBuilder<>(Integer.class);
    
    List<Integer> result = invokeSelect(ReqName.USER_APP_CNT, User.class, eBuilder, userId);
    
    return getSingleItem(result);
  }
}
