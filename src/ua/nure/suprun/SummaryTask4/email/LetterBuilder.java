package ua.nure.suprun.SummaryTask4.email;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

/**
 * Main interface for building letters.
 * 
 * @author B.Suprun
 * 
 */
public abstract class LetterBuilder<T> implements Iterable<LetterBuilder<T>.Container> {

  protected List<T> container;

  /**
   * Letter text
   */
  protected String letter;

  /**
   * Creates new letter builder.
   * 
   * @param c
   *          List of beans for which letters will be built.
   * @param name
   *          - Init parameter name
   * @param ct
   *          - for access to ServletContext
   * @throws IOException
   *           if an error with loading letter has occurred.
   */
  public LetterBuilder(List<T> c, String name, HttpServletRequest ct) throws IOException {
    container = new ArrayList<T>(c);
    letter = loadLetter(name, ct);
  }

  /**
   * Wrapper class.
   * 
   * @author B.Suprun
   * 
   */
  public final class Container {
    /**
     * Current entity.
     */
    private T ent;

    private Container(T ent) {
      this.ent = ent;
    }

    /**
     * Returns text of letter.
     * 
     * @return
     */
    public String getText() {
      return LetterBuilder.this.getText(ent);
    }

    /**
     * Returns receiver address.
     * 
     * @return
     */
    public String getEmail() {
      return LetterBuilder.this.getEmail(ent);
    }
  }

  @Override
  public Iterator<Container> iterator() {
    return new LetterIterator();
  }

  /**
   * Iterator implementation over letters.
   * 
   * @author B.Suprun
   * 
   */
  private class LetterIterator implements Iterator<Container> {
    /**
     * Just delegate to container.
     */
    private Iterator<T> it = container.iterator();

    @Override
    public boolean hasNext() {
      return it.hasNext();
    }

    @Override
    public Container next() {
      return new Container(it.next());
    }

    @Override
    public void remove() {
      it.remove();
    }

  }

  protected static String loadLetter(String name, HttpServletRequest ct) throws IOException {
    return new LatterReader().loadLetter(name, ct);
  }

  /**
   * Returns text of letter for specified entity.
   * 
   * @param ent
   *          - Current entity.
   * @return
   */
  protected abstract String getText(T ent);

  /**
   * Returns email address for specified entity.
   * 
   * @param ent
   *          - Current entity.
   * @return
   */
  protected abstract String getEmail(T ent);

}
