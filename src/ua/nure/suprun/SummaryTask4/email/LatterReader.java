package ua.nure.suprun.SummaryTask4.email;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * Class provides loading html-letter template from file.
 * 
 * @author B.Suprun
 * 
 */
public class LatterReader {

  private static final Logger LOG = Logger.getLogger(LatterReader.class);

  /**
   * Loads letter from file using path stored in as init parameter in ServletContext with name
   * <b>paramName</b>
   * 
   * @param paramName
   *          - Init-parameter name.
   * @param request
   *          - {@link HttpServletRequest} instance.
   * @return - Letter template as string.
   * @throws IOException
   *           - If an error with loading has occurred.
   */
  public String loadLetter(String paramName, HttpServletRequest request) throws IOException {
    ServletContext context = request.getServletContext();
    LOG.debug("Start load a file");
    String path = context.getInitParameter(paramName);
    String realPath = context.getRealPath(path);
    LOG.debug("The file real path is " + realPath);

    return loadLetter(realPath);

  }

  /**
   * Loads letter template from file responds to placed path.
   * 
   * @param path
   *          - Path to file.
   * @return - File as string.
   * @throws IOException
   *           If an error with loading file has occurred.
   */
  public String loadLetter(String path) throws IOException {
    LOG.debug("Start load a file");
    LOG.debug("The file real path is " + path);

    InputStreamReader in = null;
    Scanner s = null;
    StringBuilder result = new StringBuilder();
    try {
      in = new InputStreamReader(new FileInputStream(path), "utf-8");
      LOG.trace("Input stream created");
      s = new Scanner(in);

      while (s.hasNextLine()) {
        result.append(s.nextLine());
      }

      LOG.trace("Finish");
    } finally {
      if (s != null) {
        s.close();
      }
      close(in);
    }
    return result.toString();

  }

  private void close(InputStreamReader in) {
    try {
      if (in != null) {
        in.close();
      }
      LOG.trace("Stream was closed");
    } catch (IOException ex) {
      LOG.fatal(ex);
    }
  }

}
