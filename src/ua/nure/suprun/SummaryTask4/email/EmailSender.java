package ua.nure.suprun.SummaryTask4.email;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadFactory;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.log4j.Logger;

/**
 * Class provides email delivery.
 * 
 * @author B.Suprun
 * 
 */
public final class EmailSender {

  private static final Logger LOG = Logger.getLogger(EmailSender.class);

  /**
   * Login for authorization on smtp server
   */
  private static String username;

  private static final int DEFAULT_POOL_SIZE = 4;

  private static ExecutorService pool = null;

  /**
   * Pass for authorization on smtp server
   */
  private static String password;

  /**
   * Properties
   */
  private static Properties props;

  /**
   * Letter type
   */
  private static final String SUBTYPE = "text/html; charset=utf-8";

  /**
   * Session
   */
  private static Session session;

  private EmailSender() {
  }

  /**
   * Configure email delivery.
   * 
   * @param confFile
   *          - Configuration Properties file
   * @throws IOException
   *           If an error with reading properties has occurred.
   */
  public static synchronized void configure(String confFile) throws IOException {

    configureProperties(confFile);
    configureThreadPool();

    username = props.getProperty("user.email");
    password = props.getProperty("user.pass");

    session = Session.getInstance(props, new MyAuthenticator());

    LOG.debug("Email sending configured on " + username);
  }

  /**
   * Loads properties from configuration file.
   * 
   * @param confFile
   * @throws IOException
   */
  private static void configureProperties(String confFile) throws IOException {
    InputStream in = null;
    try {
      in = new FileInputStream(confFile);
      LOG.debug("Start configure email sender");
      props = new Properties();
      props.load(in);

      LOG.debug("Properties loaded");
      props.list(System.out);

    } finally {
      if (in != null) {
        in.close();
      }
    }

  }

  /**
   * Configuration of thread pool size.
   * 
   * @return
   */
  private static int configureThreadPoolSize() {
    int poolSize = DEFAULT_POOL_SIZE;
    try {
      String n = props.getProperty("send.threads", "0");
      poolSize = Integer.parseInt(n);
      if (poolSize < 1) {
        poolSize = DEFAULT_POOL_SIZE;
      }

      LOG.trace("Pool size set to --> " + poolSize);
    } catch (Exception ex) {
      LOG.error(ex);
      LOG.trace("Setting pool size to default");

      poolSize = DEFAULT_POOL_SIZE;
    }

    return poolSize;
  }

  /**
   * Configures thread pool.
   */
  private static void configureThreadPool() {
    int poolSize = configureThreadPoolSize();

    pool = Executors.newFixedThreadPool(poolSize, new ThreadFactory() {
      
      // Sets all threads as daemons 
      public Thread newThread(Runnable r) {
        Thread t = new Thread(r);
        // Set to daemon
        t.setDaemon(true);
        return t;
      }
    
    });
  }

  public static boolean isConfigured() {
    return props != null && username != null && password != null && pool != null;
  }

  /**
   * Sends email letter in separate threads.
   * 
   * @param subject
   *          - Subject of email.
   * @param text
   *          - Text of email. HTML format also allowed.
   * @param toEmail
   *          - Address of recipient.
   */
  public static void send(String subject, String text, String toEmail) {
    new Worker(toEmail, subject, text).start();

  }

  /**
   * Deliver all emails using LetterBuilder.
   * 
   * @param subject
   *          - Email subject
   * @param builder
   *          - Letter builder.
   * @throws InterruptedException
   *           Pool exception
   */
  public static <T> void deliver(final String subject, LetterBuilder<T> builder)
      throws InterruptedException {

    for (LetterBuilder<T>.Container entry : builder) {
      pool.submit(new Worker(entry.getEmail(), subject, entry.getText()));
    }
  }

  /**
   * Sends mail in single thread.
   * 
   * @param subject
   *          - Subject of email.
   * @param text
   *          - Content of email. HTML also allowed.
   * @param toEmail
   *          - Address of recipient.
   */
  public static void sendMail(String subject, String text, String toEmail) {
    try {
      if (!isConfigured()) {
        LOG.fatal("Sender is not configured");
        throw new IllegalStateException("Sender is not configured");
      }

      MimeMessage message = new MimeMessage(session);
      LOG.debug("Prepare new message to --> " + toEmail);
      message.setFrom(new InternetAddress("Admin"));
      message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(toEmail));
      message.setSubject(subject);

      message.setContent(text, SUBTYPE);

      LOG.debug("Sending...");
      Transport.send(message);
    } catch (MessagingException ex) {
      LOG.fatal("Cant send message to --> " + toEmail, ex);
    }
  }

  /**
   * Password authenticator
   * 
   * @author B.Suprun
   * 
   */
  private static class MyAuthenticator extends Authenticator {

    @Override
    protected PasswordAuthentication getPasswordAuthentication() {
      return new PasswordAuthentication(username, password);
    }
  }

  /**
   * Class provides message sending in a separate thread.
   * 
   * @author B.Suprun
   * 
   */
  private static class Worker extends Thread {
    private String toEmail;

    private String subject;

    private String text;

    public Worker(String toEmail, String subject, String text) {
      this.toEmail = toEmail;
      this.subject = subject;
      this.text = text;
    }

    @Override
    public void run() {
      sendMail(subject, text, toEmail);
    }
  }
}
