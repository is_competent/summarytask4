package ua.nure.suprun.SummaryTask4.email;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import ua.nure.suprun.SummaryTask4.bl.entity.ReportView;

/**
 * Builder for applicants notification letters.
 * 
 * @author B.Suprun
 * 
 */
public class ReportLetterBuilder extends LetterBuilder<ReportView> {

  public ReportLetterBuilder(List<ReportView> c, HttpServletRequest ct) throws IOException {
    super(c, "delivery-email", ct);
  }

  @Override
  protected String getText(ReportView ent) {
    String name = ent.getEnrLastName() + " " + ent.getEnrName() + " " + ent.getEnrPatronimic();

    StringBuilder invites = new StringBuilder();
    invites.append(ent.getUniversityName())
    .append(" - ")
    .append(ent.getDepName())
    .append(", ")
    .append(ent.getStudyMode());
    
    return letter.replaceAll("\\{content\\}", invites.toString()).replaceAll("\\{name\\}", name);
  }
  
  @Override
  protected String getEmail(ReportView ent) {
    return ent.getUEmail();
  }
}
