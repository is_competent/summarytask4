package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.UniversityBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.UserBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.CommonRoles;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Class provides authorization for user that want to restore own password.
 * 
 * @author B.Suprun
 * 
 */
public class RestoreUserCommand extends DistributedCommand {

  private static final long serialVersionUID = -353097838302935740L;
  private static final Logger LOG = Logger.getLogger(RestoreUserCommand.class);

  /**
   * Redirect to restoring page.
   */
  @Override
  protected String processGet(HttpServletRequest req, HttpSession session) {
    User u = (User) session.getAttribute("restored");

    String forward = Path.COMMAND_ERROR;
    if (u == null) {
      LOG.trace("There is no user for restoring");
      session.setAttribute("emmesage", "No user for restoring");

    } else if (CommonRoles.isInvitedAdmin(u)) {
      LOG.trace("Invited admin found");
      forward = Path.PAGE_REGISTRY_ADMIN;

    } else {
      LOG.trace("Restoring not invited");
      forward = Path.PAGE_RESTORE;
    }

    LOG.debug("Command finished");
    return forward;
  }

  /**
   * Restoring the user.
   */
  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.info("Executing post method");
    String forward = Path.COMMAND_ERROR;
    if (performDaoPost(req, session)) {
      LOG.debug("User was restored");

      session.setAttribute("info", "Operation successfully performed");
      session.removeAttribute("restored");
      forward = Path.COMMAND_INFO;
    } else {
      LOG.debug("User was not restored");

      session.setAttribute("emessage", "Can't perform operation");
    }
    return forward;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    LOG.debug("Start querying to DB");

    User u = (User) session.getAttribute("restored");
    String newPass = req.getParameter("newPass");
    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();

    // No user for restoring
    if (u == null) {
      return false;
    }

    // Update invited user
    if (CommonRoles.isInvitedAdmin(u)) {
      University un = new UniversityBuilder().create(req);
      User newUser = new UserBuilder().create(req);

      newUser.setId(u.getId());
      newUser.setLang(u.getLang());
      newUser.setRole(u.getRole());
      newUser.setPass(newPass);

      LOG.trace("Invited admin found --> " + newUser);
      return dao.registryAdmin(newUser, un);
    } else {
      // Simply restore user
      LOG.trace("Restoring not invited, user --> " + u);

      u.setPass(newPass);
      return dao.update(u);
    }
  }

}
