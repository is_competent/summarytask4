package ua.nure.suprun.SummaryTask4.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.UserBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;

/**
 * Change personal info command.
 * 
 * @author B.Suprun
 * 
 */
public class ChangeInfoCommand extends Command {

  private static final long serialVersionUID = -7096221549477247997L;
  private static final Logger LOG = Logger.getLogger(ChangeInfoCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException, AppException {
    LOG.debug("Command starts");

    HttpSession session = request.getSession();
    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();
    User user = (User) session.getAttribute(SessionConst.USER);
    User newUser = new UserBuilder().create(request);

    newUser.setId(user.getId());
    newUser.setPass(user.getPass());
    newUser.setRole(user.getRole());
    newUser.setLang(user.getLang());

    LOG.trace("User is --> " + newUser);
    String forward = Path.COMMAND_ERROR;

    if (!dao.update(newUser)) {
      LOG.debug("Nothing was changed");
      session.setAttribute("emessage", "Can't update your personal information");
    } else {
      session.setAttribute(SessionConst.USER, newUser);
      LOG.trace("Set the session attribute: user --> " + newUser);

      session.setAttribute(SessionConst.USER_LANG, user.getLang());
      LOG.trace("Set the session attribute: userlang --> " + user.getLang());

      session.setAttribute("info", "Your info has been successfully updated");
      forward = Path.COMMAND_INFO;
    }

    LOG.debug("Command finished");
    return forward;
  }
}