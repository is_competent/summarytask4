package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.CommonRoles;
import ua.nure.suprun.SummaryTask4.bl.entity.Role;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;

/**
 * View profile command.
 * 
 * @author B.Suprun
 * 
 */
public class ProfileCommand extends Command {

  private static final long serialVersionUID = 6484533159635036025L;
  private static final Logger LOG = Logger.getLogger(ProfileCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    LOG.debug("Command starts");

    Role role = (Role) request.getSession().getAttribute(SessionConst.ROLE);
    String forward = Path.COMMAND_ERROR;

    if (CommonRoles.ADMIN.getRole().equals(role)) {
      forward = Path.PAGE_ADMIN;
    }
    if (CommonRoles.ROOT.getRole().equals(role)) {
      forward = Path.PAGE_ROOT;
    }
    if (CommonRoles.ENROLLEE.getRole().equals(role)) {
      forward = Path.PAGE_ENROLLEE;
    }

    LOG.debug("Command finished");
    return forward;
  }

}