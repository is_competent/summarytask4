package ua.nure.suprun.SummaryTask4.web.command;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.sql.Date;
import java.text.DecimalFormat;
import java.util.List;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DateBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.ReportView;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.ReportDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.BaseFont;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

/**
 * Get report service implementation.
 * 
 * @author B.Suprun
 * 
 */
public class GetReportCommand extends DistributedCommand {

  private static final long serialVersionUID = 218842567473110897L;

  private static final Logger LOG = Logger.getLogger(GetReportCommand.class);

  /**
   * Path to font with Unicode support.
   */
  private static final String TO_TIMES_NEW_ROMAN_FONT = "/WEB-INF/fonts/times.ttf";

  /**
   * Path to bold font with Unicode support.
   */
  private static final String TO_TIMES_NEW_ROMAN_BOLD_FONT = "/WEB-INF/fonts/timesbd.ttf";

  /**
   * Report head text
   */
  private static final String HEAD_TEXT;

  /**
   * Report title.
   */
  private static final String TITLE_TEXT;

  /**
   * Loaded bold font.
   */
  private static BaseFont bold;

  /**
   * Loaded font with Unicode support.
   */
  private static BaseFont basic;

  /**
   * Header style.
   */
  private static Font headFont;

  /**
   * Title style.
   */
  private static Font titleFont;

  /**
   * Table head cell style.
   */
  private static Font tableHeadFont;

  /**
   * Table cell style.
   */
  private static Font tableCellFont;

  /**
   * Table column names.
   */
  private static final String[] TABLE_HEADERS;

  static {

    /**
     * aka Ministerstvo osvity i nauky Ukrainy
     */
    HEAD_TEXT = "\u041c\u0438\u043d\u0438\u0441\u0442\u0435\u0440\u0441\u0442\u0432\u043e "
        + "\u043e\u0431\u0440\u0430\u0437\u043e\u0432\u0430\u043d\u0438\u044f "
        + "\u0438 \u043d\u0430\u0443\u043a\u0438 \u0423\u043a\u0440\u0430\u0438\u043d\u044b";
    /**
     * aka Vedomost'
     */
    TITLE_TEXT = "\u0412\u0435\u0434\u043e\u043c\u043e\u0441\u0442\u044c";

    TABLE_HEADERS = new String[] { "ID", "\u0424\u0430\u043c\u0438\u043b\u0438\u044f",
        "\u0418\u043c\u044f", "\u041e\u0442\u0447\u0435\u0441\u0442\u0432\u043e",
        "\u041a\u043e\u043d\u043a\u0443\u0440\u0441\u043d\u044b\u0439 \u0431\u0430\u043b",
        "\u0424\u0430\u043a\u0443\u043b\u044c\u0442\u0435\u0442",
        "\u0424\u043e\u0440\u043c\u0430 \u043e\u0431\u0443\u0447\u0435\u043d\u0438\u044f", "Email" };
  }

  /**
   * Start of service
   */
  @Override
  public String execute(HttpServletRequest req, HttpServletResponse response) throws AppException {
    LOG.debug("Start process post");

    LOG.debug("Configure fonts");

    // load fonts and configure fonts style.
    configureFonts(req.getServletContext());
    LOG.debug("Fonts are configured");

    LOG.debug("Load report");
    performDaoGet(req, req.getSession());

    LOG.debug("Finish process post");

    // Redirect to view
    return Path.PAGE_GET_PDF;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    try {
      LOG.debug("Start build report");

      /*
       * Administrator university
       */
      University u = (University) session.getAttribute(SessionConst.UNIVER);

      // Process required reports date.
      Date d = new DateBuilder().create(req);

      /*
       * Load report from DB
       */
      ReportDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getReportDAO();

      LOG.debug("Start make pdf report");

      /*
       * Creates pdf-report
       */
      byte[] pdf = makeReport(dao.showReport(d, u));
      
      // Request store
      req.setAttribute("bytes", pdf);
    } catch (DocumentException ex) {
      LOG.fatal(ex);
      
      throw new DBException("Can't build pdf", ex);
    }

    LOG.trace("Building ok");
  }

  /**
   * Builds report in pdf format using placed beans.
   * 
   * @param beans
   *          - Table items in report.
   * @return bytes of file.
   * @throws DocumentException
   *           if an error with building pdf has occurred.
   */
  private byte[] makeReport(List<ReportView> beans) throws DocumentException {
    ByteArrayOutputStream out = new ByteArrayOutputStream();
    Document document = new Document(PageSize.A4);
    PdfWriter.getInstance(document, out);

    document.open();
    final String universityName = beans.size() > 0 ? beans.get(0).getUniversityName() : " ";
    final Date date = beans.size() > 0 ? beans.get(0).getDate() : new Date(
        System.currentTimeMillis());

    // Create header
    makeHeader(document, universityName);

    // Create table
    PdfPTable table = makeTable();

    // Fill table
    fillTable(document, table, beans);

    // Insert table into document
    table.setComplete(true);
    document.add(table);

    // Create footer.
    makeFooter(document, date);

    LOG.debug("PDF created");
    document.close();
    return out.toByteArray();
  }

  /**
   * Creates header of report.
   * 
   * @param document
   *          - {@link DocumentException} instance.
   * @param univ
   *          - Name of university for which report is being created.
   * @throws DocumentException
   *           if an error with building header has occurred.
   */
  private void makeHeader(Document document, String univ) throws DocumentException {
    // create head
    Chunk headChunk = new Chunk(HEAD_TEXT, new Font(headFont));
    Paragraph p = new Paragraph(headChunk);
    p.setAlignment(Paragraph.ALIGN_CENTER);
    document.add(p);

    // create university title
    Chunk universityChunk = new Chunk(univ, titleFont);
    p = new Paragraph(universityChunk);
    p.setAlignment(Paragraph.ALIGN_CENTER);
    document.add(p);

    // create title
    Chunk titleChunk = new Chunk(TITLE_TEXT, titleFont);
    p = new Paragraph(titleChunk);
    p.setAlignment(Paragraph.ALIGN_CENTER);
    document.add(p);

    // spacing between title and table
    document.add(new Paragraph(" ", titleFont));
  }

  /**
   * Creates report table
   * 
   * @param document
   *          - {@link Document} instance.
   * @return {@link PdfPTable} instance.
   */
  private PdfPTable makeTable() {
    // New table with TABLE_HEADERS.length columns
    PdfPTable table = new PdfPTable(TABLE_HEADERS.length);

    // 1st row is head of table.
    table.setHeaderRows(1);
    table.setSplitRows(false);
    table.setComplete(false);

    // Fit table to page.
    table.setTotalWidth(550);
    table.setLockedWidth(true);

    // Add head cells
    for (String header : TABLE_HEADERS) {
      Phrase ph = new Phrase(header, tableHeadFont);
      table.addCell(ph);
    }

    return table;
  }

  /**
   * Inserts beans into report table.
   * 
   * @param document
   *          - {@link Document} instance.
   * @param table
   *          - {@link PdfPTable} instance.
   * @param beans
   *          - Entities that will be inserted into table.
   * @throws DocumentException
   *           if an error with building table has occurred
   */
  private void fillTable(Document document, PdfPTable table, List<ReportView> beans)
      throws DocumentException {

    for (ReportView rp : beans) {
      Phrase p = new Phrase(String.valueOf(rp.getUId()), tableCellFont);
      table.addCell(p);

      p = new Phrase(rp.getEnrLastName(), tableCellFont);
      table.addCell(p);

      p = new Phrase(rp.getEnrName(), tableCellFont);
      table.addCell(p);

      p = new Phrase(rp.getEnrPatronimic(), tableCellFont);
      table.addCell(p);

      p = new Phrase(new DecimalFormat("#.##").format(rp.getAvgMark()), tableCellFont);
      table.addCell(p);

      p = new Phrase(rp.getDepName(), tableCellFont);
      table.addCell(p);

      p = new Phrase(rp.getStudyMode(), tableCellFont);
      table.addCell(p);

      p = new Phrase(rp.getUEmail(), tableCellFont);
      table.addCell(p);

      document.add(table);
    }
    LOG.debug("Table was filled");
  }

  /**
   * Creates footer for report.
   * 
   * @param document
   *          - {@link Document} instance.
   * @param date
   *          - Date of building the report.
   * @throws DocumentException
   *           - if an error with building footer has occurred
   */
  private void makeFooter(Document document, Date date) throws DocumentException {
    // Spacing after table
    document.add(new Paragraph(" ", titleFont));

    // create head
    Chunk footerChunk = new Chunk("Дата: " + date, tableCellFont);
    Paragraph footerP = new Paragraph(footerChunk);
    footerP.setAlignment(Paragraph.ALIGN_RIGHT);

    document.add(footerP);
    LOG.trace("Footer was created");
  }

  /**
   * Configure fonts for report. <br>
   * Configured only once.
   * 
   * @param context
   *          - {@link ServletContext}
   */
  private static synchronized void configureFonts(ServletContext context) {
    try {
      // load fonts that is support unicode
      // if it is not loaded yet.
      if (bold == null || basic == null) {
        bold = loadTimesNewRomanBold(context);
        basic = loadTimesNewRoman(context);
        LOG.trace("Font was load");

        // configure fonts style.
        // Head fonts
        headFont = new Font(bold, 14);
        titleFont = new Font(bold, 14);

        // Table fonts
        tableHeadFont = new Font(bold, 8);
        tableCellFont = new Font(basic, 10);
      }
    } catch (IOException | DocumentException ex) {
      LOG.fatal("Can't configure fonts", ex);
    }
  }

  /**
   * Loads font.
   * 
   * @param context
   *          {@link ServletContext}
   * @return {@link BaseFont} instance
   * @throws DocumentException
   * @throws IOException
   *           - If an error with creating Stream has occurred
   */
  private static BaseFont loadTimesNewRoman(ServletContext context) throws DocumentException, IOException {
    String realPath = context.getRealPath(TO_TIMES_NEW_ROMAN_FONT);
    return BaseFont.createFont(realPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
  }

  /**
   * Loads bold font.
   * 
   * @param context
   *          {@link ServletContext}
   * @return {@link BaseFont} instance
   * @throws DocumentException
   * @throws IOException
   *           - If an error with creating Stream has occurred
   */
  private static BaseFont loadTimesNewRomanBold(ServletContext context) throws DocumentException,
      IOException {
    String realPath = context.getRealPath(TO_TIMES_NEW_ROMAN_BOLD_FONT);
    return BaseFont.createFont(realPath, BaseFont.IDENTITY_H, BaseFont.EMBEDDED);
  }

}