package ua.nure.suprun.SummaryTask4.web.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Search Enrollee command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class SearchEnrolleeCommand extends DistributedCommand {

  private static final long serialVersionUID = -5544934070404073208L;
  private static final Logger LOG = Logger.getLogger(SearchEnrolleeCommand.class);

  @Override
  public String execute(HttpServletRequest req, HttpServletResponse response) throws AppException {
    LOG.debug("Start process");
    performDaoGet(req, req.getSession());

    LOG.debug("Finish process");
    return Path.PAGE_ADMIN_ENROLLEES;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    String cond = req.getParameter("cond");
    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();

    List<User> u;
    Map<User, Integer> map = new HashMap<User, Integer>();
    if (cond == null || cond.isEmpty()) {
      // load all users
      u = dao.getAllEnrolees();
    } else {
      // search with condition
      u = dao.search(cond);
    }
    
    
    for (User user: u) {
      map.put(user, dao.getAppCnt(user.getId()));
    }
    req.setAttribute("enrollees", map);
    LOG.debug("Users load");
  }
}