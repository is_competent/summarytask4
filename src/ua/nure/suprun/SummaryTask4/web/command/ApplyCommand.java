package ua.nure.suprun.SummaryTask4.web.command;

import java.io.Serializable;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeDeptBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeDept;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Apply command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class ApplyCommand extends DistributedCommand implements Serializable {

  private static final long serialVersionUID = -2944643994956355378L;
  private static final Logger LOG = Logger.getLogger(ApplyCommand.class);
  
  /**
   * Only post-method is allowed
   */
  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.debug("Start process post");
    String forward = Path.COMMAND_ERROR;

    if (performDaoPost(req, session)) {
      LOG.debug("Succesfully applied to department");
      
      session.setAttribute("info", "You are succesfully applied to department");
      forward = Path.COMMAND_INFO;
    } else {
      LOG.debug("Can't apply to department");
      
      session.setAttribute("emessage", "Can't apply to department");
    }

    LOG.debug("Finish process post");
    return forward;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    LOG.debug("Start apply user");
    
    User u = (User) session.getAttribute(SessionConst.USER);
    LOG.debug("User is --> " + u);
    
    EnrolleeDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getEnrolleeDAO();
    EnrolleeDept ed = new EnrolleeDeptBuilder().create(req);;
    
    ed.setEnId(u.getId());
    
    LOG.debug("EnrolleDept is --> " + ed);
    return dao.applyDep(ed);
  }
}