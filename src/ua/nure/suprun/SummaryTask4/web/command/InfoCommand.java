package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;

/**
 * Info command.
 * 
 * @author B.Suprun
 * 
 */
public class InfoCommand extends Command {

  private static final long serialVersionUID = 7662446968878853149L;
  private static final Logger LOG = Logger.getLogger(InfoCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    LOG.debug("Command starts");

    LOG.debug("Command finished");
    return Path.PAGE_INFO;
  }

}