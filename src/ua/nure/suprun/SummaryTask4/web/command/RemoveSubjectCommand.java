package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.SubjectDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Remove subject command. Async.
 * 
 * @author B.Suprun
 * 
 */
public class RemoveSubjectCommand extends DistributedCommand {

  private static final long serialVersionUID = 3642012962428902712L;
  private static final Logger LOG = Logger.getLogger(RemoveSubjectCommand.class);

  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.trace("Start handle post-request");

    try {
      if (performDaoPost(req, session)) {
        LOG.trace("Subject was removed ");

        // Update subjects in application context
        performDaoGet(req, session);
        session.setAttribute("jsonStatus", 200);
      } else {
        LOG.trace("Subject was not removed");

        throw new DBException("Subject not was removed");
      }
    } catch (Exception ex) {
      LOG.fatal(ex);

      String msg = "Nothing was changed";
      if (ex instanceof AppException) {
        msg = ex.getMessage();
      }
      session.setAttribute("jsonStatus", 400);
      session.setAttribute("emessage", msg);
    }

    LOG.trace("Done");
    return Path.COMMAND_GET_JSON;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    LOG.trace("Start remove the subject");
    RequestParser p = RequestParser.getParser(req);
    SubjectDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getSubjectDAO();
    Subject s = new Subject();

    s.setId(p.getInt("id"));
    LOG.trace("Subject is --> " + s);

    return dao.removeSubject(s);
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    LOG.info("Start retriewe subjects");
    SubjectDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getSubjectDAO();

    req.getServletContext().setAttribute("subjects", dao.getAll());
  }

}