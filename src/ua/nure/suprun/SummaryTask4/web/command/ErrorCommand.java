package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;

/**
 * Error command.
 * 
 * @author B.Suprun
 * 
 */
public class ErrorCommand extends Command {

  private static final long serialVersionUID = 4039046705679006322L;
  private static final Logger LOG = Logger.getLogger(ErrorCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response) {
    LOG.debug("Command starts");

    LOG.debug("Command finished");
    return Path.PAGE_ERROR_PAGE;
  }

}