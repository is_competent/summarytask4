package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;

/**
 * No command.
 * 
 * @author B.Suprun
 * 
 */
public class NoCommand extends Command {

	private static final long serialVersionUID = 1468999010327122404L;
  private static final Logger LOG = Logger.getLogger(NoCommand.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		LOG.debug("Command starts");

		String errorMessage = "No such command";
		request.getSession().setAttribute(SessionConst.ERROR, errorMessage);
		LOG.error("Set the request attribute: errorMessage --> " + errorMessage);

		LOG.debug("Command finished");
		return Path.COMMAND_ERROR;
	}

}