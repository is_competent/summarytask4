package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Create department command.
 * 
 * @author B.Suprun
 * 
 */
public class CreateDepartmentCommand extends DistributedCommand {

  private static final long serialVersionUID = 3009114241688621236L;
  private static final Logger LOG = Logger.getLogger(CreateDepartmentCommand.class);

  /**
   * Only POST is allowed.
   */
  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) {
    LOG.trace("Start handle post-request");
    try {
      if (performDaoPost(req, session)) {
        Department d = (Department) req.getAttribute("tempDep");
        LOG.trace("Department created --> " + d);

        session.setAttribute("jsonId", d.getId());
        session.setAttribute("jsonStatus", 200);
      } else {
        LOG.trace("Department is not created");
        
        throw new AppException("Department is not created");
      }
    } catch (Exception ex) {
      LOG.fatal(ex);
      
      String msg = "Nothing was changed";
      if (ex instanceof AppException) {
        msg = ex.getMessage();
      }
      session.setAttribute("jsonStatus", 400);
      session.setAttribute("emessage", msg);
    } 

    LOG.trace("Done");
    return Path.COMMAND_GET_JSON;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    University un = (University) session.getAttribute(SessionConst.UNIVER);
    LOG.debug("University is " + un);
    
    DepartmentDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getDepartmentDAO();
    Department d = new DepartmentBuilder().create(req);
    d.setUniversity(un.getId());

    LOG.debug("Try create new department --> " + d);
    req.setAttribute("tempDep", d);

    return dao.createNew(d);
  }

}