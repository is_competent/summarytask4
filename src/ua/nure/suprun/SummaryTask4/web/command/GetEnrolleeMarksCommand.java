package ua.nure.suprun.SummaryTask4.web.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeMark;
import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Get enrollee marks command.
 * 
 * @author B.Suprun
 * 
 */
public class GetEnrolleeMarksCommand extends DistributedCommand {
	
  private static final long serialVersionUID = -131895689147483241L;
  private static final Logger LOG = Logger.getLogger(GetEnrolleeMarksCommand.class);
	
	@Override
	protected String processGet(HttpServletRequest req, HttpSession session)
			throws AppException {
		LOG.debug("Start proccess");
		performDaoGet(req, session);
		
		LOG.debug("Command finish");
		return Path.PAGE_MARKS;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	protected void performDaoGet(HttpServletRequest req, HttpSession session)
			throws DBException {
		LOG.debug("Start extract marks");
		
		Enrollee enr = (Enrollee) session.getAttribute(SessionConst.ENR);
		LOG.debug("Enrollee is --> " + enr);
		
		EnrolleeDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getEnrolleeDAO();
		List<EnrolleeMark> result = dao.getMarks(enr);
		List<Subject> subs = (List<Subject>) req.getServletContext().getAttribute("subjects"); 
		
		// Set name of subject for each item
		for (EnrolleeMark item: result) {
			Subject s =  new Subject();
		
			s.setId(item.getSubjId());
			s = subs.get(subs.indexOf(s));
			
			item.setSubjName(s.getName());
		}
		
		LOG.debug("DAO finish");
		session.setAttribute("marks", result);
	}
}