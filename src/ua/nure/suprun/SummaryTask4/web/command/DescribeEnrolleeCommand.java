package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Describe enrollee command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class DescribeEnrolleeCommand extends DistributedCommand {

  private static final long serialVersionUID = 2807660264830920508L;
  private static final Logger LOG = Logger.getLogger(DescribeEnrolleeCommand.class);

  @Override
  public String execute(HttpServletRequest req, HttpServletResponse response) throws AppException {
    LOG.debug("Start process");
    performDaoGet(req, req.getSession());

    LOG.debug("Finish process");
    return Path.PAGE_ADMIN_DESCRIBE_ENROLLEE;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    int id = RequestParser.getParser(req).getInt("id");
    LOG.debug("Describe user --> " + id);

    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();
    EnrolleeDAO eDao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getEnrolleeDAO();

    req.setAttribute("enr", eDao.get(id));
    req.setAttribute("user", dao.get(id));
    req.setAttribute("isBlocked", dao.isBlocked(id));

    LOG.debug("User loaded");
  }
}