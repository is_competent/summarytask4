package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.AuthBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Authorization;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;

/**
 * Class provides authorization for user that want to restore own password.
 * 
 * @author B.Suprun
 * 
 */
public class AuthorizateCommand extends Command {

  private static final long serialVersionUID = 7464036606410018482L;
  private static final Logger LOG = Logger.getLogger(AuthorizateCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws AppException {

    LOG.info("Start executing command");
    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();

    // Retrieve authorization info
    Authorization a = new AuthBuilder().create(request);
    LOG.trace("Authorization code is: " + a);

    User user = dao.get(a);
    String forward = Path.COMMAND_ERROR;

    if (user != null) {
      LOG.trace("User found " + user);

      request.getSession().setAttribute("restored", user);
      request.getSession().setAttribute(SessionConst.USER_LANG, user.getLang());

      forward = Path.COMMAND_RESTORE;
    } else {
      LOG.trace("User is not found, aId: " + a.getId());

      request.getSession().setAttribute("emessage", "Brooken link");
    }

    LOG.info("Command executed");
    return forward;
  }

}
