package ua.nure.suprun.SummaryTask4.web.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Get admin university departments command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class GetAdminUniversityDepartmentsCommand extends DistributedCommand {

  private static final long serialVersionUID = -3862056867710879083L;
  private static final Logger LOG = Logger.getLogger(GetAdminUniversityDepartmentsCommand.class);

  protected String processGet(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.debug("Start process get");
    performDaoGet(req, session);

    LOG.debug("Finish process get");
    return Path.PAGE_DEPARTMENT_ADMIN;
  }

  /**
   * Load departments in university
   */
  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    DepartmentDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getDepartmentDAO();
    List<Department> result = null;
    University un = (University) session.getAttribute(SessionConst.UNIVER);
    LOG.debug("University is " + un);

    result = dao.sortByName(un, true);
    LOG.debug("Departments load");
    req.setAttribute("departments", result);
  }
}