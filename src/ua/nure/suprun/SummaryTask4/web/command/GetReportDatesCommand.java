package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.ReportDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Get report dates command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class GetReportDatesCommand extends DistributedCommand {

  private static final long serialVersionUID = -717131996453004717L;
  private static final Logger LOG = Logger.getLogger(GetReportDatesCommand.class);

  @Override
  public String execute(HttpServletRequest req, HttpServletResponse response) throws AppException {
    LOG.debug("Start process post");
    performDaoGet(req, req.getSession());
    
    LOG.debug("Finish process post");
    return Path.PAGE_ADMIN_REPORT_DATES;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    University u = (University) session.getAttribute(SessionConst.UNIVER);
    ReportDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getReportDAO();

    req.setAttribute("dates", dao.getDates(u));
    LOG.debug("Dates loaded --> " + req.getAttribute("dates"));
  }
}