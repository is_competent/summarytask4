package ua.nure.suprun.SummaryTask4.web.command;

import java.io.IOException;
import java.sql.Timestamp;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.ReportView;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.ReportDAO;
import ua.nure.suprun.SummaryTask4.email.EmailSender;
import ua.nure.suprun.SummaryTask4.email.ReportLetterBuilder;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

/**
 * Make report command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class MakeReportCommand extends DistributedCommand {

  private static final long serialVersionUID = 8817606131818343726L;
  private static final Logger LOG = Logger.getLogger(MakeReportCommand.class);

  @Override
  public String execute(HttpServletRequest req, HttpServletResponse response) throws AppException {
    HttpSession session = req.getSession();
    LOG.debug("Start process");

    String forward = Path.COMMAND_ERROR;

    if (performDaoPost(req, req.getSession())) {
      LOG.debug("Report was created");

      session.setAttribute("info", "The report has been succesfully created");
      forward = Path.COMMAND_INFO;
    } else {
      LOG.debug("Report was not created");

      session.setAttribute("emessage", "Can't create the report");
    }

    LOG.debug("Finish process");
    return forward;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    ReportDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getReportDAO();

    // Create report
    Timestamp date = dao.createReport();
    
    // Extract created records in report
    List<ReportView> inserted = dao.getInsertedEnrolees(date);
    try {
      if (inserted.isEmpty()) {
        LOG.trace("Empty report");

        throw new DMLException("Report is empty");
      } else {
        LOG.trace("Try notify enrolees");

        // Notify all newly received applicants
        notify(inserted, req);
      }
    } catch (InterruptedException | IOException ex) {
      LOG.fatal("Can't notify enrolees");

      throw new DBException("Can't notify applicants", ex);
    }

    return date != null;
  }

  /**
   * Notify applicants.
   * 
   * @param listOfApplicants
   * @param request
   * @throws InterruptedException
   * @throws IOException
   */
  private void notify(List<ReportView> listOfApplicants, HttpServletRequest request) throws InterruptedException,
      IOException {
    EmailSender.deliver("Admission campaign", new ReportLetterBuilder(listOfApplicants, request));
  }

}