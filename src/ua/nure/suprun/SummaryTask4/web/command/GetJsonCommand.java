package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;

/**
 * Returns json.
 * 
 * @author B.Suprun
 * 
 */
public class GetJsonCommand extends Command {

	private static final long serialVersionUID = 6996103659321639323L;
  private static final Logger LOG = Logger.getLogger(GetJsonCommand.class);

	@Override
	public String execute(HttpServletRequest request,
			HttpServletResponse response) {
		LOG.debug("Command starts");

		LOG.debug("Command finished");
		return Path.PAGE_JSON;
	}

}