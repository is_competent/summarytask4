package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * BlockEnrollee command.
 * 
 * @author B.Suprun
 * 
 */
public class BlockEnrolleeCommand extends DistributedCommand {

  private static final long serialVersionUID = 4829383852096274825L;
  private static final Logger LOG = Logger.getLogger(BlockEnrolleeCommand.class);

  /**
   * Only post-method allowed.
   */
  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.trace("Start handle post-request");
    String forward = Path.COMMAND_ERROR;

    if (performDaoPost(req, session)) {
      LOG.trace("User was blocked");

      session.setAttribute("info", "User was blocked");
      forward = Path.COMMAND_INFO;
    } else {
      LOG.trace("User is not blocked");

      session.setAttribute("emessage", "Can't block the user");
      forward = Path.COMMAND_ERROR;
    }

    LOG.trace("Done");
    return forward;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    int id = RequestParser.getParser(req).getInt("id");
    LOG.trace("User is --> " + id);

    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();

    return dao.block(id);
  }

}