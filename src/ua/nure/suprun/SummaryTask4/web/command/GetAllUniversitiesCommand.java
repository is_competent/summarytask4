package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UniversityDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Get all universities command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class GetAllUniversitiesCommand extends DistributedCommand {

  private static final long serialVersionUID = -6780494264478433132L;
  private static final Logger LOG = Logger.getLogger(GetAllUniversitiesCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws AppException {
    LOG.debug("Start proccess");
    performDaoGet(request, request.getSession());

    LOG.debug("Universities are loaded");
    return Path.PAGE_UNIVERSITIES;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    LOG.debug("Start extract universities");
    UniversityDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUniversityDAO();
    req.setAttribute("universities", dao.getAll());

    LOG.debug("Finish extracting universities --> " + req.getAttribute("universities"));
  }

}