package ua.nure.suprun.SummaryTask4.web.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.bl.entity.bean.DepartmentApplicationBean;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Get self applications command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class GetSelfApplicationsCommand extends DistributedCommand {

  private static final long serialVersionUID = -2476433091731207613L;
  private static final Logger LOG = Logger.getLogger(GetSelfApplicationsCommand.class);

  @Override
  public String execute(HttpServletRequest req, HttpServletResponse response) throws AppException {
    LOG.debug("Start process post");
    performDaoGet(req, req.getSession());

    LOG.debug("Finish process post");
    return Path.PAGE_ENROLEE_APPLIED;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    User u = (User) session.getAttribute(SessionConst.USER);
    EnrolleeDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getEnrolleeDAO();

    List<DepartmentApplicationBean> applications = dao.getApplications(u.getId());

    req.setAttribute("applications", applications);
    LOG.debug("Departments loaded --> " + req.getAttribute("applications"));
  }
}