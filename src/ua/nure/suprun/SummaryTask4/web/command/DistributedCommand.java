package ua.nure.suprun.SummaryTask4.web.command;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Delegate executing of command on the HTTP methods.
 * 
 * @author B.Suprun
 * 
 */
public abstract class DistributedCommand extends Command {

  private static final long serialVersionUID = 3815385090355009321L;
  private static final Logger LOG = Logger.getLogger(DistributedCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException, AppException {

    LOG.debug("Starts command");

    String method = request.getMethod().toLowerCase();
    String forward;
    
    if (method.equals("get")) {
      LOG.trace("Handle method get");
      
      forward = processGet(request, request.getSession());
    } else {
      LOG.trace("Handle method post");
      
      forward = processPost(request, request.getSession());
    }

    LOG.trace("Command finish");
    return forward;

  }

  /**
   * Invoked when method is POST.
   * 
   * @param req
   *          - {@link ServletRequest}
   * @param session
   *          - {@link HttpSession}
   * @return redirect address.
   * @throws AppException
   *           If some error has been occurred.
   */
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException,
      IOException, ServletException {
    throw new AppException("Unsupported method");
  }

  /**
   * Invoked when method is GET.
   * 
   * @param req
   *          - {@link ServletRequest}
   * @param session
   *          - {@link HttpSession}
   * @return forward address.
   * @throws AppException
   *           If some error has been occurred.
   */
  protected String processGet(HttpServletRequest req, HttpSession session) throws AppException,
      IOException, ServletException {
    throw new AppException("Unsupported method");
  }

  /**
   * DB operations for GET method.
   * 
   * @param req
   *          - {@link ServletRequest}
   * @param session
   *          - {@link HttpSession}
   * @throws DBException
   *           If some error has been occurred.
   */
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    throw new DBException("Unsupported method");
  }

  /**
   * DB operations for POST method.
   * 
   * @param req
   *          - {@link ServletRequest}
   * @param session
   *          - {@link HttpSession}
   * @return
   * 
   * @throws DBException
   *           If some error has been occurred.
   */
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    throw new DBException("Unsupported method");
  }
}