package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentSubjectBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.DepartmentSubject;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Remove department subject command. Async.
 * 
 * @author B.Suprun
 * 
 */
public class RemoveDepartmentSubjectCommand extends DistributedCommand {

  private static final long serialVersionUID = 659437037705397669L;
  private static final Logger LOG = Logger.getLogger(RemoveDepartmentSubjectCommand.class);

  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) {
    LOG.trace("Start handle post-request");
    try {
      if (performDaoPost(req, session)) {
        LOG.trace("Subject was removed");

        session.setAttribute("jsonStatus", 200);
      } else {
        LOG.trace("Subject was not removed");

        throw new AppException("Subject was not removed");
      }
    } catch (Exception ex) {
      LOG.fatal(ex);
      
      String msg = "Nothing was changed";
      if (ex instanceof AppException) {
        msg = ex.getMessage();
      }
      session.setAttribute("jsonStatus", 400);
      session.setAttribute("emessage", msg);
    } 

    LOG.trace("Done");
    return Path.COMMAND_GET_JSON;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    DepartmentDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getDepartmentDAO();
    DepartmentSubject d = new DepartmentSubjectBuilder().create(req);

    LOG.debug("Try remove subject from department --> " + d);
    return dao.removeSubject(d);
  }
}