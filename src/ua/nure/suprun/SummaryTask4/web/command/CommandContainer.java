package ua.nure.suprun.SummaryTask4.web.command;

import java.util.Map;
import java.util.TreeMap;

import org.apache.log4j.Logger;


/**
 * Holder for all commands.
 * 
 * @author B.Suprun
 * 
 */
public class CommandContainer {

  private static final Logger LOG = Logger.getLogger(CommandContainer.class);

  private static Map<String, Command> commands = new TreeMap<String, Command>();

  static {

    // out of control commands
    commands.put("login", new LoginCommand());
    commands.put("logout", new LogoutCommand());
    commands.put("noCommand", new NoCommand());
    commands.put("info", new InfoCommand());
    commands.put("error", new ErrorCommand());
    commands.put("authorizate", new AuthorizateCommand());
    commands.put("sendRestoreEmail", new SendRestoreEmailCommand());
    commands.put("restoreUser", new RestoreUserCommand());
    commands.put("createEnrollee", new CreateEnrolleeCommand());

    // Common commands
    commands.put("profile", new ProfileCommand());
    commands.put("changeLang", new ChangeLangCommand());
    commands.put("changeInfo", new ChangeInfoCommand());
    commands.put("changePass", new ChangePassCommand());
    commands.put("getJson", new GetJsonCommand());

    // Root commands
    commands.put("inviteAdmin", new InviteAdminCommand());
    commands.put("createSubject", new CreateSubjectCommand());
    commands.put("removeSubject", new RemoveSubjectCommand());
    commands.put("getReportPage", new GetReportPageCommand());
    commands.put("makeReport", new MakeReportCommand());

    // Admin commands
    commands.put("createDepartment", new CreateDepartmentCommand());
    commands.put("createDepartmentSubject", new CreateDepartmentSubjectCommand());
    commands.put("describeDepartment", new DescribeDepartmentCommand());
    commands.put("removeDepartment", new RemoveDepartmentCommand());
    commands.put("removeDepartmentSubject", new RemoveDepartmentSubjectCommand());
    commands.put("updateDepartment", new UpdateDepartmentCommand());
    commands.put("getAdminUniversityDepartments", new GetAdminUniversityDepartmentsCommand());
    commands.put("getReport", new GetReportCommand());
    commands.put("getReportDates", new GetReportDatesCommand());
    commands.put("searchEnrollee", new SearchEnrolleeCommand());
    commands.put("describeEnrollee", new DescribeEnrolleeCommand());
    commands.put("blockEnrollee", new BlockEnrolleeCommand());
    commands.put("unblockEnrollee", new UnblockEnrolleeCommand());
    commands.put("getBlocked", new GetBlockedCommand());

    // Applicant commands
    commands.put("createEnrolleeMark", new CreateEnrolleeMarkCommand());
    commands.put("removeEnrolleeMark", new RemoveEnrolleeMarkCommand());
    commands.put("getEnrolleeMarks", new GetEnrolleeMarksCommand());
    commands.put("getAllUniversities", new GetAllUniversitiesCommand());
    commands.put("apply", new ApplyCommand());
    commands.put("showUniversityDepartments", new ShowUniversityDepartmentsCommand());
    commands.put("getSelfApplications", new GetSelfApplicationsCommand());

    LOG.debug("Command container was successfully initialized");
    LOG.trace("Number of commands --> " + commands.size());
  }

  /**
   * Returns command object with the given name.
   * 
   * @param commandName
   *          Name of the command.
   * @return Command object.
   */
  public static Command get(String commandName) {
    if (commandName == null || !commands.containsKey(commandName)) {
      LOG.trace("Command not found, name --> " + commandName);
      return commands.get("noCommand");
    }

    return commands.get(commandName);
  }

}