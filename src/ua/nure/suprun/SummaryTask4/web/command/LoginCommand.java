package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.CommonRoles;
import ua.nure.suprun.SummaryTask4.bl.entity.Role;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UniversityDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Login command.
 * 
 * @author B.Suprun
 * 
 */
public class LoginCommand extends Command {

  private static final long serialVersionUID = 5698455104586773983L;
  private static final Logger LOG = Logger.getLogger(LoginCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws AppException {
    LOG.debug("Command starts");
    HttpSession session = request.getSession();
    RequestParser rParser = RequestParser.getParser(request);
    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();

    String email = rParser.getString(Params.USER_EMAIL);
    String pass = rParser.getString(Params.USER_PASS);
    LOG.trace("User email is --> " + email);

    User user = dao.login(email, pass);
    LOG.trace("Found in DB: user --> " + user);

    String forward = Path.COMMAND_ERROR;
    if (user == null) {
      session.setAttribute("emessage",
          "There is no user with such email/password or you had been blocked by administrator");
    } else {
      Role userRole = CommonRoles.getUserRole(user);
      LOG.trace("userRole --> " + userRole);

      session.setAttribute(SessionConst.USER, user);
      LOG.trace("Set the session attribute: user --> " + user);

      session.setAttribute(SessionConst.ROLE, userRole);
      LOG.trace("Set the session attribute: userRole --> " + userRole);

      session.setAttribute(SessionConst.USER_LANG, user.getLang());
      LOG.trace("Set the session attribute: userlang --> " + user.getLang());

      LOG.trace("Load accessory objects");
      loadAditionalObjects(user, userRole, session);
      LOG.info("User " + user + " logged as " + userRole.toString().toLowerCase());
      forward = Path.PROFILE;
    }

    LOG.debug("Command finished");

    return forward;
  }

  /**
   * Loads additional objects required for different roles.
   * 
   * @param u
   *          - User
   * @param role
   *          - User's role.
   * @param session
   *          - {@link HttpSession}
   * @throws DBException
   */
  private void loadAditionalObjects(User u, Role role, HttpSession session) throws DBException {
    if (CommonRoles.ADMIN.getRole().equals(role)) {
      LOG.trace("Load university");

      // load university
      UniversityDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUniversityDAO();
      session.setAttribute(SessionConst.UNIVER, dao.getByAdmin(u.getId()));
    } else if (CommonRoles.ENROLLEE.getRole().equals(role)) {
      LOG.trace("Load enrollee");

      // load enrollee obj
      EnrolleeDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getEnrolleeDAO();
      session.setAttribute(SessionConst.ENR, dao.get(u.getId()));
    }
  }
}