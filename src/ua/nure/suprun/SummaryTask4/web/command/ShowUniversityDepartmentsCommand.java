package ua.nure.suprun.SummaryTask4.web.command;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.SubjectDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Show university department command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class ShowUniversityDepartmentsCommand extends DistributedCommand {

  private static final long serialVersionUID = -1949699876554823702L;
  private static final Logger LOG = Logger.getLogger(ShowUniversityDepartmentsCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws AppException {
    LOG.debug("Start proccess");
    performDaoGet(request, request.getSession());

    LOG.debug("Departments are loaded");
    return Path.PAGE_ENROLLEE_DEPS;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    LOG.debug("Start extract universities");
    University un = new University();

    un.setId(RequestParser.getParser(req).getInt("id"));
    LOG.debug("University found --> " + un);

    DepartmentDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getDepartmentDAO();
    SubjectDAO sDao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getSubjectDAO();

    // Generate pairs with department and its required subjects
    Map<Department, List<Subject>> departments = new HashMap<>();
    for (Department d : dao.sortByName(un, true)) {
      departments.put(d, sDao.getDepartmentSubjects(d.getId()));
    }

    req.setAttribute("departments", departments);
    req.setAttribute("priorities", getPriorities(session));
    
    LOG.debug("Finish extracting departments");
  }

  private List<Integer> getPriorities(HttpSession session) throws DBException {
    User user = (User) session.getAttribute(SessionConst.USER);
    EnrolleeDAO enrDao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getEnrolleeDAO();

    List<Integer> existingPriorities = enrDao.getPriorities(user.getId());
    List<Integer> restPriorities = new ArrayList<>();

    for (int i = Constant.MAX_APPLICATIONS_COUNT; i > 0; i--) {
      if (!existingPriorities.contains(i)) {
        restPriorities.add(i);
      }
    }
    
    LOG.trace("Priorities: " + restPriorities);
    return restPriorities;
  }

}