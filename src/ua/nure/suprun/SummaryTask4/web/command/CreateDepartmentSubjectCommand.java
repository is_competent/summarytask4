package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentSubjectBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.DepartmentSubject;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * CreateDepartmentSubject command. Async.
 * 
 * @author B.Suprun
 * 
 */
public class CreateDepartmentSubjectCommand extends DistributedCommand {

  private static final long serialVersionUID = -9091079511509356042L;
  private static final Logger LOG = Logger.getLogger(CreateDepartmentSubjectCommand.class);
  
  /**
   * Only POST allowed.
   */
  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) {
    LOG.trace("Start handle post-request");
    try {
      if (performDaoPost(req, session)) {
        DepartmentSubject d = new DepartmentSubjectBuilder().create(req);
        LOG.trace("Subject created -->" + d);
        
        session.setAttribute("jsonId", d.getSubjId());
        session.setAttribute("jsonStatus", 200);
      } else {
        LOG.trace("Subject is not created");
        
        throw new AppException("Subject is not created");
      }
    } catch (Exception ex) {
      LOG.fatal(ex);
      
      String msg = "Nothing was changed";
      if (ex instanceof AppException) {
        msg = ex.getMessage();
      }
      session.setAttribute("jsonStatus", 400);
      session.setAttribute("emessage", msg);
    } 

    LOG.trace("Done");
    return Path.COMMAND_GET_JSON;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    DepartmentDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getDepartmentDAO();
    DepartmentSubject d = new DepartmentSubjectBuilder().create(req);

    LOG.debug("Try create new subject in department --> " + d);

    return dao.addSubject(d);
  }

}