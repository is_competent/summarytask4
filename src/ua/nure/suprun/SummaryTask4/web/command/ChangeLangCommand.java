package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Change language command.
 * 
 * @author B.Suprun
 * 
 */
public class ChangeLangCommand extends DistributedCommand {

  private static final long serialVersionUID = -9053666947779139732L;
  private static final Logger LOG = Logger.getLogger(ChangeLangCommand.class);

  /**
   * Only get method is allowed
   */
  @Override
  protected String processGet(HttpServletRequest req, HttpSession session) throws AppException {
    String lang = (String) req.getParameter(SessionConst.USER_LANG);
    session.setAttribute(SessionConst.USER_LANG, lang);

    performDaoGet(req, session);
    return Path.PAGE_INFO;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    User u = (User) session.getAttribute(SessionConst.USER);
    String lang = req.getParameter(SessionConst.USER_LANG);
    LOG.trace("Changing language for user --> " + u);
    LOG.trace("Language is --> " + lang);

    if (u != null) {
      try {
        LOG.trace("Write to database");
        u.setLang(lang);
        UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();

        if (dao.changeLang(u)) {
          req.setAttribute("info", "User's language has been changed");
        } else {
          req.setAttribute("info", "Can't change user's language");
          LOG.info("Can't change user's language");
        }
      } catch (DBException ex) {
        LOG.fatal("Can't change user's lang", ex);
      }
    }
  }
}