package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Change user password command.
 * 
 * @author B.Suprun
 * 
 */
public class ChangePassCommand extends DistributedCommand {

  private static final long serialVersionUID = 8176677030510499727L;
  private static final Logger LOG = Logger.getLogger(ChangePassCommand.class);

  /**
   * Only POST is allowed.
   */
  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException {
    RequestParser parser = RequestParser.getParser(req);

    String oldPass = parser.getString("oldPass");
    String newPass = parser.getString("newPass");

    User user = (User) session.getAttribute(SessionConst.USER);

    String forward = Path.COMMAND_ERROR;
    if (!user.getPass().equals(oldPass)) {
      LOG.error("User do not confirm old pass, user --> " + user);
      
      session.setAttribute("emessage", "Old password is incorrect");
    } else if (user.getPass().equals(newPass)) {
      LOG.error("User enter the old pass, user --> " + user);
      
      session.setAttribute("emessage", "Entered new password has been blocked by security policy");
    } else if (performDaoPost(req, session)) {
      LOG.trace("Password updated");
      
      session.setAttribute("info", "Your password has been changed");
      user.setPass(newPass);
      forward = Path.COMMAND_INFO;
    } else {
      LOG.trace("Password is not updated");
      
      session.setAttribute("info", "Can't change your password");
      forward = Path.COMMAND_ERROR;
    }

    return forward;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) {
    LOG.info("Start updating user");
    
    User user = (User) session.getAttribute(SessionConst.USER);
    String newPass = req.getParameter("newPass");
    User newUser = new User(user);

    newUser.setPass(newPass);

    try {
      UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();
      
      return dao.update(newUser);
    } catch (DBException ex) {
      LOG.fatal("Can't change user's password", ex);
      
      return false;
    } finally {
      LOG.debug("Updating user's password finished");
    }
  }

}