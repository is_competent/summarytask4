package ua.nure.suprun.SummaryTask4.web.command;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.db.dao.domains.SubjectDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Describe department command.
 * 
 * @author B.Suprun
 * 
 */
public class DescribeDepartmentCommand extends Command {

  private static final long serialVersionUID = 3738250517229717794L;
  private static final Logger LOG = Logger.getLogger(DescribeDepartmentCommand.class);

  @Override
  public String execute(HttpServletRequest request, HttpServletResponse response)
      throws AppException {
    LOG.debug("Start command");
    RequestParser parser = RequestParser.getParser(request);

    DepartmentDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getDepartmentDAO();
    SubjectDAO sDao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getSubjectDAO();

    Department d = dao.get(parser.getInt("id"));
    List<Subject> subs = sDao.getDepartmentSubjects(parser.getInt("id"));

    request.setAttribute("department", d);
    request.setAttribute("subj", subs);

    LOG.debug("Redirect to admin");
    return Path.PAGE_DESCRIBE_DEP_ADMIN;

  }

}