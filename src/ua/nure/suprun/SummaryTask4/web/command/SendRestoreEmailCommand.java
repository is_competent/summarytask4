package ua.nure.suprun.SummaryTask4.web.command;

import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Authorization;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.HashGenerator;

/**
 * SendRestoreEmail Command.
 * 
 * @author B.Suprun
 * 
 */
public class SendRestoreEmailCommand extends InviteAdminCommand {

  private static final long serialVersionUID = 4856771635378588478L;
  private static final Logger LOG = Logger.getLogger(SendRestoreEmailCommand.class);

  /**
   * Configure email.
   */
  public SendRestoreEmailCommand() {
    laterName = "restore-email";
    subject = "Restore your password";
  }

  /**
   * Only POST supported.
   */
  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    try {
      String email = req.getParameter("toEmail");
      Authorization a = new Authorization();
      UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();
      User u = dao.get(email);

      if (u != null) {
        LOG.debug("User found --> " + u);
        // Authorization info
        a.setId(HashGenerator.generateAuthCode(u));
        a.setUserId(u.getId());

        // inserting authorization info
        req.setAttribute("authTemp", a);
        return dao.restore(a);
      } else {
        LOG.debug("User not found");
        return false;
      }
    } catch (NoSuchAlgorithmException | UnsupportedEncodingException ex) {
      LOG.fatal(ex);
      return false;
    }
  }

}