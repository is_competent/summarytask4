package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeBuilder;
import ua.nure.suprun.SummaryTask4.bl.builder.UserBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.CommonRoles;
import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * CreateEnrollee command.
 * 
 * @author B.Suprun
 * 
 */
public class CreateEnrolleeCommand extends DistributedCommand {
  private static final long serialVersionUID = 4057595433789475467L;
  private static final Logger LOG = Logger.getLogger(CreateEnrolleeCommand.class);

  /**
   * Only POST is allowed.
   */
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.debug("Start post");

    if (performDaoPost(req, session)) {
      LOG.debug("User created");
      
      session.setAttribute("info", "You are registered successfully");
      return Path.COMMAND_INFO;
    } else {
      LOG.debug("User are not created");
      
      session.setAttribute("emessage", "Can't registry you");
      return Path.COMMAND_ERROR;
    }
  }

  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    LOG.debug("Start perform dao post");
    
    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();
    User user = new UserBuilder().create(req);
    user.setRole(CommonRoles.ENROLLEE.getRole().getId());
    LOG.debug("User extracted --> " + user);

    Enrollee enr = new EnrolleeBuilder().create(req);
    LOG.debug("Enrollee extracted --> " + enr);

    LOG.debug("Try create user");
    return dao.registryEnrolle(user, enr);
  }
}