package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeMarkBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeMark;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Create enrollee mark command. Async.
 * 
 * @author B.Suprun
 * 
 */
public class CreateEnrolleeMarkCommand extends DistributedCommand {

  private static final long serialVersionUID = -1044119070049328869L;
  private static final Logger LOG = Logger.getLogger(CreateEnrolleeMarkCommand.class);

  /**
   * Only POST is allowed.
   */
  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) {
    LOG.trace("Start handle post-request");

    try {
      if (performDaoPost(req, session)) {
        EnrolleeMark mark = (EnrolleeMark) req.getAttribute("tempMark");
        LOG.trace("Mark created --> " + mark);

        session.setAttribute("jsonId", mark.getSubjId());
        session.setAttribute("jsonStatus", 200);
      } else {
        LOG.trace("Mark is not created");
        throw new AppException("Mark is not created");
      }
    } catch (Exception ex) {
      LOG.fatal(ex);
      
      String msg = "Nothing was changed";
      if (ex instanceof AppException) {
        msg = ex.getMessage();
      }
      session.setAttribute("jsonStatus", 400);
      session.setAttribute("emessage", msg);
    } 

    LOG.trace("Done");
    return Path.COMMAND_GET_JSON;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    User u = (User) session.getAttribute(SessionConst.USER);

    EnrolleeDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getEnrolleeDAO();
    EnrolleeMark mark = new EnrolleeMarkBuilder().create(req);

    mark.setEnId(u.getId());
    LOG.debug("Try create new mark for user --> " + u);
    LOG.debug("Mark is --> " + mark);
    
    req.setAttribute("tempMark", mark);
    return dao.addMark(mark);
  }

}