package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.DepartmentDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Update department command.
 * 
 * @author B.Suprun
 * 
 */
public class UpdateDepartmentCommand extends DistributedCommand {

  private static final long serialVersionUID = 4750721688617973133L;
  private static final Logger LOG = Logger.getLogger(UpdateDepartmentCommand.class);

  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.trace("Start handle post-request");
    String forward = Path.COMMAND_ERROR;

    if (performDaoPost(req, session)) {
      LOG.trace("Department updated");

      session.setAttribute("info", "Department has been changed");
      forward = Path.COMMAND_INFO;
    } else {
      LOG.trace("Department is not changed");

      session.setAttribute("emessage", "Can't change the department");
      forward = Path.COMMAND_ERROR;
    }

    LOG.trace("Done");
    return forward;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    Department d = new DepartmentBuilder().create(req);
    LOG.trace("Department is --> " + d);
    
    DepartmentDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getDepartmentDAO();
    return dao.update(d);
  }

}