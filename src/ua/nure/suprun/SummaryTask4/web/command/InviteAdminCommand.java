package ua.nure.suprun.SummaryTask4.web.command;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.NoSuchAlgorithmException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Authorization;
import ua.nure.suprun.SummaryTask4.bl.entity.CommonRoles;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.email.EmailSender;
import ua.nure.suprun.SummaryTask4.email.LatterReader;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.HashGenerator;

/**
 * Invite admin command.
 * 
 * @author B.Suprun
 * 
 */
public class InviteAdminCommand extends DistributedCommand {

  private static final long serialVersionUID = -4950144921562458238L;

  private static final Logger LOG = Logger.getLogger(InviteAdminCommand.class);

  /**
   * Name of init parameter.
   */
  protected String laterName = "invite-email";

  /**
   * Letter subject.
   */
  protected String subject = "Invite";

  /**
   * Creates new default invited admin user and generates authorization code for it.
   * 
   * @param defaultUser
   *          - User with email.
   * @param a
   *          - In this instance will be placed authorization code.
   */
  private void makeNewUser(User defaultUser, Authorization a) {

    try {
      defaultUser.setLang("en");
      defaultUser.setLName("Invited");
      defaultUser.setName("Invited");
      defaultUser.setPatr("Invited");
      defaultUser.setPass(HashGenerator.hash("invited", "SHA-256"));
      defaultUser.setRole(CommonRoles.ADMIN.getRole().getId());

      a.setId(HashGenerator.generateAuthCode(defaultUser));
      LOG.info("AUT: " + a);
    } catch (UnsupportedEncodingException | NoSuchAlgorithmException e) {
      LOG.fatal("Cant hash password. Cant invite email--> " + defaultUser.getEmail(), e);
    }
  }

  /**
   * Sends email to specified address.
   * 
   * @param req
   * @throws IOException
   */
  protected void sendMail(HttpServletRequest req) throws IOException {
    Authorization a = (Authorization) req.getAttribute("authTemp");
    String email = req.getParameter("toEmail");

    // Read template letter text.
    String text = new LatterReader().loadLetter(laterName, req);

    // Replace placeholders in template letter
    text = text.replaceAll("\\{code\\}", a.getId()).replaceAll("\\{uid\\}",
        String.valueOf(a.getUserId()));

    // Send email
    EmailSender.send(subject, text, email);
  }

  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) throws AppException,
      IOException {
    String forward = Path.COMMAND_ERROR;
    if (performDaoPost(req, session)) {
      LOG.trace("DB SUCCES");

      LOG.trace("Try to send email");
      sendMail(req);

      session.setAttribute("info", "Instruction has been sent to " + req.getParameter("toEmail"));
      forward = Path.COMMAND_INFO;
    } else {
      LOG.trace("Is not inserted succesfully");

      session.setAttribute("emessage", "Can't send letter to following address");
    }
    return forward;
  }

  @Override
  protected String processGet(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.debug("Start command");
    
    LOG.debug("End of command");
    return Path.PAGE_INVITE;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    String to = req.getParameter("toEmail");
    Authorization a = new Authorization();
    User defUser = new User();

    // creating new default user
    defUser.setEmail(to);
    makeNewUser(defUser, a);

    // inserting authorization info
    req.setAttribute("authTemp", a);

    try {
      UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();

      return dao.inviteAdmin(defUser, a);
    } catch (DBException ex) {
      LOG.fatal("Cant invite email --> " + defUser.getEmail(), ex);
      return false;
    }
  }

}