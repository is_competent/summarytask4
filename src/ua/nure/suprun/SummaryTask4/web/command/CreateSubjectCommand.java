package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.SubjectDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Create Subject command.
 * 
 * @author B.Suprun
 * 
 */
public class CreateSubjectCommand extends DistributedCommand {

  private static final long serialVersionUID = 2801308712747646580L;
  private static final Logger LOG = Logger.getLogger(CreateSubjectCommand.class);

  /**
   * POST used for creating. Async.
   */
  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) {
    LOG.trace("Start handle post-request");
    try {
      if (performDaoPost(req, session)) {
        Subject s = (Subject) req.getAttribute("tempSubj");
        LOG.trace("Subject created --> " + s);

        session.setAttribute("jsonStatus", 200);
        session.setAttribute("jsonId", s.getId());

        // Update subjects list in app context.
        performDaoGet(req, session);
      } else {
        LOG.trace("Subject is not created");

        throw new AppException("Subject is not created");
      }
    } catch (Exception ex) {
      LOG.fatal(ex);
      
      String msg = "Nothing was changed";
      if (ex instanceof AppException) {
        msg = ex.getMessage();
      }
      session.setAttribute("jsonStatus", 400);
      session.setAttribute("emessage", msg);
    } 

    LOG.trace("Done");
    return Path.COMMAND_GET_JSON;
  }

  /**
   * Get used for retrieving.
   */
  @Override
  protected String processGet(HttpServletRequest req, HttpSession session) throws AppException {
    LOG.info("Starts comand");

    LOG.info("End of comand");
    return Path.PAGE_SUBJECTS;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    LOG.info("Start retriewe subjects");
    SubjectDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getSubjectDAO();

    req.getServletContext().setAttribute("subjects", dao.getAll());
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    RequestParser p = RequestParser.getParser(req);

    Subject s = new Subject();
    s.setName(p.getString("sname"));
    LOG.trace("Subject is --> " + s);

    SubjectDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getSubjectDAO();
    req.setAttribute("tempSubj", s);

    return dao.insert(s);
  }
}