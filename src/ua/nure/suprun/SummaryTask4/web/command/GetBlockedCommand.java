package ua.nure.suprun.SummaryTask4.web.command;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.UserDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Get blocked command implementation.
 * 
 * @author B.Suprun
 * 
 */
public class GetBlockedCommand extends DistributedCommand {
  
  private static final long serialVersionUID = -2580795859935952052L;
  private static final Logger LOG = Logger.getLogger(GetBlockedCommand.class);

  @Override
  public String execute(HttpServletRequest req, HttpServletResponse response) throws AppException {
    LOG.debug("Start process");
    performDaoGet(req, req.getSession());
   
    LOG.debug("Finish process");
    return Path.PAGE_ADMIN_ENROLLEES;
  }

  @Override
  protected void performDaoGet(HttpServletRequest req, HttpSession session) throws DBException {
    UserDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getUserDAO();
    List<User> users = dao.getBlockedEnrollees();
    Map<User, Integer> map = new HashMap<User, Integer>();
    
    for (User user: users) {
      map.put(user, dao.getAppCnt(user.getId()));
    }
    req.setAttribute("enrollees", map);
    
    LOG.debug("Users loaded");
  }
}