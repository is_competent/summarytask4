package ua.nure.suprun.SummaryTask4.web.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeMark;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.EnrolleeDAO;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Remove enrollee mark command. Async.
 * 
 * @author B.Suprun
 * 
 */
public class RemoveEnrolleeMarkCommand extends DistributedCommand {

  private static final long serialVersionUID = 673245038104358290L;
  private static final Logger LOG = Logger.getLogger(RemoveEnrolleeMarkCommand.class);

  @Override
  protected String processPost(HttpServletRequest req, HttpSession session) {
    LOG.trace("Start handle post-request");
    try {
      if (performDaoPost(req, session)) {
        LOG.trace("Mark was removed");

        session.setAttribute("jsonStatus", 200);
      } else {
        LOG.trace("Mark was not removed");
        
        throw new AppException("Mark was not removed");
      }
    } catch (Exception ex) {
      LOG.fatal(ex);
      
      String msg = "Nothing was changed";
      if (ex instanceof AppException) {
        msg = ex.getMessage();
      }
      session.setAttribute("jsonStatus", 400);
      session.setAttribute("emessage", msg);
    } 

    LOG.trace("Done");
    return Path.COMMAND_GET_JSON;
  }

  @Override
  protected boolean performDaoPost(HttpServletRequest req, HttpSession session) throws DBException {
    RequestParser parser = RequestParser.getParser(req);
    EnrolleeDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getEnrolleeDAO();

    User u = (User) session.getAttribute(SessionConst.USER);
    EnrolleeMark mark = new EnrolleeMark();

    mark.setSubjId(parser.getInt("id"));
    mark.setEnId(u.getId());
    LOG.debug("Mark is --> " + mark);

    LOG.debug("Try Remove mark for user --> " + u);
    return dao.removeMark(mark);
  }

}