package ua.nure.suprun.SummaryTask4.web.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import nl.captcha.Captcha;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;

/**
 * Servlet implementation class CaptchaSubmit. Async.
 */
@WebServlet("/cs")
public class CaptchaSubmit extends HttpServlet {
  private static final Logger LOG = Logger.getLogger(CaptchaSubmit.class);

  private static final long serialVersionUID = 3532391080569678029L;

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    try {
      Captcha c = (Captcha) request.getSession().getAttribute(Captcha.NAME);
      String answer = request.getParameter("answer");

      if (c != null && c.isCorrect(answer)) {
        LOG.trace("Captcha is correct");

        request.setAttribute("jsonStatus", 200);
      } else {
        throw new Exception();
      }
    } catch (Exception ex) {
      LOG.trace("Captcha is incorrect", ex);

      request.setAttribute("jsonStatus", 400);
    }

    request.getRequestDispatcher(Path.PAGE_JSON).forward(request, response);
  }

}
