package ua.nure.suprun.SummaryTask4.web.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.exceptions.AppException;
import ua.nure.suprun.SummaryTask4.web.command.Command;
import ua.nure.suprun.SummaryTask4.web.command.CommandContainer;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Servlet implementation class Controller
 * 
 * @author B.Suprun
 * 
 */
public class Controller extends HttpServlet {

  private static final long serialVersionUID = -6234098132227295396L;

  private static final Logger LOG = Logger.getLogger(Controller.class);

  protected void doGet(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = process(request, response);

    // Forwarding to view
    request.getRequestDispatcher(path).forward(request, response);
  }

  protected void doPost(HttpServletRequest request, HttpServletResponse response)
      throws ServletException, IOException {
    String path = process(request, response);

    // Redirecting
    response.sendRedirect(path);
  }

  private String process(HttpServletRequest request, HttpServletResponse response)
      throws IOException, ServletException {
    LOG.debug("Controller starts");
    RequestParser parser = null;
    try {
      parser = RequestParser.getParser(request);
    } catch (IllegalArgumentException ex) {
      LOG.warn(ex);
      parser = new RequestParser(request);
    }

    // extract command name from the request
    String commandName = parser.getCommand();
    LOG.trace("Request parameter: command --> " + commandName);

    // obtain command object by its name
    Command command = CommandContainer.get(commandName);
    LOG.trace("Obtained command --> " + command);

    // execute command and get forward address
    String forward = Path.COMMAND_ERROR;
    try {
      forward = command.execute(request, response);
    } catch (AppException ex) {
      request.getSession().setAttribute("emessage", ex.getMessage());
    }
    LOG.trace("Forward address --> " + forward);

    LOG.debug("Controller finished, now go to forward address --> " + forward);

    return forward;
  }
}
