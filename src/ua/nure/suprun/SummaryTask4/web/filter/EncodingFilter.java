package ua.nure.suprun.SummaryTask4.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

/**
 * Servlet Filter implementation class EncodingFilter
 */
public class EncodingFilter implements Filter {

  private static final Logger LOG = Logger.getLogger(EncodingFilter.class);

  /**
   * Default encoding
   */
  private String defEncoding;

  @Override
  public void destroy() {
    LOG.debug("Filter destruction starts");
    // Nothing to do
    LOG.debug("Filter destruction finished");
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    LOG.debug("Filter starts");

    HttpServletRequest httpRequest = (HttpServletRequest) request;
    LOG.trace("Request uri --> " + httpRequest.getRequestURI());

    String requestEncoding = request.getCharacterEncoding();
    if (requestEncoding == null) {
      LOG.trace("Request encoding = null, set encoding --> " + defEncoding);
      request.setCharacterEncoding(defEncoding);
    }

    LOG.debug("Filter finished");
    chain.doFilter(request, response);
  }

  public void init(FilterConfig fConfig) throws ServletException {
    LOG.debug("Filter initialization starts");

    defEncoding = fConfig.getInitParameter("def-encoding");
    LOG.trace("Encoding from web.xml --> " + defEncoding);

    LOG.debug("Filter initialization finished");
  }

}
