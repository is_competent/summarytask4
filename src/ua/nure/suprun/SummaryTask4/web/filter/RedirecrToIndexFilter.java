package ua.nure.suprun.SummaryTask4.web.filter;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;

/**
 * Redirect to profile within root.
 */
@WebFilter(dispatcherTypes = { DispatcherType.REQUEST }, urlPatterns = {
    "/registry.jsp", "/login.jsp", "/", "/restoreemail.jsp" })
public class RedirecrToIndexFilter implements Filter {
  private static final Logger LOG = Logger.getLogger(RedirecrToIndexFilter.class);
  
  @Override
  public void destroy() {
    LOG.debug("Filter destroyed");
  }
  
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    LOG.debug("Start service");
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    HttpServletResponse httpResponse = (HttpServletResponse) response;
    HttpSession session = httpRequest.getSession(false);

    if (session == null || session.getAttribute(SessionConst.USER) == null) {
      LOG.debug("There is no user");
      
      chain.doFilter(request, response);
    } else {
      LOG.debug("Session are not invalidated, redirect to profile");
      
      httpResponse.sendRedirect(Path.PROFILE);
    }
  }
  
  @Override
  public void init(FilterConfig fConfig) throws ServletException {
    LOG.debug("Filter init");
  }

}
