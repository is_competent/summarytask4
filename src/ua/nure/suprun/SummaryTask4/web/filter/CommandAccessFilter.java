package ua.nure.suprun.SummaryTask4.web.filter;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.bl.entity.CommonRoles;
import ua.nure.suprun.SummaryTask4.bl.entity.Role;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

/**
 * Security filter.
 * 
 * @author B.Suprun
 * 
 */
@WebFilter(servletNames = { "Controller" }, dispatcherTypes = { DispatcherType.REQUEST,
    DispatcherType.FORWARD, DispatcherType.ASYNC },
// Path to access control file
initParams = { @WebInitParam(name = "access-map", value = "/WEB-INF/configs/access.conf") })
public class CommandAccessFilter implements Filter {

  private static final Logger LOG = Logger.getLogger(CommandAccessFilter.class);

  // commands access
  private Map<Role, List<String>> accessMap = new HashMap<Role, List<String>>();

  // Out of controll commands
  private List<String> outOfControl = new ArrayList<String>();

  // Common for all roles commands
  private List<String> common = new ArrayList<String>();

  @Override
  public void destroy() {
    LOG.debug("Filter destruction starts");
    // do nothing
    LOG.debug("Filter destruction finished");
  }

  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    LOG.debug("Filter starts");

    HttpServletRequest httpRequest = (HttpServletRequest) request;
    RequestParser rp = new RequestParser(httpRequest);

    if (accessAllowed(request)) {
      LOG.debug("Filter finished, access allowed to --> " + rp.getCommand());

      chain.doFilter(request, response);
    } else {
      String errorMessasge = "You do not have permission to access the requested resource";
      request.setAttribute("emessage", errorMessasge);

      LOG.trace("Set the request attribute: emessage --> " + errorMessasge);
      request.getRequestDispatcher(Path.PAGE_ERROR_PAGE).forward(request, response);
    }
  }

  /**
   * Checks for allowing access to command.
   * 
   * @param request
   * @return
   */
  private boolean accessAllowed(ServletRequest request) {
    HttpServletRequest httpRequest = (HttpServletRequest) request;
    RequestParser parser = RequestParser.getParser(httpRequest);

    String commandName = parser.getCommand();
    if (commandName == null || commandName.isEmpty()) {
      return false;
    }

    if (outOfControl.contains(commandName)) {
      return true;
    }

    HttpSession session = httpRequest.getSession(false);
    if (session == null) {
      return false;
    }

    Role userRole = (Role) session.getAttribute(SessionConst.ROLE);
    if (userRole == null) {
      return false;
    }

    return accessMap.get(userRole).contains(commandName) || common.contains(commandName);
  }

  @Override
  public void init(FilterConfig fConfig) throws ServletException {
    LOG.debug("Filter initialization starts");
    try {
      loadAccessMap(fConfig);
      LOG.debug("Filter initialization finished");
    } catch (IOException ex) {
      LOG.fatal("Can't extract commands", ex);
    }
  }

  /**
   * Loads access map for roles.
   * 
   * @param fConfig
   * @throws FileNotFoundException
   * @throws IOException
   */
  private void loadAccessMap(FilterConfig fConfig) throws IOException {
    String path = fConfig.getInitParameter("access-map");
    String realPath = fConfig.getServletContext().getRealPath(path);

    LOG.debug("Real path is --> " + realPath);
    InputStream in = null;

    try {
      in = new FileInputStream(realPath);
      Properties props = new Properties();
      props.load(in);

      LOG.debug("Properties found: ");
      props.list(System.out);

      LOG.debug("Load common commands");
      common.addAll(asList(props, "common"));

      LOG.debug("Load out of control commands");
      outOfControl.addAll(asList(props, "no-control"));

      LOG.debug("Load root commands");
      accessMap.put(CommonRoles.ROOT.getRole(), asList(props, "root"));

      LOG.debug("Load admin commands");
      accessMap.put(CommonRoles.ADMIN.getRole(), asList(props, "admin"));

      LOG.debug("Load applicant commands");
      accessMap.put(CommonRoles.ENROLLEE.getRole(), asList(props, "enrollee"));
    } finally {
      if (in != null) {
        in.close();
      }

    }
  }

  /**
   * Extracts command names
   * 
   * @param commands
   *          - List of commands
   * @return List of command name.
   */
  private List<String> asList(Properties props, String key) {
    List<String> list = new ArrayList<String>();
    String[] commnads = props.getProperty(key).split("\\s+");

    for (String command : commnads) {
      list.add(command);
    }

    return list;
  }
}