package ua.nure.suprun.SummaryTask4.web.util;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.tomcat.util.http.fileupload.FileItem;
import org.apache.tomcat.util.http.fileupload.FileUploadException;
import org.apache.tomcat.util.http.fileupload.disk.DiskFileItemFactory;
import org.apache.tomcat.util.http.fileupload.servlet.ServletFileUpload;
import org.apache.tomcat.util.http.fileupload.servlet.ServletRequestContext;

import ua.nure.suprun.SummaryTask4.constants.Params;

/**
 * Class provides a bridge between HttpRequest and end user for bater parameter manipulation. <br>
 * 
 * <b>After parsing the request, it puts itself into HttpServletRequest as a parameter. </b>
 * 
 * @author B.Suprun
 * 
 */
public class RequestParser {
  private static final String ERROR = "Incompatible parameter type for key ";

  private static final Logger LOG = Logger.getLogger(RequestParser.class);

  private static final DiskFileItemFactory FACTORY = new DiskFileItemFactory();

  private Map<String, List<String>> params;

  private byte[] buffer;

  private boolean multipart;

  public RequestParser(HttpServletRequest req) {
    params = new HashMap<String, List<String>>();

    if (ServletFileUpload.isMultipartContent(req)) {
      parseMultipart(req);
      multipart = true;
    } else {
      parseSimple(req);
      multipart = false;
    }

    req.setAttribute(Params.REQ_PARSER, this);
  }

  /**
   * Parses an multipart request
   * 
   * @param req
   */
  private void parseMultipart(HttpServletRequest req) {
    try {
      List<FileItem> tempParams = null;

      ServletFileUpload upl = new ServletFileUpload(FACTORY);
      tempParams = upl.parseRequest(new ServletRequestContext(req));

      for (FileItem item : tempParams) {
        parseFileItem(item, req.getCharacterEncoding());
      }
    } catch (IOException | FileUploadException ex) {
      LOG.error("Can't parse content", ex);
    }
  }

  /**
   * Parses single FileItem instance.
   * 
   * @param fi
   * @param enc
   * @throws IOException
   *           if an error has occurred.
   */
  private void parseFileItem(FileItem fi, String enc) throws IOException {
    try {
      String key = fi.getFieldName();

      // handle if a field is form field
      if (fi.isFormField()) {
        String value = fi.getString(enc);
        if (!params.containsKey(key)) {
          params.put(key, new ArrayList<String>());
        }
        params.get(key).add(value);
      } else {
        LOG.trace("Buffer found");
        buffer = fi.get();
      }

    } catch (UnsupportedEncodingException ex) {
      LOG.warn("Unsupported encoding", ex);
    }
  }

  /**
   * Parses a simple url-encoded request/
   * 
   * @param req
   */
  private void parseSimple(HttpServletRequest req) {
    Map<String, String[]> temp = req.getParameterMap();

    for (Map.Entry<String, String[]> entr : temp.entrySet()) {
      String[] p = entr.getValue();
      params.put(entr.getKey(), Arrays.asList(p));
    }
  }

  /**
   * Define a multipart form.
   * 
   * @return {@code true} if the request is multipart, or {@code false} otherwise.
   */
  public boolean isMultipart() {
    return multipart;
  }

  /**
   * Returns parameter corresponded to specified key value.
   * 
   * @param key
   *          - Name of parameter.
   * @return Parameter value converted to <b>int</b>.
   * @throws IllegalArgumentException
   *           If specified parameter name does not exist or can't convert parameter value to
   *           <b>int</b>.
   */
  public int getInt(String key) {

    int result;
    try {
      String value = params.get(key).get(0);

      if (value == null) {
        throw new IllegalArgumentException("Parameter " + key + " does not exist");
      }
      result = Integer.parseInt(value);
    } catch (NumberFormatException | NullPointerException ex) {
      throw new IllegalArgumentException(ERROR + key, ex);
    }

    return result;
  }

  /**
   * Tries extract value by the key, if there is no such key returns <b>def</b> value.
   * 
   * @param key
   * @param def
   *          - Value that will be returned if some error has occurred.
   * @return
   */
  public int getInt(String key, int def) {
    try {
      return getInt(key);
    } catch (IllegalArgumentException ex) {
      return def;
    }
  }

  /**
   * Returns parameter corresponded to specified key value.
   * 
   * @param key
   *          - Name of parameter.
   * @return Parameter value converted to <b>float</b>.
   * @throws IllegalArgumentException
   *           If specified parameter name does not exist or can't convert parameter value to
   *           <b>float</b>.
   */
  public float getFloat(String key) {

    float result;

    try {
      String value = params.get(key).get(0);

      if (value == null) {
        throw new IllegalArgumentException("Parameter " + key + " does not exist");
      }
      result = Float.parseFloat(value);
    } catch (NumberFormatException | NullPointerException ex) {
      throw new IllegalArgumentException(ERROR + key, ex);
    }

    return result;
  }

  /**
   * Tries extract value by the key, if there is no such key returns <b>def</b> value.
   * 
   * @param key
   * @param def
   *          - Value that will be returned if some error has occurred.
   * @return
   */
  public float getFloat(String key, float def) {
    try {
      return getFloat(key);
    } catch (IllegalArgumentException ex) {
      return def;
    }
  }

  /**
   * Returns parameter corresponded to specified key value.
   * 
   * @param key
   *          - Name of parameter.
   * @return Parameter value converted to <b>double</b>.
   * @throws IllegalArgumentException
   *           If specified parameter name does not exist or can't convert parameter value to
   *           <b>double</b>.
   */
  public double getDouble(String key) {

    double result;

    try {
      String value = params.get(key).get(0);

      if (value == null) {
        throw new IllegalArgumentException("Parameter " + key + " does not exist");
      }
      result = Double.parseDouble(value);
    } catch (NumberFormatException | NullPointerException ex) {
      throw new IllegalArgumentException(ERROR + key, ex);
    }

    return result;
  }

  /**
   * Tries extract value by the key, if there is no such key returns <b>def</b> value.
   * 
   * @param key
   * @param def
   *          - Value that will be returned if some error has occurred.
   * @return
   */
  public double getDouble(String key, double def) {
    try {
      return getDouble(key);
    } catch (IllegalArgumentException ex) {
      return def;
    }
  }

  /**
   * Returns parameter corresponded to specified key value.
   * 
   * @param key
   *          - Name of parameter.
   * @return Parameter value.
   * @throws IllegalArgumentException
   *           If specified parameter name does not exist.
   */
  public String getString(String key) {
    try {
      String value = params.get(key).get(0);

      if (value == null) {
        throw new IllegalArgumentException("Parameter " + key + " does not exist");
      }
      return value;
    } catch (NullPointerException ex) {
      throw new IllegalArgumentException("No such key" + key, ex);
    }

  }

  /**
   * Tries extract value by the key, if there is no such key returns <b>def</b> value.
   * 
   * @param key
   * @param def
   *          - Value that will be returned if some error has occurred.
   * @return
   */
  public String getString(String key, String def) {
    try {
      return getString(key);
    } catch (IllegalArgumentException ex) {
      return def;
    }
  }

  /**
   * Returns parameter corresponded to specified key value, using default SQL date format
   * <b><i>"yyyy-MM-dd"</i></b>
   * 
   * @param key
   *          - Name of parameter.
   * @return Parameter value converted to <b>java.sql.Date</b>.
   * @throws IllegalArgumentException
   *           If specified parameter name does not exist or can't convert parameter value to
   *           <b>java.sql.Date</b>.
   */
  public Date getDate(String key) {
    return getDate(key, "yyyy-MM-dd");
  }

  /**
   * Returns parameter corresponded to specified key value, using specified date format
   * 
   * @param key
   *          - Name of parameter.
   * @param pattern
   *          - Date {@link SimpleDateFormat format}.
   * @return
   * @throws IllegalArgumentException
   *           If specified parameter name does not exist or can't convert parameter value to
   *           <b>java.sql.Date</b>.
   */
  public Date getDate(String key, String pattern) {
    String value = params.get(key).get(0);

    if (value == null) {
      throw new IllegalArgumentException("Parameter " + key + " does not exist");
    }

    java.util.Date date = null;
    try {
      date = new SimpleDateFormat(pattern).parse(value);
    } catch (ParseException ex) {
      throw new IllegalArgumentException("Date format exception for key " + key, ex);
    }

    return new Date(date.getTime());
  }

  /**
   * Returns all parameter values corresponded to <b>key</b>
   * 
   * @param key
   *          - Parameter name.
   * @return Array of parameter values.
   */
  public String[] getAllValues(String key) {
    String values[] = params.get(key).toArray(new String[0]);

    if (values == null) {
      throw new IllegalArgumentException("Parameter " + key + " does not exist");
    }
    return values;
  }

  /**
   * Returns uploaded file.<br>
   * <b>NOTE: </b> You can read file's data only once.
   * 
   * @return Array of byte.
   * @throws IOException
   *           if the form is not multipart or some another error was occurred
   */
  public byte[] getBuffer() throws IOException {
    return buffer;
  }

  /**
   * Returns command name.
   * 
   * @return
   */
  public String getCommand() {
    String value = params.get(Params.COMMAND).get(0);

    return value;
  }

  /**
   * Retrieves request parser instance.
   * 
   * @param req
   * @return
   * @throws IllegalArgumentException
   *           if no parser is found.
   */
  public static RequestParser getParser(HttpServletRequest req) {

    RequestParser parser = (RequestParser) req.getAttribute(Params.REQ_PARSER);

    if (parser == null) {
      LOG.error("No parser was found");
      throw new IllegalArgumentException("No parser was found");
    }

    return parser;
  }
}
