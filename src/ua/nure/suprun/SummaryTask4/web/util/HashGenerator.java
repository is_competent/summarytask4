package ua.nure.suprun.SummaryTask4.web.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Random;

import ua.nure.suprun.SummaryTask4.bl.entity.User;

/**
 * The class provides generating message digests.
 * 
 * @author B.Suprun
 * 
 */
public final class HashGenerator {
  
  private static final Random RANDOM = new Random();

	private static final char[] HEX_DIGITS = { '0', '1', '2', '3', '4', '5',
			'6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F' };

	/**
	 * Generate digest for the following string.
	 * 
	 * @param str
	 *            - A message.
	 * @param alg
	 *            - Security algorithm
	 * @return
	 * @throws NoSuchAlgorithmException
	 *             - If there is no such algorithm
	 * @throws UnsupportedEncodingException
	 *             - If utf-8 encoding is not supported
	 */
	public static String hash(String str, String alg)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		MessageDigest digest;
		StringBuffer hexString = new StringBuffer();
		digest = MessageDigest.getInstance(alg);
		digest.update(str.getBytes("UTF-8"));
		for (byte d : digest.digest()) {
			hexString.append(getFirstHexDigit(d)).append(getSecondHexDigit(d));
		}
		return hexString.toString();
	}

	/**
	 * Generates authorization code for specified user, using timestamp and
	 * user's email.
	 * 
	 * @param user
	 *            - user for whom code will be generated.
	 * @return SHA-128 message digest
	 * @throws NoSuchAlgorithmException
	 *             - if sha-128 is not supported
	 * @throws UnsupportedEncodingException
	 *             - if utf8 is not supported
	 */
	public static String generateAuthCode(User user)
			throws NoSuchAlgorithmException, UnsupportedEncodingException {
		String time = String.valueOf(System.currentTimeMillis());
		String message = time + user.getEmail() + RANDOM.nextLong();

		return hash(message, "SHA-1");
	}

	private static char getFirstHexDigit(byte x) {
		return HEX_DIGITS[(0xFF & x) / 16];
	}

	private static char getSecondHexDigit(byte x) {
		return HEX_DIGITS[(0xFF & x) % 16];
	}

}
