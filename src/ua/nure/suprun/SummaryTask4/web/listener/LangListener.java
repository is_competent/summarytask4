package ua.nure.suprun.SummaryTask4.web.listener;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionAttributeListener;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.jsp.jstl.core.Config;

import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.constants.SessionConst;

/**
 * Application Lifecycle Listener implementation class LangListener
 * 
 */
@WebListener
public class LangListener implements HttpSessionAttributeListener {

  private static final Logger LOG = Logger.getLogger(LangListener.class);

  public void attributeRemoved(HttpSessionBindingEvent se) {
  }

  public void attributeAdded(HttpSessionBindingEvent se) {
    configure(se.getSession(), se.getName());
  }

  public void attributeReplaced(HttpSessionBindingEvent se) {
    configure(se.getSession(), se.getName());
  }

  /**
   * Configure fmt locale.
   * 
   * @param session
   * @param atrName
   */
  private void configure(HttpSession session, String atrName) {
    if (SessionConst.USER_LANG.equals(atrName)) {
      String lang = (String) session.getAttribute(SessionConst.USER_LANG);
      LOG.trace("Users locale is " + lang);

      LOG.trace("Configure Format locale");
      Config.set(session, Config.FMT_LOCALE, new java.util.Locale(lang));
      LOG.debug("Configuration done");
    }
  }

}
