package ua.nure.suprun.SummaryTask4.web.listener;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import ua.nure.suprun.SummaryTask4.db.dao.AbstractDAOFactory;
import ua.nure.suprun.SummaryTask4.db.dao.domains.SubjectDAO;
import ua.nure.suprun.SummaryTask4.email.EmailSender;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;

/**
 * Application Lifecycle Listener implementation class ContextListener
 * 
 * @author B.Suprun
 * 
 */
@WebListener
public class ContextListener implements ServletContextListener {

  private static final Logger LOG = Logger.getLogger(ContextListener.class);

  @Override
  public void contextDestroyed(ServletContextEvent event) {
    log("Servlet context destruction starts");
    // Nothing to do
    log("Servlet context destruction finished");
  }

  @Override
  public void contextInitialized(ServletContextEvent event) {
    log("Servlet context initialization starts");

    ServletContext servletContext = event.getServletContext();

    try {
      initLog4J(servletContext);
      initCommandContainer();
      loadLocales(servletContext);
      configureEmailSender(servletContext);
      loadSubjects(servletContext);
    } catch (IOException ex) {
      log(ex.getMessage());
    }

    log("Servlet context initialization finished");
  }

  /**
   * Initializes log4j framework.
   * 
   * @param servletContext
   */
  private void initLog4J(ServletContext servletContext) {
    log("Log4J initialization started");
    try {
      PropertyConfigurator
          .configure(servletContext.getRealPath("WEB-INF/configs/log4j.properties"));
      LOG.debug("Log4j has been initialized");
    } catch (Exception ex) {
      log("Cannot configure Log4j");
    }
    log("Log4J initialization finished");
  }

  /**
   * Initializes CommandContainer.
   * 
   * @param servletContext
   */
  private void initCommandContainer() {
    try {
      Class.forName("ua.nure.suprun.SummaryTask4.web.command.CommandContainer");
    } catch (ClassNotFoundException e) {
      LOG.fatal(e);
    }
  }

  private void log(String msg) {
    System.out.println("[ContextListener] " + msg);
  }

  private void loadLocales(ServletContext context) throws IOException {
    LOG.debug("Start loading locales");

    String localesFileName = context.getInitParameter("locales");

    // obtain reale path on server
    String localesFileRealPath = context.getRealPath(localesFileName);

    // locale descriptions
    Properties locales = new Properties();
    InputStream in = null;
    try {
      in = new FileInputStream(localesFileRealPath);
      locales.load(in);
      LOG.trace("Locales loaded");
    } finally {
      if (in != null) {
        in.close();
      }
    }

    // save descriptions to servlet context
    context.setAttribute("locales", locales);
    locales.list(System.out);
    LOG.info("Locales loaded");
  }

  /**
   * Configure {@link EmailSender}
   * 
   * @param context
   */
  private void configureEmailSender(ServletContext context) {
    LOG.debug("Start configuration");
    String emailFileName = context.getInitParameter("email");

    // obtain real path on server
    String emailFileRealPath = context.getRealPath(emailFileName);
    try {
      EmailSender.configure(emailFileRealPath);
      LOG.trace("Email configured");
    } catch (IOException ex) {
      LOG.fatal(ex);
    }

    LOG.info("Configured");
  }

  private void loadSubjects(ServletContext context) {
    LOG.debug("Start loading subjects");
    try {
      SubjectDAO dao = AbstractDAOFactory.getDAO(AbstractDAOFactory.MYSQL).getSubjectDAO();
      context.setAttribute("subjects", dao.getAll());
      LOG.debug("Subjects are loaded");
    } catch (DBException ex) {
      LOG.fatal("Can't load subjects", ex);
    }
  }
}
