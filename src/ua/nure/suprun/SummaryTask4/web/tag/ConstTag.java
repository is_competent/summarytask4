package ua.nure.suprun.SummaryTask4.web.tag;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;

/**
 * Class provides extracting values of constants.
 * 
 * @author B.Suprun
 * 
 */
public class ConstTag extends SimpleTagSupport {
  private String name;

  private static final List<Class<?>> CLASSES;

  static {
    CLASSES = new ArrayList<Class<?>>();

    CLASSES.add(Params.class);
    CLASSES.add(Path.class);
    CLASSES.add(SessionConst.class);
  }

  public static void addClass(Class<?> aClass) {
    CLASSES.add(aClass);
  }

  public void setName(String constName) {
    name = constName;
  }

  @Override
  public void doTag() throws JspException, IOException {
    JspWriter out = getJspContext().getOut();

    out.print(findConstant());
  }

  private String findConstant() {

    for (Class<?> cl : CLASSES) {
      String constant = lookIn(cl);
      if (constant != null) {
        return constant;
      }
    }
    return "";
  }

  private String lookIn(Class<?> cl) {
    try {
      Field f = cl.getDeclaredField(name);
      f.setAccessible(true);
      return (String) f.get(null);
    } catch (NoSuchFieldException | IllegalAccessException ex) {
      return null;
    }
  }

}
