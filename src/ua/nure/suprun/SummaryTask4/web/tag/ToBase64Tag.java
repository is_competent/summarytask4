package ua.nure.suprun.SummaryTask4.web.tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;
import javax.xml.bind.DatatypeConverter;

/**
 * Class provides generating base64 string using byte arra.
 * 
 * @author B.Suprun
 * 
 */
public class ToBase64Tag extends SimpleTagSupport {
  private byte[] src;

  @Override
  public void doTag() throws JspException, IOException {
    JspWriter out = getJspContext().getOut();
    out.print(toBase64());
  }

  private String toBase64() {
    try {
      return DatatypeConverter.printBase64Binary(src);
    } catch (Exception ex) {
      return "";
    }
  }

  public void setSrc(byte[] src) {
    if (src != null) {
      this.src = src.clone();
    } else {
      this.src = new byte[0];
    }
  }

}
