package ua.nure.suprun.SummaryTask4.exceptions;

/**
 * Means an database exception.
 * 
 * @author B.Suprun
 * 
 */
public class DBException extends AppException {

  private static final long serialVersionUID = -8237994886752670918L;

  public DBException(String message) {
		super(message);
	}
  
  public DBException(String msg, Throwable cause) {
    super(msg, cause);
  }
}
