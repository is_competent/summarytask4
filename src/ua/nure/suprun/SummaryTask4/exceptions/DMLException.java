package ua.nure.suprun.SummaryTask4.exceptions;

/**
 * Means an exception while running a DML command has occurred.<br>
 * Common DML commands is: <br>
 * 1. SELECT<br>
 * 2. INSERT<br>
 * 3. UPDATE<br>
 * 3. DELETE<br>
 * 
 * @author B.Suprun
 * 
 */
public class DMLException extends DBException {

  private static final long serialVersionUID = 5559337589871916750L;

  public DMLException(String message) {
		super(message);
	}
  
  public DMLException(String msg, Throwable cause) {
    super(msg, cause);
  }
}
