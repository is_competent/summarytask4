package ua.nure.suprun.SummaryTask4.exceptions;

/**
 * Means an application exception.
 * 
 * @author B.Suprun
 * 
 */
public class AppException extends Exception {

  private static final long serialVersionUID = 7532239927535198559L;

  public AppException(String message) {
    super(message);
  }
  
  public AppException(String msg, Throwable cause) {
    super(msg, cause);
  }

}
