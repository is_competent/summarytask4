package ua.nure.suprun.SummaryTask4.anotation.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates package in which contains all DB entities. Used for fill parameters
 * in named parameter statement.<br>
 * You can use <:EntityName as en> for replacing it with actual table name. So,
 * you can use <?en.fieldName> for replacing it with actual table's column name.
 * 
 * @author B.Suprun
 * 
 */
@Target({ ElementType.PACKAGE })
@Retention(RetentionPolicy.RUNTIME)
public @interface Entities { }
