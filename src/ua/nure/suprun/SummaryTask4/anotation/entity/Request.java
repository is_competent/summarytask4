package ua.nure.suprun.SummaryTask4.anotation.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Storing a request related with this entity, which has been annotated
 * 
 * @author B.Suprun
 * 
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Request {

	/**
	 * Name of the request. You can restore this request by the name.
	 */
	String name();

	/**
	 * A SQL request.
	 */
	String value();
}
