package ua.nure.suprun.SummaryTask4.anotation.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates class field. Used for auto generation of Insert, Update, Delete,
 * Select requests and auto filling parameters into
 * {@link ua.nure.suprun.SummaryTask4.db.NamedParameterStatement
 * NamedParameterStatement} instance.
 * 
 * @author B.Suprun
 * 
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface DBEntField {

	/**
	 * Name of the target table column. If is empty, then name of the annotated
	 * field will be used.
	 */
	String value() default "";

	/**
	 * If is true then field is will be used for generation insert request,
	 * otherwise it will be ignored.
	 */
	boolean insertable() default true;

	/**
	 * If is true then field is will be used for generation update request,
	 * otherwise it will be ignored.
	 */
	boolean updatable() default true;
}
