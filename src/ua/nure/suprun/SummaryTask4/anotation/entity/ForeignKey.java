package ua.nure.suprun.SummaryTask4.anotation.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Used for generating joined select requests.
 * 
 * @author B.Suprun
 * 
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ForeignKey {

	/**
	 * Parent <b>row</b> name
	 */
	String parent();

	/**
	 * Child <b>row</b> name
	 */
	String child();
}
