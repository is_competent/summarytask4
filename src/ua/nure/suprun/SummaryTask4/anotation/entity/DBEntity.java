package ua.nure.suprun.SummaryTask4.anotation.entity;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates class that will be used as an entity-class. Used for generation
 * Insert, Select, Update, Delete requests.
 * 
 * @author B.Suprun
 * 
 */
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface DBEntity {

	/**
	 * Related table name.
	 */
	String value();
}
