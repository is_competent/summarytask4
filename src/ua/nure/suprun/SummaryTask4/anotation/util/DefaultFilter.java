package ua.nure.suprun.SummaryTask4.anotation.util;

import java.lang.annotation.Annotation;

/**
 * Annotation filter default implementation.
 * 
 * @author B.Suprun
 * 
 */
public class DefaultFilter implements Filter {

	/**
	 * Just accept all items.
	 */
	@Override
	public boolean accept(Annotation a) {
		return true;
	}

}