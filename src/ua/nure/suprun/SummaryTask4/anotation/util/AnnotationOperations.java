package ua.nure.suprun.SummaryTask4.anotation.util;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.commons.beanutils.MethodUtils;
import org.apache.log4j.Logger;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Entities;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;

/**
 * Class-util for annotation managing.
 * 
 * @author B.Suprun
 * 
 */
public class AnnotationOperations {
  
  private static final Logger LOG = Logger.getLogger(AnnotationOperations.class);
  
  /**
   * Used for optimization.
   */
  private static String entityPackage = null;

  /**
   * Extracts stored requests in the class <b>ent</b>.
   * 
   * @param ent
   *          - Class where requests are stored.
   * @return SQLRequests annotation object.
   * @throws IllegalArgumentException
   *           if there is no annotation
   */
  public static SQLRequests getStoredRequests(Class<?> ent) {
    if (ent.isAnnotationPresent(SQLRequests.class)) {
      return ent.getAnnotation(SQLRequests.class);
    } else {
      throw new IllegalArgumentException("The SQLRequests annotation isn't present");
    }
  }

  /**
   * Extracts name of package in which stored entities.
   * 
   * @return {@code null} if there is such package, or name of package otherwise.
   */
  public static synchronized String getEntityPackage() {
    if (entityPackage == null) {
      for (Package p : Package.getPackages()) {
        if (p.isAnnotationPresent(Entities.class)) {
          entityPackage = p.getName();
        }
      }
    }

    return entityPackage;

  }

  /**
   * Extracts database entity name from the class annotated as DBEntity.
   * 
   * @param t
   *          - Entity class.
   * @return Entity name.
   * @throws IllegalArgumentException
   *           if there is no annotation
   */
  public static String getEntityName(Class<?> t) {
    if (t.isAnnotationPresent(DBEntity.class)) {

      return t.getAnnotation(DBEntity.class).value();

    } else {

      throw new IllegalArgumentException("The DBEntity annotation isn't present");

    }
  }

  /**
   * Returns Map of pairs with class field name - related db entity column name.
   * 
   * @param t
   *          - Entity class.
   * @param annotatedBy
   *          - Class of required annotation.
   * @param filter
   *          - Filtering of result.
   * @return Map with pairs.
   */
  public static Map<String, String> getFieldNames(Class<?> t,
      Class<? extends Annotation> annotatedBy, Filter filter) {
    Map<String, String> fields = new LinkedHashMap<String, String>();

    for (Field f : getAnnotatedFields(t, annotatedBy, filter)) {
      fields.put(f.getName(), getEntityFieldName(f));
    }

    return fields;
  }

  /**
   * Same as {@link #getFieldNames(Class, Class, Filter) getFieldNames(Class, Class, Filter)} but
   * used {@link DefaultFilter DefaultFilter}
   */
  public static Map<String, String> getFieldNames(Class<?> t,
      Class<? extends Annotation> annotatedBy) {
    return getFieldNames(t, annotatedBy, new DefaultFilter());
  }

  /**
   * Returns Map of pairs with class field name - value.
   * 
   * @param t
   *          - Class-entity instance.
   * @param annotatedBy
   *          - Required annotation class.
   * @return Map of pairs with class field name - value.
   * @throws IllegalAccessException
   *           Reflection exception.
   */
  public static <T> Map<String, Object> getFieldValues(T t, Class<? extends Annotation> annotatedBy)
      throws IllegalAccessException {
    Map<String, Object> fields = new LinkedHashMap<String, Object>();

    for (Field f : getAnnotatedFields(t.getClass(), annotatedBy)) {
      Object o = invoke(f, t);
      fields.put(f.getName(), o);
    }

    return fields;
  }

  /**
   * Extracts a value from the field in JavaBean class.
   * 
   * @param f
   *          - A field which value is being extracted.
   * @param ent
   *          - An instance of the class.
   * @return Value of the field
   * @throws IllegalAccessException
   *           Reflection exception.
   */
  public static <T> Object invoke(Field f, T ent) throws IllegalAccessException {
    try {
      Class<?> cl = f.getType();
      String prefix = "get";

      if (cl == Boolean.class) {
        prefix = "is";
      }

      String name = f.getName();
      String firstLetter = String.valueOf(name.charAt(0)).toUpperCase();
      String end = new StringBuilder(name).deleteCharAt(0).toString();
      String fieldName = prefix + firstLetter + end;

      return ent.getClass().getDeclaredMethod(fieldName).invoke(ent);
    } catch (InvocationTargetException | NoSuchMethodException ex) {
      IllegalAccessException e = new IllegalAccessException();
      e.setStackTrace(ex.getStackTrace());
      throw e;
    }
  }

  /**
   * Invokes setter on ent object for specified field.
   * 
   * @param f
   *          - {@link Field} instance
   * @param ent
   *          - Object on which setter will be invoked.
   * @param value
   *          - Setter parameter.
   * @throws IllegalAccessException
   *           - Reflection exception.
   */
  public static <E> void invokeSet(Field f, E ent, Object value) throws IllegalAccessException {
    try {
      String prefix = "set";

      String name = f.getName();
      String firstLetter = String.valueOf(name.charAt(0)).toUpperCase();
      String end = new StringBuilder(name).deleteCharAt(0).toString();
      String fieldName = prefix + firstLetter + end;
      
      Class<?> paramType = f.getType(); 
      
      LOG.debug("dssd");
      MethodUtils.invokeMethod(ent, fieldName, new Object[]{value}, new Class<?>[]{paramType});
    } catch (InvocationTargetException | NoSuchMethodException ex) {
      IllegalAccessException e = new IllegalAccessException();
      e.setStackTrace(ex.getStackTrace());
      throw e;
    }
  }

  /**
   * Extract entity column name from the field's annotation. If the name is empty - class field name
   * is used.
   * 
   * @param f
   *          - An class field.
   * @return Entity column name.
   */
  private static String getEntityFieldName(Field f) {
    DBEntField an = f.getAnnotation(DBEntField.class);
    if (an.value().isEmpty()) {
      return f.getName();
    } else {
      return an.value();
    }
  }

  /**
   * Returns list of all fields from the class annotated by such Annotation.
   * 
   * @param clazz
   *          - Target class from which fields are being extracted.
   * @param annotated
   *          - Interested annotation class.
   * @param filter
   *          - Filtering of result.
   * @return List of all fields
   */
  private static List<Field> getAnnotatedFields(Class<?> clazz,
      Class<? extends Annotation> annotated, Filter filter) {
    List<Field> result = new ArrayList<Field>();

    for (Field f : clazz.getDeclaredFields()) {
      Annotation a = f.getAnnotation(annotated);

      if (a != null && filter.accept(a)) {
        result.add(f);
      }
    }

    return result;
  }

  /**
   * Such as {@link #getAnnotatedFields(Class, Class, Filter)} but DefaultFilter is used.
   * 
   * @param clazz
   * @param annotated
   * @return
   */
  public static List<Field> getAnnotatedFields(Class<?> clazz, Class<? extends Annotation> annotated) {
    return getAnnotatedFields(clazz, annotated, new DefaultFilter());
  }

  /**
   * Returns <b>Database's table column</b> name of a primary key.
   * 
   * @param t
   *          - An entity class.
   * @return Database's table column name of a primary key.
   * @throws IllegalArgumentException
   *           Reflection exception.
   */
  public static String getPKFieldEntityName(Class<?> t) {
    Collection<String> values = getFieldNames(t, DBEntPk.class).values();
    String[] res = values.toArray(new String[1]);

    if (res.length > 0) {
      return res[0];
    } else {
      throw new IllegalArgumentException("The DBEntPk annotation isn't present");
    }
  }

  /**
   * Returns <b>class field</b> name of a primary key.
   * 
   * @param t
   *          - An entity class.
   * @return class field name of a primary key.
   * @throws IllegalArgumentException
   *           Reflection exception.
   */
  public static String getPKFieldName(Class<?> t) {
    Set<String> keys = getFieldNames(t, DBEntPk.class).keySet();
    String[] res = keys.toArray(new String[1]);

    if (res.length > 0) {
      return res[0];
    } else {
      throw new IllegalArgumentException("The DBEntPk annotation isn't present");
    }
  }
}
