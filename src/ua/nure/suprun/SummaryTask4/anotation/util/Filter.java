package ua.nure.suprun.SummaryTask4.anotation.util;

import java.lang.annotation.Annotation;

/**
 * Annotation filter.
 * 
 * @author B.Suprun
 * 
 */
public interface Filter {

	/**
	 * Determine which annotation will be accepted, and which will not.
	 * 
	 * @param a
	 *            - An Annotation
	 * @return {@code true} if Annotation must be accepted, or {@code false} if
	 *         do not.
	 */
	boolean accept(Annotation a);

}
