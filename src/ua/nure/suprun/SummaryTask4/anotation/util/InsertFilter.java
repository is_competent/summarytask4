package ua.nure.suprun.SummaryTask4.anotation.util;

import java.lang.annotation.Annotation;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;

/**
 * Annotation filter implementation for extracting all fields which will be used
 * for generating Insert request
 * 
 * @author B.Suprun
 * 
 */
public class InsertFilter implements Filter {

	@Override
	public boolean accept(Annotation a) {
		DBEntField field = null;

		if (a instanceof DBEntField) {
			field = (DBEntField) a;

			// just check insertable flag in the annotation
			return field.insertable();
		} else {
			return false;
		}
	}

}
