package ua.nure.suprun.SummaryTask4.anotation.validation;

/**
 * Annotates class field having numeric type. Value stored in the field must be
 * more or equal to specified <b>value</b>.
 * 
 * @author B.Suprun
 * 
 */
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MoreOrEqual {

	/**
	 * Lowest bound include it.
	 */
	double value();
}
