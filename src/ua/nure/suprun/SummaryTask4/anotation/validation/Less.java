package ua.nure.suprun.SummaryTask4.anotation.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates class field having numeric type. Value stored in the field must be
 * less then specified <b>value</b>.
 * 
 * @author B.Suprun
 * 
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface Less {

	/**
	 * Bound value.
	 */
	double value();
}
