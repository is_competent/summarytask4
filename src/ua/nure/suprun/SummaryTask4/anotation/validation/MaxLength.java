package ua.nure.suprun.SummaryTask4.anotation.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * Annotates class field having String type. Control length of value in the
 * annotated field. Length must be less or equal to specified <b>value</b>
 * 
 * @author B.Suprun
 * 
 */
@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface MaxLength {

	/**
	 * Max allowed string length include it.
	 */
	int value();
}
