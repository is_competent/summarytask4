package ua.nure.suprun.SummaryTask4.anotation.validation.validatorsimp;

import javax.xml.bind.ValidationException;

/**
 * Class-util for validating decimal values. Including operations such as: <br>
 * <p>
 * 1. More some bound <br>
 * 2. More or equal some bound<br>
 * 3. Less some bound<br>
 * 4. Less or equal some bound<br>
 * 
 * @author B.Suprun
 * 
 */
public class DecimalValidator {
  
  private static final String ERROR = "Illegal number format: ";

	/**
	 * Checking out that <strong>target</strong> more (>) than
	 * <strong>bound</strong>
	 * 
	 * @param target
	 *            - current value
	 * @param bound
	 *            - Lowest value excluding it
	 * @throws ValidationException
	 *             if the <i>target</i> less or equal to the </i> bound</i>
	 */
	public static void more(double target, double bound)
			throws ValidationException {
		if (!(target > bound)) {
			throw new ValidationException(ERROR + target
					+ " < " + bound);
		}
	}

	/**
	 * Checking out that <strong>target</strong> less (<) than
	 * <strong>bound</strong>
	 * 
	 * @param target
	 *            - current value
	 * @param bound
	 *            - Highest value excluding it
	 * @throws ValidationException
	 *             if the <i>target</i> more or equal to the </i> bound</i>
	 */
	public static void less(double target, double bound)
			throws ValidationException {
		if (!(target < bound)) {
			throw new ValidationException( + target
					+ " > " + bound);
		}
	}

	/**
	 * Checking out that <strong>target</strong> more or equal (>=) than
	 * <strong>bound</strong>
	 * 
	 * @param target
	 *            - current value
	 * @param bound
	 *            - Lowest value including it
	 * @throws ValidationException
	 *             if the <i>target</i> less than </i> bound</i>
	 */
	public static void moreOrEqual(double target, double bound)
			throws ValidationException {
		if (!(target >= bound)) {
			throw new ValidationException(ERROR + target
					+ " < " + bound);
		}
	}

	/**
	 * Checking out that <strong>target</strong> less or equal (<=) than
	 * <strong>bound</strong>
	 * 
	 * @param target
	 *            - current value
	 * @param bound
	 *            - Highest value including it
	 * @throws ValidationException
	 *             if the <i>target</i> more than </i> bound</i>
	 */
	public static void lessOrEqual(double target, double bound)
			throws ValidationException {
		if (!(target <= bound)) {
			throw new ValidationException(ERROR + target
					+ " > " + bound);
		}
	}
}
