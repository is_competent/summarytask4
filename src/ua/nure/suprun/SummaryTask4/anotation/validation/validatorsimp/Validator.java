package ua.nure.suprun.SummaryTask4.anotation.validation.validatorsimp;

import java.lang.reflect.Field;
import java.util.List;

import javax.xml.bind.ValidationException;

import ua.nure.suprun.SummaryTask4.anotation.util.AnnotationOperations;
import ua.nure.suprun.SummaryTask4.anotation.validation.Length;
import ua.nure.suprun.SummaryTask4.anotation.validation.Less;
import ua.nure.suprun.SummaryTask4.anotation.validation.LessOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MinLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.More;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;

/**
 * Class-util for validating fields in an class annotated by such annotations:<br>
 * 1. {@link ua.nure.suprun.SummaryTask4.anotation.validation.Pattern
 * Pattern} <br>
 * 2. {@link ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength
 * MaxLength}<br>
 * 3. {@link ua.nure.suprun.SummaryTask4.anotation.validation.MinLength
 * MinLength}<br>
 * 4. {@link ua.nure.suprun.SummaryTask4.anotation.validation.Length Length}<br>
 * 5. {@link ua.nure.suprun.SummaryTask4.anotation.validation.Less Less}<br>
 * 6. {@link ua.nure.suprun.SummaryTask4.anotation.validation.More More}<br>
 * 7. {@link ua.nure.suprun.SummaryTask4.anotation.validation.LessOrEqual
 * LessOrEqual}<br>
 * 8. {@link ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual
 * MoreOrEqual}<br>
 * 
 * @author B.Suprun
 * 
 */
public class Validator {

	/**
	 * Checks all fields annotated by validation annotations.
	 * 
	 * @param ent
	 *            - A class which fields are being validated.
	 * @throws ValidationException
	 *             If a value does not match to validation rule.
	 */
	public <T> void validate(T ent) throws ValidationException {
		try {
			// String checking
			validateString(AnnotationOperations.getAnnotatedFields(
					ent.getClass(), Pattern.class), ent);
			validateStringMaxLength(AnnotationOperations.getAnnotatedFields(
					ent.getClass(), MaxLength.class), ent);
			validateStringMinLength(AnnotationOperations.getAnnotatedFields(
					ent.getClass(), MinLength.class), ent);
			validateStringLength(AnnotationOperations.getAnnotatedFields(
					ent.getClass(), Length.class), ent);

			// Number checking
			validateNumberLess(AnnotationOperations.getAnnotatedFields(
					ent.getClass(), Less.class), ent);
			validateNumberMore(AnnotationOperations.getAnnotatedFields(
					ent.getClass(), More.class), ent);
			validateNumberLessOrEqual(AnnotationOperations.getAnnotatedFields(
					ent.getClass(), LessOrEqual.class), ent);
			validateNumberMoreOrEqual(AnnotationOperations.getAnnotatedFields(
					ent.getClass(), MoreOrEqual.class), ent);
		} catch (IllegalAccessException ex) {
		  ValidationException e = new ValidationException("Illegal field format");
		  e.addSuppressed(ex);
			throw e;
		}
	}

	/**
	 * Just validates all fields annotated by <b>Pattern</b> annotation
	 * 
	 * @param values
	 *            - List of fields annotated by <b>Pattern</b> annotation
	 * @param ent
	 *            - Instance of class
	 * @throws ValidationException
	 *             - If a value does not match to validation rule.
	 * @throws IllegalAccessException
	 *             - Reflection exception
	 */
	private <T> void validateString(List<Field> values, T ent)
			throws ValidationException, IllegalAccessException {
		for (Field ob : values) {
			Pattern p = ob.getAnnotation(Pattern.class);

			// Retrieves value stored in the field
			Object o = AnnotationOperations.invoke(ob, ent);
			StringValidator.validate(o.toString(), p.value());
		}
	}

	/**
	 * Just validates all fields annotated by <b>MaxLength</b> annotation
	 * 
	 * @param values
	 *            - List of fields annotated by <b>MaxLength</b> annotation
	 * @param ent
	 *            - Instance of class
	 * @throws ValidationException
	 *             - If a value does not match to validation rule.
	 * @throws IllegalAccessException
	 *             - Reflection exception
	 */
	private <T> void validateStringMaxLength(List<Field> values, T ent)
			throws ValidationException, IllegalAccessException {
		for (Field ob : values) {
			MaxLength p = ob.getAnnotation(MaxLength.class);

			// Retrieves value stored in the field
			Object o = AnnotationOperations.invoke(ob, ent);
			if (o instanceof String) {
				StringValidator.maxLength((String) o, p.value());
			}
		}
	}

	/**
	 * Just validates all fields annotated by <b>MinLength</b> annotation
	 * 
	 * @param values
	 *            - List of fields annotated by <b>MinLength</b> annotation
	 * @param ent
	 *            - Instance of class
	 * @throws ValidationException
	 *             - If a value does not match to validation rule.
	 * @throws IllegalAccessException
	 *             - Reflection exception
	 */
	private <T> void validateStringMinLength(List<Field> values, T ent)
			throws ValidationException, IllegalAccessException {
		for (Field ob : values) {
			MinLength p = ob.getAnnotation(MinLength.class);

			// Retrieves value stored in the field
			Object o = AnnotationOperations.invoke(ob, ent);
			if (o instanceof String) {
				StringValidator.minLength((String) o, p.value());
			}
		}
	}

	/**
	 * Just validates all fields annotated by <b>Length</b> annotation
	 * 
	 * @param values
	 *            - List of fields annotated by <b>Length</b> annotation
	 * @param ent
	 *            - Instance of class
	 * @throws ValidationException
	 *             - If a value does not match to validation rule.
	 * @throws IllegalAccessException
	 *             - Reflection exception
	 */
	private <T> void validateStringLength(List<Field> values, T ent)
			throws ValidationException, IllegalAccessException {
		for (Field ob : values) {
			Length p = ob.getAnnotation(Length.class);

			// Retrieves value stored in the field
			Object o = AnnotationOperations.invoke(ob, ent);
			if (o instanceof String) {
				StringValidator.length((String) o, p.value());
			}
		}
	}

	/**
	 * Just validates all fields annotated by <b>More</b> annotation
	 * 
	 * @param values
	 *            - List of fields annotated by <b>More</b> annotation
	 * @param ent
	 *            - Instance of class
	 * @throws ValidationException
	 *             - If a value does not match to validation rule.
	 * @throws IllegalAccessException
	 *             - Reflection exception
	 */
	private <T> void validateNumberMore(List<Field> values, T ent)
			throws ValidationException, IllegalAccessException {
		for (Field ob : values) {
			More p = ob.getAnnotation(More.class);

			// Retrieves value stored in the field
			Object o = AnnotationOperations.invoke(ob, ent);
			if (o instanceof Number) {
				double value = ((Number) o).doubleValue();
				DecimalValidator.more(value, p.value());
			}
		}
	}

	/**
	 * Just validates all fields annotated by <b>Less</b> annotation
	 * 
	 * @param values
	 *            - List of fields annotated by <b>Less</b> annotation
	 * @param ent
	 *            - Instance of class
	 * @throws ValidationException
	 *             - If a value does not match to validation rule.
	 * @throws IllegalAccessException
	 *             - Reflection exception
	 */
	private <T> void validateNumberLess(List<Field> values, T ent)
			throws ValidationException, IllegalAccessException {
		for (Field ob : values) {
			Less p = ob.getAnnotation(Less.class);

			// Retrieves value stored in the field
			Object o = AnnotationOperations.invoke(ob, ent);
			if (o instanceof Number) {
				double value = ((Number) o).doubleValue();
				DecimalValidator.less(value, p.value());
			}
		}
	}

	/**
	 * Just validates all fields annotated by <b>LessOrEqual</b> annotation
	 * 
	 * @param values
	 *            - List of fields annotated by <b>LessOrEqual</b> annotation
	 * @param ent
	 *            - Instance of class
	 * @throws ValidationException
	 *             - If a value does not match to validation rule.
	 * @throws IllegalAccessException
	 *             - Reflection exception
	 */
	private <T> void validateNumberLessOrEqual(List<Field> values, T ent)
			throws ValidationException, IllegalAccessException {
		for (Field ob : values) {
			LessOrEqual p = ob.getAnnotation(LessOrEqual.class);

			// Retrieves value stored in the field
			Object o = AnnotationOperations.invoke(ob, ent);
			if (o instanceof Number) {
				double value = ((Number) o).doubleValue();
				DecimalValidator.lessOrEqual(value, p.value());
			}
		}
	}

	/**
	 * Just validates all fields annotated by <b>MoreOrEqual</b> annotation
	 * 
	 * @param values
	 *            - List of fields annotated by <b>MoreOrEqual</b> annotation
	 * @param ent
	 *            - Instance of class
	 * @throws ValidationException
	 *             - If a value does not match to validation rule.
	 * @throws IllegalAccessException
	 *             - Reflection exception
	 */
	private <T> void validateNumberMoreOrEqual(List<Field> values, T ent)
			throws ValidationException, IllegalAccessException {
		for (Field ob : values) {
			MoreOrEqual p = ob.getAnnotation(MoreOrEqual.class);

			// Retrieves value stored in the field
			Object o = AnnotationOperations.invoke(ob, ent);
			if (o instanceof Number) {
				double value = ((Number) o).doubleValue();
				DecimalValidator.moreOrEqual(value, p.value());
			}
		}
	}

}
