package ua.nure.suprun.SummaryTask4.anotation.validation.validatorsimp;

import javax.xml.bind.ValidationException;

import org.apache.log4j.Logger;

/**
 * Class-util for validating string values. Including operations such as: <br>
 * <p>
 * 1. Max allowed length of a string <br>
 * 2. Min allowed length of e string<br>
 * 3. Required length of a string<br>
 * 4. Check for pattern matching<br>
 * 
 * @author B.Suprun
 * 
 */
public class StringValidator {

  private static final String ERROR = "Illegal string format: ";

  private static final Logger LOG = Logger.getLogger(StringValidator.class);

  /**
   * Checks for matching <i>target</i> to the <i>pattern</i>
   * 
   * @param target
   *          - A string to be checked
   * @param pattern
   *          - Required pattern
   * @throws ValidationException
   *           If <i>target</i> is not matched to the <i>pattern</i>
   */
  static void validate(String target, String pattern) throws ValidationException {
    if (!target.matches(pattern)) {
      LOG.error(target + pattern);
      throw new ValidationException(ERROR + " string is: " + target +
          ", but allowed: " + pattern);
    }
  }

  /**
   * Checks for max allowed length to string.
   * 
   * @param target
   *          - A string to be checked
   * @param length
   *          - Max allowed string length including it
   * @throws ValidationException
   *           If <i>target's</i> length is more than <i>length</i>
   */
  static void maxLength(String target, int length) throws ValidationException {
    if (!(target.length() <= length)) {
      LOG.error(target);
      throw new ValidationException(ERROR + "required min length: " + length);
    }
  }

  /**
   * Checks for min allowed length to string.
   * 
   * @param target
   *          - A string to be checked
   * @param length
   *          - Min allowed string length including it
   * @throws ValidationException
   *           If <i>target's</i> length is less than <i>length</i>
   */
  static void minLength(String target, int length) throws ValidationException {
    if (!(target.length() >= length)) {
      LOG.error(target);
      throw new ValidationException(ERROR + "required max length: " + length);
    }
  }

  /**
   * Checks for required length to string.
   * 
   * @param target
   *          - A string to be checked
   * @param length
   *          - Required string length
   * @throws ValidationException
   *           If <i>target's</i> length is not equal to the <i>length</i>
   */
  static void length(String target, int length) throws ValidationException {
    if (target.length() != length) {
      LOG.error(target);
      throw new ValidationException(ERROR + "required length: " + length);
    }
  }
}
