package ua.nure.suprun.SummaryTask4.constants;

public interface SessionConst {
	String USER_LANG = "lang";
	String USER = "user";
	String ROLE = "role";
	String UNIVER = "university";
	String ENR = "enrollee";
	
	String ERROR = "emessage";

}
