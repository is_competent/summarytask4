package ua.nure.suprun.SummaryTask4.constants;

/**
 * Holder for stored requests names.
 * 
 * @author B.Suprun
 * 
 */
public interface ReqName {

	/** USER **/

	String USER_LOGIN = "login";
	String USER_LANG = "setLang";
	String USER_BY_LOGIN = "getByLogin";
	String USER_BY_ID = "getById";
	String USER_APPLIED_TO_UNIV = "appliedToUniv";

	String USER_APP_CNT = "getAppCnt";

	/** REPORT_VIEW **/

	String RV_CREATE = "create";
	String RV_SHOW = "show";
	String RV_DATES = "getDates";

	/** DEPARTMENT **/

	String DEP_BY_NAME_ASC = "sortByNameAsc";
	String DEP_BY_NAME_DESC = "sortByNameDesc";
	String DEP_BY_BUDGET_DESC = "sortByBudgetDesc";
	String DEP_BY_BUDGET_ASC = "sortByBudgetAsc";
	String DEP_BY_COMM_ASC = "sortByCommonAsc";
	String DEP_BY_COMM_DESC = "sortByCommonDesc";

	/** EENROLLEE_MARK **/

	String EM_REMOVE = "remove";

	String UNIVERSITY_GET_BY_ADMIN = "byAdmin";

	String SUBJ_GET_SUBJ = "getAllSubj";
	String ENROLLEE_APPLIED = "getApplied";
	String RV_GET_BY_TIMESTAMP = "getByTimestamp";
	String DS_REMOVE = "REMOVE";
	String USER_SEARCH = "search";
  String USER_GET_ALL_ENROLLEES = "getAllEnrolles";
  String USER_GET_BLOCKED_ENROLLEE = "getBlockedEnrollees";
  
  String RV_ENROLLEES_APPLICATIONS = "getEnrolleeApplications";
  String ENROLLEE_DEP_PRIORITIES = "getPriorities";

}
