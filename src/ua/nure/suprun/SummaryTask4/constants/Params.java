package ua.nure.suprun.SummaryTask4.constants;

/**
 * Holder for command's name.
 * 
 * @author B.Suprun
 * 
 */
public interface Params {
	String COMMAND = "command";
	String REQ_PARSER = "ReqParser";

	/** REPORT_VIEW **/

	String RV_DATE = "date";

	/** AUTHORIZATION **/

	String AUT_ID = "code";
	String AUT_USER_ID = "uid";

	/** BLACKLIST **/

	String BL_ID = "id";

	/** DEPARTMENT **/

	String DEP_ID = "id";
	String DEP_BUDGET = "budget";
	String DEP_COMMON = "com";
	String DEP_NAME = "name";
	String DEP_UNIVERSITY = "univer";

	/** SUBJECT **/

	String SUBJ_ID = "id";
	String SUBJ_NAME = "name";

	/** COMMAND_ROLE **/

	String CR_COMMAND_ID = "cid";
	String CR_ROLE_ID = "rid";

	/** DEPARTMENT_SUBJ **/

	String DS_DEP_ID = "did";
	String DS_SUBJ_ID = "id";

	/** USER **/

	String USER_ID = "uid";
	String USER_EMAIL = "email";
	String USER_LANG = "lang";
	String USER_LAST_NAME = "lname";
	String USER_NAME = "name";
	String USER_PATR = "patr";
	String USER_PASS = "pass";
	String USER_ROLE = "role";

	/** ENROLLEE **/

	String ENROLLEE_ID = USER_ID;
	String ENROLLEE_AV_MARK = "am";
	String ENROLLEE_CITY = "city";
	String ENROLLEE_DISTRICT = "distr";
	String ENROLLEE_EDUC_NAME = "edname";
	String ENROLLEE_MARKS_SCAN = "scan";

	/** ENROLLEE_DEP **/

	String ED_ENROLLEE_ID = USER_ID;
	String ED_DEP_ID = DEP_ID;

	/** ENROLLEE_MARK **/

	String EM_ENROLLEE_ID = USER_ID;
	String EM_SUBJ = "sid";
	String EM_CERT_MARK = "cmark";
	String EM_TEST_MARK = "tmark";

	/** UNIVERSITY **/

	String UNIVERSITY_ID = "id";
	String UNIVERSITY_ADMIN = "aid";
	String UNIVERSITY_NAME = "uname";

	/** ROLE **/

	String ROLE_ID = "id";
	String ROLE_NAME = "name";
	String ROLE_DESCR = "descr";
	
	String CAPTCHA_VALUE = "captcha";
  String ED_PRIORITY = "priority";
	
}
