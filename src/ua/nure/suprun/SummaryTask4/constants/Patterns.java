package ua.nure.suprun.SummaryTask4.constants;

/**
 * Holder for commonly used value patterns.
 * 
 * @author B.Suprun
 * 
 */
public interface Patterns {
	String NAME = "^[[A-z][A-я]\\s'іІїЇєЄъьЁёэЭ\\-]{3,100}$";
	String UNIVERSITY_NAME = "(?iu)^[a-zа-я\\s'іІїЇєЄъьЁёэЭ\"\\.\\-]{2,100}$";
	String SCHOOL_NAME = "(?iu)^[a-zа-я0-9\\s'іІїЇєЄъьЁёэЭ№\"\\.\\-]{3,100}$";
	String EMAIL = "^([a-z0-9_\\+-]+\\.)*[a-z0-9_\\+-]+@[a-z0-9_-]+(\\.[a-z0-9_-]+)*\\.[a-z]{2,6}$";
	String PASS_HEX = "(?i)^[0-9a-f]{64}$";
	String LANG = "(?i)^\\w+$";
}
