package ua.nure.suprun.SummaryTask4.constants;

/**
 * Path holder (jsp pages, controller commands).
 * 
 * @author B.Suprun
 * 
 */
public interface Path {

	// pages
	String PAGE_LOGIN = "/login.jsp";
	String PAGE_INVITE = "/WEB-INF/jsp/root/invite.jsp";
	String PAGE_REGISTRY_ADMIN = "/WEB-INF/jsp/admin/registry.jsp";
	String PAGE_RESTORE = "/WEB-INF/jsp/restore.jsp";
	String PAGE_ERROR_PAGE = "/WEB-INF/jsp/error.jsp";
	String PAGE_INFO = "/WEB-INF/jsp/info.jsp";
	String PAGE_SETTINGS = "/WEB-INF/jsp/settings.jsp";
	String PAGE_JSON = "/WEB-INF/jsp/json.jsp";
	
	String PAGE_ADMIN = "/WEB-INF/jsp/index.jsp";
	String PAGE_ROOT = "/WEB-INF/jsp/index.jsp";
	String PAGE_ENROLLEE = "/WEB-INF/jsp/index.jsp";
	String PAGE_SUBJECTS = "/WEB-INF/jsp/root/subjects.jsp";
	String PAGE_DEPARTMENT_ADMIN = "/WEB-INF/jsp/admin/deps.jsp";
	String PAGE_DESCRIBE_DEP_ADMIN = "/WEB-INF/jsp/admin/dep.jsp";

	
	String COMMAND_RESTORE = "/do?command=restoreUser";
	String COMMAND_INFO = "do?command=info";
	String COMMAND_ERROR = "do?command=error";
	
	String TO_CONTROLLER = "do";
	String PROFILE = "do?command=profile";
	String LOGOUT = "do?command=logout";
	String COMMAND_GET_JSON = "do?command=getJson";
	String PAGE_MARKS = "/WEB-INF/jsp/enrollee/marks.jsp";
	String PAGE_UNIVERSITIES =  "/WEB-INF/jsp/enrollee/univers.jsp";
	String PAGE_ENROLLEE_DEPS =  "/WEB-INF/jsp/enrollee/deps.jsp";
	String PAGE_ENROLEE_APPLIED = "/WEB-INF/jsp/enrollee/applied.jsp";
	String PAGE_GET_PDF = "/WEB-INF/jsp/admin/pdf.jsp";
	String PAGE_ADMIN_REPORT_DATES = "/WEB-INF/jsp/admin/reportdates.jsp";
  String PAGE_ADMIN_ENROLLEES = "/WEB-INF/jsp/admin/enrollees.jsp";
  String PAGE_ADMIN_DESCRIBE_ENROLLEE = "/WEB-INF/jsp/admin/enrollee.jsp";
  String PAGE_REPORT_PAGE = "/WEB-INF/jsp/root/report.jsp";
}