package ua.nure.suprun.SummaryTask4.constants;

/**
 * Holder for all table's column name.
 * 
 * @author B.Suprun
 * 
 */
public interface Columns {
	/** USER **/

	String USER_ID = "uId";
	String USER_NAME = "uName";
	String USER_LAST_NAME = "uLastName";
	String USER_PATR = "uPatr";
	String USER_EMAIL = "uEmail";
	String USER_ROLE = "uRole";
	String USER_PASS = "uPass";
	String USER_LANG = "uLang";

	/** UNIVERSITY **/

	String UNIV_ID = "unId";
	String UNIV_NAME = "unName";
	String UNIV_ADMIN = "unAdmin";

	/** SUBJECT **/

	String SUBJ_ID = "sId";
	String SUBJ_NAME = "sName";

	/** ROLE **/

	String ROLE_ID = "rId";
	String ROLE_NAME = "rName";
	String ROLE_DESCR = "rDescr";

	/** REPORT_VIEW **/

	String REPORT_VIEW_UNIVERSITY = "unName";
	String REPORT_VIEW_DEP_NAME = "dName";
	String REPORT_VIEW_LAST_NAME = "uLastName";
	String REPORT_VIEW_NAME = "uName";
	String REPORT_VIEW_PATR = "uPatr";
	String REPORT_VIEW_DATE = "repDate";
	String REPORT_VIEW_BUDGET = "repIsBudget";

	/** ENROLLEE_MARK **/

	String ENROLLEE_MARK_TEST = "emTestMark";
	String ENROLLEE_MARK_CERT = "emCertMark";
	String ENROLLEE_MARK_SUBJ = "emSubjId";
	String ENROLLEE_MARK_ENROLLEE = "emEnId";

	/** ENROLLEE_DEP **/

	String ENROLLEE_DEP_ENROLLEE = "edEnrId";
	String ENROLLEE_DEP_DEP = "edDeptId";

	/** ENROLLEE **/

	String ENROLLEE_AV_MARK = "eAvMark";
	String ENROLLEE_MARKS_SCAN = "eMarks";
	String ENROLLEE_EDUCATION_NAME = "eEducName";
	String ENROLLEE_DISTRICT = "eDistr";
	String ENROLLEE_CITY = "eCity";
	String ENROLLEE_ID = "eId";

	/** DEPARTMENT_SUBJECT **/

	String DEPT_SUBJ_SUBJECT = "dsSubjId";
	String DEPT_SUBJ_DEP = "dsDepId";

	/** DEPARTMENT **/

	String DEPT_UNIVERSITY = "dUniversity";
	String DEPT_COMMON = "dCommon";
	String DEPT_BUDGET = "dBudget";
	String DEPT_NAME = "dName";
	String DEPT_ID = "dId";

	/** BLACKLIST **/

	String BLACKLIST_ID = "bId";

	/** AUTHORIZATION **/

	String AUTH_ID = "aId";
	String AUTH_USER_ID = "aUId";
	String REPORT_VIEW_USER_ID = USER_ID;
	String REPORT_VIEW_USER_EMAIL = USER_EMAIL;
	String ENROLLEE_DEP_DATE = "edRegDate";
	String REPORT_VIEW_USER_MARK = "eAvMark";
	String REPORT_VIEW_ASERTION_DATE = "repAssertionDate";
  String ENROLLEE_DEP_PRIOR = "edPrior";
}
