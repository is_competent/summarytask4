package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class UniversityBuilderTest {

  public static final UniversityBuilder BUILDER = new UniversityBuilder();
  public static final University RESULT = new University();

  static {
    RESULT.setId(10);
    RESULT.setName("Test");
    RESULT.setAdmin(10);
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getInt(Columns.UNIV_ADMIN)).thenReturn(RESULT.getAdmin());
    when(rs.getInt(Columns.UNIV_ID)).thenReturn(RESULT.getId());
    when(rs.getString(Columns.UNIV_NAME)).thenReturn(RESULT.getName());
    University a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest rs = mock(HttpServletRequest.class);

    RequestParser parser = mock(RequestParser.class);


    when(parser.getInt(Params.UNIVERSITY_ADMIN, 0)).thenReturn(RESULT.getAdmin());
    when(parser.getInt(Params.UNIVERSITY_ID, 0)).thenReturn(RESULT.getId());
    when(parser.getString(Params.UNIVERSITY_NAME)).thenReturn(RESULT.getName());
    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(parser);

    University a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    when(rs.getInt(1)).thenReturn(10);
    University u = new University();
    BUILDER.setAfterInsert(u, rs);
    
    assertEquals(10, u.getId(), 0);
  }

}
