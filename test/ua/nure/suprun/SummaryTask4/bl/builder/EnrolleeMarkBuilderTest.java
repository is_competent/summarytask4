package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeMark;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class EnrolleeMarkBuilderTest {
  public static final EnrolleeMarkBuilder BUILDER = new EnrolleeMarkBuilder();
  public static final EnrolleeMark RESULT = new EnrolleeMark();

  static {
    RESULT.setCertMark(10);
    RESULT.setEnId(10);
    RESULT.setSubjId(10);
    RESULT.setSubjName("dcd");
    RESULT.setTestMark(10);
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getInt(Columns.ENROLLEE_MARK_CERT)).thenReturn(RESULT.getCertMark());
    when(rs.getInt(Columns.ENROLLEE_MARK_TEST)).thenReturn(RESULT.getTestMark());
    when(rs.getInt(Columns.ENROLLEE_MARK_ENROLLEE)).thenReturn(RESULT.getEnId());
    when(rs.getInt(Columns.ENROLLEE_MARK_SUBJ)).thenReturn(RESULT.getSubjId());
    
    EnrolleeMark a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest rs = mock(HttpServletRequest.class);

    RequestParser rp = mock(RequestParser.class);

    when(rp.getInt(Params.EM_CERT_MARK)).thenReturn(RESULT.getCertMark());
    when(rp.getInt(Params.EM_TEST_MARK)).thenReturn(RESULT.getTestMark());
    when(rp.getInt(Params.EM_ENROLLEE_ID, Constant.ID_NOT_SET)).thenReturn(RESULT.getEnId());
    when(rp.getInt(Params.EM_SUBJ)).thenReturn(RESULT.getSubjId());
    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(rp);

    EnrolleeMark a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    BUILDER.setAfterInsert(RESULT, rs);
  }

}
