package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.ReportView;
import ua.nure.suprun.SummaryTask4.constants.Columns;

public class ReportBuilderTest {

  public static final ReportBuilder BUILDER = new ReportBuilder();
  public static final ReportView RESULT = new ReportView();

  static {
    RESULT.setDate(new Date(System.currentTimeMillis()));
    RESULT.setDepName("dsdsds");
    RESULT.setEnrLastName("cdcdcd");
    RESULT.setEnrName("ccc");
    RESULT.setEnrPatronimic("cdcdcdc");
    RESULT.setStudyMode("ccdd");
    RESULT.setUniversityName("xsddcd");
    RESULT.setUId(10);
    RESULT.setAssertionDate(new Date(System.currentTimeMillis()));
    RESULT.setUEmail("cdcdcd");
    RESULT.setAvgMark(393);
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getDate(Columns.REPORT_VIEW_DATE)).thenReturn(RESULT.getDate());
    when(rs.getString(Columns.REPORT_VIEW_DEP_NAME)).thenReturn(RESULT.getDepName());
    when(rs.getString(Columns.REPORT_VIEW_LAST_NAME)).thenReturn(RESULT.getEnrLastName());
    when(rs.getString(Columns.REPORT_VIEW_NAME)).thenReturn(RESULT.getEnrName());
    when(rs.getString(Columns.REPORT_VIEW_PATR)).thenReturn(RESULT.getEnrPatronimic());
    when(rs.getString(Columns.REPORT_VIEW_BUDGET)).thenReturn(RESULT.getStudyMode());
    when(rs.getString(Columns.REPORT_VIEW_UNIVERSITY)).thenReturn(RESULT.getUniversityName());
    when(rs.getInt(Columns.REPORT_VIEW_USER_ID)).thenReturn(RESULT.getUId());
    when(rs.getDate(Columns.REPORT_VIEW_ASERTION_DATE)).thenReturn(RESULT.getAssertionDate());
    when(rs.getString(Columns.REPORT_VIEW_USER_EMAIL)).thenReturn(RESULT.getUEmail());
    when(rs.getInt(Columns.REPORT_VIEW_USER_MARK)).thenReturn((int) RESULT.getAvgMark());

    ReportView a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void testCreateHttpServletRequest() {
    HttpServletRequest req = null;
    BUILDER.create(req);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    BUILDER.setAfterInsert(RESULT, rs);
  }

}
