package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class UserBuilderTest {

  public static final UserBuilder BUILDER = new UserBuilder();
  public static final User RESULT = new User();

  static {
    RESULT.setId(10);
    RESULT.setName("Test");
    RESULT.setLName("fdddf");
    RESULT.setPatr("fdddf");
    RESULT.setPass("fdddf");
    RESULT.setEmail("fdddf");
    RESULT.setLang("fdddf");
    RESULT.setRole(1);
    
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getString(Columns.USER_EMAIL)).thenReturn(RESULT.getEmail());
    when(rs.getInt(Columns.USER_ID)).thenReturn(RESULT.getId());
    when(rs.getString(Columns.USER_LANG)).thenReturn(RESULT.getLang());
    when(rs.getString(Columns.USER_LAST_NAME)).thenReturn(RESULT.getLName());
    when(rs.getString(Columns.USER_NAME)).thenReturn(RESULT.getName());
    when(rs.getString(Columns.USER_PASS)).thenReturn(RESULT.getPass());
    when(rs.getString(Columns.USER_PATR)).thenReturn(RESULT.getPatr());
    when(rs.getInt(Columns.USER_ROLE)).thenReturn(RESULT.getRole());
    User a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest req = mock(HttpServletRequest.class);

    RequestParser rs = mock(RequestParser.class);

    when(rs.getString(Params.USER_EMAIL)).thenReturn(RESULT.getEmail());
    when(rs.getInt(Params.USER_ID, 0 )).thenReturn(RESULT.getId());
    when(rs.getString(Params.USER_LANG, "en")).thenReturn(RESULT.getLang());
    when(rs.getString(Params.USER_LAST_NAME)).thenReturn(RESULT.getLName());
    when(rs.getString(Params.USER_NAME)).thenReturn(RESULT.getName());
    when(rs.getString(Params.USER_PASS)).thenReturn(RESULT.getPass());
    when(rs.getString(Params.USER_PATR)).thenReturn(RESULT.getPatr());
    when(rs.getInt(Params.USER_ROLE, 0)).thenReturn(RESULT.getRole());
    
    when(req.getAttribute(Params.REQ_PARSER)).thenReturn(rs);

    User a = BUILDER.create(req);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    when(rs.getInt(1)).thenReturn(10);
    User u = new User();
    BUILDER.setAfterInsert(u, rs);
    
    assertEquals(10, u.getId(), 0);
  }

}
