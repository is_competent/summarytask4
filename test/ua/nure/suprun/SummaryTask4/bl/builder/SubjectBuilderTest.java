package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.Subject;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class SubjectBuilderTest {

  public static final SubjectBuilder BUILDER = new SubjectBuilder();
  public static final Subject RESULT = new Subject();

  static {
    RESULT.setId(10);
    RESULT.setName("Test");
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getInt(Columns.SUBJ_ID)).thenReturn(RESULT.getId());
    when(rs.getString(Columns.SUBJ_NAME)).thenReturn(RESULT.getName());
    Subject a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest rs = mock(HttpServletRequest.class);

    RequestParser parser = mock(RequestParser.class);
    when(parser.getInt(Params.SUBJ_ID)).thenReturn(RESULT.getId());
    when(parser.getString(Params.SUBJ_NAME)).thenReturn(RESULT.getName());
    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(parser);

    Subject a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    Subject s = new Subject();
    when(rs.getInt(1)).thenReturn(2);
    BUILDER.setAfterInsert(s, rs);

    assertEquals(2, s.getId(), 0);
  }

}
