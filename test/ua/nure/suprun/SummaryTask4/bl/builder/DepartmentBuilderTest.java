package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class DepartmentBuilderTest {
  
  public static final DepartmentBuilder BUILDER = new DepartmentBuilder();
  public static final Department RESULT = new Department();

  static {
    RESULT.setBudgetPlaces(10);
    RESULT.setCommonPlaces(10);
    RESULT.setId(10);
    RESULT.setName("test");
    RESULT.setUniversity(10);
  }
  
  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getString(Columns.DEPT_NAME)).thenReturn(RESULT.getName());
    when(rs.getInt(Columns.DEPT_BUDGET)).thenReturn(RESULT.getBudgetPlaces());
    when(rs.getInt(Columns.DEPT_COMMON)).thenReturn(RESULT.getCommonPlaces());
    when(rs.getInt(Columns.DEPT_ID)).thenReturn(RESULT.getId());
    when(rs.getInt(Columns.DEPT_UNIVERSITY)).thenReturn(RESULT.getUniversity());
    Department a = BUILDER.create(rs);
    
    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest request = mock(HttpServletRequest.class);

    RequestParser rs = mock(RequestParser.class);

    when(rs.getString(Params.DEP_NAME)).thenReturn(RESULT.getName());
    when(rs.getInt(Params.DEP_BUDGET)).thenReturn(RESULT.getBudgetPlaces());
    when(rs.getInt(Params.DEP_COMMON)).thenReturn(RESULT.getCommonPlaces());
    when(rs.getInt(Params.DEP_ID, 0)).thenReturn(RESULT.getId());
    when(rs.getInt(Params.DEP_UNIVERSITY, 0)).thenReturn(RESULT.getUniversity());
    when(request.getAttribute(Params.REQ_PARSER)).thenReturn(rs);
    Department a = BUILDER.create(request);
    
    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    
    when(rs.getInt(1)).thenReturn(10);
    Department d = new Department();
    BUILDER.setAfterInsert(d, rs);
    
    assertEquals(10, d.getId(), 0);
  }

}
