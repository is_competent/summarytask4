package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.Role;
import ua.nure.suprun.SummaryTask4.constants.Columns;

public class RoleBuilderTest {
  public static final RoleBuilder BUILDER = new RoleBuilder();
  public static final Role RESULT = new Role();

  static {
    RESULT.setId(10);
    RESULT.setName("test");
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getString(Columns.ROLE_DESCR)).thenReturn(RESULT.getDescr());
    when(rs.getInt(Columns.ROLE_ID)).thenReturn(RESULT.getId());
    when(rs.getString(Columns.ROLE_NAME)).thenReturn(RESULT.getName());
    Role a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void testCreateHttpServletRequest() {
    HttpServletRequest rs = mock(HttpServletRequest.class);
    BUILDER.create(rs);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    when(rs.getInt(1)).thenReturn(2);
    Role r = new Role();

    BUILDER.setAfterInsert(r, rs);
    assertEquals(2, r.getId());
  }

}
