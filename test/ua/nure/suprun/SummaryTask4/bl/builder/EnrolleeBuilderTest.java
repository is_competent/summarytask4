package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class EnrolleeBuilderTest {

  public static final EnrolleeBuilder BUILDER = new EnrolleeBuilder();
  public static final Enrollee RESULT = new Enrollee();

  static {
    RESULT.setAvMark(10.0);
    RESULT.setCity("test");
    RESULT.setDistrName("test");
    RESULT.setEducName("test");
    RESULT.setId(10);
    RESULT.setMarksScan(new byte[] { 10 });
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getDouble(Columns.ENROLLEE_AV_MARK)).thenReturn(RESULT.getAvMark());
    when(rs.getString(Columns.ENROLLEE_CITY)).thenReturn(RESULT.getCity());
    when(rs.getString(Columns.ENROLLEE_DISTRICT)).thenReturn(RESULT.getDistrName());
    when(rs.getString(Columns.ENROLLEE_EDUCATION_NAME)).thenReturn(RESULT.getEducName());
    when(rs.getInt(Columns.ENROLLEE_ID)).thenReturn(RESULT.getId());
    when(rs.getBytes(Columns.ENROLLEE_MARKS_SCAN)).thenReturn(RESULT.getMarksScan());

    Enrollee a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() throws IOException {
    HttpServletRequest rs = mock(HttpServletRequest.class);
    RequestParser rp = mock(RequestParser.class);

    when(rp.getDouble(Params.ENROLLEE_AV_MARK, 0)).thenReturn(RESULT.getAvMark());
    when(rp.getString(Params.ENROLLEE_CITY)).thenReturn(RESULT.getCity());
    when(rp.getString(Params.ENROLLEE_DISTRICT)).thenReturn(RESULT.getDistrName());
    when(rp.getString(Params.ENROLLEE_EDUC_NAME)).thenReturn(RESULT.getEducName());
    when(rp.getInt(Params.ENROLLEE_ID, Constant.ID_NOT_SET)).thenReturn(RESULT.getId());
    when(rp.getBuffer()).thenReturn(RESULT.getMarksScan());

    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(rp);

    Enrollee a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    BUILDER.setAfterInsert(RESULT, rs);
  }

}
