package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.Authorization;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class AuthBuilderTest {
  public static final AuthBuilder BUILDER = new AuthBuilder();
  public static final Authorization RESULT = new Authorization();

  static {
    RESULT.setId("blah-blah");
    RESULT.setUserId(1);
  }

  @Test
  public void testCreateResultSet() throws SQLException {

    ResultSet rs = mock(ResultSet.class);

    when(rs.getString(Columns.AUTH_ID)).thenReturn(RESULT.getId());
    when(rs.getInt(Columns.AUTH_USER_ID)).thenReturn(RESULT.getUserId());
    Authorization a = BUILDER.create(rs);

    assertEquals(RESULT, a);

  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest rs = mock(HttpServletRequest.class);

    RequestParser parser = mock(RequestParser.class);
    
    when(parser.getString(Params.AUT_ID)).thenReturn(RESULT.getId());
    when(parser.getInt(Params.AUT_USER_ID)).thenReturn(RESULT.getUserId());
    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(parser);
    
    Authorization a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    BUILDER.setAfterInsert(RESULT, rs);
  }

}
