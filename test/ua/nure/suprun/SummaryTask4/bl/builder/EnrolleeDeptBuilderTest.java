package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeDept;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Constant;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class EnrolleeDeptBuilderTest {

  public static final EnrolleeDeptBuilder BUILDER = new EnrolleeDeptBuilder();
  public static final EnrolleeDept RESULT = new EnrolleeDept();

  static {
    RESULT.setDepId(10);
    RESULT.setEnId(10);
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getInt(Columns.ENROLLEE_DEP_DEP)).thenReturn(RESULT.getDepId());
    when(rs.getInt(Columns.ENROLLEE_DEP_ENROLLEE)).thenReturn(RESULT.getEnId());
    EnrolleeDept a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest rs = mock(HttpServletRequest.class);

    RequestParser parser = mock(RequestParser.class);

    when(parser.getInt(Params.ED_DEP_ID)).thenReturn(RESULT.getDepId());
    when(parser.getInt(Params.ED_ENROLLEE_ID, Constant.ID_NOT_SET)).thenReturn(
        RESULT.getEnId());
    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(parser);

    EnrolleeDept a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    BUILDER.setAfterInsert(RESULT, rs);
  }

}
