package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.Blacklist;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class BlacklistBuilderTest {
  
  public static final BlacklistBuilder BUILDER = new BlacklistBuilder();
  public static final Blacklist RESULT = new Blacklist();

  static {
    RESULT.setId(1);
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getInt(Columns.BLACKLIST_ID)).thenReturn(RESULT.getId());
    Blacklist a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() {
    HttpServletRequest rs = mock(HttpServletRequest.class);

    RequestParser parser = mock(RequestParser.class);
    
    when(parser.getInt(Params.BL_ID)).thenReturn(RESULT.getId());
    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(parser);
    
    Blacklist a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    BUILDER.setAfterInsert(RESULT, rs);
  }

}
