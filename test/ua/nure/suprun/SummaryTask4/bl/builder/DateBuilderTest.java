package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class DateBuilderTest {
  
  public static final DateBuilder BUILDER = new DateBuilder();
  public static final Date RESULT = new Date(System.currentTimeMillis());
  
  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getDate(Columns.REPORT_VIEW_DATE)).thenReturn(RESULT);
    Date a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest rs = mock(HttpServletRequest.class);

    RequestParser parser = mock(RequestParser.class);
    
    when(parser.getDate(Params.RV_DATE)).thenReturn(RESULT);
    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(parser);
    
    Date a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test(expected = UnsupportedOperationException.class)
  public void testSetAfterInsert() throws SQLException {
    BUILDER.setAfterInsert(null, null);
  }

}
