package ua.nure.suprun.SummaryTask4.bl.builder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.servlet.http.HttpServletRequest;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.DepartmentSubject;
import ua.nure.suprun.SummaryTask4.constants.Columns;
import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;

public class DepartmentSubjectBuilderTest {
  public static final DepartmentSubjectBuilder BUILDER = new DepartmentSubjectBuilder();
  public static final DepartmentSubject RESULT = new DepartmentSubject();

  static {
    RESULT.setDepId(10);
    RESULT.setSubjId(10);
  }

  @Test
  public void testCreateResultSet() throws SQLException {
    ResultSet rs = mock(ResultSet.class);

    when(rs.getInt(Columns.DEPT_SUBJ_DEP)).thenReturn(RESULT.getDepId());
    when(rs.getInt(Columns.DEPT_SUBJ_SUBJECT)).thenReturn(RESULT.getSubjId());
    DepartmentSubject a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testCreateHttpServletRequest() {
    HttpServletRequest rs = mock(HttpServletRequest.class);

    RequestParser parser = mock(RequestParser.class);

    when(parser.getInt(Params.DS_DEP_ID)).thenReturn(RESULT.getDepId());
    when(parser.getInt(Params.DS_SUBJ_ID)).thenReturn(RESULT.getSubjId());
    when(rs.getAttribute(Params.REQ_PARSER)).thenReturn(parser);

    DepartmentSubject a = BUILDER.create(rs);

    assertEquals(RESULT, a);
  }

  @Test
  public void testSetAfterInsert() throws SQLException {
    ResultSet rs = mock(ResultSet.class);
    BUILDER.setAfterInsert(RESULT, rs);
  }

}
