package ua.nure.suprun.SummaryTask4.bl.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class AuthorizationTest {

  @Test
  public void testHashCode() {
    Authorization a = new Authorization();
    a.toString();
    a.setId("dsddsdfdfdffdf");

    assertNotNull(a.hashCode());
  }

  @Test
  public void testSetId() {
    Authorization a = new Authorization();
    a.setId("test");

    assertEquals("test", a.getId());
  }

  @Test
  public void testSetUserId() {
    Authorization a = new Authorization();
    a.setUserId(1);

    assertEquals(1, a.getUserId(), 0);
  }
  
  @Test
  public void testEquals() {
    Authorization a = new Authorization();
    Authorization a2 = new Authorization();
    a.setId("test");
    a2.setId("test2");
    assertFalse(a.equals(null));
    assertFalse(a.equals(new Object()));
    assertFalse(a.equals(a2));
    
    a2.setId("test");
    assertTrue(a.equals(a2));
    
    a2.setId(null);
    assertFalse(a.equals(a2));
    a2.setId("test");
    a.setId(null);
    assertFalse(a.equals(a2));
    
    assertTrue(a.equals(a));
  }

}
