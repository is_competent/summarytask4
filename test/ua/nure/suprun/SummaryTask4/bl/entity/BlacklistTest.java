package ua.nure.suprun.SummaryTask4.bl.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class BlacklistTest {

  @Test
  public void testHashCode() {
    Blacklist bl = new Blacklist();

    bl.setId(10);
    assertEquals(31 + 10, bl.hashCode(), 0);
  }

  @Test
  public void testSetId() {
    Blacklist bl = new Blacklist();

    bl.setId(10);
    assertEquals(10, bl.getId(), 0);
  }

  @Test
  public void testEqualsObject() {
    Blacklist a = new Blacklist();
    Blacklist a2 = new Blacklist();
    a.setId(1);
    a2.setId(2);
    
    assertFalse(a.equals(null));
    assertFalse(a.equals(new Object()));
    assertFalse(a.equals(a2));
    
    a2.setId(1);
    assertTrue(a.equals(a2));
    
    assertTrue(a.equals(a));
  }

}
