package ua.nure.suprun.SummaryTask4.bl.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import javax.xml.bind.ValidationException;

import org.junit.Test;

public class DepartmentTest {

  @Test
  public void testHashCode() {
    Department d = new Department();

    d.setId(1);

    assertEquals(31 + 1, d.hashCode(), 0);
  }

  @Test
  public void testSetId() {
    Department d = new Department();

    d.setId(1);

    assertEquals(1, d.getId(), 0);
  }

  @Test
  public void testSetName() {
    Department d = new Department();

    d.setName("name");

    assertEquals("name", d.getName());
  }

  @Test
  public void testSetCommonPlaces() {
    Department d = new Department();

    d.setCommonPlaces(10);

    assertEquals(10, d.getCommonPlaces(), 0);
  }

  @Test
  public void testSetBudgetPlaces() {
    Department d = new Department();

    d.setBudgetPlaces(10);

    assertEquals(10, d.getBudgetPlaces(), 0);
  }

  @Test
  public void testSetUniversity() {
    Department d = new Department();

    d.setUniversity(10);

    assertEquals(10, d.getUniversity(), 0);
  }

  @Test
  public void testToString() {
    Department d = new Department();

    d.setCommonPlaces(10);
    
    assertNotNull(d.toString());
  }

  @Test
  public void testEqualsObject() {
    Department a = new Department();
    Department a2 = new Department();
    a.setId(1);
    a2.setId(2);
    
    assertFalse(a.equals(null));
    assertFalse(a.equals(new Object()));
    assertFalse(a.equals(a2));
    
    a2.setId(1);
    assertTrue(a.equals(a2));
    assertTrue(a.equals(a));
  }

  @Test
  public void testValidate() {
    Department d = new Department();

    d.setCommonPlaces(10);
    d.setBudgetPlaces(10);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateNegative() throws ValidationException{
    Department d = new Department();

    d.setCommonPlaces(1);
    d.setBudgetPlaces(10);
    d.validate();
  }

}
