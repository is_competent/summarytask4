package ua.nure.suprun.SummaryTask4.bl.entity;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class EntityTest {

  @Test
  public void test() {
    new Authorization();
    new Blacklist();
    new Department();
    new DepartmentSubject();
    new Enrollee();
    new EnrolleeDept();
    new EnrolleeMark();
    new ReportView();
    new Role();
    new Subject();
    new University();
    new User();
  }

  @Test
  public void testInvitedAdmin() {
    User u = new User();
    u.setName("Invited");
    u.setLName("Invited");
    u.setPatr("Invited");
    u.setRole(CommonRoles.ADMIN.getRole().getId());
    assertTrue(CommonRoles.isInvitedAdmin(u));
  }
  
  @Test
  public void testInvitedAdminFalse() {
    User u = new User();
    u.setName("Invited");
    u.setLName("Inviteddsdsds");
    u.setPatr("Invited");
    u.setRole(CommonRoles.ADMIN.getRole().getId());
    assertFalse(CommonRoles.isInvitedAdmin(u));
    assertFalse(CommonRoles.isInvitedAdmin(null));
  }
  
  @Test
  public void testRoot() {
    User u = new User();
    u.setName("root");
    u.setLName("root");
    u.setPatr("root");
    u.setEmail("root@root.com");
    
    u.setRole(CommonRoles.ROOT.getRole().getId());
    assertTrue(CommonRoles.isDefaultRoot(u));
  }
  
  @Test
  public void testGetRole() {
    User uAdmin = new User();
    uAdmin.setRole(CommonRoles.ADMIN.getRole().getId());
    
    assertEquals(CommonRoles.ADMIN.getRole(), CommonRoles.getUserRole(uAdmin));
    assertEquals(CommonRoles.NO_CONTROL.getRole(), CommonRoles.getUserRole(null));
  }

}
