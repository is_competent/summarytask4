package ua.nure.suprun.SummaryTask4.anotation.validation.validators;

import javax.xml.bind.ValidationException;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.anotation.util.TestEntity;
import ua.nure.suprun.SummaryTask4.anotation.validation.validatorsimp.DecimalValidator;
import ua.nure.suprun.SummaryTask4.anotation.validation.validatorsimp.StringValidator;
import ua.nure.suprun.SummaryTask4.anotation.validation.validatorsimp.Validator;

public class ValidatorTest {

  @Test(expected=ValidationException.class)
  public void testValidate() throws ValidationException {
    new Validator().validate(new TestEntity());
  }
  
  @Test
  public void testValidateTrue() throws ValidationException {
    new DecimalValidator();
    new StringValidator();
    
    TestEntity en = new TestEntity();
    en.setId(2);
    en.setRole(1);
    en.setName("Suprun");
    
    new Validator().validate(en);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateFalse() throws ValidationException {
    TestEntity en = new TestEntity();
    en.setId(10);
    en.setRole(1);
    en.setName("Suprun");
    
    new Validator().validate(en);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateFalseRole() throws ValidationException {
    TestEntity en = new TestEntity();
    en.setId(1);
    en.setRole(40);
    en.setName("Suprun");
    
    new Validator().validate(en);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateFalseName() throws ValidationException {
    TestEntity en = new TestEntity();
    en.setId(1);
    en.setRole(1);
    en.setName("Supruncdicidicidcidicidcidicidcidi");
    
    new Validator().validate(en);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateFalseLowBoundId() throws ValidationException {
    TestEntity en = new TestEntity();
    en.setId(-10);
    en.setRole(1);
    en.setName("Suprun");
    
    new Validator().validate(en);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateFalseLowBoundRole() throws ValidationException {
    TestEntity en = new TestEntity();
    en.setId(1);
    en.setRole(-1);
    en.setName("Suprun");
    
    new Validator().validate(en);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateFalseLowBoundName() throws ValidationException {
    TestEntity en = new TestEntity();
    en.setId(1);
    en.setRole(1);
    en.setName("S");
    
    new Validator().validate(en);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateFalseLowBoundTest() throws ValidationException {
    TestEntity en = new TestEntity();
    en.setId(1);
    en.setRole(1);
    en.setName("Suprun");
    en.setTest("fd");
    
    new Validator().validate(en);
  }
  
  @Test(expected = ValidationException.class)
  public void testValidateFalsePattern() throws ValidationException {
    TestEntity en = new TestEntity();
    en.setId(1);
    en.setRole(1);
    en.setName("Suprun535");
    
    new Validator().validate(en);
  }

}
