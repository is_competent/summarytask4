package ua.nure.suprun.SummaryTask4.anotation.util;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntity;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.anotation.validation.Length;
import ua.nure.suprun.SummaryTask4.anotation.validation.Less;
import ua.nure.suprun.SummaryTask4.anotation.validation.LessOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.MaxLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.MinLength;
import ua.nure.suprun.SummaryTask4.anotation.validation.More;
import ua.nure.suprun.SummaryTask4.anotation.validation.MoreOrEqual;
import ua.nure.suprun.SummaryTask4.anotation.validation.Pattern;

@SQLRequests({
  @Request(name = "thisTable", value = "<:tn> {id}{name}"),
  @Request(name = "thisAndAnother", value = "<:Blacklist><:tn><:User><:Authorization as a><:Department> {a.id}"),
})
@DBEntity("MOCK_USER")
public class TestEntity {
  
  @MoreOrEqual(0)
  @LessOrEqual(2)
  @DBEntField(value = "mockId", updatable = false)
  @DBEntPk
  private int id;

  @MinLength(3)
  @MaxLength(10)
  @Pattern("^[[A-z][A-я]\\s'іІїЇєЄъьЁёэЭ\\-]+")
  @DBEntField(value = "mockName", insertable = false)
  private String name = "fvf";

  @More(0)
  @Less(4)
  @DBEntField("mockRole")
  private int role;
  
  @Length(3)
  @DBEntField(value = "mockName", insertable = false, updatable = false)
  private String test = "Hel";
  
  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public int getRole() {
    return role;
  }

  public void setRole(int role) {
    this.role = role;
  }

  public String getTest() {
    return test;
  }

  public void setTest(String test) {
    this.test = test;
  }

}
