package ua.nure.suprun.SummaryTask4.anotation.util;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Map;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntField;
import ua.nure.suprun.SummaryTask4.anotation.entity.DBEntPk;
import ua.nure.suprun.SummaryTask4.anotation.entity.Request;
import ua.nure.suprun.SummaryTask4.anotation.entity.SQLRequests;
import ua.nure.suprun.SummaryTask4.bl.entity.User;

public class AnnotationOperationsTest {

  private static final String[] requests = { "<:tn> {id}{name}",
      "<:Blacklist><:tn><:User><:Authorization as a><:Department> {a.id}" };

  @Test
  public void testGetStoredRequests() {
    SQLRequests reqs = AnnotationOperations.getStoredRequests(TestEntity.class);
    assertNotNull(reqs);

    int i = 0;
    for (Request r : reqs.value()) {
      assertEquals(r.value(), requests[i++]);
    }
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetStoredRequestsException() {
    AnnotationOperations.getStoredRequests(String.class);
  }

  @Test
  public void testGetEntityPackage() {
    // Load package
    new User();

    String pack = AnnotationOperations.getEntityPackage();
    assertNotNull(pack);
    AnnotationOperations.getEntityPackage();
    assertNotNull(pack);
  }

  @Test
  public void testGetEntityName() {
    String entityName = AnnotationOperations.getEntityName(TestEntity.class);

    assertEquals("MOCK_USER", entityName);
  }

  @Test(expected = IllegalArgumentException.class)
  public void testGetEntityNameException() {
    AnnotationOperations.getEntityName(String.class);
  }

  @Test
  public void testGetFieldNamesClassOfQClassOfQextendsAnnotationFilter() {
    Map<String, String> res = AnnotationOperations.getFieldNames(TestEntity.class,
        DBEntField.class, new UpdateFilter());
    assertEquals(2, res.size(), 0);
    
    res = AnnotationOperations.getFieldNames(TestEntity.class,
        DBEntField.class, new InsertFilter());
    assertEquals(2, res.size(), 0);
  }

  @Test
  public void testGetFieldNamesClassOfQClassOfQextendsAnnotation() {
    Map<String, String> res = AnnotationOperations.getFieldNames(TestEntity.class,
        DBEntField.class);
    assertEquals(4, res.size(), 0);
  }

  @Test
  public void testGetFieldValues() throws IllegalAccessException {
    Map<String, Object> res = AnnotationOperations.getFieldValues(new TestEntity(),
        DBEntField.class);
    assertEquals(4, res.size(), 0);
  }

  @Test
  public void testGetAnnotatedFields() {
    List<Field> f = AnnotationOperations.getAnnotatedFields(TestEntity.class, DBEntPk.class);
    assertEquals(1, f.size(), 0);
  }

  @Test
  public void testGetPKFieldEntityName() {
    String s = AnnotationOperations.getPKFieldEntityName(TestEntity.class);
    assertEquals("mockId", s);
  }

  @Test
  public void testGetPKFieldName() {
    String s = AnnotationOperations.getPKFieldName(TestEntity.class);
    assertEquals("id", s);
  }

}
