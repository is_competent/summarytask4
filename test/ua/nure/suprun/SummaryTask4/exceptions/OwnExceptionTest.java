package ua.nure.suprun.SummaryTask4.exceptions;

import org.junit.Test;

public class OwnExceptionTest {

  @Test(expected = AppException.class)
  public void testAppException() throws AppException {
    throw new AppException("ex");
  }
  
  @Test(expected = DBException.class)
  public void testDBException() throws AppException {
    throw new DBException("ex");
  }
  
  @Test(expected = DMLException.class)
  public void testDMLException() throws AppException {
    throw new DMLException("ex");
  }

}
