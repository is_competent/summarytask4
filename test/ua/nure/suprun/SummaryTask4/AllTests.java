package ua.nure.suprun.SummaryTask4;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import ua.nure.suprun.SummaryTask4.anotation.util.AnnotationOperationsTest;
import ua.nure.suprun.SummaryTask4.anotation.validation.validators.ValidatorTest;
import ua.nure.suprun.SummaryTask4.bl.builder.AuthBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.BlacklistBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.DateBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentSubjectBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeDeptBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeMarkBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.ReportBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.RoleBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.SubjectBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.UniversityBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.builder.UserBuilderTest;
import ua.nure.suprun.SummaryTask4.bl.entity.AuthorizationTest;
import ua.nure.suprun.SummaryTask4.bl.entity.BlacklistTest;
import ua.nure.suprun.SummaryTask4.bl.entity.DepartmentTest;
import ua.nure.suprun.SummaryTask4.bl.entity.EntityTest;
import ua.nure.suprun.SummaryTask4.db.builder.DefaultRequestBuilderTest;
import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.BasicDAOImpTest;
import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.DepartmentDAOImpTest;
import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.EnrolleeDAOImpTest;
import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.ReportDAOImpTest;
import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.RoleDAOImpTest;
import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.SubjectDAOImpTest;
import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.UniversityDAOImpTest;
import ua.nure.suprun.SummaryTask4.db.daoimp.mysql.UserDAOImpTest;
import ua.nure.suprun.SummaryTask4.exceptions.OwnExceptionTest;
import ua.nure.suprun.SummaryTask4.web.controller.ControllerTest;
import ua.nure.suprun.SummaryTask4.web.filter.EncodingFilterTest;
import ua.nure.suprun.SummaryTask4.web.filter.RedirecrToIndexFilterTest;

@RunWith(Suite.class)
@SuiteClasses({ ValidatorTest.class, AnnotationOperationsTest.class, OwnExceptionTest.class,
    DefaultRequestBuilderTest.class, EntityTest.class, AuthorizationTest.class,
    DepartmentTest.class, BlacklistTest.class, AuthBuilderTest.class, DepartmentBuilderTest.class,
    DateBuilderTest.class, BlacklistBuilderTest.class, EnrolleeBuilderTest.class,
    DepartmentSubjectBuilderTest.class, EnrolleeDeptBuilderTest.class,
    EnrolleeMarkBuilderTest.class, ReportBuilderTest.class, RoleBuilderTest.class,
    SubjectBuilderTest.class, UniversityBuilderTest.class,
    UserBuilderTest.class, ControllerTest.class, EncodingFilterTest.class, 
    RedirecrToIndexFilterTest.class, UserDAOImpTest.class, UniversityDAOImpTest.class,
    SubjectDAOImpTest.class, RoleDAOImpTest.class, ReportDAOImpTest.class,
    EnrolleeDAOImpTest.class, DepartmentDAOImpTest.class, BasicDAOImpTest.class})
public class AllTests { }
