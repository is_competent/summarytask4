package ua.nure.suprun.SummaryTask4.web.controller;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.constants.Params;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.web.util.RequestParser;
public class ControllerTest {

  @Test
  public void testDoGetHttpServletRequestHttpServletResponse() throws ServletException, IOException {
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    RequestDispatcher disp = mock(RequestDispatcher.class);
    
    RequestParser parser = mock(RequestParser.class);
    when(parser.getCommand()).thenReturn("info");
    when(req.getAttribute(Params.REQ_PARSER)).thenReturn(parser);
    when(req.getRequestDispatcher(Path.PAGE_INFO)).thenReturn(disp);
    
    new Controller().doGet(req, resp);
    verify(disp).forward(req, resp);
    
  }

  @Test
  public void testDoPostHttpServletRequestHttpServletResponse() throws ServletException, IOException {
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    
    RequestParser parser = mock(RequestParser.class);
    when(parser.getCommand()).thenReturn("info");
    when(req.getAttribute(Params.REQ_PARSER)).thenReturn(parser);
    
    new Controller().doPost(req, resp);
    verify(resp).sendRedirect(Path.PAGE_INFO);
  }

}
