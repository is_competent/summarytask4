package ua.nure.suprun.SummaryTask4.web.filter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
public class EncodingFilterTest {

  @Test
  public void testDestroy() {
    new EncodingFilter().destroy();
  }

  @Test
  public void testInit() throws IOException, ServletException {
    FilterConfig conf = mock(FilterConfig.class);
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    FilterChain chain = mock(FilterChain.class);
    
    when(req.getCharacterEncoding()).thenReturn(null);
    when(conf.getInitParameter("def-encoding")).thenReturn("utf-8");
    
    EncodingFilter en = new EncodingFilter();
    en.init(conf);
    en.doFilter(req, resp, chain);
    verify(req).setCharacterEncoding("utf-8");
  }

}
