package ua.nure.suprun.SummaryTask4.web.filter;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.constants.Path;
import ua.nure.suprun.SummaryTask4.constants.SessionConst;
public class RedirecrToIndexFilterTest {

  @Test
  public void testDoFilter() throws IOException, ServletException {
    HttpServletRequest req = mock(HttpServletRequest.class);
    HttpServletResponse resp = mock(HttpServletResponse.class);
    HttpSession session = mock(HttpSession.class);
    
    when(req.getSession(false)).thenReturn(session);
    when(session.getAttribute(SessionConst.USER)).thenReturn(new User());
    
    new RedirecrToIndexFilter().doFilter(req, resp, null);
    verify(resp).sendRedirect(Path.PROFILE);
  }

}
