package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.db.NamedParameterStatement;
import ua.nure.suprun.SummaryTask4.db.builder.DefaultRequestBuilder;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

@RunWith(MockitoJUnitRunner.class)
public class BasicDAOImpTest {

  @Mock
  private DataSource ds;

  @Mock
  private Connection con;

  @Mock
  private PreparedStatement st;

  @Mock
  private ResultSet rs;

  private BasicDAOImp<Department> dao;

  @Spy
  private DepartmentBuilder builder = new DepartmentBuilder();

  @Before
  public void setUp() throws Exception {
    Date date = new Date(System.currentTimeMillis());
    when(rs.getDate(anyInt())).thenReturn(date);
    when(rs.getDate(anyString())).thenReturn(date);
    
    when(ds.getConnection()).thenReturn(con);
    when(con.prepareStatement(anyString(), anyInt())).thenReturn(st);
    when(st.executeQuery()).thenReturn(rs);
    when(st.executeUpdate()).thenReturn(1, 0);
    when(rs.next()).thenReturn(true, false);
    when(rs.getInt(1)).thenReturn(2);
    when(st.getGeneratedKeys()).thenReturn(rs);
    when(st.execute()).thenReturn(true, false);

    dao = new BasicDAOImp<>(builder, ds);
  }

  @Test
  public void testBasicDAOImpEntBuilderOfTRequestBuilderDataSource() {
    new BasicDAOImp<Department>(new DepartmentBuilder(), 
        new DefaultRequestBuilder(), ds);
  }

  @Test(expected=DMLException.class)
  public void testInvokeSelectEStringEntBuilderOfE() throws SQLException, DMLException {
    when(st.executeQuery()).thenThrow(new SQLException());
    dao.invokeSelect(new Department(), "jjcjdjdcd", new DepartmentBuilder());
  }

  @Test(expected = SQLException.class)
  public void testInvokeSimpleInsertENamedParameterStatementEntBuilderOfE() throws SQLException {
    Department d = new Department();
    d.setName("ddcdcd");
    d.setBudgetPlaces(10000);
    d.setCommonPlaces(10);
    dao.invokeSimpleUpd(d, new NamedParameterStatement(con, "cscdcd"));
  }

  @Test
  public void testExecute() throws DMLException {
    
    Department d = new Department();
    d.setName("ddcdcd");
    d.setBudgetPlaces(10);
    d.setCommonPlaces(10);
    assertTrue(dao.execute(d, "cdcd"));
    assertFalse(dao.execute(d, "cdcd"));
    assertFalse(dao.execute(null, "cdcd"));
  }
  
  @Test(expected = DMLException.class)
  public void testExecuteSQLError() throws DMLException, SQLException {
    when(st.execute()).thenThrow(new SQLException());
   
    dao.execute(null, "cdcd");
  }
  
  @Test(expected = DMLException.class)
  public void testExecuteValidError() throws DMLException, SQLException {
    Department d = new Department();
    d.setName("ddcdcd");
    d.setBudgetPlaces(100);
    d.setCommonPlaces(10);
   
    dao.execute(d, "cdcd");
  }

}
