package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import ua.nure.suprun.SummaryTask4.bl.builder.RoleBuilder;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

@RunWith(MockitoJUnitRunner.class)
public class RoleDAOImpTest {
  
  @Mock
  private DataSource ds;
  
  @Mock
  private Connection con;
  
  @Mock
  private PreparedStatement st;
  
  @Mock
  private ResultSet rs;
  
  private RoleDAOImp dao;
  
  @Spy
  private RoleBuilder builder = new RoleBuilder();
  
  @Before
  public void setUp() throws Exception {
    Date date = new Date(System.currentTimeMillis());
    when(rs.getDate(anyInt())).thenReturn(date);
    when(rs.getDate(anyString())).thenReturn(date);
    
    when(ds.getConnection()).thenReturn(con);
    when(con.prepareStatement(anyString(), anyInt())).thenReturn(st);
    when(st.executeQuery()).thenReturn(rs);
    when(st.executeUpdate()).thenReturn(1, 0);
    when(rs.next()).thenReturn(true, false);
    when(rs.getInt(1)).thenReturn(2);
    when(st.getGeneratedKeys()).thenReturn(rs);
    
    
    dao = new RoleDAOImp(builder, ds);
  }

  @Test
  public void testGetAll() throws DMLException {
    assertEquals(1, dao.getAll().size(),0);
  }

}
