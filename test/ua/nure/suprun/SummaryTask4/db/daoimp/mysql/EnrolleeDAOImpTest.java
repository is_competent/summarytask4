package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import ua.nure.suprun.SummaryTask4.bl.builder.EnrolleeBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeDept;
import ua.nure.suprun.SummaryTask4.bl.entity.EnrolleeMark;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

@RunWith(MockitoJUnitRunner.class)
public class EnrolleeDAOImpTest {
  
  @Mock
  private DataSource ds;
  
  @Mock
  private Connection con;
  
  @Mock
  private PreparedStatement st;
  
  @Mock
  private ResultSet rs;
  
  private EnrolleeDAOImp dao;
  
  @Spy
  private EnrolleeBuilder builder = new EnrolleeBuilder();
  
  @Before
  public void setUp() throws Exception {
    Date date = new Date(System.currentTimeMillis());
    when(rs.getDate(anyInt())).thenReturn(date);
    when(rs.getDate(anyString())).thenReturn(date);
    when(rs.getBytes(anyString())).thenReturn(new byte[1]);
    
    when(ds.getConnection()).thenReturn(con);
    
    when(con.prepareStatement(anyString(), anyInt())).thenReturn(st);
    when(st.executeQuery()).thenReturn(rs);
    when(st.executeUpdate()).thenReturn(1, 0);
    when(rs.next()).thenReturn(true, false);
    when(rs.getInt(1)).thenReturn(2);
    when(st.getGeneratedKeys()).thenReturn(rs);
    
    
    dao = new EnrolleeDAOImp(builder, ds);
  }

  @Test
  public void testGet() throws DMLException {
    assertNotNull(dao.get(1));
  }

  @Test
  public void testAddMark() throws DMLException {
    EnrolleeMark m = new EnrolleeMark();
    m.setCertMark(10);
    m.setEnId(10);
    m.setSubjId(10);
    m.setTestMark(200);
    assertTrue(dao.addMark(m));
  }

  @Test
  public void testRemoveMark() throws DMLException {
    EnrolleeMark m = new EnrolleeMark();
    m.setCertMark(10);
    m.setEnId(10);
    m.setSubjId(10);
    m.setTestMark(200);
    assertTrue(dao.removeMark(m));
  }

  @Test
  public void testGetMarks() throws DMLException {
    assertEquals(1, dao.getMarks(new Enrollee()).size(), 0);
  }

  @Test
  public void testApplyDep() throws DMLException {
    EnrolleeDept d = new EnrolleeDept();
    d.setDepId(10);
    d.setEnId(10);
    assertTrue(dao.applyDep(d));
  }
  
  @Test(expected = DMLException.class)
  public void testApplyDepErrorDup() throws DMLException, SQLException {
    when(st.executeUpdate()).thenThrow(new SQLException("Duplicate entry"));
    EnrolleeDept d = new EnrolleeDept();
    d.setDepId(10);
    d.setEnId(10);
    assertTrue(dao.applyDep(d));
  }
  
  @Test(expected = DMLException.class)
  public void testApplyDepErrorSubj() throws DMLException, SQLException {
    when(st.executeUpdate()).thenThrow(new SQLException("Subjects are not mutched"));
    EnrolleeDept d = new EnrolleeDept();
    d.setDepId(10);
    d.setEnId(10);
    assertTrue(dao.applyDep(d));
  }
  
  @Test(expected = DMLException.class)
  public void testApplyDepErrorAnother() throws DMLException, SQLException {
    when(st.executeUpdate()).thenThrow(new SQLException("Another"));
    EnrolleeDept d = new EnrolleeDept();
    d.setDepId(10);
    d.setEnId(10);
    assertTrue(dao.applyDep(d));
  }

  @Test
  public void testGetApplications() throws DMLException {
    assertEquals(1, dao.getApplications(12).size(), 0);
  }
  
  @Test(expected = DMLException.class)
  public void testGetApplicationsError() throws DMLException, SQLException {
    when(st.executeQuery()).thenThrow(new SQLException());
    dao.getApplications(12);
  }

}
