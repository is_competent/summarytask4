package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import ua.nure.suprun.SummaryTask4.bl.builder.ReportBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

@RunWith(MockitoJUnitRunner.class)
public class ReportDAOImpTest {

  @Mock
  private DataSource ds;

  @Mock
  private Connection con;

  @Mock
  private PreparedStatement st;

  @Mock
  private ResultSet rs;

  private ReportDAOImp dao;

  @Spy
  private ReportBuilder builder = new ReportBuilder();

  @Before
  public void setUp() throws Exception {
    Date date = new Date(System.currentTimeMillis());
    when(rs.getDate(anyInt())).thenReturn(date);
    when(rs.getDate(anyString())).thenReturn(date);
    
    when(ds.getConnection()).thenReturn(con);
    when(con.prepareStatement(anyString(), anyInt())).thenReturn(st);
    when(st.executeQuery()).thenReturn(rs);
    when(st.executeUpdate()).thenReturn(1, 0);
    when(rs.next()).thenReturn(true, false);
    when(rs.getInt(1)).thenReturn(2);
    when(st.getGeneratedKeys()).thenReturn(rs);

    dao = new ReportDAOImp(builder, ds);
  }

  @Test
  public void testCreateReport() throws DMLException, SQLException {
    when(rs.getTimestamp(anyString())).thenReturn(new Timestamp(System.currentTimeMillis()));
    assertNotNull(dao.createReport());
    assertNull(dao.createReport());
  }

  @SuppressWarnings("unchecked")
  @Test(expected = DMLException.class)
  public void testCreateReportError() throws DMLException, SQLException {
    when(rs.getTimestamp(anyString())).thenThrow(SQLException.class);
    dao.createReport();
  }

  @Test
  public void testShowReport() throws DMLException {
    assertEquals(1, dao.showReport(new Date(System.currentTimeMillis()), new University()).size(),
        0);
  }

  @Test
  public void testGetDates() throws DMLException {
    assertEquals(1, dao.getDates(new University()).size(), 0);
  }
  
  @SuppressWarnings("unchecked")
  @Test(expected = DMLException.class)
  public void testGetDatesError() throws DMLException, SQLException {
    when(st.executeQuery()).thenThrow(SQLException.class);
    assertEquals(1, dao.getDates(new University()).size(), 0);
  }

  @Test
  public void testGetInsertedEnrolees() throws DMLException {
    assertEquals(1,
        dao.getInsertedEnrolees(new Timestamp(System.currentTimeMillis())).size(), 0);
  }
  
  @SuppressWarnings("unchecked")
  @Test(expected = DMLException.class)
  public void testGetInsertedEnroleesError() throws DMLException, SQLException {
    when(st.executeQuery()).thenThrow(SQLException.class);
    assertEquals(1,
        dao.getInsertedEnrolees(new Timestamp(System.currentTimeMillis())).size(), 0);
  }

}
