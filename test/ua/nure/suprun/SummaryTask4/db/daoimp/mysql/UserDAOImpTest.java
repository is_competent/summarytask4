package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import ua.nure.suprun.SummaryTask4.bl.builder.UserBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Authorization;
import ua.nure.suprun.SummaryTask4.bl.entity.Enrollee;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.bl.entity.User;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

@RunWith(MockitoJUnitRunner.class)
public class UserDAOImpTest {
  
  @Mock
  private DataSource ds;
  
  @Mock
  private Connection con;
  
  @Mock
  private PreparedStatement st;
  
  @Mock
  private ResultSet rs;
  
  private UserDAOImp dao;
  
  @Spy
  private UserBuilder builder = new UserBuilder();
  
  @Before
  public void setUp() throws Exception {
    Date date = new Date(System.currentTimeMillis());
    when(rs.getDate(anyInt())).thenReturn(date);
    when(rs.getDate(anyString())).thenReturn(date);
    
    when(ds.getConnection()).thenReturn(con);
    when(con.prepareStatement(anyString(), anyInt())).thenReturn(st);
    when(st.executeQuery()).thenReturn(rs);
    when(st.executeUpdate()).thenReturn(1, 0);
    when(rs.next()).thenReturn(true, false);
    when(rs.getInt(1)).thenReturn(2);
    when(st.getGeneratedKeys()).thenReturn(rs);
    
    
    dao = new UserDAOImp(builder, ds);
  }

  @Test
  public void testLogin() throws DMLException {
      assertTrue(dao.login("test", "test") != null);
      assertFalse(dao.login("test", "test") != null);
  }

  @Test
  public void testGetString() throws DMLException {
    assertTrue(dao.get("test") != null);
    assertFalse(dao.get("test") != null);
  }

  @Test
  public void testGetInt() throws DMLException {
    assertTrue(dao.get(10) != null);
    assertFalse(dao.get(10) != null);
  }

  @Test
  public void testGetAuthorization() throws DMLException {
    assertFalse(dao.get(new Authorization()) == null);
    assertTrue(dao.get(new Authorization()) == null);
  }
  
  @SuppressWarnings("unchecked")
  @Test(expected = DMLException.class)
  public void testGetAuthorizationError() throws DMLException, SQLException {
    when(st.executeUpdate()).thenThrow(SQLException.class);
    assertTrue(dao.get(new Authorization()) == null);
  }

  @Test
  public void testBlock() throws DMLException {
    assertTrue(dao.block(10));
    assertFalse(dao.block(10));
  }

  @Test
  public void testUnblock() throws DMLException {
    assertTrue(dao.unblock(10));
    assertFalse(dao.unblock(10));
  }

  @Test(expected = DMLException.class)
  public void testRestore() throws DMLException {
    assertTrue(dao.restore(new Authorization()));
    assertFalse(dao.restore(new Authorization()));
  }
  
  @Test
  public void testRestoreWithoutEx() throws DMLException {
    Authorization a = new Authorization();
    a.setId("asaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaayyyyyyyyyyy");
    a.setUserId(10);
    assertTrue(dao.restore(a));
    assertFalse(dao.restore(a));
  }

  @Test
  public void testChangeLang() throws DMLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
    
    assertTrue(dao.changeLang(u));
    assertFalse(dao.changeLang(u));
  
  }

  @Test(expected = DMLException.class)
  public void testRegistryEnrolle() throws DMLException, SQLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
    
    Enrollee e = new Enrollee();
    e.setAvMark(10);
    e.setCity("cdcdcd");
    e.setDistrName("cdcdc");
    e.setEducName("cdcdcdcd");
    e.setMarksScan(new byte[2]);
    assertFalse(dao.registryEnrolle(u, e));
    verify(con).rollback();
  }
  
  @Test
  public void testRegistryEnrolleNoError() throws DMLException, SQLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
    
    Enrollee e = new Enrollee();
    e.setAvMark(10);
    e.setCity("cdcdcd");
    e.setDistrName("cdcdc");
    e.setEducName("cdcdcdcd");
    e.setMarksScan(new byte[2]);
    
    when(st.executeUpdate()).thenReturn(1, 1,0);
    assertTrue(dao.registryEnrolle(u, e));
    verify(con).commit();
    
    when(st.executeUpdate()).thenReturn(1, 0);
  }

  @Test
  public void testInviteAdmin() throws SQLException, DMLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
    
    Authorization a = new Authorization();
    a.setId("asaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaayyyyyyyyyyy");
    a.setUserId(10);
    
    when(st.executeUpdate()).thenReturn(1, 1,1);
    assertTrue(dao.inviteAdmin(u, a));
    verify(con).commit();    
  }
  
  @Test(expected = DMLException.class)
  public void testInviteAdminError() throws SQLException, DMLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
    
    Authorization a = new Authorization();
    a.setId("asaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaayyyyyyyyyyy");
    a.setUserId(10);
    
    when(st.executeUpdate()).thenReturn(1, 1, 0);
    assertTrue(dao.inviteAdmin(u, a));
    verify(con).rollback();    
  }

  @Test
  public void testSearch() throws DMLException {
    assertEquals(1, dao.search("cdcdc").size(), 0);
  }
  
  @SuppressWarnings("unchecked")
  @Test(expected = DMLException.class)
  public void testSearchError() throws DMLException, SQLException {
    when(st.executeQuery()).thenThrow(SQLException.class);
    dao.search("dsdsd");
  }
  
  @Test
  public void testGetAllEnrolees() throws DMLException {
    assertEquals(1, dao.getAllEnrolees().size(), 0);
  }

  @Test
  public void testRegistryAdmin() throws DMLException, SQLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
    
    University un = new University();
    un.setName("asaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaayyyyyyyyyyy");
    un.setId(10);
    
    when(st.executeUpdate()).thenReturn(1, 1, 1);
    assertTrue(dao.registryAdmin(u, un));
    verify(con).commit();   
  }
  
  @Test(expected = DMLException.class)
  public void testRegistryAdminError() throws DMLException, SQLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
    
    University un = new University();
    un.setName("asaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaayyyyyyyyyyy");
    un.setId(10);
    
    when(st.executeUpdate()).thenReturn(1, 0);
    assertTrue(dao.registryAdmin(u, un));
    verify(con).rollback();   
  }

  @Test
  public void testUpdateUser() throws DMLException, SQLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
        
    when(st.executeUpdate()).thenReturn(1, 0);
    assertTrue(dao.update(u));
  }
  
  @SuppressWarnings("unchecked")
  @Test(expected = DMLException.class)
  public void testUpdateUserError() throws DMLException, SQLException {
    User u = new User();
    u.setId(10);
    u.setLang("en");
    u.setEmail("testdsds@test.com");
    u.setLName("cdcdcd");
    u.setName("dscdcd");
    u.setPatr("dsdsds");
    u.setPass("aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaabbbbbbbbbbb");
    u.setRole(1);
        
    when(st.executeUpdate()).thenThrow(SQLException.class);
    assertTrue(dao.update(u));
  }

  @Test
  public void testIsBlocked() throws DMLException {
    assertTrue(dao.isBlocked(1));
    assertFalse(dao.isBlocked(1));
  }

  @Test
  public void testGetBlockedEnrollees() throws DMLException {
    assertEquals(1, dao.getBlockedEnrollees().size(),0);
  }

}
