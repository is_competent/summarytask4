package ua.nure.suprun.SummaryTask4.db.daoimp.mysql;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.mockito.Matchers.anyInt;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

import javax.sql.DataSource;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Spy;
import org.mockito.runners.MockitoJUnitRunner;

import ua.nure.suprun.SummaryTask4.bl.builder.DepartmentBuilder;
import ua.nure.suprun.SummaryTask4.bl.entity.Department;
import ua.nure.suprun.SummaryTask4.bl.entity.DepartmentSubject;
import ua.nure.suprun.SummaryTask4.bl.entity.University;
import ua.nure.suprun.SummaryTask4.exceptions.DBException;
import ua.nure.suprun.SummaryTask4.exceptions.DMLException;

@RunWith(MockitoJUnitRunner.class)
public class DepartmentDAOImpTest {

  @Mock
  private DataSource ds;

  @Mock
  private Connection con;

  @Mock
  private PreparedStatement st;

  @Mock
  private ResultSet rs;

  private DepartmentDAOImp dao;

  @Spy
  private DepartmentBuilder builder = new DepartmentBuilder();

  @Before
  public void setUp() throws Exception {
    Date date = new Date(System.currentTimeMillis());
    when(rs.getDate(anyInt())).thenReturn(date);
    when(rs.getDate(anyString())).thenReturn(date);
    
    when(ds.getConnection()).thenReturn(con);
    when(con.prepareStatement(anyString(), anyInt())).thenReturn(st);
    when(st.executeQuery()).thenReturn(rs);
    when(st.executeUpdate()).thenReturn(1, 0);
    when(rs.next()).thenReturn(true, false);
    when(rs.getInt(1)).thenReturn(2);
    when(st.getGeneratedKeys()).thenReturn(rs);

    dao = new DepartmentDAOImp(builder, ds);
  }

  @Test
  public void testSortByName() throws DMLException {
    assertEquals(1, dao.sortByName(new University(), true).size(), 0);
  }

  @Test
  public void testSortByNameDesc() throws DMLException {
    assertEquals(1, dao.sortByName(new University(), false).size(), 0);
  }

  @Test
  public void testSortByBudgetDesc() throws DMLException {
    assertEquals(1, dao.sortByBudget(new University(), false).size(), 0);
  }

  @Test
  public void testSortByBudget() throws DMLException {
    assertEquals(1, dao.sortByBudget(new University(), true).size(), 0);
  }

  @Test
  public void testSortByCommon() throws DMLException {
    assertEquals(1, dao.sortByCommon(new University(), true).size(), 0);
  }

  @Test
  public void testSortByCommonDesc() throws DMLException {
    assertEquals(1, dao.sortByCommon(new University(), false).size(), 0);
  }

  @Test
  public void testAddSubject() throws DMLException {
    DepartmentSubject ds = new DepartmentSubject();
    ds.setDepId(10);
    ds.setSubjId(10);
    assertTrue(dao.addSubject(ds));
    assertFalse(dao.addSubject(ds));
  }

  @Test
  public void testRemoveSubject() throws DMLException {
    DepartmentSubject ds = new DepartmentSubject();
    ds.setDepId(10);
    ds.setSubjId(10);
    assertTrue(dao.removeSubject(ds));
    assertFalse(dao.removeSubject(ds));
  }

  @Test
  public void testRemove() throws DMLException {
    assertTrue(dao.remove(new Department()));
    assertFalse(dao.remove(new Department()));
  }

  @Test
  public void testCreateNew() throws DMLException {
    Department d = new Department();
    d.setBudgetPlaces(10);
    d.setCommonPlaces(20);
    d.setName("dsddsds");
    d.setUniversity(10);
    assertTrue(dao.createNew(d));
    assertFalse(dao.createNew(d));
  }

  @Test
  public void testGet() throws DBException {
    assertNotNull(dao.get(10));
  }
  
  @Test(expected = DMLException.class)
  public void testGetError() throws DBException {
    assertNotNull(dao.get(10));
    dao.get(10);
  }

}
