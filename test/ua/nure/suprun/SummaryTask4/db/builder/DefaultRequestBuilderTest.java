package ua.nure.suprun.SummaryTask4.db.builder;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

import ua.nure.suprun.SummaryTask4.anotation.util.TestEntity;
import ua.nure.suprun.SummaryTask4.bl.entity.User;

public class DefaultRequestBuilderTest {
  public static RequestBuilder DEF = new DefaultRequestBuilder();

  @Test
  public void testCreateInsertRequest() {
    String sql = DEF.createInsertRequest(new TestEntity());
    String origin = "INSERT INTO `MOCK_USER` (`mockId`, `mockRole`) VALUES (:id, :role)";

    assertEquals(origin, sql);
  }

  @Test
  public void testCreateSelectByPKRequest() {
    String sql = DEF.createSelectByPKRequest(new TestEntity());
    String origin = "SELECT * FROM `MOCK_USER` WHERE `MOCK_USER`.`mockId` = :id";

    assertEquals(origin, sql);
  }

  @Test
  public void testCreateDeleteByPKRequest() {
    String sql = DEF.createDeleteByPKRequest(new TestEntity());
    String origin = "DELETE FROM `MOCK_USER` WHERE `mockId`=:id";

    assertEquals(origin, sql);
  }

  @Test
  public void testCreateUpdateRequest() {
    String sql = DEF.createUpdateRequest(new TestEntity());
    String origin = "UPDATE `MOCK_USER` SET `mockName` = :name, `mockRole` = :role WHERE `mockId` = :id";
    assertEquals(origin, sql);
  }

  @Test
  public void testCreateSelectAllRequest() {
    String sql = DEF.createSelectAllRequest(TestEntity.class);
    String origin = "SELECT * FROM `MOCK_USER`";
    assertEquals(origin, sql);
  }

  @Test
  public void testGetStoredRequest() {
    new User();
    String sql = DEF.getStoredRequest(TestEntity.class, "thisTable");
    String origin = "`MOCK_USER` `mockId``mockName`";

    assertEquals(origin, sql);

    sql = DEF.getStoredRequest(TestEntity.class, "thisAndAnother");
    origin = "`BLACKLIST``MOCK_USER``USER``AUTH``DEPT` `aId`";
    assertEquals(origin, sql);
  }

  @Test
  public void testGetStoredRequests() {
    int s = DEF.getStoredRequests(TestEntity.class).length;

    assertEquals(s, 2, 0);
  }

}
